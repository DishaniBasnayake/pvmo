ALTER TABLE `pvmo_db`.`tbl_ticket_user` 
ADD UNIQUE INDEX `no_duplicate` (`USER_ID` ASC, `TICKET_ID` ASC);

CREATE TABLE `tbl_document` (
  `ID` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `CONTENT` longblob,
  `REMARK` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `STATUS` tinyint(4) DEFAULT NULL,
  `SORT_ORDER` int(4) DEFAULT NULL,
  `CREATED_BY` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `LAST_UPDATE_BY` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LAST_UPDATE_ON` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `pvmo_db`.`tbl_ticket_document` 
DROP COLUMN `REMARK`,
DROP COLUMN `CONTENT`,
ADD COLUMN `DOCUMENT_ID` CHAR(32) NULL AFTER `TICKET_ID`;

ALTER TABLE `pvmo_db`.`tbl_document` 
ADD COLUMN `FILE_NAME` VARCHAR(500) NULL AFTER `REMARK`,
ADD COLUMN `CONTENT_TYPE` VARCHAR(255) NULL AFTER `FILE_NAME`;

SET collation_connection = 'utf8mb4_unicode_ci';
ALTER DATABASE pvmo_db CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

ALTER TABLE tbl_document CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE tbl_ticket CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE tbl_ticket_document CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

ALTER TABLE `pvmo_db`.`tbl_ticket_document` 
ADD UNIQUE INDEX `no_duplicates` (`TICKET_ID` ASC);

