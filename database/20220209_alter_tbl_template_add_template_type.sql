SET SQL_SAFE_UPDATES = 0;

UPDATE `pvmo_db`.`tbl_template` SET `TYPE` = 'EMERGENCY' WHERE (`ID` = '1');
UPDATE `pvmo_db`.`tbl_template` SET `TYPE` = 'MANUAL' WHERE (`ID` = '3');
UPDATE `pvmo_db`.`tbl_template` SET `TYPE` = 'MANUAL' WHERE (`ID` = '4');
UPDATE `pvmo_db`.`tbl_template` SET `TEMPLATE_ID` = 'T002', `TYPE` = 'NONE' WHERE (`ID` = '2');
UPDATE `pvmo_db`.`tbl_template` SET `TEMPLATE_ID` = 'T001' WHERE (`ID` = '1');

update tbl_ticket SET TICKET_TYPE = 'MANUAL' where TICKET_TYPE='WEB';
update tbl_ticket SET TEMPLATE_ID = '1' where TICKET_TYPE='EMERGENCY';
update tbl_ticket SET TEMPLATE_ID = '3' where TICKET_TYPE='MANUAL';
update tbl_ticket SET STATUS = 0 , TICKET_STATUS = 0 where TICKET_TYPE='AXIO自動入力';

INSERT INTO `pvmo_db`.`tbl_template` (`ID`, `TEMPLATE_ID`, `TITLE`, `TYPE`, `CONTENT`, `STATUS`, `SORT_ORDER`) VALUES ('5', 'T005', 'AXIO自動入力', 'AXIO自動入力', '{\n	\"tasks\": [{\n		\"title\": \"AXIO自動入力\",\n		\"visible\": true,\n		\"eventName\": \"\",\n		\"eventNumber\": \"\",\n		\"eventDetails01\": \"\",\n		\"eventDetails02\": \"\",\n		\"pcsNumber\": \"\",\n		\"equipment\": \"\",\n		\"note\": \"\",\n		\"picture\": \"\"\n	}]\n}', '0', '0');
update tbl_ticket SET TEMPLATE_ID = '5' where TICKET_TYPE= 'AXIO自動入力';

SET SQL_SAFE_UPDATES = 1;