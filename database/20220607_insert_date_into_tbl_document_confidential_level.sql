INSERT INTO `tbl_document_confidential_level` (`ID`, `NAME`, `VALUE`) VALUES ('CON124561', 'ENGINEER', '0');
INSERT INTO `tbl_document_confidential_level` (`ID`, `NAME`, `VALUE`) VALUES ('CON124562', 'GUEST', '0');
INSERT INTO `tbl_document_confidential_level` (`ID`, `NAME`, `VALUE`) VALUES ('CON124563', 'SERVICE_CENTER_MANAGER', '1');
INSERT INTO `tbl_document_confidential_level` (`ID`, `NAME`, `VALUE`) VALUES ('CON124564', 'AREA_MANAGER', '2');
INSERT INTO `tbl_document_confidential_level` (`ID`, `NAME`, `VALUE`) VALUES ('CON124565', 'MONITERING_CENTER', '3');
INSERT INTO `tbl_document_confidential_level` (`ID`, `NAME`, `VALUE`) VALUES ('CON124566', 'SYSTEM_MANAGER', '4');
INSERT INTO `tbl_document_confidential_level` (`ID`, `NAME`, `VALUE`) VALUES ('CON124567', 'HEAD_OFFICE_STAFF', '4');
