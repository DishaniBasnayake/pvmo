-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: pvmo_16102021
-- ------------------------------------------------------
-- Server version	5.5.62-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `facility`
--

DROP TABLE IF EXISTS `facility`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `facility` (
  `ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NAME` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LATITUDE` double DEFAULT NULL,
  `LONGITUDE` double DEFAULT NULL,
  `ALTITUDE` double DEFAULT NULL,
  `MINIMUM_POWER_GENERATION` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MAXIMUM_POWER_GENERATION` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NORMAL_LEVEL` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ALERT_LEVEL` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CRITICAL_LEVEL` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `REMARK` text COLLATE utf8mb4_unicode_ci,
  `STATUS` tinyint(4) DEFAULT NULL,
  `SORT_ORDER` int(4) DEFAULT NULL,
  `CREATED_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `LAST_UPDATE_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LAST_UPDATE_ON` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facility`
--

LOCK TABLES `facility` WRITE;
/*!40000 ALTER TABLE `facility` DISABLE KEYS */;
/*!40000 ALTER TABLE `facility` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facility_group`
--

DROP TABLE IF EXISTS `facility_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `facility_group` (
  `ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `FACILITY_GROUP_NAME` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `REMARK` text COLLATE utf8mb4_unicode_ci,
  `STATUS` tinyint(4) DEFAULT NULL,
  `SORT_ORDER` int(4) DEFAULT NULL,
  `CREATED_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `LAST_UPDATE_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LAST_UPDATE_ON` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facility_group`
--

LOCK TABLES `facility_group` WRITE;
/*!40000 ALTER TABLE `facility_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `facility_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facility_has_facility_group`
--

DROP TABLE IF EXISTS `facility_has_facility_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `facility_has_facility_group` (
  `ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `FACILITY_GROUP_ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `FACILITY_ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `REMARK` text COLLATE utf8mb4_unicode_ci,
  `STATUS` tinyint(4) DEFAULT NULL,
  `SORT_ORDER` int(4) DEFAULT NULL,
  `CREATED_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `LAST_UPDATE_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LAST_UPDATE_ON` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_facility_has_facility_group_facility_group1_idx` (`FACILITY_GROUP_ID`),
  KEY `fk_facility_has_facility_group_facility1_idx` (`FACILITY_ID`),
  CONSTRAINT `fk_facility_has_facility_group_facility1` FOREIGN KEY (`FACILITY_ID`) REFERENCES `facility` (`ID`),
  CONSTRAINT `fk_facility_has_facility_group_facility_group1` FOREIGN KEY (`FACILITY_GROUP_ID`) REFERENCES `facility_group` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facility_has_facility_group`
--

LOCK TABLES `facility_has_facility_group` WRITE;
/*!40000 ALTER TABLE `facility_has_facility_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `facility_has_facility_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flyway_schema_history`
--

DROP TABLE IF EXISTS `flyway_schema_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flyway_schema_history` (
  `installed_rank` int(11) NOT NULL,
  `version` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `script` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `checksum` int(11) DEFAULT NULL,
  `installed_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `installed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `execution_time` int(11) NOT NULL,
  `success` tinyint(1) NOT NULL,
  PRIMARY KEY (`installed_rank`),
  KEY `flyway_schema_history_s_idx` (`success`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flyway_schema_history`
--

LOCK TABLES `flyway_schema_history` WRITE;
/*!40000 ALTER TABLE `flyway_schema_history` DISABLE KEYS */;
INSERT INTO `flyway_schema_history` VALUES (1,'1.0','Initial version','SQL','V1.0__Initial_version.sql',-78721759,'root','2021-10-05 11:00:08',40,1),(2,'1.1','Alter sample','SQL','V1.1__Alter_sample.sql',1031024450,'root','2021-10-05 11:00:09',25,1),(3,'1.2','Create Table','SQL','V1.2__Create_Table.sql',1536181769,'root','2021-10-05 11:00:09',274,1),(4,'1.3','Add Tables','SQL','V1.3__Add_Tables.sql',1141566469,'root','2021-10-05 11:00:09',74,1),(5,'1.4','Update table schedule','SQL','V1.4__Update_table_schedule.sql',1450372917,'root','2021-10-05 11:00:09',89,1),(6,'1.5','Update tables','SQL','V1.5__Update_tables.sql',1707354428,'root','2021-10-05 11:00:09',122,1),(7,'1.6','Update User Table','SQL','V1.6__Update_User_Table.sql',506984262,'root','2021-10-05 11:00:09',51,1),(8,'1.7','Upadate User Table','SQL','V1.7__Upadate_User_Table.sql',-1189257578,'root','2021-10-05 11:00:09',61,1),(9,'1.8','Create Site & Service center tables','SQL','V1.8__Create_Site_&_Service_center_tables.sql',1911639489,'root','2021-10-05 11:00:09',45,1),(10,'2.1','Create Site & Service center tables2','SQL','V2.1__Create_Site_&_Service_center_tables2.sql',1911639489,'root','2021-10-14 20:49:18',6,0);
/*!40000 ALTER TABLE `flyway_schema_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inspection_instruction`
--

DROP TABLE IF EXISTS `inspection_instruction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `inspection_instruction` (
  `ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `TITLE` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `INSPECTION_INSTRUCTION` text COLLATE utf8mb4_unicode_ci,
  `STRING_MAP` longblob,
  `ABNORMALITY_CONTENT` text COLLATE utf8mb4_unicode_ci,
  `CAUSE_OF_ABNORMALITY` text COLLATE utf8mb4_unicode_ci,
  `NUMBER_OF_NOTIFICATION_REPORTED` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `TELEPHONE_NUMBER` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `REMARK` text COLLATE utf8mb4_unicode_ci,
  `STATUS` tinyint(4) DEFAULT NULL,
  `SORT_ORDER` int(4) DEFAULT NULL,
  `CREATED_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `LAST_UPDATE_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LAST_UPDATE_ON` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inspection_instruction`
--

LOCK TABLES `inspection_instruction` WRITE;
/*!40000 ALTER TABLE `inspection_instruction` DISABLE KEYS */;
/*!40000 ALTER TABLE `inspection_instruction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inspector`
--

DROP TABLE IF EXISTS `inspector`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `inspector` (
  `ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `FIRST_NAME` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LAST_NAME` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `EMERGENCY_CONTACT_NUMBER` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `EMAIL` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `REMARK` text COLLATE utf8mb4_unicode_ci,
  `STATUS` tinyint(4) DEFAULT NULL,
  `SORT_ORDER` int(4) DEFAULT NULL,
  `CREATED_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `LAST_UPDATE_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LAST_UPDATE_ON` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inspector`
--

LOCK TABLES `inspector` WRITE;
/*!40000 ALTER TABLE `inspector` DISABLE KEYS */;
/*!40000 ALTER TABLE `inspector` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log`
--

DROP TABLE IF EXISTS `log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `log` (
  `ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ACTION_NAME` char(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ACTION_STATE` char(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ACCESS_HOST` char(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `REMARK` text COLLATE utf8mb4_unicode_ci,
  `STATUS` tinyint(4) DEFAULT NULL,
  `SORT_ORDER` int(4) DEFAULT NULL,
  `CREATED_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `LAST_UPDATE_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LAST_UPDATE_ON` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log`
--

LOCK TABLES `log` WRITE;
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
/*!40000 ALTER TABLE `log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `monitering_center`
--

DROP TABLE IF EXISTS `monitering_center`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `monitering_center` (
  `ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `FIRST_NAME` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LAST_NAME` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `EMERGENCY_CONTACT_NUMBER` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `EMAIL` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LOCATION` text COLLATE utf8mb4_unicode_ci,
  `ADDRESS` text COLLATE utf8mb4_unicode_ci,
  `REMARK` text COLLATE utf8mb4_unicode_ci,
  `STATUS` tinyint(4) DEFAULT NULL,
  `SORT_ORDER` int(4) DEFAULT NULL,
  `CREATED_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `LAST_UPDATE_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LAST_UPDATE_ON` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `monitering_center`
--

LOCK TABLES `monitering_center` WRITE;
/*!40000 ALTER TABLE `monitering_center` DISABLE KEYS */;
/*!40000 ALTER TABLE `monitering_center` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sample`
--

DROP TABLE IF EXISTS `sample`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sample` (
  `id` char(32) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `inserted_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sample`
--

LOCK TABLES `sample` WRITE;
/*!40000 ALTER TABLE `sample` DISABLE KEYS */;
/*!40000 ALTER TABLE `sample` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedule`
--

DROP TABLE IF EXISTS `schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `schedule` (
  `ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `INSPECTOR_ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `TITLE` text COLLATE utf8mb4_unicode_ci,
  `CONTENT` text COLLATE utf8mb4_unicode_ci,
  `LOCATION` text COLLATE utf8mb4_unicode_ci,
  `SCHEDULE_DATE` datetime DEFAULT NULL,
  `START_TIME` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `END_TIME` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `REMARK` text COLLATE utf8mb4_unicode_ci,
  `STATUS` tinyint(4) DEFAULT NULL,
  `SORT_ORDER` int(4) DEFAULT NULL,
  `CREATED_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `LAST_UPDATE_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LAST_UPDATE_ON` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_schedule_inspector_idx` (`INSPECTOR_ID`),
  CONSTRAINT `fk_schedule_inspector` FOREIGN KEY (`INSPECTOR_ID`) REFERENCES `inspector` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedule`
--

LOCK TABLES `schedule` WRITE;
/*!40000 ALTER TABLE `schedule` DISABLE KEYS */;
/*!40000 ALTER TABLE `schedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_center`
--

DROP TABLE IF EXISTS `service_center`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `service_center` (
  `ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `SERVICE_CENTER_NAME` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CONTACT_NUMBER` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ADDRESS` text COLLATE utf8mb4_unicode_ci,
  `EMAIL` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `REGION` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PREFECTURE` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `REMARK` text COLLATE utf8mb4_unicode_ci,
  `STATUS` tinyint(4) DEFAULT NULL,
  `SORT_ORDER` int(4) DEFAULT NULL,
  `CREATED_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `LAST_UPDATE_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LAST_UPDATE_ON` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_center`
--

LOCK TABLES `service_center` WRITE;
/*!40000 ALTER TABLE `service_center` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_center` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_center_manager`
--

DROP TABLE IF EXISTS `service_center_manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `service_center_manager` (
  `ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `FIRST_NAME` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LAST_NAME` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `EMERGENCY_CONTACT_NUMBER` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `EMAIL` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ADDRESS` text COLLATE utf8mb4_unicode_ci,
  `REMARK` text COLLATE utf8mb4_unicode_ci,
  `STATUS` tinyint(4) DEFAULT NULL,
  `SORT_ORDER` int(4) DEFAULT NULL,
  `CREATED_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `LAST_UPDATE_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LAST_UPDATE_ON` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_center_manager`
--

LOCK TABLES `service_center_manager` WRITE;
/*!40000 ALTER TABLE `service_center_manager` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_center_manager` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site`
--

DROP TABLE IF EXISTS `site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site` (
  `ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `URL` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `USERNAME` char(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PASSWORD` char(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `REGION` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PREFECTURE` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `REMARK` text COLLATE utf8mb4_unicode_ci,
  `STATUS` tinyint(4) DEFAULT NULL,
  `SORT_ORDER` int(4) DEFAULT NULL,
  `CREATED_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `LAST_UPDATE_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site`
--

LOCK TABLES `site` WRITE;
/*!40000 ALTER TABLE `site` DISABLE KEYS */;
/*!40000 ALTER TABLE `site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_company`
--

DROP TABLE IF EXISTS `tbl_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_company` (
  `ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CODE` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NAME` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `KATAKANA_NAME` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CONTACT_NUMBER` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `EMAIL` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ADDRESS` text COLLATE utf8mb4_unicode_ci,
  `REMARK` text COLLATE utf8mb4_unicode_ci,
  `STATUS` tinyint(4) DEFAULT NULL,
  `SORT_ORDER` int(4) DEFAULT NULL,
  `CREATED_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `LAST_UPDATE_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LAST_UPDATE_ON` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `CODE_UNIQUE` (`CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_company`
--

LOCK TABLES `tbl_company` WRITE;
/*!40000 ALTER TABLE `tbl_company` DISABLE KEYS */;
INSERT INTO `tbl_company` VALUES ('1521885341171712','TEST001','TEST,Company 1',NULL,'000-000-000','s@g','Tokyo, Company 1,Hiroshi OHTAKA',NULL,1,NULL,NULL,'2021-10-10 18:27:18',NULL,'2021-10-10 18:27:18'),('1521885382393856','COM002','Company 002',NULL,'124-000-000','com002@pvmo.com','Company 002,Hiroshi OHTAKA',NULL,0,NULL,NULL,'2021-10-10 18:27:28',NULL,'2021-10-12 06:23:18'),('1521956253990912','COM001','Company 001',NULL,'123-000-000','com001@pvmo.com','Company 001,Hiroshi OHTAKA',NULL,0,NULL,NULL,'2021-10-10 23:15:51',NULL,'2021-10-12 06:22:31'),('1526107203133440','COM003','Company 3',NULL,'003-446-998','com3@gmail.com','sample',NULL,1,NULL,NULL,'2021-10-22 16:46:06',NULL,'2021-10-22 16:46:06');
/*!40000 ALTER TABLE `tbl_company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_consumer`
--

DROP TABLE IF EXISTS `tbl_consumer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_consumer` (
  `ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `REGION_ID` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PREFECTURE_ID` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CONTACT_NUMBER` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `EMAIL` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ADDRESS` text COLLATE utf8mb4_unicode_ci,
  `REMARK` text COLLATE utf8mb4_unicode_ci,
  `STATUS` tinyint(4) DEFAULT NULL,
  `SORT_ORDER` int(4) DEFAULT NULL,
  `CREATED_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `LAST_UPDATE_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LAST_UPDATE_ON` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_consumer`
--

LOCK TABLES `tbl_consumer` WRITE;
/*!40000 ALTER TABLE `tbl_consumer` DISABLE KEYS */;
INSERT INTO `tbl_consumer` VALUES ('1525103760605184',NULL,NULL,'Consumer 1','111-111-111','con1@gmail.com','sampleaddress',NULL,0,NULL,NULL,'2021-10-19 20:43:05',NULL,'2021-10-19 20:43:05'),('1525103884705792',NULL,NULL,'Consumer 2','022-123-556','con2@gmail.com','address 2',NULL,0,NULL,NULL,'2021-10-19 20:43:35',NULL,'2021-10-19 20:43:35');
/*!40000 ALTER TABLE `tbl_consumer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_prefecture`
--

DROP TABLE IF EXISTS `tbl_prefecture`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_prefecture` (
  `ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `REGION_ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `PREFECTURE` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `REMARK` text COLLATE utf8mb4_unicode_ci,
  `STATUS` tinyint(4) DEFAULT NULL,
  `SORT_ORDER` int(4) DEFAULT NULL,
  `CREATED_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `LAST_UPDATE_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LAST_UPDATE_ON` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_tbl_prefecture_tbl_region_idx` (`REGION_ID`),
  CONSTRAINT `fk_tbl_prefecture_tbl_region` FOREIGN KEY (`REGION_ID`) REFERENCES `tbl_region` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_prefecture`
--

LOCK TABLES `tbl_prefecture` WRITE;
/*!40000 ALTER TABLE `tbl_prefecture` DISABLE KEYS */;
INSERT INTO `tbl_prefecture` VALUES ('1530328739610624','1524670275751936','Hokkaido',NULL,0,NULL,NULL,'2021-11-03 15:03:34',NULL,'2021-11-03 15:03:34'),('1569856324758','1524670306013184','Aomori',NULL,0,NULL,NULL,'2021-10-18 15:19:14',NULL,'2021-10-18 15:19:14'),('1666324589562','1524670306013184','Iwate',NULL,0,NULL,NULL,'2021-10-18 15:19:14',NULL,'2021-10-18 15:19:14'),('1689666632545','1524670306013184','Miyagi',NULL,0,NULL,NULL,'2021-10-18 15:19:14',NULL,'2021-10-18 15:19:14'),('1699585554569','1524670306013184','Akita',NULL,0,NULL,NULL,'2021-10-18 15:19:14',NULL,'2021-10-18 15:19:14'),('1714455858596','1524670306013184','Fukushima',NULL,0,NULL,NULL,'2021-10-18 15:19:14',NULL,'2021-10-18 15:19:14'),('1714526398565','1524670306013184','Yamagata',NULL,0,NULL,NULL,'2021-10-18 15:19:14',NULL,'2021-10-18 15:19:14');
/*!40000 ALTER TABLE `tbl_prefecture` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_region`
--

DROP TABLE IF EXISTS `tbl_region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_region` (
  `ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `REGION` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `REMARK` text COLLATE utf8mb4_unicode_ci,
  `STATUS` tinyint(4) DEFAULT NULL,
  `SORT_ORDER` int(4) DEFAULT NULL,
  `CREATED_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `LAST_UPDATE_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LAST_UPDATE_ON` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_region`
--

LOCK TABLES `tbl_region` WRITE;
/*!40000 ALTER TABLE `tbl_region` DISABLE KEYS */;
INSERT INTO `tbl_region` VALUES ('1524670275751936','Hokkaido',NULL,0,NULL,NULL,'2021-10-18 15:19:14',NULL,'2021-10-18 15:19:14'),('1524670306013184','Tohoku',NULL,1,NULL,NULL,'2021-10-18 15:19:21',NULL,'2021-11-08 19:21:24'),('1524670306013352','Kanto',NULL,0,NULL,NULL,'2021-10-18 15:19:21',NULL,'2021-10-18 15:19:21'),('1524670442563985','Chubu',NULL,0,NULL,NULL,'2021-10-18 15:19:21',NULL,'2021-10-18 15:19:21'),('1525569874125552','Chugoku',NULL,0,NULL,NULL,'2021-10-18 15:19:21',NULL,'2021-10-18 15:19:21'),('1585995623345886','Kyushu',NULL,0,NULL,NULL,'2021-10-18 15:19:21',NULL,'2021-10-18 15:19:21'),('1589562314582364','Kansai',NULL,0,NULL,NULL,'2021-10-18 15:19:21',NULL,'2021-10-18 15:19:21'),('1599668745235778','Shikoku',NULL,0,NULL,NULL,'2021-10-18 15:19:21',NULL,'2021-10-18 15:19:21');
/*!40000 ALTER TABLE `tbl_region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_service_center`
--

DROP TABLE IF EXISTS `tbl_service_center`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_service_center` (
  `ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CODE` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NAME` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CONTACT_NUMBER` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ADDRESS` text CHARACTER SET utf8,
  `EMAIL` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `REGION` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PREFECTURE` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `REMARK` text CHARACTER SET utf8,
  `STATUS` tinyint(4) DEFAULT NULL,
  `SORT_ORDER` int(4) DEFAULT NULL,
  `CREATED_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `LAST_UPDATE_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LAST_UPDATE_ON` datetime DEFAULT NULL,
  `COMPANY_ID` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `CODE_UNIQUE` (`CODE`,`COMPANY_ID`),
  KEY `FK9k030av5mqu0dpuvb7v1u1ymb` (`COMPANY_ID`),
  CONSTRAINT `FK9k030av5mqu0dpuvb7v1u1ymb` FOREIGN KEY (`COMPANY_ID`) REFERENCES `tbl_company` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_service_center`
--

LOCK TABLES `tbl_service_center` WRITE;
/*!40000 ALTER TABLE `tbl_service_center` DISABLE KEYS */;
INSERT INTO `tbl_service_center` VALUES ('1521924743299072','SC002','SC002-C2','123-123-125','111,Tokyo','sc007@g.com','Hokkaido','Tokyo',NULL,0,NULL,NULL,'2021-10-10 21:07:38',NULL,'2021-10-14 03:19:25','1521885382393856'),('1521924743299073','SC001','SC001-C2','123-123-125','111,Tokyo','sc007@g.com','Hokkaido','Tokyo',NULL,0,NULL,NULL,'2021-10-10 21:07:38',NULL,'2021-10-15 01:56:06','1521885382393856'),('1521956641148928','SC001','SC001-C1','000-123-456','Tokyo','q@g','Hokkaido','Tokyo',NULL,0,NULL,NULL,'2021-10-10 23:17:25',NULL,'2021-10-14 04:34:00','1521956253990912'),('1522310477533184','SC002','SC002-C1','000-123-456','qqqq','q@g','Hokkaido','Tokyo',NULL,0,NULL,NULL,'2021-10-11 23:17:11',NULL,'2021-10-14 03:19:14','1521956253990912'),('1531022895112192','sc001','service center 1','022-156-789','sample address','sc1@gmail.com','Hokkaido','Tokyo',NULL,0,NULL,NULL,'2021-11-05 14:08:06',NULL,'2021-11-05 14:08:06','1526107203133440');
/*!40000 ALTER TABLE `tbl_service_center` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_service_center_site`
--

DROP TABLE IF EXISTS `tbl_service_center_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_service_center_site` (
  `ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `REMARK` text COLLATE utf8mb4_unicode_ci,
  `STATUS` tinyint(4) DEFAULT NULL,
  `SORT_ORDER` int(4) DEFAULT NULL,
  `CREATED_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `LAST_UPDATE_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LAST_UPDATE_ON` datetime DEFAULT NULL,
  `SERVICE_CENTER_ID` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SITE_ID` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `unique_service_center_site` (`SERVICE_CENTER_ID`,`SITE_ID`),
  KEY `fk_site` (`SITE_ID`),
  CONSTRAINT `fk_service_center` FOREIGN KEY (`SERVICE_CENTER_ID`) REFERENCES `tbl_service_center` (`ID`),
  CONSTRAINT `fk_site` FOREIGN KEY (`SITE_ID`) REFERENCES `tbl_site` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_service_center_site`
--

LOCK TABLES `tbl_service_center_site` WRITE;
/*!40000 ALTER TABLE `tbl_service_center_site` DISABLE KEYS */;
INSERT INTO `tbl_service_center_site` VALUES ('1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1521924743299073','1');
/*!40000 ALTER TABLE `tbl_service_center_site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_service_center_user`
--

DROP TABLE IF EXISTS `tbl_service_center_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_service_center_user` (
  `ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `REMARK` text COLLATE utf8mb4_unicode_ci,
  `STATUS` tinyint(4) DEFAULT NULL,
  `SORT_ORDER` int(4) DEFAULT NULL,
  `CREATED_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `LAST_UPDATE_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LAST_UPDATE_ON` datetime DEFAULT NULL,
  `SERVICE_CENTER_ID` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `USER_ID` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `unique_service_center_site` (`SERVICE_CENTER_ID`,`USER_ID`),
  KEY `fk_site` (`USER_ID`),
  CONSTRAINT `fk_service_center_user` FOREIGN KEY (`SERVICE_CENTER_ID`) REFERENCES `tbl_service_center` (`ID`),
  CONSTRAINT `fk_user_service_center` FOREIGN KEY (`USER_ID`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_service_center_user`
--

LOCK TABLES `tbl_service_center_user` WRITE;
/*!40000 ALTER TABLE `tbl_service_center_user` DISABLE KEYS */;
INSERT INTO `tbl_service_center_user` VALUES ('1',NULL,0,NULL,NULL,NULL,NULL,NULL,'1521924743299073','1523081230528512');
/*!40000 ALTER TABLE `tbl_service_center_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_site`
--

DROP TABLE IF EXISTS `tbl_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_site` (
  `ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `COMPANY_ID` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CODE` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `URL` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CAMERA_URL` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `USERNAME` char(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PASSWORD` char(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `KANA_NAME` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SPN` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `POST_CODE` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CONTACT_NUMBER` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `EMAIL` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `REGION` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PREFECTURE` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ADDRESS` text COLLATE utf8mb4_unicode_ci,
  `REMARK` text COLLATE utf8mb4_unicode_ci,
  `STATUS` tinyint(4) DEFAULT NULL,
  `SORT_ORDER` int(4) DEFAULT NULL,
  `CREATED_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `LAST_UPDATE_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LAST_UPDATE_ON` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `CODE_UNIQUE` (`CODE`,`COMPANY_ID`),
  KEY `fk_site_company` (`COMPANY_ID`),
  CONSTRAINT `fk_site_company` FOREIGN KEY (`COMPANY_ID`) REFERENCES `tbl_company` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_site`
--

LOCK TABLES `tbl_site` WRITE;
/*!40000 ALTER TABLE `tbl_site` DISABLE KEYS */;
INSERT INTO `tbl_site` VALUES ('1','1521885382393856','SITE001','www.site1.com',NULL,'1',NULL,'SITE001','',NULL,NULL,'123-123-123','site001@gmail.com','Hokkaido','Tokyo','Tokyo',NULL,0,NULL,NULL,NULL,NULL,'2021-11-16 19:13:32'),('1531037833617408','1526107203133440','SITE01','www.site2.com',NULL,NULL,NULL,'SITE 1','','','','111-222-333','s12@gmail.com','Hokkaido','Tokyo','sample address',NULL,0,NULL,NULL,'2021-11-05 15:08:53',NULL,'2021-11-16 21:23:49'),('1534949503234048','1521885382393856','SITE0033',NULL,NULL,NULL,NULL,'site 3','','003','','033-455-999','site3@gmail.com','Hokkaido','Tokyo','sample address',NULL,0,NULL,NULL,'2021-11-16 16:25:31',NULL,'2021-11-16 19:13:36');
/*!40000 ALTER TABLE `tbl_site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_site_client`
--

DROP TABLE IF EXISTS `tbl_site_client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_site_client` (
  `ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `SITE_ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `COMPANY_NAME` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `REPRESENTATIVE` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CLIENT_TYPE` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `REMARK` text COLLATE utf8mb4_unicode_ci,
  `STATUS` tinyint(4) DEFAULT NULL,
  `SORT_ORDER` int(4) DEFAULT NULL,
  `CREATED_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `LAST_UPDATE_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LAST_UPDATE_ON` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_tbl_site_client_tbl_site1_idx` (`SITE_ID`),
  CONSTRAINT `fk_tbl_site_client_tbl_site1` FOREIGN KEY (`SITE_ID`) REFERENCES `tbl_site` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_site_client`
--

LOCK TABLES `tbl_site_client` WRITE;
/*!40000 ALTER TABLE `tbl_site_client` DISABLE KEYS */;
INSERT INTO `tbl_site_client` VALUES ('1532870948175872','1531037833617408','com2','','type 2',NULL,0,NULL,NULL,'2021-11-10 19:27:51',NULL,'2021-11-16 21:26:44'),('1533136330563584','1531037833617408','client 1','','type 1',NULL,0,NULL,NULL,'2021-11-11 13:27:41',NULL,'2021-11-11 13:27:41'),('1533136421429248','1531037833617408','company 1','','type 1',NULL,0,NULL,NULL,'2021-11-11 13:28:04',NULL,'2021-11-11 13:28:04'),('1533142224351232','1531037833617408','client 1',NULL,'type 2',NULL,0,NULL,NULL,'2021-11-11 13:51:40',NULL,'2021-11-11 13:51:40');
/*!40000 ALTER TABLE `tbl_site_client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_site_construction_company`
--

DROP TABLE IF EXISTS `tbl_site_construction_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_site_construction_company` (
  `ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `SITE_ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ELECTRICITY` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CIVIL_ENGINEERING` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `FRAME` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `REMARK` text COLLATE utf8mb4_unicode_ci,
  `STATUS` tinyint(4) DEFAULT NULL,
  `SORT_ORDER` int(4) DEFAULT NULL,
  `CREATED_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `LAST_UPDATE_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LAST_UPDATE_ON` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_tbl_site_construction_company_tbl_site1_idx` (`SITE_ID`),
  CONSTRAINT `fk_tbl_site_construction_company_tbl_site1` FOREIGN KEY (`SITE_ID`) REFERENCES `tbl_site` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_site_construction_company`
--

LOCK TABLES `tbl_site_construction_company` WRITE;
/*!40000 ALTER TABLE `tbl_site_construction_company` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_site_construction_company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_site_contract_information`
--

DROP TABLE IF EXISTS `tbl_site_contract_information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_site_contract_information` (
  `ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `SITE_ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ORDER_RECEIVED_DATE` datetime DEFAULT NULL,
  `SI_DATE_PLANED` datetime DEFAULT NULL,
  `SI_DATE_CONFIRMED` datetime DEFAULT NULL,
  `POWER_GENERATION_PLANT_TYPE` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MAIN_ENGINEER` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ACTING_PERSON` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ANNUAL_INSPECTION` text COLLATE utf8mb4_unicode_ci,
  `MONTHLY_INSPECTION` text COLLATE utf8mb4_unicode_ci,
  `FINE_INSPECTION` text COLLATE utf8mb4_unicode_ci,
  `REMORT_MONITORING` text COLLATE utf8mb4_unicode_ci,
  `POWER_GENERATION_REPORT` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `REMARK` text COLLATE utf8mb4_unicode_ci,
  `STATUS` tinyint(4) DEFAULT NULL,
  `SORT_ORDER` int(4) DEFAULT NULL,
  `CREATED_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `LAST_UPDATE_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LAST_UPDATE_ON` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_tbl_site_contract_information_tbl_site1_idx` (`SITE_ID`),
  CONSTRAINT `fk_tbl_site_contract_information_tbl_site1` FOREIGN KEY (`SITE_ID`) REFERENCES `tbl_site` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_site_contract_information`
--

LOCK TABLES `tbl_site_contract_information` WRITE;
/*!40000 ALTER TABLE `tbl_site_contract_information` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_site_contract_information` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_site_emergency_contact`
--

DROP TABLE IF EXISTS `tbl_site_emergency_contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_site_emergency_contact` (
  `ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `SITE_ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `SERVICE_CENTER_COMPANY` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SERVICE_CENTER_NAME` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SERVICE_CENTER_TELEPHONE_NUMBER` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SERVICE_CENTER_EMAIL` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SERVICE_CENTER_NOTE` text COLLATE utf8mb4_unicode_ci,
  `MAIN_ENGINEER_COMPANY` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MAIN_ENGINEER_NAME` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MAIN_ENGINEER_TELEPHONE` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MAIN_ENGINEER_EMAIL` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MAIN_ENGINEER_NOTE` text COLLATE utf8mb4_unicode_ci,
  `OM_STAFF_COMPANY` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `OM_STAFF_NAME` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `OM_STAFF_TELEPHONE` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `OM_STAFF_EMAIL` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `OM_STAFF_NOTE` text COLLATE utf8mb4_unicode_ci,
  `OPERATOR_COMPANY` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `OPERATOR_NAME` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `OPERATOR_TELEPHONE` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `OPERATOR_EMAIL` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `OPERATOR_NOTE` text COLLATE utf8mb4_unicode_ci,
  `REMARK` text COLLATE utf8mb4_unicode_ci,
  `STATUS` tinyint(4) DEFAULT NULL,
  `SORT_ORDER` int(4) DEFAULT NULL,
  `CREATED_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `LAST_UPDATE_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LAST_UPDATE_ON` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_tbl_site_emergency_contact_tbl_site_idx` (`SITE_ID`),
  CONSTRAINT `fk_tbl_site_emergency_contact_tbl_site` FOREIGN KEY (`SITE_ID`) REFERENCES `tbl_site` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_site_emergency_contact`
--

LOCK TABLES `tbl_site_emergency_contact` WRITE;
/*!40000 ALTER TABLE `tbl_site_emergency_contact` DISABLE KEYS */;
INSERT INTO `tbl_site_emergency_contact` VALUES ('1535020342231040','1531037833617408','test','','','','','','','','','','','','','','','','','','','',NULL,0,NULL,NULL,'2021-11-16 21:13:45',NULL,'2021-11-16 21:13:45');
/*!40000 ALTER TABLE `tbl_site_emergency_contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_site_external_contact`
--

DROP TABLE IF EXISTS `tbl_site_external_contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_site_external_contact` (
  `ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `SITE_ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `COMPANY` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DIVISION` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `TELEPHONE_NUMBER_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `TELEPHONE_NUMBER_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PRIORITY` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `EMAIL` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NOTE` text COLLATE utf8mb4_unicode_ci,
  `AUTOMATIC_EMAIL` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `REMARK` text COLLATE utf8mb4_unicode_ci,
  `STATUS` tinyint(4) DEFAULT NULL,
  `SORT_ORDER` int(4) DEFAULT NULL,
  `CREATED_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `LAST_UPDATE_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LAST_UPDATE_ON` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_tbl_site_external_contact_tbl_site_idx` (`SITE_ID`),
  CONSTRAINT `fk_tbl_site_external_contact_tbl_site` FOREIGN KEY (`SITE_ID`) REFERENCES `tbl_site` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_site_external_contact`
--

LOCK TABLES `tbl_site_external_contact` WRITE;
/*!40000 ALTER TABLE `tbl_site_external_contact` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_site_external_contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_site_internal_contact`
--

DROP TABLE IF EXISTS `tbl_site_internal_contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_site_internal_contact` (
  `ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `SITE_ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `COMPANY` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DIVISION` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `TELEPHONE_NUMBER` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `EMAIL` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NOTE` text COLLATE utf8mb4_unicode_ci,
  `AUTOMATIC_EMAIL` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `REMARK` text COLLATE utf8mb4_unicode_ci,
  `STATUS` tinyint(4) DEFAULT NULL,
  `SORT_ORDER` int(4) DEFAULT NULL,
  `CREATED_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `LAST_UPDATE_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LAST_UPDATE_ON` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_tbl_site_internal_contact_tbl_site_idx` (`SITE_ID`),
  CONSTRAINT `fk_tbl_site_internal_contact_tbl_site` FOREIGN KEY (`SITE_ID`) REFERENCES `tbl_site` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_site_internal_contact`
--

LOCK TABLES `tbl_site_internal_contact` WRITE;
/*!40000 ALTER TABLE `tbl_site_internal_contact` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_site_internal_contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_site_monitoring_structure`
--

DROP TABLE IF EXISTS `tbl_site_monitoring_structure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_site_monitoring_structure` (
  `ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `SITE_ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `FACILITY_MAINTENANCE` text COLLATE utf8mb4_unicode_ci,
  `MONITORING_TYPE` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `OM_OBJECTIVE` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MONITORING_CAMERA` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NUMBER_OF_CAMERA` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `STRING_MONITORING` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NUMBER_OF_STRING` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `GRAPH_MONITORING` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `REMARK` text COLLATE utf8mb4_unicode_ci,
  `STATUS` tinyint(4) DEFAULT NULL,
  `SORT_ORDER` int(4) DEFAULT NULL,
  `CREATED_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `LAST_UPDATE_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LAST_UPDATE_ON` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_tbl_site_monitoring_structure_tbl_site1_idx` (`SITE_ID`),
  CONSTRAINT `fk_tbl_site_monitoring_structure_tbl_site1` FOREIGN KEY (`SITE_ID`) REFERENCES `tbl_site` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_site_monitoring_structure`
--

LOCK TABLES `tbl_site_monitoring_structure` WRITE;
/*!40000 ALTER TABLE `tbl_site_monitoring_structure` DISABLE KEYS */;
INSERT INTO `tbl_site_monitoring_structure` VALUES ('1534931673976832','1531037833617408','test','test','Yes','No','3','Yes','5','No',NULL,0,NULL,NULL,'2021-11-16 15:12:58',NULL,'2021-11-16 15:12:58'),('1534961274650624','1534949503234048','sample','','Yes',NULL,'',NULL,'',NULL,NULL,0,NULL,NULL,'2021-11-16 17:13:24',NULL,'2021-11-16 17:13:53');
/*!40000 ALTER TABLE `tbl_site_monitoring_structure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_site_power_generatin_info_log`
--

DROP TABLE IF EXISTS `tbl_site_power_generatin_info_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_site_power_generatin_info_log` (
  `ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `POWER_GENERATION_STATUS_ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `LOG_CONTENT` text COLLATE utf8mb4_unicode_ci,
  `REMARK` text COLLATE utf8mb4_unicode_ci,
  `STATUS` tinyint(4) DEFAULT NULL,
  `SORT_ORDER` int(4) DEFAULT NULL,
  `CREATED_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `LAST_UPDATE_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LAST_UPDATE_ON` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_tbl_power_generation_status_idx` (`POWER_GENERATION_STATUS_ID`),
  CONSTRAINT `fk_tbl_power_generation_status1` FOREIGN KEY (`POWER_GENERATION_STATUS_ID`) REFERENCES `tbl_power_generation_status` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_site_power_generatin_info_log`
--

LOCK TABLES `tbl_site_power_generatin_info_log` WRITE;
/*!40000 ALTER TABLE `tbl_site_power_generatin_info_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_site_power_generatin_info_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_site_power_generation_capacity`
--

DROP TABLE IF EXISTS `tbl_site_power_generation_capacity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_site_power_generation_capacity` (
  `ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `SITE_ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `POWER_GENERATION_CAPACITY` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `COMBINED_CAPACITY` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PANEL_CAPACITY` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PCS_CAPACITY` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ELECTRIC_SELLING_PRICE` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `REMARK` text COLLATE utf8mb4_unicode_ci,
  `STATUS` tinyint(4) DEFAULT NULL,
  `SORT_ORDER` int(4) DEFAULT NULL,
  `CREATED_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `LAST_UPDATE_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LAST_UPDATE_ON` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_tbl_power_generation_capacity_tbl_site_idx` (`SITE_ID`),
  CONSTRAINT `fk_tbl_power_generation_capacity_tbl_site` FOREIGN KEY (`SITE_ID`) REFERENCES `tbl_site` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_site_power_generation_capacity`
--

LOCK TABLES `tbl_site_power_generation_capacity` WRITE;
/*!40000 ALTER TABLE `tbl_site_power_generation_capacity` DISABLE KEYS */;
INSERT INTO `tbl_site_power_generation_capacity` VALUES ('1533518058074112','1531037833617408','','','','','',NULL,0,NULL,NULL,'2021-11-12 15:20:57',NULL,'2021-11-16 21:23:53');
/*!40000 ALTER TABLE `tbl_site_power_generation_capacity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_site_power_generation_status`
--

DROP TABLE IF EXISTS `tbl_site_power_generation_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_site_power_generation_status` (
  `ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `SITE_ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DATE` datetime DEFAULT NULL,
  `ACTUAL_GENERATION_AMOUNT` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AI_PREDICTION` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SUSPENDED_PCS` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PCS_LINK` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `REMARK` text COLLATE utf8mb4_unicode_ci,
  `STATUS` tinyint(4) DEFAULT NULL,
  `SORT_ORDER` int(4) DEFAULT NULL,
  `CREATED_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `LAST_UPDATE_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LAST_UPDATE_ON` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_tbl_site_power_generation_status_tbl_site_idx` (`SITE_ID`),
  CONSTRAINT `fk_tbl_site_power_generation_status_tbl_site` FOREIGN KEY (`SITE_ID`) REFERENCES `tbl_site` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_site_power_generation_status`
--

LOCK TABLES `tbl_site_power_generation_status` WRITE;
/*!40000 ALTER TABLE `tbl_site_power_generation_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_site_power_generation_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_site_power_supply_system`
--

DROP TABLE IF EXISTS `tbl_site_power_supply_system`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_site_power_supply_system` (
  `ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `SITE_ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `STEEL_TOWER_NUMBER` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CONNECTING_TOWER_NUMBER` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CONNECTING_DISTANCE` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CONNECTING_VOLTAGE` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `REMARK` text COLLATE utf8mb4_unicode_ci,
  `STATUS` tinyint(4) DEFAULT NULL,
  `SORT_ORDER` int(4) DEFAULT NULL,
  `CREATED_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `LAST_UPDATE_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LAST_UPDATE_ON` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_tbl_site_power_supply_system_tbl_site1_idx` (`SITE_ID`),
  CONSTRAINT `fk_tbl_site_power_supply_system_tbl_site1` FOREIGN KEY (`SITE_ID`) REFERENCES `tbl_site` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_site_power_supply_system`
--

LOCK TABLES `tbl_site_power_supply_system` WRITE;
/*!40000 ALTER TABLE `tbl_site_power_supply_system` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_site_power_supply_system` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_site_status`
--

DROP TABLE IF EXISTS `tbl_site_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_site_status` (
  `ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `SITE_ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DATE` datetime DEFAULT NULL,
  `TIME_OF_ERROR_OCCURRED` datetime DEFAULT NULL,
  `TIME_OF_RECOVERY` datetime DEFAULT NULL,
  `ERROR_CONTENT` text COLLATE utf8mb4_unicode_ci,
  `SITE_STATUS` text COLLATE utf8mb4_unicode_ci,
  `URGENT_LEVEL` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `REMARK` text COLLATE utf8mb4_unicode_ci,
  `STATUS` tinyint(4) DEFAULT NULL,
  `SORT_ORDER` int(4) DEFAULT NULL,
  `CREATED_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `LAST_UPDATE_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LAST_UPDATE_ON` datetime DEFAULT NULL,
  `SPN` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_tbl_site_status_tbl_site_idx` (`SITE_ID`),
  CONSTRAINT `fk_tbl_site_status_tbl_site` FOREIGN KEY (`SITE_ID`) REFERENCES `tbl_site` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_site_status`
--

LOCK TABLES `tbl_site_status` WRITE;
/*!40000 ALTER TABLE `tbl_site_status` DISABLE KEYS */;
INSERT INTO `tbl_site_status` VALUES ('1532517306503168','1','2021-11-09 00:00:00','1970-01-01 03:04:00',NULL,'','','',NULL,0,NULL,NULL,'2021-11-09 19:28:53',NULL,'2021-11-09 19:29:52',''),('1533135484092416','1531037833617408','2021-11-04 00:00:00','1970-01-01 00:56:00',NULL,'','','',NULL,0,NULL,NULL,'2021-11-11 13:24:15',NULL,'2021-11-16 16:06:42','');
/*!40000 ALTER TABLE `tbl_site_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_site_urgent_response`
--

DROP TABLE IF EXISTS `tbl_site_urgent_response`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_site_urgent_response` (
  `ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `SITE_ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `URGENT_RESPONSE` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `URGENT_RESPONSE_DESCRIPTION` text COLLATE utf8mb4_unicode_ci,
  `GRASS_REMOVAL` text COLLATE utf8mb4_unicode_ci,
  `HARBICIDE` text COLLATE utf8mb4_unicode_ci,
  `SNOW_REMOVAL` text COLLATE utf8mb4_unicode_ci,
  `MAINTENANCE_BY_PCS_MANUFACTURER` text COLLATE utf8mb4_unicode_ci,
  `REMARK` text COLLATE utf8mb4_unicode_ci,
  `STATUS` tinyint(4) DEFAULT NULL,
  `SORT_ORDER` int(4) DEFAULT NULL,
  `CREATED_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `LAST_UPDATE_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LAST_UPDATE_ON` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_tbl_site_urgent_response_tbl_site1_idx` (`SITE_ID`),
  CONSTRAINT `fk_tbl_site_urgent_response_tbl_site1` FOREIGN KEY (`SITE_ID`) REFERENCES `tbl_site` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_site_urgent_response`
--

LOCK TABLES `tbl_site_urgent_response` WRITE;
/*!40000 ALTER TABLE `tbl_site_urgent_response` DISABLE KEYS */;
INSERT INTO `tbl_site_urgent_response` VALUES ('1534911802531840','1531037833617408','No','','test data','','test','',NULL,0,NULL,NULL,'2021-11-16 13:52:06',NULL,'2021-11-16 14:18:40');
/*!40000 ALTER TABLE `tbl_site_urgent_response` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_user`
--

DROP TABLE IF EXISTS `tbl_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_user` (
  `ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `USERNAME` char(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `PASSWORD` char(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `FIRST_NAME` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LAST_NAME` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `KATAKANA_NAME` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `EMERGENCY_CONTACT_NUMBER` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ADDRESS` text CHARACTER SET utf8,
  `EMAIL` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `GENDER` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AGE` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DEPARTMENT_1` char(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DEPARTMENT_2` char(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `WORK_BASE` text CHARACTER SET utf8,
  `DATE_OF_HIRING` datetime DEFAULT NULL,
  `DATE_OF_LEAVING` datetime DEFAULT NULL,
  `QUALIFICATION` text CHARACTER SET utf8,
  `OM_QUALIFICATION` text CHARACTER SET utf8,
  `USER_ID` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `USER_TYPE` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PROFILE_PICTURE` blob,
  `REMARK` text CHARACTER SET utf8,
  `STATUS` tinyint(4) DEFAULT NULL,
  `SORT_ORDER` int(4) DEFAULT NULL,
  `CREATED_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `LAST_UPDATE_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LAST_UPDATE_ON` datetime DEFAULT NULL,
  `COMPANY_ID` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `USERID_UNIQUE` (`USER_ID`),
  KEY `fk_user_company_idx` (`COMPANY_ID`),
  CONSTRAINT `fk_user_company` FOREIGN KEY (`COMPANY_ID`) REFERENCES `tbl_company` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_user`
--

LOCK TABLES `tbl_user` WRITE;
/*!40000 ALTER TABLE `tbl_user` DISABLE KEYS */;
INSERT INTO `tbl_user` VALUES ('1522376739188736','Admin','$2a$10$SohUYi1bEQ6mPlgiU9M6w..b4X5TlRXkq8F14vCGRDwcOOdYElTWS','Admin','A','','123-123-456',NULL,'admin@pvmo.com','male','35','HR','HR','HR','2021-10-12 00:00:00','2021-10-13 00:00:00','HR','HR','ADMIN','ADMIN',NULL,NULL,9,NULL,NULL,'2021-10-12 03:46:48',NULL,'2021-10-12 06:33:09','1521885382393856'),('152308123052851','Supervisor','$2a$10$SohUYi1bEQ6mPlgiU9M6w..b4X5TlRXkq8F14vCGRDwcOOdYElTWS','Supervisor','A',NULL,'123-123-456',NULL,'supervisor@gmail.com','male','35','HR','HR','HR','2021-10-19 00:00:00','2022-10-19 00:00:00','HR','HR','U002-C1','SUPERVISOR',NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'1521885382393856'),('1523081230528512','sampath','$2a$10$DdmAy3IkSm/xlq43jPUSe.iB.thQgActMdKB/HRFvGG0cjsfdTE8S','sampath','pallekumbura','','123-123-456',NULL,'sc001@g.com','male','35','HR','HR','HR,HR Dip,HR','2021-10-10 00:00:00','2021-10-21 00:00:00','HR,HR Dip,HR','HR,HR Dip,HR','U001-C1',NULL,NULL,NULL,0,NULL,NULL,'2021-10-14 03:33:23',NULL,'2021-10-14 03:33:23','1521885382393856');
/*!40000 ALTER TABLE `tbl_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `ID` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `USERNAME` char(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `PASSWORD` char(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `FIRST_NAME` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LAST_NAME` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `KATAKANA_NAME` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `EMERGENCY_CONTACT_NUMBER` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ADDRESS` text COLLATE utf8mb4_unicode_ci,
  `EMAIL` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `GENDER` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AGE` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DEPARTMENT_1` char(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DEPARTMENT_2` char(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `WORK_BASE` text COLLATE utf8mb4_unicode_ci,
  `DATE_OF_HIRING` datetime DEFAULT NULL,
  `DATE_OF_LEAVING` datetime DEFAULT NULL,
  `QUALIFICATION` text COLLATE utf8mb4_unicode_ci,
  `OM_QUALIFICATION` text COLLATE utf8mb4_unicode_ci,
  `USER_ID` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `USER_TYPE` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `REMARK` text COLLATE utf8mb4_unicode_ci,
  `STATUS` tinyint(4) DEFAULT NULL,
  `SORT_ORDER` int(4) DEFAULT NULL,
  `CREATED_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `LAST_UPDATE_BY` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LAST_UPDATE_ON` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `USERNAME_UNIQUE` (`USERNAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-16 17:59:02
