package com.axiohelix.pvmo.config;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import com.fasterxml.jackson.databind.ObjectMapper;

public class CustomAuthenticationFailureHandler implements AuthenticationFailureHandler {

	private static final Logger logger = LoggerFactory.getLogger(CustomAuthenticationFailureHandler.class);
	
	public static final String BAD_CREADENTIALS_ERROR = "Bad credentials";
	public static final String BAD_CREADENTIALS_ERROR_MESSAGE = "悪い資格情報";

	private ObjectMapper objectMapper = new ObjectMapper();

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {
		logger.error("Responding with unauthorized error. Message - {}", exception.getMessage());
		response.setStatus(HttpStatus.UNAUTHORIZED.value());
		response.setContentType(MediaType.APPLICATION_JSON.toString());
		response.setCharacterEncoding(StandardCharsets.UTF_8.displayName());
		
		String customErrorMessage;
		
		if(exception.getMessage().equals(BAD_CREADENTIALS_ERROR)){
			customErrorMessage = BAD_CREADENTIALS_ERROR;
		}else {
			customErrorMessage = exception.getMessage();
		}
		
		Map<String, Object> data = new HashMap<>();
		data.put("timestamp", Calendar.getInstance().getTime());
		data.put("exception", customErrorMessage);

		response.getWriter().println(objectMapper.writeValueAsString(data));
	}
}
