
package com.axiohelix.pvmo.config;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

@Configuration
public class I18nConfiguration implements WebMvcConfigurer {

	private static final String EN = "en";
	private static final String JP = "jp";

	@Value("${global.application.language}")
	private String language;

	/*@Bean
	public SessionLocaleResolver localeResolver() {
		SessionLocaleResolver slr = new SessionLocaleResolver();
		if (EN.equals(language)) {
			slr.setDefaultLocale(Locale.ENGLISH); // set the default locale to en
		} else if (JP.equals(language)) {
			slr.setDefaultLocale(Locale.JAPANESE);
		}

		return slr;

	}*/
	
	@Bean
	public CookieLocaleResolver localeResolver() {
		CookieLocaleResolver clr = new CookieLocaleResolver();
		if (EN.equals(language)) {
			clr.setDefaultLocale(Locale.ENGLISH); // set the default locale to en
		} else if (JP.equals(language)) {
			clr.setDefaultLocale(Locale.JAPANESE);
		}
	    return clr;
	}

	@Bean
	public MessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasename("classpath:messages"); // specify the message file prefix
		messageSource.setCacheSeconds(60 * 60); // reload messages every 1 hour
		return messageSource;
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(localeChangeInterceptor());
	}

	@Bean
	public LocaleChangeInterceptor localeChangeInterceptor() {
		LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
		lci.setParamName("lang");
		return lci;
	}

}
