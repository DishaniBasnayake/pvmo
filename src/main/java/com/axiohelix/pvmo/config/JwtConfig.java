package com.axiohelix.pvmo.config;

import org.springframework.beans.factory.annotation.Value;

import lombok.Getter;

@Getter
public class JwtConfig {

	@Value("${security.jwt.uri:/api/auth/login/**}")
	private String loginUri;

	@Value("${security.jwt.uri:/api/admin/**}")
	private String adminUri;

	@Value("${security.jwt.header:X-Authorization}")
	private String authorizationHeader;

	@Value("${security.jwt.uri:/api/core/document/downloader/**}")
	private String downloaderUrl;
	
	@Value("${security.jwt.uri:/api/v1/user/saveUpdate}")
	private String registerUri;

	@Value("${security.jwt.header:Token}")
	private String tokenHeader;

	@Value("${security.jwt.header:ExpiresInMills}")
	private String expireHeader;

	@Value("${security.jwt.header:RefreshExpireHeader}")
	private String refreshExpireHeader;

	@Value("${security.jwt.prefix:Bearer:}")
	private String prefix;

	@Value("${security.jwt.expiration:#{1000*60*30}}")
	private int expiration;

	@Value("${security.jwt.header:#{1000*60*30}}")
	private int refreshExpiration;

	@Value("${security.jwt.secret:JwtSecretKey}")
	private String secret;

}
