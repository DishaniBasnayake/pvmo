package com.axiohelix.pvmo.config;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.Date;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.axiohelix.pvmo.model.SessionUser;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JwtUsernameAndPasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	private static final Logger logger = LoggerFactory.getLogger(JwtUsernameAndPasswordAuthenticationFilter.class);
	
	private ObjectMapper objectMapper = new ObjectMapper();
	
	private AuthenticationManager authManager;
	private final JwtConfig jwtConfig;
	
	public JwtUsernameAndPasswordAuthenticationFilter(AuthenticationManager authManager, JwtConfig jwtConfig) {
		this.authManager = authManager;
		this.jwtConfig = jwtConfig;
		this.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher(jwtConfig.getLoginUri(), "POST"));
	}
	
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {
		
		try {
			UserCredentials creds = new ObjectMapper().readValue(request.getInputStream(), UserCredentials.class);
			UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
					creds.getUsername(), creds.getPassword(), Collections.emptyList());
			return authManager.authenticate(authToken);
			
		} catch (IOException ex) { 
			logger.error("Could not set user authentication in security context", ex);
			throw new RuntimeException(ex.getMessage()); 
		}
	}
	
	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
												   Authentication auth) throws IOException, ServletException {
		Long now = System.currentTimeMillis();
		int expiresInMills = jwtConfig.getExpiration();
		SessionUser user = (SessionUser) auth.getPrincipal();

		String token = Jwts.builder()
			.setSubject(user.getUsername())
			.claim("authorities", auth.getAuthorities().stream()
				.map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
			.setIssuedAt(new Date(now))
			//.setExpiration(new Date(now + expiresInMills)) 
			.signWith(SignatureAlgorithm.HS512, jwtConfig.getSecret().getBytes())
			.compact();
		response.setContentType(MediaType.APPLICATION_JSON.toString());
		response.setCharacterEncoding(StandardCharsets.UTF_8.displayName());
		
		/**
		response.addHeader(jwtConfig.getHeader(), jwtConfig.getPrefix() + token);
		response.addHeader(jwtConfig.getExpireHeader(), String.valueOf(expiresInMills));
		**/
		String usertype = "NONE";
		
		if(auth.getAuthorities().stream()
		.map(GrantedAuthority::getAuthority).collect(Collectors.toList()).size() > 0) {
			usertype = auth.getAuthorities().stream()
					.map(GrantedAuthority::getAuthority).collect(Collectors.toList()).get(0);
		}
		
		Map<String, Object> data = new HashMap<>();
		data.put("timestamp", Calendar.getInstance().getTime());
		data.put("token", jwtConfig.getPrefix()+token);
		data.put("email", user.getUsername());
		data.put("usertype", usertype.replaceFirst(com.axiohelix.pvmo.util.CommonConstant.ROLE_PREFIX, ""));

		response.getWriter().println(objectMapper.writeValueAsString(data));
	}

	private static class UserCredentials {
	    private String username, password, remeberMe;
	    
	    public String getUsername() {
			return username;
		}	    
	    public void setUsername(String username) {
			this.username = username;
		}
	    public String getPassword() {
			return password;
		}
	    public void setPassword(String password) {
			this.password = password;
		}
		public String getRemeberMe() {
			return remeberMe;
		}
		public void setRemeberMe(String remeberMe) {
			this.remeberMe = remeberMe;
		}
	}
}