package com.axiohelix.pvmo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.server.SecurityWebFilterChain;

import com.axiohelix.pvmo.service.impl.CustomOAuth2UserServiceImpl;

import lombok.RequiredArgsConstructor;

@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, jsr250Enabled = true, prePostEnabled = true)
public class MultiHttpSecurityConfig {

	@Autowired
	private UserDetailsService userDetailsService;

	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(daoAuthenticationProvider());

	}
	
	@Autowired
    private CustomOAuth2UserServiceImpl oauthUserService;

	public AuthenticationProvider daoAuthenticationProvider() {
		DaoAuthenticationProvider impl = new DaoAuthenticationProvider();
		impl.setUserDetailsService(userDetailsService);
		impl.setPasswordEncoder(new BCryptPasswordEncoder());
		impl.setHideUserNotFoundExceptions(false);
		return impl;
	}

	/**
	 * 
	 * Form login security configuration
	 *
	 */
	@Configuration
	@Order(1)
	public class FormLoginWebSecurityConfig extends WebSecurityConfigurerAdapter {

		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http.csrf().disable().httpBasic().and().exceptionHandling()

					.and().authorizeRequests().antMatchers("/").permitAll().and().authorizeRequests()
					.antMatchers("/css/**", "/dist/**", "/fonts/**", "/images/**", "/js/**").permitAll().and()
					.authorizeRequests().antMatchers("/map/**", "/plugins/**", "/vendor/**", "/webfonts/**","/site/{id:\\d+}/stringmap","/site/{id:\\d+}/devices/**").permitAll()
					.antMatchers("/oauth/**").permitAll()
					.anyRequest().authenticated().and()

					.formLogin().loginPage("/login").failureUrl("/login?error=true")
						.defaultSuccessUrl("/dashboard", true).permitAll().and().rememberMe().key("uniqueAndSecret")
						.tokenValiditySeconds(86400).userDetailsService(userDetailsService).and().logout()
						.logoutSuccessUrl("/").deleteCookies("JSESSIONID").permitAll()
						.and()
					.oauth2Login()
						.loginPage("/login")
						.failureUrl("/login?error=true")
						.defaultSuccessUrl("/dashboard", true).permitAll().and().rememberMe().key("uniqueAndSecret")
						.tokenValiditySeconds(86400).userDetailsService(userDetailsService).and().logout()
						.logoutSuccessUrl("/").deleteCookies("JSESSIONID").permitAll();

			http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.ALWAYS);
			//http.sessionManagement().maximumSessions(1).expiredUrl("/login?expired");

		}

	}

	/**
	 * 
	 * API Security configuration
	 *
	 */
	@Configuration
	@Order(0)
	public class ApiWebSecurityConfig extends WebSecurityConfigurerAdapter {

		@Bean
		public JwtConfig getJwtConfig() {
			return new JwtConfig();
		}

		private CustomAuthenticationFailureHandler customAuthenitcationFailurehander() {
			return new CustomAuthenticationFailureHandler();
		}

		@Bean("authenticationManager")
		@Override
		public AuthenticationManager authenticationManagerBean() throws Exception {
			return super.authenticationManagerBean();
		}

		@Autowired
		private JwtAuthenticationEntryPoint jwtUnauthorizedHandler;

		@Override
		protected void configure(HttpSecurity http) throws Exception {

			http.antMatcher("/api/**").cors().and().csrf().disable().httpBasic().and().exceptionHandling()
					.authenticationEntryPoint(jwtUnauthorizedHandler).and().sessionManagement()
					.sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().authorizeRequests()
					.antMatchers("/css/**", "/dist/**", "/fonts/**", "/images/**", "/js/**").permitAll()
					.antMatchers("/map/**", "/plugins/**", "/vendor/**", "/webfonts/**").permitAll()
					.antMatchers("/api/auth/**").permitAll()
					.antMatchers("/api/user/checkUsernameAvailability", "/api/user/checkEmailAvailability").permitAll()
					/* .antMatchers(HttpMethod.GET, "/api/site/**", "/api/users/**").permitAll() */
					.anyRequest().authenticated();

			http.addFilter(jwtUsernameAndPasswordAuthenticationFilter());
			http.addFilterBefore(new JwtAuthenticationFilter(getJwtConfig()),
					JwtUsernameAndPasswordAuthenticationFilter.class);

		}

		private JwtUsernameAndPasswordAuthenticationFilter jwtUsernameAndPasswordAuthenticationFilter()
				throws Exception {
			JwtUsernameAndPasswordAuthenticationFilter filter = new JwtUsernameAndPasswordAuthenticationFilter(
					authenticationManager(), getJwtConfig());
			filter.setAuthenticationFailureHandler(customAuthenitcationFailurehander());
			return filter;
		}

		@Bean
		public JwtConfig jwtConfig() {
			return new JwtConfig();
		}

	}

}
