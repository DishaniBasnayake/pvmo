package com.axiohelix.pvmo.config;

import java.util.Locale;

import javax.sql.DataSource;

import org.apache.ibatis.io.VFS;
import org.apache.ibatis.session.AutoMappingBehavior;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.generator.logging.log4j.Log4jImpl;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.boot.autoconfigure.SpringBootVFS;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;



@Configuration
public class MybatisConfig {

	@Bean
	public DataSourceTransactionManager transactionManager(DataSource dataSource) {
		DataSourceTransactionManager transactionManager = new DataSourceTransactionManager();
		transactionManager.setDataSource(dataSource);

		return transactionManager;
	}

	@Bean
	public SqlSessionFactory sqlSessionFactory(DataSource dataSource) throws Exception {
		SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
		sessionFactory.setVfs(SpringBootVFS.class);
		sessionFactory.setDataSource(dataSource);

		VFS.addImplClass(SpringBootVFS.class);

		org.apache.ibatis.session.Configuration config = new org.apache.ibatis.session.Configuration();
		config.setLogPrefix("com.axiohelix.mybatis.");
		config.setMapUnderscoreToCamelCase(true);
		config.setAutoMappingBehavior(AutoMappingBehavior.FULL);
		config.getTypeAliasRegistry().registerAliases("com.axiohelix.pvmo.entity");
		sessionFactory.setConfiguration(config);

		PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
		sessionFactory.setMapperLocations(resolver.getResources("classpath:/mybatis/**/*.xml"));

		return sessionFactory.getObject();
	}
	
	

}
