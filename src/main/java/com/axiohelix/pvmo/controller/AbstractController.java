package com.axiohelix.pvmo.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.util.ResourceUtils;

import com.axiohelix.pvmo.entity.User;
import com.axiohelix.pvmo.entity.UserWithBLOBs;
import com.axiohelix.pvmo.model.SessionUser;
import com.axiohelix.pvmo.service.UserService;
import com.axiohelix.pvmo.type.UserType;
import com.axiohelix.pvmo.util.CommonConstant;
import com.axiohelix.pvmo.util.CommonFunctions;

public class AbstractController {

	private static final Logger LOGGER = LogManager.getLogger(AbstractController.class);
	protected static final String ticketPrefix = "TICKET";

	@Value("${spring.profiles.active}")
	private String activePropertyPrefix;
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	@Autowired
	private UserService userService;

	protected SessionUser getCurrentUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth instanceof OAuth2AuthenticationToken) {
			OidcUser oauthUser = (OidcUser) auth.getPrincipal();
			String uname = oauthUser.getEmail();
			String firstName = oauthUser.getGivenName();
			String lastName = oauthUser.getFamilyName();
			String fullName = oauthUser.getFullName();
			User user = userService.findByUsername(uname);
			if(user == null) {
				user = new UserWithBLOBs();
				user.setEmail(uname);
				user.setFirstName(firstName);
				user.setLastName(lastName);
				user.setUsername(fullName);
				user.setUserType(UserType.USER.getDbValue());
				user.setPassword(CommonConstant.DEFAULT_PASSWORD.toString());
				userService.saveUpdate((UserWithBLOBs) user);
			}
			/*Set<GrantedAuthority> ga = new HashSet<>();
			ga.add(new SimpleGrantedAuthority(com.axiohelix.pvmo.util.CommonConstant.ROLE_PREFIX+user.getUserType()));
			return new SessionUser(user.getEmail(), "", ga, user.getId(), user.getUserType());*/
			//return (SessionUser) userDetailsService.loadUserByUsername(user.getEmail());
			
		}
		
		return (SessionUser) userDetailsService.loadUserByUsername(CommonFunctions.getCurrentUserName());
	}
	
	protected String getTlogDateString() {
		Scanner s = null;
		try {
			File f = ResourceUtils.getFile("classpath:static/files/tlog.txt");
			
			InputStream input = new FileInputStream(f);
			
			s = new Scanner(input);
			HashMap<String, String> appMap = new HashMap<>();
			String txt = null;
			String[] parts = null; 
			
			while(s.hasNext()) {
				txt = s.next();
				
				if(txt != null && !txt.isEmpty()) {
					parts = txt.split("=");
					appMap.put(parts[0], parts[1]);
				}
			}
			
			if(appMap.containsKey("ticket.tlog.date")) {
				return appMap.get("ticket.tlog.date");
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			if(s != null) s.close();
		}
		
		return "";
	}
	
	protected int getTlogSize() {
		Scanner s = null;
		try {
			File f = ResourceUtils.getFile("classpath:static/files/tlog.txt");
			
			InputStream input = new FileInputStream(f);
			
			s = new Scanner(input);
			HashMap<String, String> appMap = new HashMap<>();
			String txt = null;
			String[] parts = null; 
			
			while(s.hasNext()) {
				txt = s.next();
				
				if(txt != null && !txt.isEmpty()) {
					parts = txt.split("=");
					appMap.put(parts[0], parts[1]);
				}
			}
			
			if(appMap.containsKey("ticket.tlog.size")) {
				return Integer.valueOf(appMap.get("ticket.tlog.size"));
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(s != null) s.close();
		}
		
		return 0;
	}

}
