package com.axiohelix.pvmo.controller;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.axiohelix.pvmo.entity.Company;
import com.axiohelix.pvmo.entity.CompanyExample;
import com.axiohelix.pvmo.entity.CompanyWithBLOBs;
import com.axiohelix.pvmo.entity.Prefecture;
import com.axiohelix.pvmo.entity.PrefectureExample;
import com.axiohelix.pvmo.entity.Region;
import com.axiohelix.pvmo.entity.RegionExample;
import com.axiohelix.pvmo.entity.ServiceCenter;
import com.axiohelix.pvmo.entity.ServiceCenterExample;
import com.axiohelix.pvmo.entity.ServiceCenterWithBLOBs;
import com.axiohelix.pvmo.entity.SiteWithBLOBs;
import com.axiohelix.pvmo.entity.UserWithBLOBs;
import com.axiohelix.pvmo.service.CompanyService;
import com.axiohelix.pvmo.service.PrefectureService;
import com.axiohelix.pvmo.service.RegionService;
import com.axiohelix.pvmo.type.Status;
import com.axiohelix.pvmo.util.CommonConstant;
import com.axiohelix.pvmo.util.CommonResponse;
import com.axiohelix.pvmo.util.CommonValidation;
import com.github.pagehelper.PageInfo;

@Controller
@RequestMapping(CommonConstant.CONTROLLER_URL_COMPANY)
public class CompanyController {

	private static final Logger LOGGER = LogManager.getLogger(CompanyController.class);

	@Autowired
	private CompanyService companyService;
	@Autowired
	private RegionService regionService;
	@Autowired
	private PrefectureService prefectureService;

	private static final String EN = "en";
	private static final String JP = "jp";
	@Value("${global.application.language}")
	private String language;
	

	/*
	 * @GetMapping("") public String index(
	 * 
	 * @RequestParam(value = "pageNum", defaultValue =
	 * CommonConstant.DEFAULT_PAGINATION_PAGE) int pageNum,
	 * 
	 * @RequestParam(value = "pageSize", defaultValue =
	 * CommonConstant.DEFAULT_PAGINATION_SIZE) int pageSize, Model model) {
	 * 
	 * PageInfo<Company> page = list(pageNum, pageSize);
	 * 
	 * model.addAttribute("page", page); model.addAttribute("company", new
	 * CompanyWithBLOBs()); return "company/list"; }
	 */

	@GetMapping("")
	public String index(
			@RequestParam(value = "pageNum", defaultValue = CommonConstant.DEFAULT_PAGINATION_PAGE) int pageNum,
			@RequestParam(value = "pageSize", defaultValue = CommonConstant.DEFAULT_PAGINATION_SIZE) int pageSize,
			Model model) {

		model.addAttribute("company", new CompanyWithBLOBs());
		//model.addAttribute(language);

		return "company/list";
	}

	@ResponseBody
	@GetMapping("/data/{search}")
	public CommonResponse index(@PathVariable(value = "search") String search) {

		CommonResponse commonResponse = new CommonResponse();

		CompanyExample companyExample = new CompanyExample();
		companyExample.createCriteria().andStatusEqualTo(Status.ACTIVE.getDbValue().byteValue());
		companyExample.setOrderByClause("CREATED_ON DESC");

		List<CompanyWithBLOBs> list = companyService.selectByExampleWithBLOBs(companyExample);

		commonResponse.setStatus(true);
		commonResponse.getPayload().add(list);
		return commonResponse;
	}

	private PageInfo<Company> list(int pageNum, int pageSize) {
		CompanyExample companyExample = new CompanyExample();
		companyExample.createCriteria().andStatusEqualTo(Status.ACTIVE.getDbValue().byteValue());
		companyExample.setOrderByClause("CREATED_ON DESC");

		PageInfo<Company> page = companyService.selectByExampleWithBLOBs(companyExample, pageNum, pageSize);
		return page;
	}

	@GetMapping("/{id}")
	public String view(Model model, @PathVariable("id") String id) {
		CompanyWithBLOBs company = companyService.selectByPrimaryKeyWithBLOBs(id);
		
		RegionExample regionExample = new RegionExample();
		regionExample.createCriteria().andStatusEqualTo(Status.ACTIVE.getDbValue().byteValue());
		regionExample.setOrderByClause("CREATED_ON DESC");
		
		PrefectureExample prefectureExample = new PrefectureExample();
		prefectureExample.createCriteria().andStatusEqualTo(Status.ACTIVE.getDbValue().byteValue());
		prefectureExample.setOrderByClause("CREATED_ON DESC");
		
		List<Region> regions = regionService.selectByExample(regionExample);
		List<Prefecture> prefectures = prefectureService.selectByExample(prefectureExample);
		
		model.addAttribute("company", company);
		model.addAttribute("serviceCenter", new ServiceCenterWithBLOBs());
		model.addAttribute("user", new UserWithBLOBs());
		model.addAttribute("site", new SiteWithBLOBs());
		model.addAttribute("regions", regions);
		model.addAttribute("prefectures", prefectures);
		
		return "company/view";
	}

	@ResponseBody
	@GetMapping("/{id}/delete")
	public CommonResponse delete(Model model, @PathVariable("id") String id) {

		CommonResponse commonResponse = new CommonResponse();
		CompanyWithBLOBs company = companyService.selectByPrimaryKeyWithBLOBs(id);
		if (company == null) {
			commonResponse.setStatus(false);
			commonResponse.getErrorMessages().add(CommonConstant.MSG_RECORD_NOT_FOUND);
			return commonResponse;
		}
		company.setStatus(Status.DELETED.getDbValue().byteValue());
		companyService.updateByPrimaryKeySelective(company);

		commonResponse.setStatus(true);
		return commonResponse;
	}

	@ResponseBody
	@GetMapping("/{id}/data")
	public CommonResponse companyData(@PathVariable("id") String id) {
		Company company = companyService.selectByPrimaryKey(id);
		CommonResponse commonResponse = new CommonResponse();
		commonResponse.setStatus(true);
		commonResponse.getPayload().add(company);
		return commonResponse;
	}

	@ResponseBody
	@PostMapping("/saveUpdate")
	public CommonResponse saveUpdate(@ModelAttribute("company") CompanyWithBLOBs company) {

		CompanyExample example = new CompanyExample();
		Optional<Company> companyEx = null;

		if (CommonValidation.stringNullValidation(company.getId())) {
			example.createCriteria().andCodeEqualTo(company.getCode());
			companyEx = companyService.selectByExample(example).stream().findFirst();
		} else {
			Company companyPr = companyService.selectByPrimaryKey(company.getId());
			example.createCriteria().andCodeEqualTo(company.getCode()).andCodeNotEqualTo(companyPr.getCode());
			companyEx = companyService.selectByExample(example).stream().findFirst();
		}

		if (companyEx.isPresent()) {
			CommonResponse response = new CommonResponse();
			response.getErrorMessages().add(CommonConstant.MSG_COMPANY_CODE + " " + companyEx.get().getCode() + " "
					+ CommonConstant.MSG_IS_ALREADY_EXISTS);
			return response;
		}

		return companyService.saveUpdate(company);
	}



}
