package com.axiohelix.pvmo.controller;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.axiohelix.pvmo.entity.CompanyWithBLOBs;
import com.axiohelix.pvmo.entity.ServiceCenter;
import com.axiohelix.pvmo.entity.ServiceCenterExample;
import com.axiohelix.pvmo.entity.ServiceCenterWithBLOBs;
import com.axiohelix.pvmo.service.CompanyService;
import com.axiohelix.pvmo.service.ServiceCenterService;
import com.axiohelix.pvmo.type.Status;
import com.axiohelix.pvmo.util.CommonConstant;
import com.axiohelix.pvmo.util.CommonResponse;
import com.axiohelix.pvmo.util.CommonValidation;

@Controller
@RequestMapping(CommonConstant.CONTROLLER_URL_COMPANY + "/{cid}/" + CommonConstant.CONTROLLER_URL_SERVICE_CENTER)
public class CompanyServiceCenterController {

	private static final Logger LOGGER = LogManager.getLogger(CompanyServiceCenterController.class);
	
	@Autowired
	private CompanyService companyService;
	@Autowired
	private ServiceCenterService serviceCenterService;

	@ResponseBody
	@GetMapping("/data/{search}")
	public CommonResponse index(@PathVariable("cid") String cid, @PathVariable(value = "search") String search) {

		CommonResponse commonResponse = new CommonResponse();

		ServiceCenterExample example = new ServiceCenterExample();
		example.createCriteria().andStatusEqualTo(Status.ACTIVE.getDbValue().byteValue()).andCompanyIdEqualTo(cid);
		example.setOrderByClause("CREATED_ON DESC");

		List<ServiceCenterWithBLOBs> list = serviceCenterService.selectByExampleWithBLOBs(example);

		commonResponse.setStatus(true);
		commonResponse.getPayload().add(list);
		return commonResponse;
	}

}
