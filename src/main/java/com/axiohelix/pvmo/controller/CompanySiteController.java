package com.axiohelix.pvmo.controller;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.axiohelix.pvmo.entity.CompanyWithBLOBs;
import com.axiohelix.pvmo.entity.Site;
import com.axiohelix.pvmo.entity.SiteExample;
import com.axiohelix.pvmo.entity.SiteWithBLOBs;
import com.axiohelix.pvmo.model.Search;
import com.axiohelix.pvmo.service.CompanyService;
import com.axiohelix.pvmo.service.SiteService;
import com.axiohelix.pvmo.type.Status;
import com.axiohelix.pvmo.util.CommonConstant;
import com.axiohelix.pvmo.util.CommonResponse;
import com.axiohelix.pvmo.util.CommonValidation;

@Controller
@RequestMapping(CommonConstant.CONTROLLER_URL_COMPANY + "/{cid}/" + CommonConstant.CONTROLLER_URL_SITE)
public class CompanySiteController {

	private static final Logger LOGGER = LogManager.getLogger(CompanySiteController.class);

	@Autowired
	private CompanyService companyService;
	@Autowired
	private SiteService siteService;

	@ResponseBody
	@GetMapping("/data/{region}/{serviceCenter}")
	public CommonResponse index(@PathVariable("cid") String cid, Search search) {

		CommonResponse commonResponse = new CommonResponse();

		SiteExample example = new SiteExample();
		example.createCriteria().andStatusEqualTo(Status.ACTIVE.getDbValue().byteValue()).andCompanyIdEqualTo(cid);
		example.setOrderByClause("CREATED_ON DESC");

		List<SiteWithBLOBs> list = siteService.selectByExampleWithBLOBs(example);

		commonResponse.setStatus(true);
		commonResponse.getPayload().add(list);
		return commonResponse;
	}

	@GetMapping("/{id}")
	public String view(Model model, @PathVariable("cid") String cid, @PathVariable("id") String id) {
		CompanyWithBLOBs company = companyService.selectByPrimaryKeyWithBLOBs(cid);
		SiteWithBLOBs site = siteService.selectByPrimaryKeyWithBLOBs(id);
		model.addAttribute("company", company);
		model.addAttribute("site", site);
		return "service-center/view";
	}

	@ResponseBody
	@GetMapping("/{id}/delete")
	public CommonResponse delete(Model model, @PathVariable("id") String id) {

		CommonResponse commonResponse = new CommonResponse();
		SiteWithBLOBs company = siteService.selectByPrimaryKeyWithBLOBs(id);
		if (company == null) {
			commonResponse.setStatus(false);
			commonResponse.getErrorMessages().add(CommonConstant.MSG_RECORD_NOT_FOUND);
			return commonResponse;
		}
		company.setStatus(Status.DELETED.getDbValue().byteValue());
		siteService.updateByPrimaryKeySelective(company);

		commonResponse.setStatus(true);
		return commonResponse;
	}

	@ResponseBody
	@GetMapping("/{id}/data")
	public CommonResponse companyData(@PathVariable("id") String id, Site site) {
		SiteWithBLOBs company = siteService.selectByPrimaryKeyWithBLOBs(id);
		CommonResponse commonResponse = new CommonResponse();
		commonResponse.setStatus(true);
		commonResponse.getPayload().add(company);
		return commonResponse;
	}

	@ResponseBody
	@PostMapping("/saveUpdate")
	public CommonResponse saveUpdate(@PathVariable("cid") String cid, @ModelAttribute("site") SiteWithBLOBs site) {
		SiteExample example = new SiteExample();
		Optional<Site> siteEx = null;
		site.setCompanyId(cid);

		if (CommonValidation.stringNullValidation(site.getId())) {
			example.createCriteria().andCompanyIdEqualTo(site.getCompanyId()).andCodeEqualTo(site.getCode());
			siteEx = siteService.selectByExample(example).stream().findFirst();
		} else {
			Site sitePr = siteService.selectByPrimaryKey(site.getId());
			example.createCriteria().andCompanyIdEqualTo(site.getCompanyId()).andCodeEqualTo(site.getCode())
					.andCodeNotEqualTo(sitePr.getCode());
			siteEx = siteService.selectByExample(example).stream().findFirst();
		}

		if (siteEx.isPresent()) {
			CommonResponse response = new CommonResponse();
			response.getErrorMessages().add(CommonConstant.MSG_SITE_CODE + " " + siteEx.get().getCode() + " "
					+ CommonConstant.MSG_IS_ALREADY_EXISTS);
			return response;
		}

		return siteService.saveUpdate(site);
	}

}
