package com.axiohelix.pvmo.controller;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.axiohelix.pvmo.entity.CompanyWithBLOBs;
import com.axiohelix.pvmo.entity.User;
import com.axiohelix.pvmo.entity.UserExample;
import com.axiohelix.pvmo.entity.UserWithBLOBs;
import com.axiohelix.pvmo.service.CompanyService;
import com.axiohelix.pvmo.service.UserService;
import com.axiohelix.pvmo.type.Status;
import com.axiohelix.pvmo.util.CommonConstant;
import com.axiohelix.pvmo.util.CommonResponse;
import com.axiohelix.pvmo.util.CommonValidation;

@Controller
@RequestMapping(CommonConstant.CONTROLLER_URL_COMPANY + "/{cid}/" + CommonConstant.CONTROLLER_URL_USER)
public class CompanyUserController {

	private static final Logger LOGGER = LogManager.getLogger(CompanyUserController.class);

	@Autowired
	private CompanyService companyService;
	@Autowired
	private UserService userService;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@ResponseBody
	@GetMapping("/data/{search}")
	public CommonResponse index(@PathVariable("cid") String cid, @PathVariable(value = "search") String search) {

		CommonResponse commonResponse = new CommonResponse();

		UserExample example = new UserExample();
		example.createCriteria().andStatusEqualTo(Status.ACTIVE.getDbValue().byteValue()).andCompanyIdEqualTo(cid);
		example.setOrderByClause("CREATED_ON DESC");

		List<UserWithBLOBs> list = userService.selectByExampleWithBLOBs(example);

		commonResponse.setStatus(true);
		commonResponse.getPayload().add(list);
		return commonResponse;
	}

	@GetMapping("/{id}")
	public String view(Model model, @PathVariable("cid") String cid, @PathVariable("id") String id) {
		CompanyWithBLOBs company = companyService.selectByPrimaryKeyWithBLOBs(cid);
		UserWithBLOBs user = userService.selectByPrimaryKeyWithBLOBs(id);
		model.addAttribute("company", company);
		model.addAttribute("user", user);
		return "service-center/view";
	}

	@ResponseBody
	@GetMapping("/{id}/delete")
	public CommonResponse delete(Model model, @PathVariable("id") String id) {

		CommonResponse commonResponse = new CommonResponse();
		UserWithBLOBs company = userService.selectByPrimaryKeyWithBLOBs(id);
		if (company == null) {
			commonResponse.setStatus(false);
			commonResponse.getErrorMessages().add(CommonConstant.MSG_RECORD_NOT_FOUND);
			return commonResponse;
		}
		company.setStatus(Status.DELETED.getDbValue().byteValue());
		userService.updateByPrimaryKeySelective(company);

		commonResponse.setStatus(true);
		return commonResponse;
	}

	@ResponseBody
	@GetMapping("/{id}/data")
	public CommonResponse companyData(@PathVariable("id") String id, User user) {
		UserWithBLOBs company = userService.selectByPrimaryKeyWithBLOBs(id);
		CommonResponse commonResponse = new CommonResponse();
		commonResponse.setStatus(true);
		commonResponse.getPayload().add(company);
		return commonResponse;
	}

	@ResponseBody
	@PostMapping("/saveUpdate")
	public CommonResponse saveUpdate(@PathVariable("cid") String cid, @ModelAttribute("user") UserWithBLOBs user) {
		UserExample example = new UserExample();
		Optional<User> userEx = null;
		user.setCompanyId(cid);

		if (CommonValidation.stringNullValidation(user.getId())) {
			example.createCriteria().andCompanyIdEqualTo(user.getCompanyId()).andUserIdEqualTo(user.getUserId());
			userEx = userService.selectByExample(example).stream().findFirst();
		} else {
			User userPr = userService.selectByPrimaryKey(user.getId());
			example.createCriteria().andCompanyIdEqualTo(user.getCompanyId()).andUserIdEqualTo(user.getUserId())
					.andUserIdNotEqualTo(userPr.getUserId());
			userEx = userService.selectByExample(example).stream().findFirst();
		}

		if (userEx.isPresent()) {
			CommonResponse response = new CommonResponse();
			response.getErrorMessages().add(CommonConstant.MSG_USER_CODE + " " + userEx.get().getUserId()
					+ " " + CommonConstant.MSG_IS_ALREADY_EXISTS);
			return response;
		}
		user.setUsername(user.getFirstName());
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		return userService.saveUpdate(user);
	}

}
