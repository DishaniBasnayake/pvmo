package com.axiohelix.pvmo.controller;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.axiohelix.pvmo.entity.Company;
import com.axiohelix.pvmo.entity.CompanyExample;
import com.axiohelix.pvmo.entity.CompanyWithBLOBs;
import com.axiohelix.pvmo.entity.Consumer;
import com.axiohelix.pvmo.entity.ConsumerExample;
import com.axiohelix.pvmo.entity.ConsumerWithBLOBs;
import com.axiohelix.pvmo.entity.ServiceCenterWithBLOBs;
import com.axiohelix.pvmo.entity.UserWithBLOBs;
import com.axiohelix.pvmo.service.CompanyService;
import com.axiohelix.pvmo.service.ConsumerService;
import com.axiohelix.pvmo.type.Status;
import com.axiohelix.pvmo.util.CommonConstant;
import com.axiohelix.pvmo.util.CommonResponse;
import com.axiohelix.pvmo.util.CommonValidation;
import com.github.pagehelper.PageInfo;

@Controller
@RequestMapping(CommonConstant.CONTROLLER_URL_CONSUMER)
public class ConsumerController {
	
	private static final Logger LOGGER = LogManager.getLogger(ConsumerController.class);

	@Autowired
	private ConsumerService consumerService;
	
	
	@GetMapping("")
	public String index(
			@RequestParam(value = "pageNum", defaultValue = CommonConstant.DEFAULT_PAGINATION_PAGE) int pageNum,
			@RequestParam(value = "pageSize", defaultValue = CommonConstant.DEFAULT_PAGINATION_SIZE) int pageSize,
			Model model) {

		model.addAttribute("consumer", new ConsumerWithBLOBs());
		return "consumer/list";
	}
	
	@ResponseBody
	@GetMapping("/data/{search}")
	public CommonResponse index(@PathVariable(value = "search") String search) {

		CommonResponse commonResponse = new CommonResponse();
		
		ConsumerExample consumerExample = new ConsumerExample();
		consumerExample.createCriteria().andStatusEqualTo(Status.ACTIVE.getDbValue().byteValue());
		consumerExample.setOrderByClause("CREATED_ON DESC");
		
		List<ConsumerWithBLOBs> list = consumerService.selectByExampleWithBLOBs(consumerExample);
		
		commonResponse.setStatus(true);
		commonResponse.getPayload().add(list);
		return commonResponse;
	}
	
	private PageInfo<Consumer> list(int pageNum, int pageSize) {
		ConsumerExample consumerExample = new ConsumerExample();
		consumerExample.createCriteria().andStatusEqualTo(Status.ACTIVE.getDbValue().byteValue());
		consumerExample.setOrderByClause("CREATED_ON DESC");

		PageInfo<Consumer> page = consumerService.selectByExampleWithBLOBs(consumerExample, pageNum, pageSize);
		return page;
	}
	
	@GetMapping("/{id}")
	public String view(Model model, @PathVariable("id") String id) {
		ConsumerWithBLOBs consumer = consumerService.selectByPrimaryKeyWithBLOBs(id);
		model.addAttribute("consumer", consumer);
		return "consumer/view";
	}
	
	@ResponseBody
	@GetMapping("/{id}/delete")
	public CommonResponse delete(Model model,@PathVariable("id") String id) {

		CommonResponse commonResponse = new CommonResponse();
		ConsumerWithBLOBs consumer = consumerService.selectByPrimaryKeyWithBLOBs(id);
		if(consumer == null) {
			commonResponse.setStatus(false);
			commonResponse.getErrorMessages().add(CommonConstant.MSG_RECORD_NOT_FOUND);
			return commonResponse;
		}
		consumer.setStatus(Status.DELETED.getDbValue().byteValue());
		consumerService.updateByPrimaryKeySelective(consumer);
		
		commonResponse.setStatus(true);
		return commonResponse;
	}
	
	@ResponseBody
	@GetMapping("/{id}/data")
	public CommonResponse companyData(@PathVariable("id") String id, Company compnay) {
		Consumer consumer = consumerService.selectByPrimaryKey(id);
		CommonResponse commonResponse = new CommonResponse();
		commonResponse.setStatus(true);
		commonResponse.getPayload().add(consumer);
		return commonResponse;
	}
	
	@ResponseBody
	@PostMapping("/saveUpdate")
	public CommonResponse saveUpdate(@ModelAttribute("consumer") ConsumerWithBLOBs consumer) {
		return consumerService.saveUpdate(consumer);
	}

}
