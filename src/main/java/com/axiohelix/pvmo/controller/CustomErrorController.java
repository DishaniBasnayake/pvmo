package com.axiohelix.pvmo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.autoconfigure.web.servlet.error.AbstractErrorController;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class CustomErrorController extends AbstractErrorController  {

	public CustomErrorController(DefaultErrorAttributes errorAttributes) {
		super(errorAttributes);
	}

	@RequestMapping(value = "/error", consumes = MediaType.ALL_VALUE)
    public ModelAndView handleError(HttpServletRequest request,Exception exception,ModelAndView mv,
    		@RequestHeader(value = "referer", required = false) final String referer) {
		Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
		Throwable error = (Throwable) request.getAttribute(RequestDispatcher.ERROR_EXCEPTION);
		Object message = request.getAttribute(RequestDispatcher.ERROR_MESSAGE);
		Object path = request.getAttribute(RequestDispatcher.ERROR_REQUEST_URI);
		List<Object> errors = new ArrayList<Object>();
		errors.add(status);errors.add(error);errors.add(message);errors.add(path);
		
		Map<String, Object> errorAttributes = super.getErrorAttributes(request, ErrorAttributeOptions.defaults());
		errorAttributes.keySet().removeIf(key -> key.toLowerCase().contentEquals("trace"));
	  		
        mv.addObject("backUrl",referer);
        mv.addObject("errors",errorAttributes.values().toArray());    
        
	    mv.setViewName("error");
	    return mv;
    }
 
}
