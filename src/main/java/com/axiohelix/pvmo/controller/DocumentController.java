package com.axiohelix.pvmo.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.axiohelix.pvmo.entity.DocumentWithBLOBs;
import com.axiohelix.pvmo.service.DocumentService;
import com.axiohelix.pvmo.util.CommonConstant;
import com.axiohelix.pvmo.util.CommonResponse;

@Controller
@RequestMapping({CommonConstant.CONTROLLER_URL_DOCUMENT, CommonConstant.API_CONTROLLER_URL_DOCUMENT})
public class DocumentController extends AbstractController{

	private static final Logger LOGGER = LogManager.getLogger(DocumentController.class);

	@Autowired
	DocumentService documentService;
	
	@ResponseBody
	@GetMapping("/{id}/data")
	public CommonResponse templateData(@PathVariable("id") String id) throws InterruptedException {
		//Thread.sleep(5000);
		DocumentWithBLOBs document = documentService.selectByPrimaryKeyWithBLOBs(id);
		CommonResponse commonResponse = new CommonResponse();
		commonResponse.setStatus(true);
		commonResponse.getPayload().add(document);
		return commonResponse;
	}
	
	
	
	
	
	

}
