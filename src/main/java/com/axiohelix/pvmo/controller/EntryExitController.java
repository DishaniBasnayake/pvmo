package com.axiohelix.pvmo.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.axiohelix.pvmo.entity.custom.SiteUserGPSLogExtended;
import com.axiohelix.pvmo.model.EntryExitSearch;
import com.axiohelix.pvmo.service.SiteUserGPSLogService;
import com.axiohelix.pvmo.util.CommonConstant;
import com.axiohelix.pvmo.util.CommonResponse;

@Controller
@RequestMapping({ CommonConstant.CONTROLLER_URL_ENTRY_EXIT })
public class EntryExitController extends AbstractController {

	private static final Logger LOGGER = LogManager.getLogger(EntryExitController.class);

	@Autowired
	private SiteUserGPSLogService siteUserGpsLogService;

	@GetMapping("")
	public String index(@ModelAttribute EntryExitSearch search,Model model) {
		model.addAttribute("search", search);
		return "entryexit/list";
	}

	@ResponseBody
	@GetMapping("/data")
	public CommonResponse data(@ModelAttribute EntryExitSearch search, Pageable pageable) throws InterruptedException {

		CommonResponse commonResponse = new CommonResponse();
		Page<SiteUserGPSLogExtended> list = siteUserGpsLogService.searchEntryExit(search, pageable);
		
		Thread.sleep(1000);
		
		commonResponse.setStatus(true);
		commonResponse.getPayload().add(list);
		return commonResponse;
	}

}
