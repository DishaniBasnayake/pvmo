package com.axiohelix.pvmo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.axiohelix.pvmo.entity.Facility;
import com.axiohelix.pvmo.entity.User;

import com.axiohelix.pvmo.service.UserService;

@Controller
@RequestMapping("/facility")
public class FacilityController {
	
//	@GetMapping({"","/"})
//	public String list(Model model) {
//		model.addAttribute("facilities", null);
//		return "facility";
//	}
//
//	@GetMapping("/{id}")
//	public String view(Model model, @PathVariable("id") String id) {
//		return "facility/view";
//	}
//	
//	@GetMapping("/add")
//	public String add(@PathVariable("id") String id, Model model) {
//		return "facility/add";
//	}
//	
//	@GetMapping("/edit/{id}")
//    public String update(@PathVariable("id") String id, Model model) {
//		return "facility/edit";
//    }
//	
//	@PostMapping("/save")
//	public String save(@ModelAttribute("facility") Facility facility) {
//        // save to database
//        return "redirect:/";
//    }
//	
//	@GetMapping("/delete/{id}")
//    public String delete(@PathVariable("id") String id) {
//        // call delete method 
//        return "redirect:/";
//    }
	
	@GetMapping("/draw-map")
    public String draw() {
        // call delete method 
        return "facility/draw";
    }

	
}
