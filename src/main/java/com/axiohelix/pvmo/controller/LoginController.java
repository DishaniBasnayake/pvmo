package com.axiohelix.pvmo.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.WebAttributes;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.axiohelix.pvmo.entity.Prefecture;
import com.axiohelix.pvmo.entity.PrefectureExample;
import com.axiohelix.pvmo.entity.Region;
import com.axiohelix.pvmo.entity.RegionExample;
import com.axiohelix.pvmo.entity.ServiceCenter;
import com.axiohelix.pvmo.entity.ServiceCenterExample;
import com.axiohelix.pvmo.entity.Site;
import com.axiohelix.pvmo.entity.SiteExample;
import com.axiohelix.pvmo.entity.SiteWithBLOBs;
import com.axiohelix.pvmo.entity.TicketWithBLOBs;
import com.axiohelix.pvmo.model.Search;
import com.axiohelix.pvmo.model.SessionUser;
import com.axiohelix.pvmo.service.CompanyService;
import com.axiohelix.pvmo.service.PrefectureService;
import com.axiohelix.pvmo.service.RegionService;
import com.axiohelix.pvmo.service.ServiceCenterService;
import com.axiohelix.pvmo.service.ServiceCenterSiteService;
import com.axiohelix.pvmo.service.ServiceCenterUserService;
import com.axiohelix.pvmo.service.SiteService;
import com.axiohelix.pvmo.type.Status;
import com.axiohelix.pvmo.type.UserType;
import com.axiohelix.pvmo.util.CommonResponse;

@Controller
public class LoginController extends AbstractController {

	private String username;
	@Autowired
	CompanyService companyService;
	@Autowired
	SiteService siteService;
	@Autowired
	private ServiceCenterUserService serviceCenterUserService;
	@Autowired
	private ServiceCenterSiteService serviceCenterSiteService;
	@Autowired
	private RegionService regionService;
	@Autowired
	private PrefectureService prefectureService;
	@Autowired
	private ServiceCenterService serviceCenterService;

	@GetMapping("/login")
	public String login(HttpServletRequest request, Model model, String error, String logout) {
		if (error != null) {
			HttpSession session = request.getSession(false);
			String errorMessage = null;
			if (session != null) {
				AuthenticationException ex = (AuthenticationException) session
						.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
				if (ex != null) {
					if (ex instanceof UsernameNotFoundException) {
						UsernameNotFoundException unfEx = (UsernameNotFoundException) ex;
						errorMessage = unfEx.getMessage();
					} else {
						errorMessage = ex.getMessage();
					}
				}
			}

			model.addAttribute("error", errorMessage);
		}
		if (logout != null) {
			model.addAttribute("message", "logged out successfully.");
			request.getSession().invalidate();
		}

		return "login";
	}

	@GetMapping("/dashboard")
	public String dashboard(Model model,Search search,HttpServletRequest request) {
		model.addAttribute("site", new SiteWithBLOBs());
		model.addAttribute("ticket", new TicketWithBLOBs());

		CommonResponse commonResponse = new CommonResponse();
		SessionUser currentUser = getCurrentUser();
		request.getSession().setAttribute("sessionuser", currentUser);
		
		List<Site> list = new ArrayList<Site>();

		if (currentUser.getUserType().equals(UserType.USER.name())) {
			Set<String> siteIdSet = new HashSet<>();
			list = serviceCenterUserService.getServiceCenters(currentUser.getUserId()).stream()
					.flatMap(sc -> serviceCenterSiteService.getSites(sc.getId()).stream())
					.filter(s -> siteIdSet.add(s.getId())).collect(Collectors.toList());

		} else {

			SiteExample siteExample = new SiteExample();
			siteExample.createCriteria().andStatusEqualTo(Status.ACTIVE.getDbValue().byteValue());
			siteExample.setOrderByClause("CREATED_ON DESC");

			list = siteService.selectByExample(siteExample);

		}
		
		RegionExample regionExample = new RegionExample();
		regionExample.createCriteria().andStatusEqualTo(Status.ACTIVE.getDbValue().byteValue());
		regionExample.setOrderByClause("CREATED_ON DESC");
		
		List<Region> regions = regionService.selectByExample(regionExample);
		
		PrefectureExample prefectureExample = new PrefectureExample();
		prefectureExample.createCriteria().andStatusEqualTo(Status.ACTIVE.getDbValue().byteValue());
		prefectureExample.setOrderByClause("CREATED_ON DESC");
		
		List<Prefecture> prefectures = prefectureService.selectByExample(prefectureExample);
		
		List<ServiceCenter> serviceCenters = new ArrayList<ServiceCenter>();
		
		ServiceCenterExample scExample = new ServiceCenterExample();
		ServiceCenterExample.Criteria scExampleCriteria = scExample.createCriteria();
		if(search.getRegion() != null && !search.getRegion().equals("0")) {
			scExampleCriteria.andStatusEqualTo(Status.ACTIVE.getDbValue().byteValue());
			scExampleCriteria.andRegionEqualTo(search.getRegion());

			serviceCenters = serviceCenterService.selectByExample(scExample);
			final List<ServiceCenter> serviceCenterF = serviceCenters;
			
			if(search.getServiceCenter() != null && !search.getServiceCenter().equals("0")) {
				list = list.stream().filter(site -> site.getServiceCenters().stream().anyMatch(scs-> search.getServiceCenter().equals(scs.getId()))).collect(Collectors.toList());
			}else {
				list = list.stream().filter(site -> site.getServiceCenters().stream().anyMatch(scs-> serviceCenterF.stream().anyMatch(sc->sc.getId().equals(scs.getId())))).collect(Collectors.toList());
			}
		}
		
		model.addAttribute("regions", regions);
		model.addAttribute("prefectures", prefectures);
		model.addAttribute("serviceCenters", serviceCenters );
		model.addAttribute("sites", list);
		return "dashboard";
	}

	/*
	 * @GetMapping("/login") public String login() { return "login"; }
	 * 
	 * @RequestMapping(value = "/userLogin", method = RequestMethod.POST) public
	 * String recoverPass(@RequestParam("j_username") String username) { if
	 * (username.equals("supervisor@gmail.com")) { return "dashboard-supervisor"; }
	 * if (username.equals("user@gmail.com")) { return "dashboard-user"; } else {
	 * return "dashboard"; } }
	 * 
	 * @GetMapping("/dashboard") public String dashboard() { Object principal =
	 * SecurityContextHolder.getContext().getAuthentication().getPrincipal(); String
	 * username = ""; if (principal instanceof UserDetails) { username =
	 * ((UserDetails)principal).getUsername(); } else { username =
	 * principal.toString(); } if(username.equals("Supervisor")) { return
	 * "redirect:/dashboard-supervisor"; } if (username.equals("User")) { return
	 * "redirect:/dashboard-user"; } else { return "dashboard"; } }
	 * 
	 * 
	 */

	// setting-dash

	@GetMapping("/setting")
	public String setting() {
		return "setting/dashboard";
	}

	// ........................

	@GetMapping("/facility")
	public String facility() {
		return "facility";
	}

	@GetMapping("/facility-group")
	public String facilityGroup() {
		return "facility-group";
	}

	@GetMapping("/user-detail")
	public String userDetail() {
		return "user-detail";
	}

	@GetMapping("/company-facility-group")
	public String companyFacilityGroup() {
		return "company-facility-group";
	}

	@GetMapping("/prefecture-facility-group")
	public String prefectureFacilityGroup() {
		return "prefecture-facility-group";
	}

	@GetMapping("/facility-detail")
	public String FacilityDetail() {
		return "facility-detail";
	}

	@GetMapping("/fac-new-group")
	public String facNewGroup() {
		return "fac-new-group";
	}

	@GetMapping("/service-center-old")
	public String serviceCenter() {
		return "service-center";
	}

	@GetMapping("/service-center-view")
	public String serviceCenterView() {
		return "service-center-view";
	}

	@GetMapping("/site-old")
	public String site() {
		return "site";
	}

	@GetMapping("/site-group")
	public String siteGroup() {
		return "site-group";
	}

	@GetMapping("/facility-new")
	public String facilityNew() {
		return "facility-new";
	}

	@GetMapping("/site-view")
	public String siteView() {
		return "site-view";
	}

	@GetMapping("/ticket-old")
	public String ticket() {
		return "ticket";
	}

	@GetMapping("/ticket-view")
	public String ticketView() {
		return "ticket-view";
	}

	// Supervisor controllers

	@GetMapping("/ticket-supervisor")
	public String ticketSupervisor() {
		return "ticket-supervisor";
	}

	@GetMapping("/ticket-supervisor-view")
	public String ticketSupervisorView() {
		return "ticket-supervisor-view";
	}

	@GetMapping("/service-center-supervisor")
	public String serviceCenterSupervisor() {
		return "service-center-supervisor";
	}

	@GetMapping("/site-supervisor")
	public String siteSupervisor() {
		return "supervisor/site";
	}

	@GetMapping("/dashboard-supervisor")
	public String dashboardSupervisor() {
		return "supervisor/dashboard";
	}

	@GetMapping("/admission")
	public String admission() {
		return "supervisor/admission";
	}

	@GetMapping("/power-plant")
	public String powerPlant() {
		return "supervisor/power-plant";
	}

	@GetMapping("/location")
	public String location() {
		return "supervisor/location";
	}

	@GetMapping("/inspection")
	public String inspection() {
		return "supervisor/inspection-report";
	}

	@GetMapping("/monthly-report")
	public String monthlyReport() {
		return "supervisor/monthly-report";
	}

	// ...........

	// user controllers

	@GetMapping("/ticket-user")
	public String ticketUser() {
		return "ticket-user";
	}

	@GetMapping("/ticket-user-view")
	public String ticketUserView() {
		return "ticket-user-view";
	}

	@GetMapping("/service-center-user")
	public String serviceCenterUser() {
		return "service-center-user";
	}

	@GetMapping("/service-center-user-view")
	public String serviceCenterUserView() {
		return "service-center-user-view";
	}

	@GetMapping("/site-user")
	public String siteUser() {
		return "site-user";
	}

	@GetMapping("/dashboard-user")
	public String dashboardUser() {
		return "dashboard-user";
	}

	// ....................

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
