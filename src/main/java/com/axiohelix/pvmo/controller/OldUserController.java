package com.axiohelix.pvmo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.axiohelix.pvmo.entity.User;
import com.axiohelix.pvmo.entity.UserExample;
import com.axiohelix.pvmo.entity.UserWithBLOBs;
import com.axiohelix.pvmo.service.UserService;
import com.axiohelix.pvmo.type.Status;

@Controller
@RequestMapping("/user-old")
public class OldUserController {
	
	@Autowired 
	private UserService userService;

	@GetMapping({"","/"})
	public String list(Model model) {
		UserExample example = new UserExample();
		//example.createCriteria().andStatusEqualTo(Status.ACTIVE.getDbValue().byteValue());
		List<UserWithBLOBs> users = userService.selectByExampleWithBLOBs(example);
		model.addAttribute("users", users);
		return "user";
	}

	@GetMapping("/{id}")
	public String view(Model model, @PathVariable("id") String id) {
		User user = null;// userService.getUser(id);
		model.addAttribute("user", user);
		return "user/view";
	}
	
	@GetMapping("/add")
	public String add(@PathVariable("id") String id, Model model) {
		User user = new User();
		model.addAttribute("user",user);
		return "user/add";
	}
	
	@GetMapping("/edit/{id}")
    public String update(@PathVariable("id") String id, Model model) {
		User user = null; //get user account by id;
        model.addAttribute("user", user);
        return "user/edit";
    }
	
	@PostMapping("/save")
	public String save(@ModelAttribute("user") User user) {
        // save user to database
        return "redirect:/";
    }
	
	@GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") String id) {
        // call delete user method 
        return "redirect:/";
    }

	
}
