package com.axiohelix.pvmo.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.axiohelix.pvmo.entity.Prefecture;
import com.axiohelix.pvmo.entity.PrefectureExample;
import com.axiohelix.pvmo.entity.PrefectureWithBLOBs;
import com.axiohelix.pvmo.entity.Region;
import com.axiohelix.pvmo.entity.RegionExample;
import com.axiohelix.pvmo.service.PrefectureService;
import com.axiohelix.pvmo.type.Status;
import com.axiohelix.pvmo.util.CommonConstant;
import com.axiohelix.pvmo.util.CommonResponse;
import com.github.pagehelper.PageInfo;

@Controller
@RequestMapping(CommonConstant.CONTROLLER_URL_PREFECTURE)

public class PrefectureController {

	private static final Logger LOGGER = LogManager.getLogger(ConsumerController.class);

	@Autowired
	private PrefectureService prefectureService;

	@GetMapping("")
	public String index(
			@RequestParam(value = "pageNum", defaultValue = CommonConstant.DEFAULT_PAGINATION_PAGE) int pageNum,
			@RequestParam(value = "pageSize", defaultValue = CommonConstant.DEFAULT_PAGINATION_SIZE) int pageSize,
			Model model) {
		model.addAttribute("prefecture", new PrefectureWithBLOBs());
		return "setting/region-view";
	}

	@ResponseBody
	@GetMapping("/data/{search}")
	  public CommonResponse index( @PathVariable(value = "search") String search) {
	  
	  CommonResponse commonResponse = new CommonResponse();
	  
	  PrefectureExample prefectureExample = new PrefectureExample();
	  prefectureExample.createCriteria().andStatusEqualTo(Status.ACTIVE.getDbValue().byteValue());
	  prefectureExample.setOrderByClause("CREATED_ON DESC");
	  
	  List<PrefectureWithBLOBs> list =
	  prefectureService.selectByExampleWithBLOBs(prefectureExample);
	  
	  commonResponse.setStatus(true); commonResponse.getPayload().add(list); return
	  commonResponse; 
	  }
	 
	


	private PageInfo<Prefecture> list(int pageNum, int pageSize) {
		PrefectureExample prefectureExample = new PrefectureExample();
		prefectureExample.createCriteria().andStatusEqualTo(Status.ACTIVE.getDbValue().byteValue());
		prefectureExample.setOrderByClause("CREATED_ON DESC");

		PageInfo<Prefecture> page = prefectureService.selectByExampleWithBLOBs(prefectureExample, pageNum, pageSize);
		return page;
	}

	@ResponseBody
	@GetMapping("/{id}/delete")
	public CommonResponse delete(Model model, @PathVariable("id") String id) {

		CommonResponse commonResponse = new CommonResponse();
		PrefectureWithBLOBs prefecture = prefectureService.selectByPrimaryKeyWithBLOBs(id);
		if (prefecture == null) {
			commonResponse.setStatus(false);
			commonResponse.getErrorMessages().add(CommonConstant.MSG_RECORD_NOT_FOUND);
			return commonResponse;
		}
		prefecture.setStatus(Status.DELETED.getDbValue().byteValue());
		prefectureService.updateByPrimaryKeySelective(prefecture);

		commonResponse.setStatus(true);
		return commonResponse;
	}

	@ResponseBody

	@GetMapping("/{id}/data")
	public CommonResponse companyData(@PathVariable("id") String id) {
		Prefecture prefecture = prefectureService.selectByPrimaryKey(id);
		CommonResponse commonResponse = new CommonResponse();
		commonResponse.setStatus(true);
		commonResponse.getPayload().add(prefecture);
		return commonResponse;
	}

	@ResponseBody

	@PostMapping("/saveUpdate")
	public CommonResponse saveUpdate(@ModelAttribute("prefecture") PrefectureWithBLOBs prefecture) {
		return prefectureService.saveUpdate(prefecture);
	}

}
