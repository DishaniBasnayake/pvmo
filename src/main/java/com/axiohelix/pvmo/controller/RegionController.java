package com.axiohelix.pvmo.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.axiohelix.pvmo.entity.CompanyWithBLOBs;
import com.axiohelix.pvmo.entity.Prefecture;
import com.axiohelix.pvmo.entity.PrefectureWithBLOBs;
import com.axiohelix.pvmo.entity.Region;
import com.axiohelix.pvmo.entity.RegionExample;
import com.axiohelix.pvmo.entity.RegionWithBLOBs;
import com.axiohelix.pvmo.entity.ServiceCenterWithBLOBs;
import com.axiohelix.pvmo.entity.Site;
import com.axiohelix.pvmo.entity.SiteWithBLOBs;
import com.axiohelix.pvmo.entity.UserWithBLOBs;
import com.axiohelix.pvmo.service.PrefectureService;
import com.axiohelix.pvmo.service.RegionService;
import com.axiohelix.pvmo.type.Status;
import com.axiohelix.pvmo.util.CommonConstant;
import com.axiohelix.pvmo.util.CommonResponse;
import com.github.pagehelper.PageInfo;

@Controller
@RequestMapping(CommonConstant.CONTROLLER_URL_REGION)

public class RegionController {

	private static final Logger LOGGER = LogManager.getLogger(ConsumerController.class);

	@Autowired
	private RegionService regionService;
	@Autowired
	private PrefectureService prefectureService;


	@GetMapping("")
	public String index(
			@RequestParam(value = "pageNum", defaultValue = CommonConstant.DEFAULT_PAGINATION_PAGE) int pageNum,
			@RequestParam(value = "pageSize", defaultValue = CommonConstant.DEFAULT_PAGINATION_SIZE) int pageSize,
			Model model) {
		model.addAttribute("region", new RegionWithBLOBs());
		return "setting/region-list";
	}

	@ResponseBody
	@GetMapping("/data/{search}")
	public CommonResponse index(@PathVariable(value = "search") String search) {

		CommonResponse commonResponse = new CommonResponse();

		RegionExample regionExample = new RegionExample();
		regionExample.createCriteria().andStatusEqualTo(Status.ACTIVE.getDbValue().byteValue());
		regionExample.setOrderByClause("CREATED_ON DESC");

		List<RegionWithBLOBs> list = regionService.selectByExampleWithBLOBs(regionExample);

		commonResponse.setStatus(true);
		commonResponse.getPayload().add(list);
		return commonResponse;
	}

	private PageInfo<Region> list(int pageNum, int pageSize) {
		RegionExample regionExample = new RegionExample();
		regionExample.createCriteria().andStatusEqualTo(Status.ACTIVE.getDbValue().byteValue());
		regionExample.setOrderByClause("CREATED_ON DESC");

		PageInfo<Region> page = regionService.selectByExampleWithBLOBs(regionExample, pageNum, pageSize);
		return page;
	}

	@GetMapping("/{id}")
	public String view(Model model, @PathVariable("id") String id) {
		RegionWithBLOBs region = regionService.selectByPrimaryKeyWithBLOBs(id);
		model.addAttribute("region", region);
		model.addAttribute("prefecture", new PrefectureWithBLOBs());
		return "setting/region-view";
	}
	
	@ResponseBody

	@GetMapping("/{id}/delete")
	public CommonResponse delete(Model model, @PathVariable("id") String id) {

		CommonResponse commonResponse = new CommonResponse();
		RegionWithBLOBs region = regionService.selectByPrimaryKeyWithBLOBs(id);
		if (region == null) {
			commonResponse.setStatus(false);
			commonResponse.getErrorMessages().add(CommonConstant.MSG_RECORD_NOT_FOUND);
			return commonResponse;
		}
		region.setStatus(Status.DELETED.getDbValue().byteValue());
		regionService.updateByPrimaryKeySelective(region);

		commonResponse.setStatus(true);
		return commonResponse;
	}

	@ResponseBody

	@GetMapping("/{id}/data")
	public CommonResponse companyData(@PathVariable("id") String id) {
		Region region = regionService.selectByPrimaryKey(id);
		CommonResponse commonResponse = new CommonResponse();
		commonResponse.setStatus(true);
		commonResponse.getPayload().add(region);
		return commonResponse;
	}

	@ResponseBody

	@PostMapping("/saveUpdate")
	public CommonResponse saveUpdate(@ModelAttribute("region") RegionWithBLOBs region) {
		return regionService.saveUpdate(region);
	}

}
