package com.axiohelix.pvmo.controller;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.axiohelix.pvmo.entity.Prefecture;
import com.axiohelix.pvmo.entity.PrefectureExample;
import com.axiohelix.pvmo.entity.PrefectureWithBLOBs;
import com.axiohelix.pvmo.service.PrefectureService;
import com.axiohelix.pvmo.service.RegionService;
import com.axiohelix.pvmo.type.Status;
import com.axiohelix.pvmo.util.CommonConstant;
import com.axiohelix.pvmo.util.CommonResponse;
import com.axiohelix.pvmo.util.CommonValidation;

@Controller
@RequestMapping(CommonConstant.CONTROLLER_URL_REGION + "/{rid}/" + CommonConstant.CONTROLLER_URL_PREFECTURE)
public class RegionPrefectureController {

	private static final Logger LOGGER = LogManager.getLogger(RegionPrefectureController.class);
	
	@Autowired
	private RegionService regionService;
	@Autowired
	private PrefectureService prefectureService;

	@ResponseBody
	@GetMapping("/data/{search}")
	public CommonResponse index(@PathVariable("rid") String rid, @PathVariable(value = "search") String search) {

		CommonResponse commonResponse = new CommonResponse();

		PrefectureExample example = new PrefectureExample();
		example.createCriteria().andStatusEqualTo(Status.ACTIVE.getDbValue().byteValue()).andRegionIdEqualTo(rid);
		example.setOrderByClause("CREATED_ON DESC");

		List<PrefectureWithBLOBs> list = prefectureService.selectByExampleWithBLOBs(example);

		commonResponse.setStatus(true);
		commonResponse.getPayload().add(list);
		return commonResponse;
	}

}
