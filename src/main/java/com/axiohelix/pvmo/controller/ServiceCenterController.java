package com.axiohelix.pvmo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.axiohelix.pvmo.entity.CompanyExample;
import com.axiohelix.pvmo.entity.CompanyWithBLOBs;
import com.axiohelix.pvmo.entity.Region;
import com.axiohelix.pvmo.entity.RegionExample;
import com.axiohelix.pvmo.entity.ServiceCenter;
import com.axiohelix.pvmo.entity.ServiceCenterExample;
import com.axiohelix.pvmo.entity.ServiceCenterWithBLOBs;
import com.axiohelix.pvmo.entity.SiteWithBLOBs;
import com.axiohelix.pvmo.entity.UserWithBLOBs;
import com.axiohelix.pvmo.model.SessionUser;
import com.axiohelix.pvmo.service.RegionService;
import com.axiohelix.pvmo.service.ServiceCenterService;
import com.axiohelix.pvmo.service.ServiceCenterUserService;
import com.axiohelix.pvmo.type.Status;
import com.axiohelix.pvmo.type.UserType;
import com.axiohelix.pvmo.util.CommonConstant;
import com.axiohelix.pvmo.util.CommonFunctions;
import com.axiohelix.pvmo.util.CommonResponse;
import com.axiohelix.pvmo.util.CommonValidation;

@Controller
@RequestMapping(CommonConstant.CONTROLLER_URL_SERVICE_CENTER)
public class ServiceCenterController extends AbstractController {

	private static final Logger LOGGER = LogManager.getLogger(ServiceCenterController.class);

	@Autowired
	private ServiceCenterService serviceCenterService;
	@Autowired
	private ServiceCenterUserService serviceCenterUserService;
	@Autowired
	private RegionService regionService;

	@GetMapping("")
	public String index(Model model) {

		model.addAttribute("serviceCenter", new ServiceCenterWithBLOBs());
		return "service-center/list";
	}

	@ResponseBody
	@GetMapping("/data/{search}")
	public CommonResponse index(@PathVariable(value = "search") String search) {

		CommonResponse commonResponse = new CommonResponse();		
		SessionUser currentUser = getCurrentUser();
		List<ServiceCenterWithBLOBs> list = new ArrayList<ServiceCenterWithBLOBs>();
		
		if(currentUser.getUserType().equals(UserType.USER.name())) {
			list = serviceCenterUserService.getServiceCenters(currentUser.getUserId());
		}else {
		
			ServiceCenterExample serviceCenterExample = new ServiceCenterExample();
			serviceCenterExample.createCriteria().andStatusEqualTo(Status.ACTIVE.getDbValue().byteValue());		
			serviceCenterExample.setOrderByClause("CREATED_ON DESC");
			
			list = serviceCenterService.selectByExampleWithBLOBs(serviceCenterExample);
		}

		commonResponse.setStatus(true);
		commonResponse.getPayload().add(list);
		return commonResponse;
	}

	@GetMapping("/{id}")
	public String view(Model model, @PathVariable("id") String id) {
		ServiceCenterWithBLOBs serviceCenter = serviceCenterService.selectByPrimaryKeyWithBLOBs(id);
		model.addAttribute("serviceCenter", serviceCenter);
		model.addAttribute("site", new SiteWithBLOBs());
		model.addAttribute("user", new UserWithBLOBs());
		return "service-center/view";
	}

	@ResponseBody
	@GetMapping("/{id}/delete")
	public CommonResponse delete(Model model, @PathVariable("id") String id) {

		CommonResponse commonResponse = new CommonResponse();
		ServiceCenterWithBLOBs serviceCenter = serviceCenterService.selectByPrimaryKeyWithBLOBs(id);
		if (serviceCenter == null) {
			commonResponse.setStatus(false);
			commonResponse.getErrorMessages().add(CommonConstant.MSG_RECORD_NOT_FOUND);
			return commonResponse;
		}
		serviceCenter.setStatus(Status.DELETED.getDbValue().byteValue());
		serviceCenterService.updateByPrimaryKeySelective(serviceCenter);

		commonResponse.setStatus(true);
		return commonResponse;
	}

	@ResponseBody
	@GetMapping("/{id}/data")
	public CommonResponse serviceCenterData(@PathVariable("id") String id) {
		ServiceCenter serviceCenter = serviceCenterService.selectByPrimaryKey(id);
		
		CommonResponse commonResponse = new CommonResponse();
		commonResponse.setStatus(true);
		commonResponse.getPayload().add(serviceCenter);
		
		return commonResponse;
	}

	@ResponseBody
	@PostMapping("/saveUpdate")
	public CommonResponse saveUpdate(@ModelAttribute("serviceCenter") ServiceCenterWithBLOBs serviceCenter) {

		ServiceCenterExample example = new ServiceCenterExample();
		Optional<ServiceCenter> serviceCenterEx = null;

		if (CommonValidation.stringNullValidation(serviceCenter.getId())) {
			example.createCriteria().andCompanyIdEqualTo(serviceCenter.getCompanyId())
					.andCodeEqualTo(serviceCenter.getCode());
			serviceCenterEx = serviceCenterService.selectByExample(example).stream().findFirst();
		} else {
			ServiceCenter serviceCenterPr = serviceCenterService.selectByPrimaryKey(serviceCenter.getId());
			example.createCriteria().andCompanyIdEqualTo(serviceCenterPr.getCompanyId())
					.andCodeEqualTo(serviceCenter.getCode()).andCodeNotEqualTo(serviceCenterPr.getCode());
			serviceCenterEx = serviceCenterService.selectByExample(example).stream().findFirst();
		}

		if (serviceCenterEx.isPresent()) {
			CommonResponse response = new CommonResponse();
			response.getErrorMessages().add(CommonConstant.MSG_SERVICE_CENTER_CODE + " "
					+ serviceCenterEx.get().getCode() + " " + CommonConstant.MSG_IS_ALREADY_EXISTS);
			return response;
		}

		return serviceCenterService.saveUpdate(serviceCenter);
	}

}
