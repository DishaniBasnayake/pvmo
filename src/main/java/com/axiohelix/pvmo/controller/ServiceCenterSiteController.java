package com.axiohelix.pvmo.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.axiohelix.pvmo.entity.ServiceCenter;
import com.axiohelix.pvmo.entity.Site;
import com.axiohelix.pvmo.entity.SiteWithBLOBs;
import com.axiohelix.pvmo.model.AssignedSiteDTO;
import com.axiohelix.pvmo.model.Search;
import com.axiohelix.pvmo.service.ServiceCenterService;
import com.axiohelix.pvmo.service.ServiceCenterSiteService;
import com.axiohelix.pvmo.service.SiteService;
import com.axiohelix.pvmo.util.CommonConstant;
import com.axiohelix.pvmo.util.CommonResponse;
import com.axiohelix.pvmo.util.Snowflake;

@Controller
@RequestMapping(CommonConstant.CONTROLLER_URL_SERVICE_CENTER + "/{scid}/" + CommonConstant.CONTROLLER_URL_SITE)
public class ServiceCenterSiteController {

	private static final Logger LOGGER = LogManager.getLogger(ServiceCenterSiteController.class);
	public static final String NOT_ASSIGNED = "not-assigned";

	@Autowired
	private SiteService siteService;
	@Autowired
	private ServiceCenterService serviceCenterService;
	@Autowired
	private ServiceCenterSiteService serviceCenterSiteService;

	@ResponseBody
	@GetMapping("/data/{region}/{serviceCenter}")
	public CommonResponse index(@PathVariable("scid") String scid, Search search) {

		CommonResponse commonResponse = new CommonResponse();

		List<SiteWithBLOBs> list = serviceCenterSiteService.getSites(scid);

		commonResponse.setStatus(true);
		commonResponse.getPayload().add(list);
		return commonResponse;
	}

	@ResponseBody
	@GetMapping("/data/not-assigned")
	public CommonResponse getNotAssignedSites(@PathVariable("scid") String scid) {

		CommonResponse commonResponse = new CommonResponse();
		ServiceCenter sc = serviceCenterService.selectByPrimaryKey(scid); 

		List<Site> list = serviceCenterSiteService.getNotAssignedSites(sc.getCompanyId(),scid);

		commonResponse.setStatus(true);
		commonResponse.getPayload().add(list);
		return commonResponse;
	}

	@ResponseBody
	@PostMapping("/saveUpdate")
	public CommonResponse saveUpdate(@PathVariable("scid") String scid, @RequestBody List<AssignedSiteDTO> sites) {

		CommonResponse commonResponse = new CommonResponse();

		for (AssignedSiteDTO assignedSiteDTO : sites) {
			serviceCenterSiteService.addServiceCenterSite(String.valueOf(Snowflake.newId()), scid,
					assignedSiteDTO.getId());
		}

		commonResponse.setStatus(true);
		return commonResponse;
	}
	
	@ResponseBody
	@PostMapping("/{id}/unassign")
	public CommonResponse unassign(@PathVariable("scid") String scid, @PathVariable("id") String id ) {

		CommonResponse commonResponse = new CommonResponse();

		serviceCenterSiteService.deleteServiceCenterSite(scid,id);

		commonResponse.setStatus(true);
		return commonResponse;
	}

}
