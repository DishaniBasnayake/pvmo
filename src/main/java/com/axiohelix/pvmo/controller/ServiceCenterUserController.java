
package com.axiohelix.pvmo.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.axiohelix.pvmo.entity.ServiceCenter;
import com.axiohelix.pvmo.entity.User;
import com.axiohelix.pvmo.entity.UserWithBLOBs;
import com.axiohelix.pvmo.model.AssignedUserDTO;
import com.axiohelix.pvmo.service.ServiceCenterService;
import com.axiohelix.pvmo.service.ServiceCenterUserService;
import com.axiohelix.pvmo.type.UserType;
import com.axiohelix.pvmo.util.CommonConstant;
import com.axiohelix.pvmo.util.CommonResponse;
import com.axiohelix.pvmo.util.Snowflake;

@Controller
@RequestMapping(CommonConstant.CONTROLLER_URL_SERVICE_CENTER + "/{scid}/" + CommonConstant.CONTROLLER_URL_USER)
public class ServiceCenterUserController {

	private static final Logger LOGGER = LogManager.getLogger(ServiceCenterUserController.class);

	@Autowired
	private ServiceCenterService serviceCenterService;
	@Autowired
	private ServiceCenterUserService serviceCenterUserService;

	@ResponseBody
	@GetMapping("/data/{search}")
	public CommonResponse index(@PathVariable("scid") String scid, @PathVariable(value = "search") String search) {

		CommonResponse commonResponse = new CommonResponse();

		List<UserWithBLOBs> list = serviceCenterUserService.getUsers(scid);

		commonResponse.setStatus(true);
		commonResponse.getPayload().add(list);
		return commonResponse;
	}

	@ResponseBody
	@GetMapping("/data/not-assigned")
	public CommonResponse getNotAssignedUsers(@PathVariable("scid") String scid) {

		CommonResponse commonResponse = new CommonResponse();
		ServiceCenter sc = serviceCenterService.selectByPrimaryKey(scid);

		List<UserWithBLOBs> list = serviceCenterUserService.getNotAssignedUsers(sc.getCompanyId(), scid);

		commonResponse.setStatus(true);
		commonResponse.getPayload().add(list);
		return commonResponse;
	}

	@ResponseBody
	@PostMapping("/saveUpdate/{type}")
	public CommonResponse saveUpdate(@PathVariable("scid") String scid, @PathVariable("type") String type,
			@RequestBody List<AssignedUserDTO> users) {

		CommonResponse commonResponse = new CommonResponse();
		String userType;
		if (type.equals(UserType.MANAGER.getDtoValue())) {
			userType = UserType.MANAGER.getDbValue();
		} else if (type.equals(UserType.SUPERVISOR.getDtoValue())) {
			userType = UserType.SUPERVISOR.getDbValue();
		} else {
			userType = UserType.EMPLOYEE.getDbValue();
		}

		for (AssignedUserDTO assignedUserDTO : users) {
			serviceCenterUserService.addServiceCenterUser(String.valueOf(Snowflake.newId()), scid,
					assignedUserDTO.getId(), userType);
		}

		commonResponse.setStatus(true);
		return commonResponse;
	}

	@ResponseBody
	@PostMapping("/{id}/unassign")
	public CommonResponse unassign(@PathVariable("scid") String scid, @PathVariable("id") String id) {

		CommonResponse commonResponse = new CommonResponse();

		serviceCenterUserService.deleteServiceCenterUser(scid, id);

		commonResponse.setStatus(true);
		return commonResponse;
	}

}
