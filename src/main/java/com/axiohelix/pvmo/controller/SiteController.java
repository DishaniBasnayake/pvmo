package com.axiohelix.pvmo.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.file.DirectoryStream;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.axiohelix.pvmo.entity.CompanyWithBLOBs;
import com.axiohelix.pvmo.entity.PCS;
import com.axiohelix.pvmo.entity.Panel;
import com.axiohelix.pvmo.entity.Site;
import com.axiohelix.pvmo.entity.SiteClientExample;
import com.axiohelix.pvmo.entity.SiteClientWithBLOBs;
import com.axiohelix.pvmo.entity.SiteConstructionCompanyExample;
import com.axiohelix.pvmo.entity.SiteConstructionCompanyWithBLOBs;
import com.axiohelix.pvmo.entity.SiteContractInformationExample;
import com.axiohelix.pvmo.entity.SiteContractInformationWithBLOBs;
import com.axiohelix.pvmo.entity.SiteDocument;
import com.axiohelix.pvmo.entity.SiteDocumentWithBLOBs;
import com.axiohelix.pvmo.entity.SiteEmergencyContactExample;
import com.axiohelix.pvmo.entity.SiteEmergencyContactWithBLOBs;
import com.axiohelix.pvmo.entity.SiteExample;
import com.axiohelix.pvmo.entity.SiteMonitoringStructureExample;
import com.axiohelix.pvmo.entity.SiteMonitoringStructureWithBLOBs;
import com.axiohelix.pvmo.entity.SitePersonInChargeExample;
import com.axiohelix.pvmo.entity.SitePersonInChargeWithBLOBs;
import com.axiohelix.pvmo.entity.SitePowerGenerationCapacityExample;
import com.axiohelix.pvmo.entity.SitePowerGenerationCapacityWithBLOBs;
import com.axiohelix.pvmo.entity.SitePowerGenerationStatus;
import com.axiohelix.pvmo.entity.SitePowerGenerationStatusExample;
import com.axiohelix.pvmo.entity.SitePowerGenerationStatusWithBLOBs;
import com.axiohelix.pvmo.entity.SitePowerSupplySystemExample;
import com.axiohelix.pvmo.entity.SitePowerSupplySystemWithBLOBs;
import com.axiohelix.pvmo.entity.SiteStatusExample;
import com.axiohelix.pvmo.entity.SiteStatusWithBLOBs;
import com.axiohelix.pvmo.entity.SiteUrgentResponseExample;
import com.axiohelix.pvmo.entity.SiteUrgentResponseWithBLOBs;
import com.axiohelix.pvmo.entity.SiteUserFixpointshooting;
import com.axiohelix.pvmo.entity.SiteUserFixpointshootingExample;
import com.axiohelix.pvmo.entity.SiteWithBLOBs;
import com.axiohelix.pvmo.entity.TemplateExample;
import com.axiohelix.pvmo.entity.TemplateWithBLOBs;
import com.axiohelix.pvmo.entity.Ticket;
import com.axiohelix.pvmo.entity.TicketExample;
import com.axiohelix.pvmo.entity.TicketWithBLOBs;
import com.axiohelix.pvmo.entity.User;
import com.axiohelix.pvmo.file.exception.FileStorageException;
import com.axiohelix.pvmo.model.DeviceCSV;
import com.axiohelix.pvmo.model.DirectoryItem;
import com.axiohelix.pvmo.model.Search;
import com.axiohelix.pvmo.model.SessionUser;
import com.axiohelix.pvmo.service.DeviceService;
import com.axiohelix.pvmo.service.ServiceCenterSiteService;
import com.axiohelix.pvmo.service.ServiceCenterUserService;
import com.axiohelix.pvmo.service.SiteClientService;
import com.axiohelix.pvmo.service.SiteConstructionCompanyService;
import com.axiohelix.pvmo.service.SiteContractInformationService;
import com.axiohelix.pvmo.service.SiteDocumentService;
import com.axiohelix.pvmo.service.SiteEmergencyContactService;
import com.axiohelix.pvmo.service.SiteMonitoringStructureService;
import com.axiohelix.pvmo.service.SitePersonInChargeService;
import com.axiohelix.pvmo.service.SitePowerGenerationCapacityService;
import com.axiohelix.pvmo.service.SitePowerGenerationStatusService;
import com.axiohelix.pvmo.service.SitePowerSupplySystemService;
import com.axiohelix.pvmo.service.SiteService;
import com.axiohelix.pvmo.service.SiteStatusService;
import com.axiohelix.pvmo.service.SiteUrgentResponseService;
import com.axiohelix.pvmo.service.SiteUserFixpointshootingService;
import com.axiohelix.pvmo.service.TemplateService;
import com.axiohelix.pvmo.service.TicketService;
import com.axiohelix.pvmo.service.TicketUserService;
import com.axiohelix.pvmo.type.DocumentConfidentialLevelType;
import com.axiohelix.pvmo.type.Status;
import com.axiohelix.pvmo.type.TicketType;
import com.axiohelix.pvmo.type.UserType;
import com.axiohelix.pvmo.util.CommonConstant;
import com.axiohelix.pvmo.util.CommonResponse;
import com.axiohelix.pvmo.util.CommonValidation;
import com.axiohelix.pvmo.util.DirectoryItemType;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;

@Controller
@RequestMapping({ CommonConstant.CONTROLLER_URL_SITE, CommonConstant.API_CONTROLLER_URL_SITE })
public class SiteController extends AbstractController {

	private static final Logger LOGGER = LogManager.getLogger(SiteController.class);

	@Autowired
	private SiteService siteService;
	@Autowired
	private SiteStatusService siteStatusService;
	@Autowired
	private SitePowerGenerationStatusService sitePowerGenerationStatusService;
	@Autowired
	private SiteClientService siteClientService;
	@Autowired
	private SitePowerGenerationCapacityService sitePowerGenerationCapacityService;
	@Autowired
	private SiteConstructionCompanyService siteConstructionCompanyService;
	@Autowired
	private SitePowerSupplySystemService sitePowerSupplySystemService;
	@Autowired
	private SiteContractInformationService siteContractInformationService;
	@Autowired
	private SiteUrgentResponseService siteUrgentResponseService;
	@Autowired
	private SiteMonitoringStructureService siteMonitoringStructureService;
	@Autowired
	private SiteEmergencyContactService siteEmergencyContactService;
	@Autowired
	private SitePersonInChargeService sitePersonInChargeService;
	@Autowired
	private ServiceCenterUserService serviceCenterUserService;
	@Autowired
	private ServiceCenterSiteService serviceCenterSiteService;
	@Autowired
	private DeviceService deviceService;
	
	@Autowired
	private TemplateService templateService;
	@Autowired
	private TicketService ticketService;
	@Autowired
	private TicketUserService ticketUserService;
	@Autowired
	private SiteUserFixpointshootingService siteUserFixpointshootingService;
	@Autowired
	private SiteDocumentService siteDocumentService;

	private List<CompanyWithBLOBs> comlist;
	
	@Value("${site.document.path}")
	private String documentPath;
	
	@GetMapping("")
	public String index(Model model) {
		model.addAttribute("site", new SiteWithBLOBs());
		return "site/list";
	}
	
	@ResponseBody
	@GetMapping("/data/{region}/{serviceCenter}")
	public CommonResponse index(Search search) {

		CommonResponse commonResponse = new CommonResponse();
		SessionUser currentUser = getCurrentUser();
		List<Site> list = new ArrayList<Site>();

		if (currentUser.getUserType().equals(UserType.USER.name())) {
			Set<String> siteIdSet = new HashSet<>();
			if(search.getRegion() != null && !search.getRegion().equals("0")) {
				list = serviceCenterUserService.getServiceCenters(currentUser.getUserId()).stream()
						.filter(sc->sc.getRegion().equals(search.getRegion()))
						.flatMap(sc -> serviceCenterSiteService.getSites(sc.getId()).stream())
						.filter(s -> siteIdSet.add(s.getId())).collect(Collectors.toList());

				if(search.getServiceCenter() != null && !search.getServiceCenter().equals("0"))
					list = list.stream().filter(site->site.getServiceCenters().stream()
							.anyMatch(sc->sc.getId().equals(search.getServiceCenter()))).collect(Collectors.toList());
			}else {
				list = serviceCenterUserService.getServiceCenters(currentUser.getUserId()).stream()
						.flatMap(sc -> serviceCenterSiteService.getSites(sc.getId()).stream())
						.filter(s -> siteIdSet.add(s.getId())).collect(Collectors.toList());
			}

		} else {

			SiteExample siteExample = new SiteExample();
			SiteExample.Criteria siteExampleCriteria = siteExample.createCriteria();
			
			siteExampleCriteria.andStatusEqualTo(Status.ACTIVE.getDbValue().byteValue());
			if(search.getRegion() != null && !search.getRegion().equals("0"))
				siteExampleCriteria.andRegionEqualTo(search.getRegion());
			siteExample.setOrderByClause("CREATED_ON DESC");
			
			list = siteService.selectByExample(siteExample);
			if(search.getServiceCenter() != null && !search.getServiceCenter().equals("0"))
				list = list.stream().filter(site->site.getServiceCenters().stream()
						.anyMatch(sc->sc.getId().equals(search.getServiceCenter()))).collect(Collectors.toList());

		}

		commonResponse.setStatus(true);
		commonResponse.getPayload().add(list);
		return commonResponse;
	}
	
	@ResponseBody
	@GetMapping("/bulk-data/{search}")
	public CommonResponse bulkData(@PathVariable(value = "search") String search) {

		CommonResponse commonResponse = new CommonResponse();
		SessionUser currentUser = getCurrentUser();
		List<Site> list = new ArrayList<Site>();

		if (currentUser.getUserType().equals(UserType.USER.name())) {
			Set<String> siteIdSet = new HashSet<>();
			list = serviceCenterUserService.getServiceCenters(currentUser.getUserId()).stream()
					.flatMap(sc -> serviceCenterSiteService.getSites(sc.getId()).stream())
					.filter(s -> siteIdSet.add(s.getId())).collect(Collectors.toList());
			
			list.stream().forEach(s -> {
				Set<String> userIdSet = new HashSet<>();
				List<User> companions = s.getServiceCenters().stream()
						.flatMap(sc -> serviceCenterUserService.getUsers(sc.getId()).stream().filter(u-> !u.getId().equals(currentUser.getUserId())))
						.filter(u -> userIdSet.add(u.getId())).collect(Collectors.toList());
				s.setCompanions(companions);
				
				TicketExample ticketExample = new TicketExample();
				ticketExample.createCriteria().andSiteIdEqualTo(s.getId());
				ticketExample.createCriteria().andStatusEqualTo(Status.ACTIVE.getDbValue().byteValue());
				ticketExample.setOrderByClause("CREATED_ON DESC, TICKET_ID DESC");
				
				List<Ticket> tickets = ticketService.selectByExample(ticketExample);
				tickets = tickets.stream().filter(t-> ticketUserService.getUsers(t.getId()).stream()
						.map(tu->tu.getId()).collect(Collectors.toList())
						.contains(currentUser.getUserId())).collect(Collectors.toList());
				
				s.setTickets(tickets);
				
			});

		} else {

			SiteExample siteExample = new SiteExample();
			siteExample.createCriteria().andStatusEqualTo(Status.ACTIVE.getDbValue().byteValue());
			siteExample.setOrderByClause("CREATED_ON DESC");

			list = siteService.selectByExample(siteExample);
				 
			list.stream().forEach(s -> {
				s.setCompanions(s.getServiceCenters().stream().flatMap(sc -> serviceCenterUserService.getUsers(sc.getId()).stream())
				.collect(Collectors.toList()));				

				TicketExample ticketExample = new TicketExample();
				ticketExample.createCriteria().andSiteIdEqualTo(s.getId());
				ticketExample.createCriteria().andStatusEqualTo(Status.ACTIVE.getDbValue().byteValue());
				ticketExample.setOrderByClause("CREATED_ON DESC, TICKET_ID DESC");
				
				s.setTickets(ticketService.selectByExample(ticketExample));
			});

		}

		commonResponse.setStatus(true);
		commonResponse.getPayload().add(list);
		return commonResponse;
	}

	@GetMapping("/{id}")
	public String view(Model model, @PathVariable("id") String id) {
		SiteWithBLOBs site = siteService.selectByPrimaryKeyWithBLOBs(id);

		SiteStatusExample siteStatusExample = new SiteStatusExample();
		siteStatusExample.createCriteria().andSiteIdEqualTo(id);
		SiteStatusWithBLOBs siteStatus = siteStatusService.selectByExampleWithBLOBs(siteStatusExample).stream()
				.findFirst().orElse(new SiteStatusWithBLOBs());

		SitePowerGenerationStatusExample sitePowerGenerationStatusExample = new SitePowerGenerationStatusExample();
		sitePowerGenerationStatusExample.createCriteria().andSiteIdEqualTo(id);
		SitePowerGenerationStatus sitePowerGenerationStatus = sitePowerGenerationStatusService
				.selectByExample(sitePowerGenerationStatusExample).stream().findFirst()
				.orElse(new SitePowerGenerationStatus());

		SiteClientExample siteClientExample = new SiteClientExample();
		siteClientExample.createCriteria().andSiteIdEqualTo(id);
		SiteClientWithBLOBs siteClient = siteClientService.selectByExampleWithBLOBs(siteClientExample).stream()
				.findFirst().orElse(new SiteClientWithBLOBs());

		SitePowerGenerationCapacityExample sitePowerGenerationCapacityExample = new SitePowerGenerationCapacityExample();
		sitePowerGenerationCapacityExample.createCriteria().andSiteIdEqualTo(id);
		SitePowerGenerationCapacityWithBLOBs sitePowerGenerationCapacity = sitePowerGenerationCapacityService
				.selectByExampleWithBLOBs(sitePowerGenerationCapacityExample).stream().findFirst()
				.orElse(new SitePowerGenerationCapacityWithBLOBs());

		SiteConstructionCompanyExample siteConstructionCompanyExample = new SiteConstructionCompanyExample();
		siteConstructionCompanyExample.createCriteria().andSiteIdEqualTo(id);
		SiteConstructionCompanyWithBLOBs siteConstructionCompany = siteConstructionCompanyService
				.selectByExampleWithBLOBs(siteConstructionCompanyExample).stream().findFirst()
				.orElse(new SiteConstructionCompanyWithBLOBs());

		SitePowerSupplySystemExample sitePowerSupplySystemExample = new SitePowerSupplySystemExample();
		sitePowerSupplySystemExample.createCriteria().andSiteIdEqualTo(id);
		SitePowerSupplySystemWithBLOBs sitePowerSupplySystem = sitePowerSupplySystemService
				.selectByExampleWithBLOBs(sitePowerSupplySystemExample).stream().findFirst()
				.orElse(new SitePowerSupplySystemWithBLOBs());

		SiteContractInformationExample siteContractInformationExample = new SiteContractInformationExample();
		siteContractInformationExample.createCriteria().andSiteIdEqualTo(id);
		SiteContractInformationWithBLOBs siteContractInformation = siteContractInformationService
				.selectByExampleWithBLOBs(siteContractInformationExample).stream().findFirst()
				.orElse(new SiteContractInformationWithBLOBs());

		SiteUrgentResponseExample siteUrgentResponseExample = new SiteUrgentResponseExample();
		siteUrgentResponseExample.createCriteria().andSiteIdEqualTo(id);
		SiteUrgentResponseWithBLOBs siteUrgentResponse = siteUrgentResponseService
				.selectByExampleWithBLOBs(siteUrgentResponseExample).stream().findFirst()
				.orElse(new SiteUrgentResponseWithBLOBs());

		SiteMonitoringStructureExample siteMonitoringStructureExample = new SiteMonitoringStructureExample();
		siteMonitoringStructureExample.createCriteria().andSiteIdEqualTo(id);
		SiteMonitoringStructureWithBLOBs siteMonitoringStructure = siteMonitoringStructureService
				.selectByExampleWithBLOBs(siteMonitoringStructureExample).stream().findFirst()
				.orElse(new SiteMonitoringStructureWithBLOBs());

		SiteEmergencyContactExample siteEmergencyContactExample = new SiteEmergencyContactExample();
		siteEmergencyContactExample.createCriteria().andSiteIdEqualTo(id);
		SiteEmergencyContactWithBLOBs siteEmergencyContact = siteEmergencyContactService
				.selectByExampleWithBLOBs(siteEmergencyContactExample).stream().findFirst()
				.orElse(new SiteEmergencyContactWithBLOBs());

		SitePersonInChargeExample sitePersonInChargeExample = new SitePersonInChargeExample();
		sitePersonInChargeExample.createCriteria().andSiteIdEqualTo(id);
		SitePersonInChargeWithBLOBs sitePersonInCharge = sitePersonInChargeService
				.selectByExampleWithBLOBs(sitePersonInChargeExample).stream().findFirst()
				.orElse(new SitePersonInChargeWithBLOBs());
		
		/** temporary code: Chamilka **/
		String dir = documentPath+"\\"+site.getCode();
		
		if (!Files.exists(Paths.get(dir), LinkOption.NOFOLLOW_LINKS)) {
		    try {
				Files.createDirectory(Paths.get(dir));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		List<DirectoryItem> items = new ArrayList<DirectoryItem>();
	    try (DirectoryStream<Path> stream = Files.newDirectoryStream(Paths.get(dir))) {
	        for (Path path : stream) {
	            if (Files.isDirectory(path)) {
	            	items.add(new DirectoryItem(path.getFileName().toString(),Files.getLastModifiedTime(path).toMillis(),Files.getOwner(path).getName().toString(),Files.size(path),DirectoryItemType.DIRECTORY,path));
	            }
	            else {
	            	items.add(new DirectoryItem(path.getFileName().toString(),Files.getLastModifiedTime(path).toMillis(),Files.getOwner(path).getName().toString(),Files.size(path),DirectoryItemType.FILE,path));
	            }
	        }
	    } catch (IOException e) {
			e.printStackTrace();
		}
	    /** end temporary code: chamilka **/

		model.addAttribute("site", site);
		model.addAttribute("siteStatus", siteStatus);
		model.addAttribute("sitePowerGenerationStatus", sitePowerGenerationStatus);
		model.addAttribute("siteClient", siteClient);
		model.addAttribute("sitePowerGenerationCapacity", sitePowerGenerationCapacity);
		model.addAttribute("siteConstructionCompany", siteConstructionCompany);
		model.addAttribute("sitePowerSupplySystem", sitePowerSupplySystem);
		model.addAttribute("siteContractInformation", siteContractInformation);
		model.addAttribute("siteUrgentResponse", siteUrgentResponse);
		model.addAttribute("siteMonitoringStructure", siteMonitoringStructure);
		model.addAttribute("sitePowerGenerationStatus", sitePowerGenerationStatus);
		model.addAttribute("siteEmergencyContact", siteEmergencyContact);
		model.addAttribute("sitePersonInCharge", sitePersonInCharge);
		model.addAttribute("siteDocuments", items); //temporary code: chamilka

		/*
		 * CompanyExample companyExample = new CompanyExample();
		 * companyExample.createCriteria().andStatusEqualTo(Status.ACTIVE.getDbValue().
		 * byteValue()); companyExample.setOrderByClause("CREATED_ON DESC");
		 * comlist= companyService.selectByExampleWithBLOBs(companyExample);
		 */

		model.addAttribute("ticket", new TicketWithBLOBs());

		return "site/view";
	}

	@ResponseBody
	@GetMapping("/{id}/delete")
	public CommonResponse delete(Model model, @PathVariable("id") String id) {

		CommonResponse commonResponse = new CommonResponse();
		SiteWithBLOBs site = siteService.selectByPrimaryKeyWithBLOBs(id);
		if (site == null) {
			commonResponse.setStatus(false);
			commonResponse.getErrorMessages().add(CommonConstant.MSG_RECORD_NOT_FOUND);
			return commonResponse;
		}
		site.setStatus(Status.DELETED.getDbValue().byteValue());
		siteService.updateByPrimaryKeySelective(site);

		commonResponse.setStatus(true);
		return commonResponse;
	}

	@ResponseBody
	@GetMapping("/{id}/data")
	public CommonResponse siteData(@PathVariable("id") String id) {
		Site site = siteService.selectByPrimaryKey(id);
		CommonResponse commonResponse = new CommonResponse();

		TemplateExample templateExample = new TemplateExample();
		templateExample.createCriteria().andStatusEqualTo(Status.ACTIVE.getDbValue().byteValue());
		templateExample.setOrderByClause("CREATED_ON DESC");

		List<TemplateWithBLOBs> templates = templateService.selectByExampleWithBLOBs(templateExample);
		List<User> users = site.getServiceCenters().stream()
				.flatMap(sc -> serviceCenterUserService.getUsers(sc.getId()).stream()).collect(Collectors.toList());		
		List<String> ticketTypes = EnumSet.allOf(TicketType.class).stream().map(tt-> tt.getDbValue()).collect(Collectors.toList()); 

		commonResponse.setStatus(true);
		commonResponse.getPayload().add(site);
		commonResponse.getPayload().add(templates);
		commonResponse.getPayload().add(users);
		commonResponse.getPayload().add(ticketTypes);
		return commonResponse;
	}

	@ResponseBody
	@GetMapping("/{id}/ticket/data")
	public CommonResponse siteTicketData(@PathVariable("id") String id) {
		CommonResponse commonResponse = new CommonResponse();

		TicketExample ticketExample = new TicketExample();
		ticketExample.createCriteria().andSiteIdEqualTo(id);
		ticketExample.createCriteria().andStatusEqualTo(Status.ACTIVE.getDbValue().byteValue());
		ticketExample.setOrderByClause("CREATED_ON DESC, TICKET_ID DESC");

		List<TicketWithBLOBs> tickets = ticketService.selectByExampleWithBLOBs(ticketExample);

		commonResponse.setStatus(true);
		commonResponse.getPayload().add(tickets);
		return commonResponse;
	}
	
	@ResponseBody
	@GetMapping("/{id}/gallery/data")
	public CommonResponse siteGalleryData(@PathVariable("id") String id) {
		CommonResponse commonResponse = new CommonResponse();

		SiteUserFixpointshootingExample siteUserFixpointshootingExample = new SiteUserFixpointshootingExample();
		siteUserFixpointshootingExample.createCriteria().andSiteIdEqualTo(id).andLocationImageNotEqualTo("").andStatusEqualTo(Status.ACTIVE.getDbValue().byteValue());
		siteUserFixpointshootingExample.setOrderByClause("CREATED_ON DESC");

		final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		List<SiteUserFixpointshooting> siteUserFixpointshootings = siteUserFixpointshootingService.selectByExample(siteUserFixpointshootingExample);
		Map<String, List<SiteUserFixpointshooting>> siteUserFixpointshootingsbyDay = siteUserFixpointshootings.stream()
		        .collect(Collectors.groupingBy(d -> formatter.format(d.getCreatedOn())));

		TreeMap<String,List<SiteUserFixpointshooting>> siteUserFixporintshootingsbyDaySorted = new TreeMap<String, List<SiteUserFixpointshooting>>(Collections.reverseOrder());
		siteUserFixporintshootingsbyDaySorted.putAll(siteUserFixpointshootingsbyDay);
		
		commonResponse.setStatus(true);
		commonResponse.getPayload().add(siteUserFixporintshootingsbyDaySorted);
		return commonResponse;
	}

	@ResponseBody
	@PostMapping("/saveUpdate")
	public CommonResponse saveUpdate(@ModelAttribute("site") SiteWithBLOBs site) {

		SiteExample example = new SiteExample();
		Optional<Site> siteEx = null;

		if (CommonValidation.stringNullValidation(site.getId())) {
			example.createCriteria().andCompanyIdEqualTo(site.getCompanyId()).andCodeEqualTo(site.getCode());
			siteEx = siteService.selectByExample(example).stream().findFirst();
		} else {
			Site sitePr = siteService.selectByPrimaryKey(site.getId());
			example.createCriteria().andCompanyIdEqualTo(sitePr.getCompanyId()).andCodeEqualTo(site.getCode())
					.andCodeNotEqualTo(sitePr.getCode());
			siteEx = siteService.selectByExample(example).stream().findFirst();
		}

		if (siteEx.isPresent()) {
			CommonResponse response = new CommonResponse();
			response.getErrorMessages().add(CommonConstant.MSG_SITE_CODE + " " + siteEx.get().getCode() + " "
					+ CommonConstant.MSG_IS_ALREADY_EXISTS);
			return response;
		}

		return siteService.saveUpdate(site);
	}

	@ResponseBody
	@PostMapping("/saveUpdateStatus")
	public CommonResponse saveUpdateStatus(@ModelAttribute("siteStatus") SiteStatusWithBLOBs siteStatus) {
		return siteStatusService.saveUpdate(siteStatus);
	}

	@ResponseBody
	@PostMapping("/saveUpdatePowerGenerationStatus")
	public CommonResponse saveUpdateStatus(
			@ModelAttribute("sitePowerGenerationStatus") SitePowerGenerationStatusWithBLOBs sitePowerGenerationStatus) {
		return sitePowerGenerationStatusService.saveUpdate(sitePowerGenerationStatus);
	}

	@ResponseBody
	@PostMapping("/saveUpdateSiteClient")
	public CommonResponse saveUpdateSiteClient(@ModelAttribute("siteClient") SiteClientWithBLOBs siteClient) {
		return siteClientService.saveUpdate(siteClient);
	}

	@ResponseBody
	@PostMapping("/saveUpdateSitePowerGenerationCapacity")
	public CommonResponse saveUpdateSitePowerGenerationCapacity(
			@ModelAttribute("sitePowerGenerationCapacity") SitePowerGenerationCapacityWithBLOBs sitePowerGenerationCapacity) {
		return sitePowerGenerationCapacityService.saveUpdate(sitePowerGenerationCapacity);
	}

	@ResponseBody
	@PostMapping("/saveUpdateSiteConstructionCompany")
	public CommonResponse saveUpdateSiteConstructionCompany(
			@ModelAttribute("siteConstructionCompany") SiteConstructionCompanyWithBLOBs siteConstructionCompany) {
		return siteConstructionCompanyService.saveUpdate(siteConstructionCompany);
	}

	@ResponseBody
	@PostMapping("/saveUpdateSitePowerSupplySystem")
	public CommonResponse saveUpdatesitePowerSupplySystem(
			@ModelAttribute("sitePowerSupplySystem") SitePowerSupplySystemWithBLOBs sitePowerSupplySystem) {
		return sitePowerSupplySystemService.saveUpdate(sitePowerSupplySystem);
	}

	@ResponseBody
	@PostMapping("/saveUpdateSiteContractInformation")
	public CommonResponse saveUpdateSiteContractInformation(
			@ModelAttribute("siteContractInformation") SiteContractInformationWithBLOBs siteContractInformation) {
		return siteContractInformationService.saveUpdate(siteContractInformation);
	}

	@ResponseBody
	@PostMapping("/saveUpdateSiteUrgentResponse")
	public CommonResponse saveUpdateSiteUrgentResponse(
			@ModelAttribute("siteUrgentResponse") SiteUrgentResponseWithBLOBs siteUrgentResponse) {
		return siteUrgentResponseService.saveUpdate(siteUrgentResponse);
	}

	@ResponseBody
	@PostMapping("/saveUpdateSiteMonitoringStructure")
	public CommonResponse saveUpdateSiteMonitoringStructure(
			@ModelAttribute("siteMonitoringStructure") SiteMonitoringStructureWithBLOBs siteMonitoringStructure) {
		return siteMonitoringStructureService.saveUpdate(siteMonitoringStructure);
	}

	@ResponseBody
	@PostMapping("/saveUpdateSiteEmergencyContactData")
	public CommonResponse saveUpdateSiteEmergencyContactData(
			@ModelAttribute("siteEmergencyContact") SiteEmergencyContactWithBLOBs siteEmergencyContact) {
		return siteEmergencyContactService.saveUpdate(siteEmergencyContact);
	}

	@ResponseBody
	@PostMapping("/saveUpdateSitePersonInCharge")
	public CommonResponse saveUpdateSitePersonInCharge(
			@ModelAttribute("sitePersonInCharge") SitePersonInChargeWithBLOBs sitePersonInCharge) {
		return sitePersonInChargeService.saveUpdate(sitePersonInCharge);
	}

	@ResponseBody
	@PostMapping("/saveUpdateImage")
	public CommonResponse saveUpdateImage(@RequestBody SiteWithBLOBs site) {
		SiteWithBLOBs existingSite = siteService.selectByPrimaryKeyWithBLOBs(site.getId());
		if (existingSite != null) {
			existingSite.setMapImage(site.getMapImage());
			return siteService.saveUpdate(site);
		} else {
			CommonResponse commonResponse = new CommonResponse();
			commonResponse.setStatus(false);
			commonResponse.getErrorMessages().add(CommonConstant.MSG_RECORD_NOT_FOUND);
			return commonResponse;
		}
	}

	@ResponseBody
	@PostMapping("/saveUpdateJson")
	public CommonResponse saveUpdateJson(@RequestBody SiteWithBLOBs site) {
		SiteWithBLOBs existingSite = siteService.selectByPrimaryKeyWithBLOBs(site.getId());
		if (existingSite != null) {
			existingSite.setMapJson(site.getMapJson());
			return siteService.saveUpdate(site);
		} else {
			CommonResponse commonResponse = new CommonResponse();
			commonResponse.setStatus(false);
			commonResponse.getErrorMessages().add(CommonConstant.MSG_RECORD_NOT_FOUND);
			return commonResponse;
		}
	}
	
	@ResponseBody
	@PostMapping("/uploadcsv")
    public List<DeviceCSV> uploadCSVFile(@RequestParam("file") MultipartFile file, @RequestParam("siteId") String siteId, Model model) {
		List<DeviceCSV> csvDevices = null;
		if (file.isEmpty()) {
			model.addAttribute("message", "Please select a CSV file to upload.");
			model.addAttribute("status", false);
		} else {

			// parse CSV file to create a list of `DeviceCSV` objects
			try (Reader reader = new BufferedReader(new InputStreamReader(file.getInputStream()))) {

				// create csv bean reader
				CsvToBean<DeviceCSV> csvToBean = new CsvToBeanBuilder(reader).withType(DeviceCSV.class)
						.withIgnoreLeadingWhiteSpace(true).build();

				// convert `CsvToBean` object to list of devices
				csvDevices = csvToBean.parse();

				//Map<String, Map<String, Map<String, Map<String, Map<String,Panel>>>>> deviceMap = new HashMap<String, Map<String,Map<String,Map<String,Map<String,Panel>>>>>();
				Map<String, Map<String, Map<String, Map<String, Panel>>>> pcsMap = new HashMap<String, Map<String,Map<String,Map<String,Panel>>>>();
				Map<String, Panel> panelMap = new HashMap<String, Panel>();
				Map<String, Map<String, Panel>> panelStringMap = new HashMap<String, Map<String,Panel>>();
				Map<String, Map<String, Map<String, Panel>>> jcbMap = new HashMap<String, Map<String,Map<String,Panel>>>();

				for(DeviceCSV device:csvDevices) {
					//if(deviceMap.get(device.getId())!=null) {
												
						//put into Maps
						/*if(deviceMap.get(device.getId())!=null) {
							pcsMap = deviceMap.get(device.getId());							
						}*/						
						if(pcsMap!=null) {
							jcbMap = pcsMap.get(device.getPcs());
							if(jcbMap==null) {
								jcbMap = new HashMap<String, Map<String,Map<String,Panel>>>();
							}
						}
						if(jcbMap!=null) {
							panelStringMap = jcbMap.get(device.getJcb());	
							if(panelStringMap == null) {
								panelStringMap = new HashMap<String, Map<String,Panel>>();
							}
						}
						if(panelStringMap!=null) {
							panelMap = panelStringMap.get(device.getStr());
							if(panelMap == null) {
								panelMap = new HashMap<String, Panel>();
							}
						}
						
						//create Panel
						Panel panel = new Panel();
						panel.setObjectId(device.getPanel());
						panel.setDeviceId(device.getId());
						panelMap.put(device.getPanel(), panel);
						panelStringMap.put(device.getStr(), panelMap);
						jcbMap.put(device.getJcb(), panelStringMap);
						pcsMap.put(device.getPcs(), jcbMap);
						//deviceMap.put(device.getId(), pcsMap);
						

						

					//}
					//devicesMap.put(device.getId(), new Device(device.getId()"Device-"+device.getId());
					
				}
				deviceService.saveDeviceWithComponents(pcsMap,siteId);

				// save devices list on model
				// load saved devices list
				//List<PCS> pcsl = deviceService.findBySiteIdWithComponents(siteId, "0", "1");
				//model.addAttribute("devices", pcsl);
				model.addAttribute("status", true);

			} catch (Exception ex) {
				ex.printStackTrace();
				model.addAttribute("message", "An error occurred while processing the CSV file.");
				model.addAttribute("status", false);
			}
		}
		return csvDevices;
		
	}
	
	@ResponseBody
	@GetMapping("/{id}/devices/{from}/{to}")
	public List<PCS> getDevices(Model model, @PathVariable("id") String id, @PathVariable("from") String from, @PathVariable("to") String to) {
		List<PCS> pcses = deviceService.findBySiteIdWithComponents(id, from, to);
		/*for(int i=0;i<80;i++) {
			devices.addAll(devices);
		}*/
		return pcses;
	}
	
	@GetMapping("/{id}/stringmap")
	public String getStringMapView(Model model, @PathVariable("id") String id) {
		SiteWithBLOBs site = siteService.selectByPrimaryKeyWithBLOBs(id);
		model.addAttribute("site", site);
		return "site/mapview";
	}
	
	@ResponseBody
	@GetMapping("/{id}/error")
	public List<String> getSiteErrorPanels(Model model, @PathVariable("id") String id) {
		List<String> errors = siteService.getNishikataPanelIdsWithErrors();
		return errors;
	}
	
	@ResponseBody
	@GetMapping("/{id}/documents")
	public CommonResponse listDocuments(@PathVariable("id") String siteId) throws InterruptedException {
//		File folder = new File(documentPath+"\\"+siteId);
//		List<DirectoryItem> items = new ArrayList<DirectoryItem>();
//		
//		for (final File fileEntry : folder.listFiles()) {
//	        if (fileEntry.isDirectory()) {
//	        	items.add(new DirectoryItem(fileEntry.getName(),fileEntry.lastModified(),fileEntry.length(),DirectoryItemType.Directory));
//	        } else {
//	        	items.add(new DirectoryItem(fileEntry.getName(),DirectoryItemType.File));
//	        }
//	    }
		
		Site site = siteService.selectByPrimaryKey(siteId);
		String dir = documentPath+"\\"+site.getCode();
		List<DirectoryItem> items = new ArrayList<DirectoryItem>();
	    try (DirectoryStream<Path> stream = Files.newDirectoryStream(Paths.get(dir))) {
	        for (Path path : stream) {
	            if (!Files.isDirectory(path)) {
	            	items.add(new DirectoryItem(path.getFileName().toString(),Files.getLastModifiedTime(path).toMillis(),Files.getOwner(path).getName().toString(),Files.size(path),DirectoryItemType.DIRECTORY,path));
	            }
	            else {
	            	items.add(new DirectoryItem(path.getFileName().toString(),Files.getLastModifiedTime(path).toMillis(),Files.getOwner(path).getName().toString(),Files.size(path),DirectoryItemType.FILE,path));
	            }
	        }
	    } catch (IOException e) {
			e.printStackTrace();
		}

		CommonResponse commonResponse = new CommonResponse();
		commonResponse.setStatus(true);
		commonResponse.getPayload().add(items);
		return commonResponse;
	}
	

	public List<CompanyWithBLOBs> getComlist() {
		return comlist;
	}

	public void setComlist(List<CompanyWithBLOBs> comlist) {
		this.comlist = comlist;
	}
	
	@ResponseBody
	@PostMapping("/{id}/documents")
	public CommonResponse documents(@PathVariable("id") String siteId,@RequestBody DirectoryItem dir) throws InterruptedException {
		
		boolean isRoot = false;
		Site site = siteService.selectByPrimaryKey(siteId);
		String basePath = documentPath+"\\"+site.getCode();
		
		if(dir.getPath() == null) {			
			dir.setPath(Paths.get(basePath));			
		}
		if(dir.getPath().endsWith(Paths.get(basePath))) {
			isRoot = true;
		}
		
		List<DirectoryItem> items = new ArrayList<DirectoryItem>();
	    try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir.getPath())) {
        	Path parentPath = dir.getPath().getParent();
        	DirectoryItem parent = new DirectoryItem(parentPath.getFileName().toString(),Files.getLastModifiedTime(parentPath).toMillis(),Files.getOwner(parentPath).getName().toString(),Files.size(parentPath),DirectoryItemType.DIRECTORY,parentPath,null,isRoot);
	    	items.add(new DirectoryItem(DirectoryItem.DUMMYNAME,0L,"",0L,DirectoryItemType.FILE,dir.getPath(),parent,false));
	        for (Path path : stream) {
	            if (Files.isDirectory(path)) {
	            	items.add(new DirectoryItem(path.getFileName().toString(),Files.getLastModifiedTime(path).toMillis(),Files.getOwner(path).getName().toString(),Files.size(path),DirectoryItemType.DIRECTORY,path,parent,isRoot));
	            }
	            else {
	            	items.add(new DirectoryItem(path.getFileName().toString(),Files.getLastModifiedTime(path).toMillis(),Files.getOwner(path).getName().toString(),Files.size(path),DirectoryItemType.FILE,path,parent,isRoot));
	            }
	        }
	    } catch (IOException e) {
			e.printStackTrace();
		}

		CommonResponse commonResponse = new CommonResponse();
		commonResponse.setStatus(true);
		commonResponse.getPayload().add(items);
		return commonResponse;
	}
	
	@ResponseBody
	@PostMapping("/{id}/documents/create-folder")
	public CommonResponse createFolder(@PathVariable("id") String siteId,@RequestBody DirectoryItem dir) throws InterruptedException {
		String errorMsg = "";
		boolean status = false;
		Site site = siteService.selectByPrimaryKey(siteId);
		String basePath = documentPath+"\\"+site.getCode();
		
		if(dir.getPath() == null) {			
			dir.setPath(Paths.get(basePath));			
		}
		
	    try {
	    	Files.createDirectory(dir.getPath().resolve(dir.getName()));
	    	status = true;
	    } catch (FileAlreadyExistsException e) {
			e.printStackTrace();
			errorMsg = e.getLocalizedMessage() + " Already Exists!";
		} catch(Exception e) {
			e.printStackTrace();
			errorMsg = e.getLocalizedMessage();
		}

		CommonResponse commonResponse = new CommonResponse();
		List<String> errorList = new ArrayList<String>();
		errorList.add(errorMsg);
		commonResponse.setStatus(status);
		commonResponse.setErrorMessages(errorList);
		return commonResponse;
	}
	
	@ResponseBody
	@PostMapping("/{id}/documents/upload-file")
	public CommonResponse uploadFile(@PathVariable("id") String siteId,@RequestParam("file") MultipartFile uploadFile,
			@RequestParam("path") Path path,@RequestParam("documentConfidentialLevel") String documentConfidentialLevel) throws InterruptedException {
		String errorMsg = "";
		boolean status = false;
		Site site = siteService.selectByPrimaryKey(siteId);
		String basePath = documentPath+"\\"+site.getCode();
		
		if(path.toString().equals("undefined")) {			
			path = Paths.get(basePath);			
		}
		
		try {
			if (uploadFile.getName().isEmpty()) {
				throw new Exception("Please select a file to upload.");
			} else {
				String fileName = StringUtils.cleanPath(uploadFile.getOriginalFilename());
		        try {
		            // Check if the file's name contains invalid characters
		            if(fileName.contains("..")) {
		                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
		            }
	
		            // Copy file to the target location (Replacing existing file with the same name)
		            Path targetLocation = path.resolve(fileName);
		            Files.copy(uploadFile.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
		            status = true;
		            SiteDocumentWithBLOBs siteDocument = new SiteDocumentWithBLOBs();
		            siteDocument.setSiteId(siteId);
		            siteDocument.setConfidentialLevelId(documentConfidentialLevel);
		            siteDocument.setFileName(fileName);
		            siteDocument.setFolder(path.getFileName().toString());
		            siteDocument.setPath(targetLocation.toString());
		            siteDocument.setStatus(Status.ACTIVE.getDbValue().byteValue());
		            
		            siteDocumentService.saveUpdate(siteDocument);
		            
		        } catch (IOException ex) {
		            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
		        }
	
			}
		}catch (Exception e) {
			errorMsg = e.getLocalizedMessage();
		}

		CommonResponse commonResponse = new CommonResponse();
		List<String> errorList = new ArrayList<String>();
		errorList.add(errorMsg);
		commonResponse.setStatus(status);
		commonResponse.setErrorMessages(errorList);
		return commonResponse;
	}

}


