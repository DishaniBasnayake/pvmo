
package com.axiohelix.pvmo.controller;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.axiohelix.pvmo.entity.Site;
import com.axiohelix.pvmo.entity.SiteUserFixpointshooting;
import com.axiohelix.pvmo.entity.SiteUserFixpointshootingWithBLOBs;
import com.axiohelix.pvmo.entity.User;
import com.axiohelix.pvmo.model.ElogCSV;
import com.axiohelix.pvmo.model.SessionUser;
import com.axiohelix.pvmo.model.SiteUserFixpointshootingList;
import com.axiohelix.pvmo.service.SiteService;
import com.axiohelix.pvmo.service.SiteUserFixpointshootingService;
import com.axiohelix.pvmo.service.UserService;
import com.axiohelix.pvmo.util.CommonConstant;
import com.axiohelix.pvmo.util.CommonResponse;
import com.axiohelix.pvmo.util.LogType;
import com.opencsv.CSVWriter;

@Controller
@RequestMapping({
		CommonConstant.CONTROLLER_URL_SITE + "/{sid}/" + CommonConstant.CONTROLLER_URL_USER
				+ CommonConstant.CONTROLLER_URL_FIXPOINTSHOOTING,
		CommonConstant.API_CONTROLLER_URL_SITE + "/{sid}/" + CommonConstant.CONTROLLER_URL_USER
				+ CommonConstant.CONTROLLER_URL_FIXPOINTSHOOTING })
public class SiteUserFixpointshootingController extends AbstractController {
	private static final Logger LOGGER = LogManager.getLogger(SiteUserFixpointshootingController.class);

	@Autowired
	SiteUserFixpointshootingService siteUserFixpointshootingService;
	
	@ResponseBody
	@PostMapping(value = "/saveUpdateAll",produces = "application/json;charset=UTF-8")
	public CommonResponse saveUpdateAll(@PathVariable("sid") String sid,
			@RequestBody SiteUserFixpointshootingList siteUserFixpointshootingList) {

		SessionUser currentUser = getCurrentUser();
		
		CommonResponse commonResponse = new CommonResponse();
		for (SiteUserFixpointshootingWithBLOBs siteUserFixpointshooting : siteUserFixpointshootingList.getSiteUserFixpointshootingList()) {
			siteUserFixpointshooting.setSiteId(sid);
			siteUserFixpointshooting.setUserId(currentUser.getUserId());
			commonResponse = siteUserFixpointshootingService.saveUpdate(siteUserFixpointshooting);
		}
		
		commonResponse.setStatus(true);
		return commonResponse;
	}
	

}
