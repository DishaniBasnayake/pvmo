
package com.axiohelix.pvmo.controller;

import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.axiohelix.pvmo.entity.Site;
import com.axiohelix.pvmo.entity.SiteUserGPSLog;
import com.axiohelix.pvmo.entity.SiteUserGPSLogWithBLOBs;
import com.axiohelix.pvmo.entity.User;
import com.axiohelix.pvmo.entity.custom.SiteUserGPSLogExtended;
import com.axiohelix.pvmo.model.ElogCSV;
import com.axiohelix.pvmo.model.SessionUser;
import com.axiohelix.pvmo.service.SiteService;
import com.axiohelix.pvmo.service.SiteUserGPSLogService;
import com.axiohelix.pvmo.service.UserService;
import com.axiohelix.pvmo.util.CommonConstant;
import com.axiohelix.pvmo.util.CommonResponse;
import com.axiohelix.pvmo.util.LogType;
import com.opencsv.CSVWriter;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

@Controller
@RequestMapping({
		CommonConstant.CONTROLLER_URL_SITE + "/{sid}/" + CommonConstant.CONTROLLER_URL_USER
				+ CommonConstant.CONTROLLER_URL_GPS,
		CommonConstant.API_CONTROLLER_URL_SITE + "/{sid}/" + CommonConstant.CONTROLLER_URL_USER
				+ CommonConstant.CONTROLLER_URL_GPS })
public class SiteUserGPSLogController extends AbstractController {
	private static final Logger LOGGER = LogManager.getLogger(SiteUserGPSLogController.class);

	@Autowired
	SiteUserGPSLogService siteUserGPSLogService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	SiteService siteService;
	
	@Value("${ticket.elog.path}")
	private String elogPath;

	@ResponseBody
	@PostMapping("/saveUpdate")
	public CommonResponse saveUpdate(@PathVariable("sid") String sid,
			@RequestBody SiteUserGPSLogWithBLOBs siteUserGPSLog) {

		SessionUser currentUser = getCurrentUser();
		
		CommonResponse commonResponse = new CommonResponse();
		siteUserGPSLog.setUserId(currentUser.getUserId());
		commonResponse = siteUserGPSLogService.saveUpdate(siteUserGPSLog);
		
		//write to eLog
		try {
			CSVWriter writer = new CSVWriter(new FileWriter(elogPath, true));
			
			//List<ElogCSV> list = new ArrayList<ElogCSV>();
			
			Object obj = commonResponse.getPayload().get(0);
			
			SiteUserGPSLogWithBLOBs sug = (SiteUserGPSLogWithBLOBs) obj;
			
			ElogCSV elogCSV = new ElogCSV();
			//elogCSV.setId(sug.getId());
			elogCSV.setLatitude(sug.getLatitude());
			elogCSV.setLongitude(sug.getLongitude());
			elogCSV.setLogType(LogType.getValueByDbValue(sug.getLogType()));
			elogCSV.setLogTime(sug.getLogTime());
			elogCSV.setIp(sug.getIp());
			
			User u = userService.selectByPrimaryKey(sug.getUserId());
			Site s = siteService.selectByPrimaryKey(sug.getSiteId());
			
			if (u != null) {
				elogCSV.setUserName(u.getUsername());
			}
			if (s != null) {
				elogCSV.setSiteName(s.getName());
			}
			
			//list.add(elogCSV);
			
			//StatefulBeanToCsvBuilder<ElogCSV> builder = new StatefulBeanToCsvBuilder<ElogCSV>(writer);
			//StatefulBeanToCsv<ElogCSV> beanWriter = builder.build();
			
			//beanWriter.write(elogCSV);
			
			String[] record = new String[] {elogCSV.getUserName(), elogCSV.getLongitude().toString(), 
						elogCSV.getLatitude().toString(), elogCSV.getLogType(), elogCSV.getLogTime().toString(), 
						elogCSV.getSiteName(), elogCSV.getIp()};
			
			writer.writeNext(record); 
			writer.close();
		} catch (/* CsvDataTypeMismatchException | CsvRequiredFieldEmptyException | */ IOException e) {
			System.out.println("Unable to write to eLog.CSV Id:" + siteUserGPSLog.getId());
			e.printStackTrace();
		}
		
		commonResponse.setStatus(true);
		return commonResponse;
	}
	
	@ResponseBody
	@GetMapping("/latest")
	public CommonResponse latest(@PathVariable("sid") String sid) {

		CommonResponse commonResponse = new CommonResponse();

		List<SiteUserGPSLogExtended> siteUserGPSLogs = siteUserGPSLogService.latest(sid);

		commonResponse.setStatus(true);
		commonResponse.getPayload().add(siteUserGPSLogs);
		return commonResponse;
	}

	

}
