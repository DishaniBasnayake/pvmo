package com.axiohelix.pvmo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.axiohelix.pvmo.entity.SiteWithBLOBs;
import com.axiohelix.pvmo.entity.Template;
import com.axiohelix.pvmo.entity.TemplateExample;
import com.axiohelix.pvmo.entity.TemplateWithBLOBs;
import com.axiohelix.pvmo.entity.Ticket;
import com.axiohelix.pvmo.entity.TicketExample;
import com.axiohelix.pvmo.entity.TicketWithBLOBs;
import com.axiohelix.pvmo.model.SessionUser;
import com.axiohelix.pvmo.service.ServiceCenterSiteService;
import com.axiohelix.pvmo.service.ServiceCenterUserService;
import com.axiohelix.pvmo.service.TemplateService;
import com.axiohelix.pvmo.service.TicketService;
import com.axiohelix.pvmo.type.Status;
import com.axiohelix.pvmo.type.UserType;
import com.axiohelix.pvmo.util.CommonConstant;
import com.axiohelix.pvmo.util.CommonResponse;
import com.axiohelix.pvmo.util.CommonValidation;

@Controller
@RequestMapping({CommonConstant.CONTROLLER_URL_TEMPLATE, CommonConstant.API_CONTROLLER_URL_TEMPLATE})
public class TemplateController extends AbstractController{

	private static final Logger LOGGER = LogManager.getLogger(TemplateController.class);

	@Autowired
	private TicketService ticketService;
	@Autowired
	private ServiceCenterUserService serviceCenterUserService;
	@Autowired
	private ServiceCenterSiteService serviceCenterSiteService;
	@Autowired
	private TemplateService templateService;

	@GetMapping("")
	public String index(Model model) {

		model.addAttribute("ticket", new TicketWithBLOBs());
		return "ticket/list";
	}

	@ResponseBody
	@GetMapping("/data/{search}")
	public CommonResponse index(@PathVariable(value = "search") String search) {

		CommonResponse commonResponse = new CommonResponse();
		List<Template> list = new ArrayList<Template>();

		TemplateExample templateExample = new TemplateExample();
		templateExample.createCriteria().andStatusEqualTo(Status.ACTIVE.getDbValue().byteValue());
		templateExample.setOrderByClause("CREATED_ON DESC");

		list = templateService.selectByExample(templateExample);

		commonResponse.setStatus(true);
		commonResponse.getPayload().add(list);
		return commonResponse;
	}
	
	/***
	@ResponseBody
	@GetMapping("/data/{search}")
	public CommonResponse index(@PathVariable(value = "search") String search) {

		CommonResponse commonResponse = new CommonResponse();
		SessionUser currentUser = getCurrentUser();
		List<TicketWithBLOBs> list = new ArrayList<TicketWithBLOBs>();

		if (currentUser.getUserType().equals(UserType.USER.name())) {
			List<SiteWithBLOBs> userSiteList = serviceCenterUserService.getServiceCenters(currentUser.getUserId())
					.stream().flatMap(sc -> serviceCenterSiteService.getSites(sc.getId()).stream())
					.collect(Collectors.toList());
			List<String> userSiteIds = userSiteList.stream().map(site -> site.getId()).collect(Collectors.toList());

			TicketExample ticketExample = new TicketExample();

			ticketExample.createCriteria().andSiteIdIn(userSiteIds);
			ticketExample.createCriteria().andStatusEqualTo(Status.ACTIVE.getDbValue().byteValue());
			ticketExample.setOrderByClause("CREATED_ON DESC");

			list = ticketService.selectByExampleWithBLOBs(ticketExample);
		} else {

			TicketExample ticketExample = new TicketExample();
			ticketExample.createCriteria().andStatusEqualTo(Status.ACTIVE.getDbValue().byteValue());
			ticketExample.setOrderByClause("CREATED_ON DESC");

			list = ticketService.selectByExampleWithBLOBs(ticketExample);
		}

		commonResponse.setStatus(true);
		commonResponse.getPayload().add(list);
		return commonResponse;
	}

	@GetMapping("/{id}")
	public String view(Model model, @PathVariable("id") String id) {

		TicketWithBLOBs ticket = ticketService.selectByPrimaryKeyWithBLOBs(id);
		model.addAttribute("ticket", ticket);

		return "ticket-view";
	}

	@ResponseBody
	@PostMapping("/saveUpdate")
	public CommonResponse saveUpdate(@ModelAttribute("ticket") TicketWithBLOBs ticket) {

		TicketExample example = new TicketExample();
		Optional<Ticket> ticketEx = null;

		if (CommonValidation.stringNullValidation(ticket.getId())) {
			example.createCriteria().andSiteIdEqualTo(ticket.getSiteId()).andTicketIdEqualTo(ticket.getTicketId());
			ticketEx = ticketService.selectByExample(example).stream().findFirst();
			ticket.addInsertDetails();
		} else {
			Ticket ticketPr = ticketService.selectByPrimaryKey(ticket.getId());
			example.createCriteria().andSiteIdEqualTo(ticketPr.getSiteId()).andTicketIdEqualTo(ticket.getTicketId())
					.andTicketIdNotEqualTo(ticketPr.getTicketId());
			ticketEx = ticketService.selectByExample(example).stream().findFirst();
			ticket.addUpdateDetails();
		}

		if (ticketEx.isPresent()) {
			CommonResponse response = new CommonResponse();
			response.getErrorMessages().add(CommonConstant.MSG_TICKET_ID + " " + ticketEx.get().getTicketId() + " "
					+ CommonConstant.MSG_IS_ALREADY_EXISTS);
			return response;
		}

		return ticketService.saveUpdate(ticket);
	}

	@ResponseBody
	@GetMapping("/{id}/delete")
	public CommonResponse delete(Model model, @PathVariable("id") String id) {

		CommonResponse commonResponse = new CommonResponse();
		TicketWithBLOBs ticket = ticketService.selectByPrimaryKeyWithBLOBs(id);
		if (ticket == null) {
			commonResponse.setStatus(false);
			commonResponse.getErrorMessages().add(CommonConstant.MSG_RECORD_NOT_FOUND);
			return commonResponse;
		}
		ticket.setStatus(Status.DELETED.getDbValue().byteValue());
		ticketService.updateByPrimaryKeySelective(ticket);

		commonResponse.setStatus(true);
		return commonResponse;
	}
	
	***/

	@ResponseBody
	@GetMapping("/{id}/data")
	public CommonResponse templateData(@PathVariable("id") String id) {
		TemplateWithBLOBs template = templateService.selectByPrimaryKeyWithBLOBs(id);
		CommonResponse commonResponse = new CommonResponse();
		commonResponse.setStatus(true);
		commonResponse.getPayload().add(template);
		return commonResponse;
	}

}
