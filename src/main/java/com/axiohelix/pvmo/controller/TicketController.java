package com.axiohelix.pvmo.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Collectors;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.axiohelix.pvmo.entity.DocumentWithBLOBs;
import com.axiohelix.pvmo.entity.Site;
import com.axiohelix.pvmo.entity.SiteExample;
import com.axiohelix.pvmo.entity.SiteWithBLOBs;
import com.axiohelix.pvmo.entity.TemplateExample;
import com.axiohelix.pvmo.entity.TemplateWithBLOBs;
import com.axiohelix.pvmo.entity.Ticket;
import com.axiohelix.pvmo.entity.TicketExample;
import com.axiohelix.pvmo.entity.TicketWithBLOBs;
import com.axiohelix.pvmo.entity.UserWithBLOBs;
import com.axiohelix.pvmo.model.AssignedUserDTO;
import com.axiohelix.pvmo.model.Search;
import com.axiohelix.pvmo.model.SessionUser;
import com.axiohelix.pvmo.model.TlogCSV;
import com.axiohelix.pvmo.service.DocumentService;
import com.axiohelix.pvmo.service.ServiceCenterSiteService;
import com.axiohelix.pvmo.service.ServiceCenterUserService;
import com.axiohelix.pvmo.service.SiteService;
import com.axiohelix.pvmo.service.TemplateService;
import com.axiohelix.pvmo.service.TicketDocumentService;
import com.axiohelix.pvmo.service.TicketService;
import com.axiohelix.pvmo.service.TicketUserService;
import com.axiohelix.pvmo.type.Status;
import com.axiohelix.pvmo.type.TemplateType;
import com.axiohelix.pvmo.type.TicketStatus;
import com.axiohelix.pvmo.type.TicketType;
import com.axiohelix.pvmo.type.UserType;
import com.axiohelix.pvmo.util.CommonConstant;
import com.axiohelix.pvmo.util.CommonResponse;
import com.axiohelix.pvmo.util.CommonValidation;
import com.axiohelix.pvmo.util.Snowflake;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameTranslateMappingStrategy;

@Controller
@EnableScheduling
@RequestMapping({ CommonConstant.CONTROLLER_URL_TICKET, CommonConstant.API_CONTROLLER_URL_TICKET })
public class TicketController extends AbstractController {

	private static final Logger LOGGER = LogManager.getLogger(TicketController.class);

	@Autowired
	private TicketService ticketService;
	@Autowired
	private ServiceCenterUserService serviceCenterUserService;
	@Autowired
	private ServiceCenterSiteService serviceCenterSiteService;
	@Autowired
	private TemplateService templateService;
	@Autowired
	private TicketUserService ticketUserService;
	@Autowired
	private TicketDocumentService ticketDocumentService;
	@Autowired
	private DocumentService documentService;
	@Autowired
	private SiteService siteService;
	
	@Value("${ticket.tlog.path}")
	private String tlogPath;

	@GetMapping("")
	public String index(Model model) {

		model.addAttribute("ticket", new TicketWithBLOBs());
		return "ticket/list";
	}

	@ResponseBody
	@GetMapping("/data/{region}/{serviceCenter}")
	public CommonResponse index(Search search) {

		CommonResponse commonResponse = new CommonResponse();
		SessionUser currentUser = getCurrentUser();
		List<Ticket> list = new ArrayList<Ticket>();

		if (currentUser.getUserType().equals(UserType.USER.name())) {
			List<SiteWithBLOBs> userSiteList = serviceCenterUserService.getServiceCenters(currentUser.getUserId())
					.stream().flatMap(sc -> serviceCenterSiteService.getSites(sc.getId()).stream())
					.collect(Collectors.toList());
			List<String> userSiteIds = userSiteList.stream().map(site -> site.getId()).collect(Collectors.toList());

			/*
			 * TicketExample ticketExample = new TicketExample();
			 * 
			 * ticketExample.createCriteria().andSiteIdIn(userSiteIds);
			 * ticketExample.createCriteria().andStatusEqualTo(Status.ACTIVE.getDbValue().
			 * byteValue()); ticketExample.setOrderByClause("CREATED_ON DESC");
			 * 
			 * list = ticketService.selectByExampleWithBLOBs(ticketExample);
			 */
			if(userSiteIds != null && userSiteIds.size() > 0) {
				TicketExample ticketExample = new TicketExample();
				ticketExample.createCriteria().andSiteIdIn(userSiteIds);
				ticketExample.createCriteria().andStatusEqualTo(Status.ACTIVE.getDbValue().byteValue());
				ticketExample.setOrderByClause("CREATED_ON DESC, TICKET_ID DESC");
				
				List<Ticket> tickets = ticketService.selectByExample(ticketExample);
				list = tickets.stream().filter(t-> ticketUserService.getUsers(t.getId()).stream()
						.map(tu->tu.getId()).collect(Collectors.toList())
						.contains(currentUser.getUserId())).collect(Collectors.toList());
				
				if(search.getRegion() != null && !search.getRegion().equals("0")) {
					SiteExample siteExample = new SiteExample();
					siteExample.createCriteria().andRegionEqualTo(search.getRegion());
					List<String> sites = siteService.selectByExample(siteExample).stream().map(site -> site.getId()).collect(Collectors.toList());
					list = list.stream().filter(ticket -> sites.stream().anyMatch(site -> site.equals(ticket.getSite().getId()))).collect(Collectors.toList());
					if(search.getServiceCenter() != null && !search.getServiceCenter().equals("0"))
						list = list.stream().filter(ticket-> serviceCenterSiteService.getSites(search.getServiceCenter()).stream()
								.anyMatch(site -> site.getId().equals(ticket.getSite().getId()))).collect(Collectors.toList());
				}
			}
			
		} else {

			TicketExample ticketExample = new TicketExample();
			TicketExample.Criteria ticketExampleCriteria = ticketExample.createCriteria();
			ticketExampleCriteria.andStatusEqualTo(Status.ACTIVE.getDbValue().byteValue());
			
			if(search.getRegion() != null && !search.getRegion().equals("0")) {
				SiteExample siteExample = new SiteExample();
				siteExample.createCriteria().andRegionEqualTo(search.getRegion());
				List<String> sites = siteService.selectByExample(siteExample).stream().map(site -> site.getId()).collect(Collectors.toList());
				
				if(sites != null && sites.size() > 0) {
					ticketExampleCriteria.andSiteIdIn(sites);
					ticketExample.setOrderByClause("CREATED_ON DESC, TICKET_ID DESC");
				
					list = ticketService.selectByExample(ticketExample);
					if(search.getServiceCenter() != null && !search.getServiceCenter().equals("0"))
						list = list.stream().filter(ticket-> serviceCenterSiteService.getSites(search.getServiceCenter()).stream()
								.anyMatch(site -> site.getId().equals(ticket.getSite().getId()))).collect(Collectors.toList());
				}
			}else {
				list = ticketService.selectByExample(ticketExample);
			}
		}

		commonResponse.setStatus(true);
		commonResponse.getPayload().add(list);
		return commonResponse;
	}

	@GetMapping("/{id}")
	public String view(Model model, @PathVariable("id") String id) {

		TicketWithBLOBs ticket = ticketService.selectByPrimaryKeyWithBLOBs(id);		
		model.addAttribute("ticket", ticket);

		return "ticket-view";
	}

	@ResponseBody
	@Transactional
	@PostMapping("/saveUpdate")
	public CommonResponse saveUpdate(@ModelAttribute("ticket") TicketWithBLOBs ticket) throws JsonProcessingException {

		CommonResponse commonResponse = new CommonResponse();
		TicketExample example = new TicketExample();
		Optional<Ticket> ticketEx = null;

		if (CommonValidation.stringNullValidation(ticket.getId())) {
			example.createCriteria().andSiteIdEqualTo(ticket.getSiteId()).andTicketIdEqualTo(ticket.getTicketId());
			ticketEx = ticketService.selectByExample(example).stream().findFirst();
			ticket.setStatus(Status.ACTIVE.getDbValue().byteValue());
			ticket.addInsertDetails();
		} else {
			Ticket ticketPr = ticketService.selectByPrimaryKey(ticket.getId());
			example.createCriteria().andSiteIdEqualTo(ticketPr.getSiteId()).andTicketIdEqualTo(ticket.getTicketId())
					.andTicketIdNotEqualTo(ticketPr.getTicketId());
			ticketEx = ticketService.selectByExample(example).stream().findFirst();
			ticket.addUpdateDetails();
		}

		if (ticketEx.isPresent()) {
			CommonResponse response = new CommonResponse();
			response.getErrorMessages().add(CommonConstant.MSG_TICKET_ID + " " + ticketEx.get().getTicketId() + " "
					+ CommonConstant.MSG_IS_ALREADY_EXISTS);
			return response;
		}
		

		CommonResponse responseWithSavedTicket = ticketService.saveUpdate(ticket);
		ticket = (TicketWithBLOBs) responseWithSavedTicket.getPayload().get(0);
		
		final String ticketId = ticket.getId();

		ObjectMapper objectMapper = new ObjectMapper();
		
		List<AssignedUserDTO> assignedSupervisorDTOs = objectMapper.readValue(ticket.getAssignedSupervisors(), new TypeReference<ArrayList<AssignedUserDTO>>(){});
		List<AssignedUserDTO> assignedEmployeeDTOs = objectMapper.readValue(ticket.getAssignedEmployees(), new TypeReference<ArrayList<AssignedUserDTO>>(){});
		
		ticketUserService.getUsers(ticket.getId()).stream().forEach((u)->{
			ticketUserService.deleteTicketUser(ticketId,u.getId());
		});
		
		assignedSupervisorDTOs.stream().forEach((auDTOs) -> {
			ticketUserService.addTicketUser(String.valueOf(Snowflake.newId()), ticketId, auDTOs.getId(), UserType.SUPERVISOR.getDbValue());
		});
		
		assignedEmployeeDTOs.stream().forEach((auDTOs) -> {
			ticketUserService.addTicketUser(String.valueOf(Snowflake.newId()), ticketId, auDTOs.getId(), UserType.EMPLOYEE.getDbValue());
		});
		
		
		try {
			if(ticket.getDocument() != null && !ticket.getDocument().getOriginalFilename().isEmpty()) {
				
				ticketDocumentService.getDocuments(ticket.getId()).forEach((i)->{
					documentService.deleteByPrimaryKey(i.getId());
				});
				
				DocumentWithBLOBs document = new DocumentWithBLOBs();
				document.setStatus(Status.ACTIVE.getDbValue().byteValue());
				document.setContent(ticket.getDocument().getBytes());
				document.setContentType(ticket.getDocument().getContentType());
				document.setFileName(ticket.getDocument().getOriginalFilename());
				document.addInsertDetails();
				document.addUpdateDetails();
				CommonResponse responseWithSavedDocument = documentService.saveUpdate(document);
				document = (DocumentWithBLOBs) responseWithSavedDocument.getPayload().get(0);
				
				ticketDocumentService.addTicketDocument(String.valueOf(Snowflake.newId()), ticket.getId(), document.getId());
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		ticket.setTicketStatus(TicketStatus.PENDING.getDbValue());
		ticketService.saveUpdate(ticket);
		commonResponse.setStatus(true);
		return commonResponse;
	}

	@ResponseBody
	@GetMapping("/{id}/delete")
	public CommonResponse delete(Model model, @PathVariable("id") String id) {

		CommonResponse commonResponse = new CommonResponse();
		TicketWithBLOBs ticket = ticketService.selectByPrimaryKeyWithBLOBs(id);
		if (ticket == null) {
			commonResponse.setStatus(false);
			commonResponse.getErrorMessages().add(CommonConstant.MSG_RECORD_NOT_FOUND);
			return commonResponse;
		}
		ticket.setStatus(Status.DELETED.getDbValue().byteValue());
		ticketService.updateByPrimaryKeySelective(ticket);

		commonResponse.setStatus(true);
		return commonResponse;
	}

	@ResponseBody
	@GetMapping("/{id}/data")
	public CommonResponse ticketData(@PathVariable("id") String id) {

		TicketWithBLOBs ticket = ticketService.selectByPrimaryKeyWithBLOBs(id);
		
		TemplateExample templateExample = new TemplateExample();
		templateExample.createCriteria().andStatusEqualTo(Status.ACTIVE.getDbValue().byteValue());
		templateExample.setOrderByClause("CREATED_ON DESC");

		List<TemplateWithBLOBs> templates = templateService.selectByExampleWithBLOBs(templateExample);
		
		List<UserWithBLOBs> users =  ticketUserService.getNotAssignedUsers(ticket.getSiteId(), id);
		
		List<String> ticketTypes = EnumSet.allOf(TicketType.class).stream().map(tt-> tt.getDbValue()).collect(Collectors.toList()); 
		
		CommonResponse commonResponse = new CommonResponse();
		commonResponse.setStatus(true);
		commonResponse.getPayload().add(ticket);
		commonResponse.getPayload().add(templates);
		commonResponse.getPayload().add(users);
		commonResponse.getPayload().add(ticketTypes);
		return commonResponse;
	}
	
	@ResponseBody
	@Transactional
	@PostMapping("/saveUpdate/data")
	public CommonResponse saveUpdateEmergencyTicket(@RequestBody String payload) throws JsonProcessingException {
		
		
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		TicketWithBLOBs ticket = objectMapper.readValue(payload, TicketWithBLOBs.class);

		CommonResponse commonResponse = new CommonResponse();
		TicketExample example = new TicketExample();
		Optional<Ticket> ticketEx = null;

		if (CommonValidation.stringNullValidation(ticket.getId())) {
			example.createCriteria().andSiteIdEqualTo(ticket.getSiteId()).andTicketIdEqualTo(ticket.getTicketId());
			ticketEx = ticketService.selectByExample(example).stream().findFirst();
			ticket.setStatus(Status.ACTIVE.getDbValue().byteValue());
			ticket.addInsertDetails();
		} else {
			Ticket ticketPr = ticketService.selectByPrimaryKey(ticket.getId());
			example.createCriteria().andSiteIdEqualTo(ticketPr.getSiteId()).andTicketIdEqualTo(ticket.getTicketId())
					.andTicketIdNotEqualTo(ticketPr.getTicketId());
			ticketEx = ticketService.selectByExample(example).stream().findFirst();
			ticket.addUpdateDetails();
		}

		if (ticketEx.isPresent()) {
			CommonResponse response = new CommonResponse();
			response.getErrorMessages().add(CommonConstant.MSG_TICKET_ID + " " + ticketEx.get().getTicketId() + " "
					+ CommonConstant.MSG_IS_ALREADY_EXISTS);
			return response;
		}
		

		CommonResponse responseWithSavedTicket = ticketService.saveUpdate(ticket);
		ticket = (TicketWithBLOBs) responseWithSavedTicket.getPayload().get(0);
		
		final String ticketId = ticket.getId();
		SessionUser currentUser = getCurrentUser();
		
		ticketUserService.addTicketUser(String.valueOf(Snowflake.newId()), ticketId, currentUser.getUserId(), UserType.EMPLOYEE.getDbValue());

		/*objectMapper = new ObjectMapper();
		
		List<AssignedUserDTO> assignedSupervisorDTOs = objectMapper.readValue(ticket.getAssignedSupervisors(), new TypeReference<ArrayList<AssignedUserDTO>>(){});
		List<AssignedUserDTO> assignedEmployeeDTOs = objectMapper.readValue(ticket.getAssignedEmployees(), new TypeReference<ArrayList<AssignedUserDTO>>(){});
		
		ticketUserService.getUsers(ticket.getId()).stream().forEach((u)->{
			ticketUserService.deleteTicketUser(ticketId,u.getId());
		});
		
		assignedSupervisorDTOs.stream().forEach((auDTOs) -> {
			ticketUserService.addTicketUser(String.valueOf(Snowflake.newId()), ticketId, auDTOs.getId(), UserType.SUPERVISOR.getDbValue());
		});
		
		assignedEmployeeDTOs.stream().forEach((auDTOs) -> {
			ticketUserService.addTicketUser(String.valueOf(Snowflake.newId()), ticketId, auDTOs.getId(), UserType.EMPLOYEE.getDbValue());
		});*/
				
		ticket.setTicketStatus(TicketStatus.PENDING.getDbValue());
		ticketService.saveUpdate(ticket);
		commonResponse.setStatus(true);
		return commonResponse;
	}
	
	@Scheduled(cron = "*/30 * * * * *")
	public void readTLog() {
		Scanner scanner = null;
		try {
			InputStream inputStream = new FileInputStream(tlogPath);
			
			if (inputStream != null) {
				List<TlogCSV> tlogList = null;
				
				Map<String, String> columnMapping = new HashMap<String, String>();
				columnMapping.put("ID", "id");
				columnMapping.put("SPN", "spn");
				columnMapping.put("SPN-Sub", "spnSub");
				columnMapping.put("Site Name", "siteName");
				columnMapping.put("Ticket Number", "ticketNumber");
				columnMapping.put("Start Date", "startDate");
				columnMapping.put("Start Time", "startTime");
				columnMapping.put("Ticket Owner", "ticketOwner");
				columnMapping.put("Status", "status");
				
				HeaderColumnNameTranslateMappingStrategy<TlogCSV> strategy = new HeaderColumnNameTranslateMappingStrategy<TlogCSV>();
				strategy.setType(TlogCSV.class);
				strategy.setColumnMapping(columnMapping);
				
				Reader reader = new BufferedReader(new InputStreamReader(inputStream, "Shift_JIS"));
				
				CsvToBeanBuilder csvToBean = new CsvToBeanBuilder(reader).withType(TlogCSV.class)
						.withOrderedResults(true)
						.withIgnoreLeadingWhiteSpace(true)
						.withMappingStrategy(strategy);
				
				tlogList = csvToBean.build().parse();
				
				if (tlogList.size() > 0) {
					TicketWithBLOBs ticket = null;
					SiteWithBLOBs site = null;
					List<TicketWithBLOBs> tickets = new ArrayList<TicketWithBLOBs>();
					StringBuilder ticketTitle = null;
					StringBuilder dateString = null;
					SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd HH:mm");
					Integer siteTicketCount = ticketService.getExistingTicketCount();
					Date createdOn = null;
					
					for (TlogCSV tlc : tlogList) {
						if (tlc.getSpn() != null 
								&& tlc.getStartDate() != null && !tlc.getStartDate().isEmpty()
								&& tlc.getStartTime() != null && !tlc.getStartTime().isEmpty()) {
							
							site = siteService.getSiteBySPN(tlc.getSpn());
							ticketTitle = new StringBuilder();
							ticketTitle
								.append((tlc.getSiteName() != null && !tlc.getSiteName().isEmpty()) ? tlc.getSiteName() : "")
								.append((tlc.getStatus() != null && !tlc.getStatus().isEmpty()) ? (" " + tlc.getStatus()) : "");
							
							dateString = new StringBuilder();
							dateString
								.append(tlc.getStartDate())
								.append(" ")
								.append(tlc.getStartTime());
							
							createdOn = sdf.parse(dateString.toString());
							
							
							if (site != null && site.getId() != null && DateUtils.isSameDay(createdOn, new Date())) {
								ticket = new TicketWithBLOBs();
								ticket.setId(String.valueOf(Snowflake.newId()));
								ticket.setSiteId(site.getId());
								ticket.setTemplateId(TemplateType.AXIO_AUTOFILL.getDbValue());
								ticket.setTicketType(com.axiohelix.pvmo.util.TicketType.AXIO_AUTO_FILL.getDisplayValue());
								ticket.setTicketTitle(ticketTitle.toString());
								ticket.setDescription(ticketTitle.append(" by ").append(tlc.getTicketOwner()).toString());
								ticket.setCreatedOn(createdOn);
								ticket.setCreatedBy(tlc.getTicketOwner());
								ticket.setStatus(Status.ACTIVE.getDbValue().byteValue());
								ticket.setTicketStatus(TicketStatus.PENDING.getDbValue());
								
								tickets.add(ticket);
								
							} else {
								System.out.println("Site not found by SPN:" + tlc.getSpn() + " or old record found on the tLog.csv");
							}
						
						} else {
							System.out.println("Incomplete tLogEntry:" + tlc.getId());
						}
						
					}
					
					SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
					Date tLogDay = sdf2.parse(getTlogDateString());
					String tds = null;
					String ts = null;
					
					if (!DateUtils.isSameDay(tLogDay, new Date())) {
						tds = sdf2.format(new Date());
						ts = String.valueOf(tickets.size());
						
					} else {
						tickets.subList(0, getTlogSize()).clear();

						int s = getTlogSize();
						s += tickets.size();
						
						ts = String.valueOf(s);
						
					}
					
					
					if (tickets.size() > 0) {
						for (TicketWithBLOBs t : tickets) {
							t.setTicketId(ticketPrefix + String.format("%04d", ++siteTicketCount));
						}
					}
					
					ticketService.insertMultiple(tickets);
					try {
						tickets.stream().forEach(t->{
							List<UserWithBLOBs> users = ticketUserService.getNotAssignedUsers(t.getSiteId(), t.getId());
							users.stream().forEach(u->{
								ticketUserService.addTicketUser(String.valueOf(Snowflake.newId()), t.getId(), u.getId(), u.getUserType());
							});
						});
					}catch (Exception e) {
						LOGGER.error("readTLog() :: Ticket users add faild with Error : " + e.getMessage());
					}
					
					File f = ResourceUtils.getFile("classpath:static/files/tlog.txt");
					
					InputStream input = new FileInputStream(f);
					
					scanner = new Scanner(input);
					HashMap<String, String> appMap = new HashMap<>();
					String txt = null;
					String[] parts = null; 
					
					while(scanner.hasNext()) {
						txt = scanner.next();
						
						if(txt != null && !txt.isEmpty()) {
							parts = txt.split("=");
							appMap.put(parts[0], parts[1]);
						}
					}
					
					if (tds != null) {
						appMap.put("ticket.tlog.date", tds);
					}
					appMap.put("ticket.tlog.size", ts);
					
					String writeText = "";
					
					for (String k : appMap.keySet()) {
						writeText += (k + "=" + appMap.get(k) + "\n");
					}
					
					FileWriter fw = new FileWriter(f);
					fw.write(writeText);
					fw.close();
				}
			}
			
		} catch (IllegalStateException | ParseException | IOException e) {
			LOGGER.error("readTLog() :: AXIOAUTOFILL Ticket add faild with Error : " + e.getMessage());
		} finally {
			if(scanner != null) scanner.close();
		}
		
	}
	
}
