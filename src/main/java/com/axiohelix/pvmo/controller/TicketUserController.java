
package com.axiohelix.pvmo.controller;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.catalina.mapper.Mapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.asm.TypeReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.axiohelix.pvmo.entity.Site;
import com.axiohelix.pvmo.entity.SiteUserGPSLogWithBLOBs;
import com.axiohelix.pvmo.entity.Ticket;
import com.axiohelix.pvmo.entity.TicketUser;
import com.axiohelix.pvmo.entity.TicketUserDTO;
import com.axiohelix.pvmo.entity.TicketWithBLOBs;
import com.axiohelix.pvmo.entity.User;
import com.axiohelix.pvmo.entity.UserWithBLOBs;
import com.axiohelix.pvmo.model.AssignedUserDTO;
import com.axiohelix.pvmo.model.ElogCSV;
import com.axiohelix.pvmo.model.SessionUser;
import com.axiohelix.pvmo.service.TicketService;
import com.axiohelix.pvmo.service.TicketUserService;
import com.axiohelix.pvmo.service.UserService;
import com.axiohelix.pvmo.type.TicketStatus;
import com.axiohelix.pvmo.type.UserType;
import com.axiohelix.pvmo.util.CommonConstant;
import com.axiohelix.pvmo.util.CommonResponse;
import com.axiohelix.pvmo.util.CustomMappingStrategy;
import com.axiohelix.pvmo.util.LogType;
import com.axiohelix.pvmo.util.RLogColumnBean;
import com.axiohelix.pvmo.util.Snowflake;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

import lombok.Getter;
import lombok.Setter;

@Controller
@RequestMapping({ CommonConstant.CONTROLLER_URL_TICKET + "/{tid}/" + CommonConstant.CONTROLLER_URL_USER,
		CommonConstant.API_CONTROLLER_URL_TICKET + "/{tid}/" + CommonConstant.CONTROLLER_URL_USER })
public class TicketUserController extends AbstractController {

	private static final Logger LOGGER = LogManager.getLogger(TicketUserController.class);

	@Value("${ticket.rlog.path}")
	private String rlogPath;

	@Autowired
	private TicketService ticketService;
	@Autowired
	private TicketUserService ticketUserService;
	@Autowired
	private UserService userService;

	@ResponseBody
	@GetMapping("/data/{search}")
	public CommonResponse index(@PathVariable("tid") String tid, @PathVariable(value = "search") String search) {

		CommonResponse commonResponse = new CommonResponse();

		List<UserWithBLOBs> list = ticketUserService.getUsers(tid);

		commonResponse.setStatus(true);
		commonResponse.getPayload().add(list);
		return commonResponse;
	}

	@ResponseBody
	@GetMapping("/data/not-assigned")
	public CommonResponse getNotAssignedUsers(@PathVariable("tid") String tid) {

		CommonResponse commonResponse = new CommonResponse();
		Ticket ticket = ticketService.selectByPrimaryKey(tid);

		List<UserWithBLOBs> list = ticketUserService.getUsers(tid);
		List<UserWithBLOBs> listUnAssigned = ticketUserService.getNotAssignedUsers(ticket.getSiteId(), tid);

		list.addAll(listUnAssigned);

		list.stream().forEach(i -> {
			if (!listUnAssigned.contains(i)) {
				i.setSelected(true);
			}
		});

		commonResponse.setStatus(true);
		commonResponse.getPayload().add(list);
		return commonResponse;
	}

	@ResponseBody
	@PostMapping("/saveUpdate/{type}")
	public CommonResponse saveUpdate(@PathVariable("tid") String tid, @PathVariable("type") String type,
			@RequestBody List<AssignedUserDTO> users) {

		CommonResponse commonResponse = new CommonResponse();
		String userType;
		if (type.equals(UserType.MANAGER.getDtoValue())) {
			userType = UserType.MANAGER.getDbValue();
		} else if (type.equals(UserType.SUPERVISOR.getDtoValue())) {
			userType = UserType.SUPERVISOR.getDbValue();
		} else {
			userType = UserType.EMPLOYEE.getDbValue();
		}

		for (AssignedUserDTO assignedUserDTO : users) {
			ticketUserService.addTicketUser(String.valueOf(Snowflake.newId()), tid, assignedUserDTO.getId(), userType);
		}

		commonResponse.setStatus(true);
		return commonResponse;
	}

	@ResponseBody
	@PostMapping("/{id}/unassign")
	public CommonResponse unassign(@PathVariable("tid") String tid, @PathVariable("id") String id) {

		CommonResponse commonResponse = new CommonResponse();

		ticketUserService.deleteTicketUser(tid, id);

		commonResponse.setStatus(true);
		return commonResponse;
	}

	@ResponseBody
	@Transactional
	@PostMapping("/saveUpdate")
	public CommonResponse saveTicketSubmitData(@RequestBody String payload) throws JsonProcessingException {

		SessionUser currentUser = getCurrentUser();

		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		TicketUser ticketUser = objectMapper.readValue(payload, TicketUser.class);

		ticketUser.setContent(payload);
		ticketUser.setUserId(currentUser.getUserId());

		ticketUser.addUpdateDetails();

		ticketUserService.updateTicketUser(ticketUser.getId(), currentUser.getUserId(), ticketUser.getContent());

		TicketWithBLOBs ticket = ticketService.selectByPrimaryKeyWithBLOBs(ticketUser.getId());
		ticket.setTicketStatus(TicketStatus.COMPLETED.getDbValue());
		ticket.addUpdateDetails();
		CommonResponse commonResponse = ticketService.saveUpdate(ticket);
		ticket = (TicketWithBLOBs) commonResponse.getPayload().get(0);

		SimpleDateFormat dtf = new SimpleDateFormat("yyMMdd HH:mm");
		SimpleDateFormat df = new SimpleDateFormat("yyMMdd");
		SimpleDateFormat tf = new SimpleDateFormat("HH:mm");

		RLogColumnBean rowData = new RLogColumnBean();
		
		try {
			rowData.setId("R");
			rowData.setSpn(ticket.getSite().getSpn());
			rowData.setSpnSub("");
			rowData.setSiteName(ticket.getSite().getName());
			rowData.setTicketNumber(ticket.getTicketId());
			rowData.setStartDate(df.format(ticket.getCreatedOn()));
			rowData.setStartTime(tf.format(ticket.getCreatedOn()));
			String ticketOwner = ticket.getCreatedBy();
			
			/***
			if (ticket.getDescription() != null) {
				try {
					int lastIndex = ticket.getDescription().lastIndexOf("by ");
					ticketOwner = ticket.getDescription().substring(lastIndex + 1);
				} catch (Exception e) {
					LOGGER.error("saveTicketSubmitData() :: Find Ticket owner from Ticket description faild with Error : "
							+ e.getMessage());
				}
			}
			***/
	
			rowData.setTicketOwner(ticketOwner);
			
			TicketStatus ticketStatus = TicketStatus.getByDBValue(ticket.getTicketStatus());
			String ticketStatusDisplayName = ticketStatus != null ? ticketStatus.getDisplayName() : "";
			rowData.setStatus(ticketStatusDisplayName);
	
			objectMapper = new ObjectMapper();
			objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	
			EmployeeData employeeData = null;
			try {
				employeeData = objectMapper.convertValue(
						objectMapper.readTree(ticketUser.getContent()).get("template").get("tasks").get(0), EmployeeData.class);
			} catch (Exception e) {
				LOGGER.error(
						"saveTicketSubmitData() :: TicketUser content JSON parse faild with Error : " + e.getMessage());
			}
	
			if (employeeData != null) {
				rowData.setEndDate(df.format(new Date()));
				rowData.setEndTime(tf.format(new Date()));
				rowData.setEventName(employeeData.getEventName());
				rowData.setEventNumber(employeeData.getEventNumber());
				rowData.setEventDetail1(employeeData.getEventDetails01());
				rowData.setEventDetails2(employeeData.getEventDetails02());
				rowData.setPcsNumber(employeeData.getPcsNumber());
				rowData.setEquipment(employeeData.getEquipment());
				rowData.setNote(employeeData.getNote());
				rowData.setPicture(employeeData.getPicture());
			}
	
			User u = userService.selectByPrimaryKey(ticketUser.getUserId());
			if (u != null) {
				rowData.setEngineerName(u.getFirstName());
			}
		}catch (Exception e) {
			LOGGER.error(
					"saveTicketSubmitData() :: CSV raw data creation faild with Error : " + e.getMessage());
		}

		addRowToCSV(rowData);
		
		objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		
		try {			
			System.out.println(objectMapper.writeValueAsString(rowData));			
		} catch (Exception e) {
			LOGGER.error(
					"saveTicketSubmitData() :: rowData JSON parse faild with Error : " + e.getMessage());
		}

		commonResponse = new CommonResponse();
		commonResponse.setStatus(true);
		return commonResponse;
	}

	public void addRowToCSV(RLogColumnBean rowData) {
		try {
			CSVWriter writer = new CSVWriter(new FileWriter(rlogPath, true));

			/*
			 * String[] header = { "ID", "SPN", "SPN-Sub", "Site Name", "Ticket number",
			 * "Start Date", "Start Time", "Ticket Owner", "Status", "End Date", "End Time",
			 * "Event Name", "Event#", "Event Detail1", "Event Detail2", "PCS#",
			 * "Equipment", "Engineer Name", "Note", "Picture" };
			 */

			CustomMappingStrategy<RLogColumnBean> mappingStrategy = new CustomMappingStrategy<RLogColumnBean>();
			mappingStrategy.setType(RLogColumnBean.class);

			StatefulBeanToCsv<RLogColumnBean> beanToCsv = new StatefulBeanToCsvBuilder<RLogColumnBean>(writer)
					.withMappingStrategy(mappingStrategy).withSeparator(',').withApplyQuotesToAll(false).build();

			try {
				beanToCsv.write(rowData);
			} catch (CsvDataTypeMismatchException e) {
				LOGGER.error("addRowToCSV() :: RLog CSV data write faild with Error : " + e.getMessage());
			} catch (CsvRequiredFieldEmptyException e) {
				LOGGER.error("addRowToCSV() :: RLog CSV data write faild with Error : " + e.getMessage());
			} finally {
				writer.flush();
				writer.close();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
class EmployeeData {
	String eventName;
	String eventNumber;
	String eventDetails01;
	String eventDetails02;
	String pcsNumber;
	String equipment;
	String note;
	String picture;
}
