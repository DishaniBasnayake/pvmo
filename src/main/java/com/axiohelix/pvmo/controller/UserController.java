package com.axiohelix.pvmo.controller;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.axiohelix.pvmo.entity.User;
import com.axiohelix.pvmo.entity.UserExample;
import com.axiohelix.pvmo.entity.UserWithBLOBs;
import com.axiohelix.pvmo.service.UserService;
import com.axiohelix.pvmo.type.Status;
import com.axiohelix.pvmo.type.UserType;
import com.axiohelix.pvmo.util.CommonConstant;
import com.axiohelix.pvmo.util.CommonResponse;
import com.axiohelix.pvmo.util.CommonValidation;

@Controller
@RequestMapping(CommonConstant.CONTROLLER_URL_USER)
public class UserController {

	private static final Logger LOGGER = LogManager.getLogger(UserController.class);

	@Autowired
	private UserService userService;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@GetMapping("")
	public String index(Model model) {

		model.addAttribute("user", new UserWithBLOBs());
		return "user/list";
	}
	
	@ResponseBody
	@GetMapping("/data/{search}")
	public CommonResponse index(@PathVariable(value = "search") String search) {

		CommonResponse commonResponse = new CommonResponse();
		
		UserExample userExample = new UserExample();
		userExample.createCriteria().andStatusEqualTo(Status.ACTIVE.getDbValue().byteValue()).andCompanyIdIsNotNull();
		userExample.setOrderByClause("CREATED_ON DESC");
		
		List<UserWithBLOBs> list = userService.selectByExampleWithBLOBs(userExample);
		
		commonResponse.setStatus(true);
		commonResponse.getPayload().add(list);
		return commonResponse;
	}

	@GetMapping("/{id}")
	public String view(Model model, @PathVariable("id") String id) {
		UserWithBLOBs user = userService.selectByPrimaryKeyWithBLOBs(id);
		model.addAttribute("user", user);
		return "user/view";
	}

	@ResponseBody
	@GetMapping("/{id}/delete")
	public CommonResponse delete(Model model,@PathVariable("id") String id) {

		CommonResponse commonResponse = new CommonResponse();
		UserWithBLOBs user = userService.selectByPrimaryKeyWithBLOBs(id);
		if(user == null) {
			commonResponse.setStatus(false);
			commonResponse.getErrorMessages().add(CommonConstant.MSG_RECORD_NOT_FOUND);
			return commonResponse;
		}
		user.setStatus(Status.DELETED.getDbValue().byteValue());
		userService.updateByPrimaryKeySelective(user);
		
		commonResponse.setStatus(true);
		return commonResponse;
	}

	@ResponseBody
	@GetMapping("/{id}/data")
	public CommonResponse userData(@PathVariable("id") String id) {
		User user = userService.selectByPrimaryKey(id);
		CommonResponse commonResponse = new CommonResponse();
		commonResponse.setStatus(true);
		commonResponse.getPayload().add(user);
		return commonResponse;
	}

	@ResponseBody
	@PostMapping("/saveUpdate")
	public CommonResponse saveUpdate(@ModelAttribute("user") UserWithBLOBs user) {
		
		UserExample example = new UserExample();
		Optional<User> userEx = null;

		if (CommonValidation.stringNullValidation(user.getId())) {
			example.createCriteria().andCompanyIdEqualTo(user.getCompanyId()).andUserIdEqualTo(user.getUserId());
			userEx = userService.selectByExample(example).stream().findFirst();
		} else {
			User userPr = userService.selectByPrimaryKey(user.getId());
			example.createCriteria().andCompanyIdEqualTo(userPr.getCompanyId()).andUserIdEqualTo(user.getUserId())
					.andUserIdNotEqualTo(userPr.getUserId());
			userEx = userService.selectByExample(example).stream().findFirst();
		}

		if (userEx.isPresent()) {
			CommonResponse response = new CommonResponse();
			response.getErrorMessages().add(CommonConstant.MSG_USER_CODE + " " + userEx.get().getUserId()
					+ " " + CommonConstant.MSG_IS_ALREADY_EXISTS);
			return response;
		}
		user.setUsername(user.getFirstName());
		user.setUserType(UserType.USER.getDbValue());
		if (CommonValidation.stringNullValidation(user.getId())) {
			user.setPassword(bCryptPasswordEncoder.encode(CommonConstant.DEFAULT_PASSWORD));
		}
		return userService.saveUpdate(user);
	}

}
