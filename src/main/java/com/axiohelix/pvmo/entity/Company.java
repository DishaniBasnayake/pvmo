package com.axiohelix.pvmo.entity;

import java.util.Date;

public class Company extends GenericEntity{

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_company.ID
	 * @mbg.generated  Fri Oct 08 18:52:11 IST 2021
	 */
	private String id;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_company.CODE
	 * @mbg.generated  Fri Oct 08 18:52:11 IST 2021
	 */
	private String code;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_company.NAME
	 * @mbg.generated  Fri Oct 08 18:52:11 IST 2021
	 */
	private String name;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_company.KATAKANA_NAME
	 * @mbg.generated  Fri Oct 08 18:52:11 IST 2021
	 */
	private String katakanaName;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_company.CONTACT_NUMBER
	 * @mbg.generated  Fri Oct 08 18:52:11 IST 2021
	 */
	private String contactNumber;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_company.EMAIL
	 * @mbg.generated  Fri Oct 08 18:52:11 IST 2021
	 */
	private String email;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_company.STATUS
	 * @mbg.generated  Fri Oct 08 18:52:11 IST 2021
	 */
	private Byte status;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_company.SORT_ORDER
	 * @mbg.generated  Fri Oct 08 18:52:11 IST 2021
	 */
	private Integer sortOrder;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_company.CREATED_BY
	 * @mbg.generated  Fri Oct 08 18:52:11 IST 2021
	 */
	private String createdBy;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_company.CREATED_ON
	 * @mbg.generated  Fri Oct 08 18:52:11 IST 2021
	 */
	private Date createdOn;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_company.LAST_UPDATE_BY
	 * @mbg.generated  Fri Oct 08 18:52:11 IST 2021
	 */
	private String lastUpdateBy;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_company.LAST_UPDATE_ON
	 * @mbg.generated  Fri Oct 08 18:52:11 IST 2021
	 */
	private Date lastUpdateOn;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_company.ID
	 * @return  the value of tbl_company.ID
	 * @mbg.generated  Fri Oct 08 18:52:11 IST 2021
	 */
	public String getId() {
		return id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_company.ID
	 * @param id  the value for tbl_company.ID
	 * @mbg.generated  Fri Oct 08 18:52:11 IST 2021
	 */
	public void setId(String id) {
		this.id = id == null ? null : id.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_company.CODE
	 * @return  the value of tbl_company.CODE
	 * @mbg.generated  Fri Oct 08 18:52:11 IST 2021
	 */
	public String getCode() {
		return code;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_company.CODE
	 * @param code  the value for tbl_company.CODE
	 * @mbg.generated  Fri Oct 08 18:52:11 IST 2021
	 */
	public void setCode(String code) {
		this.code = code == null ? null : code.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_company.NAME
	 * @return  the value of tbl_company.NAME
	 * @mbg.generated  Fri Oct 08 18:52:11 IST 2021
	 */
	public String getName() {
		return name;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_company.NAME
	 * @param name  the value for tbl_company.NAME
	 * @mbg.generated  Fri Oct 08 18:52:11 IST 2021
	 */
	public void setName(String name) {
		this.name = name == null ? null : name.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_company.KATAKANA_NAME
	 * @return  the value of tbl_company.KATAKANA_NAME
	 * @mbg.generated  Fri Oct 08 18:52:11 IST 2021
	 */
	public String getKatakanaName() {
		return katakanaName;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_company.KATAKANA_NAME
	 * @param katakanaName  the value for tbl_company.KATAKANA_NAME
	 * @mbg.generated  Fri Oct 08 18:52:11 IST 2021
	 */
	public void setKatakanaName(String katakanaName) {
		this.katakanaName = katakanaName == null ? null : katakanaName.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_company.CONTACT_NUMBER
	 * @return  the value of tbl_company.CONTACT_NUMBER
	 * @mbg.generated  Fri Oct 08 18:52:11 IST 2021
	 */
	public String getContactNumber() {
		return contactNumber;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_company.CONTACT_NUMBER
	 * @param contactNumber  the value for tbl_company.CONTACT_NUMBER
	 * @mbg.generated  Fri Oct 08 18:52:11 IST 2021
	 */
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber == null ? null : contactNumber.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_company.EMAIL
	 * @return  the value of tbl_company.EMAIL
	 * @mbg.generated  Fri Oct 08 18:52:11 IST 2021
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_company.EMAIL
	 * @param email  the value for tbl_company.EMAIL
	 * @mbg.generated  Fri Oct 08 18:52:11 IST 2021
	 */
	public void setEmail(String email) {
		this.email = email == null ? null : email.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_company.STATUS
	 * @return  the value of tbl_company.STATUS
	 * @mbg.generated  Fri Oct 08 18:52:11 IST 2021
	 */
	public Byte getStatus() {
		return status;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_company.STATUS
	 * @param status  the value for tbl_company.STATUS
	 * @mbg.generated  Fri Oct 08 18:52:11 IST 2021
	 */
	public void setStatus(Byte status) {
		this.status = status;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_company.SORT_ORDER
	 * @return  the value of tbl_company.SORT_ORDER
	 * @mbg.generated  Fri Oct 08 18:52:11 IST 2021
	 */
	public Integer getSortOrder() {
		return sortOrder;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_company.SORT_ORDER
	 * @param sortOrder  the value for tbl_company.SORT_ORDER
	 * @mbg.generated  Fri Oct 08 18:52:11 IST 2021
	 */
	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_company.CREATED_BY
	 * @return  the value of tbl_company.CREATED_BY
	 * @mbg.generated  Fri Oct 08 18:52:11 IST 2021
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_company.CREATED_BY
	 * @param createdBy  the value for tbl_company.CREATED_BY
	 * @mbg.generated  Fri Oct 08 18:52:11 IST 2021
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy == null ? null : createdBy.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_company.CREATED_ON
	 * @return  the value of tbl_company.CREATED_ON
	 * @mbg.generated  Fri Oct 08 18:52:11 IST 2021
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_company.CREATED_ON
	 * @param createdOn  the value for tbl_company.CREATED_ON
	 * @mbg.generated  Fri Oct 08 18:52:11 IST 2021
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_company.LAST_UPDATE_BY
	 * @return  the value of tbl_company.LAST_UPDATE_BY
	 * @mbg.generated  Fri Oct 08 18:52:11 IST 2021
	 */
	public String getLastUpdateBy() {
		return lastUpdateBy;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_company.LAST_UPDATE_BY
	 * @param lastUpdateBy  the value for tbl_company.LAST_UPDATE_BY
	 * @mbg.generated  Fri Oct 08 18:52:11 IST 2021
	 */
	public void setLastUpdateBy(String lastUpdateBy) {
		this.lastUpdateBy = lastUpdateBy == null ? null : lastUpdateBy.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_company.LAST_UPDATE_ON
	 * @return  the value of tbl_company.LAST_UPDATE_ON
	 * @mbg.generated  Fri Oct 08 18:52:11 IST 2021
	 */
	public Date getLastUpdateOn() {
		return lastUpdateOn;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_company.LAST_UPDATE_ON
	 * @param lastUpdateOn  the value for tbl_company.LAST_UPDATE_ON
	 * @mbg.generated  Fri Oct 08 18:52:11 IST 2021
	 */
	public void setLastUpdateOn(Date lastUpdateOn) {
		this.lastUpdateOn = lastUpdateOn;
	}
		
}