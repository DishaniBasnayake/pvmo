package com.axiohelix.pvmo.entity;

public class ConsumerWithBLOBs extends Consumer {
   
    private String address;

    private String remark;

    
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

	
}