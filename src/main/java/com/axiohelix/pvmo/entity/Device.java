package com.axiohelix.pvmo.entity;

import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Getter
@Setter
@NoArgsConstructor
public class Device {
    private String id;
	private String siteId;
	private String code;
	private String objectId;
	private String longitude;
	private String latitude;
	private Byte status;
	private Integer sortOrder;
	private String objectType;
	private List<PCS> pcses;
    
}