package com.axiohelix.pvmo.entity;

import lombok.*;
import java.util.Date;

@Data
@Getter
@Setter

public class Facility {
	
	private String id;
	private String name;
	private Double latitude;
	private Double longitude;
	private Double altitude;
	private String minimumPowerGeneration;
	private String maximumPowerGeneration;
	private String alterLevel;
	private String criticalLevel;
	private String remark;
	private Byte status;
	private Integer sortOrder;
	private String createdBy;
	private Date createdOn;
	private String lastUpdateBy;
	private Date lastUpdateOn;
	
	

}
