package com.axiohelix.pvmo.entity;

import java.util.Date;

import com.axiohelix.pvmo.util.CommonFunctions;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GenericEntity {

	private String id;
	private Byte status;
	private String createdBy;
	private Date createdOn;
	private String lastUpdateBy;
	private Date lastUpdateOn;
	
	public void addInsertDetails() {
		this.setCreatedOn(new Date());
		this.setCreatedBy(CommonFunctions.getCurrentUserName());
	}
	
	public void addUpdateDetails() {
		this.setLastUpdateOn(new Date());
		this.setLastUpdateBy(CommonFunctions.getCurrentUserName());
	}

}
