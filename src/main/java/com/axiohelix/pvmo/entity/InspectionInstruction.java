package com.axiohelix.pvmo.entity;

import lombok.*;
import java.util.Date;

import com.mysql.cj.jdbc.Blob;

@Data
@Getter
@Setter

public class InspectionInstruction {

	private String id;
	private String title;
	private Blob stringMap;
	private String abnormalityContent;
	private String causeOfAbnormality;
	private String numberOfNotificationReported;
	private String telephoneNumber;
	private String remark;
	private Byte status;
	private Integer sortOrder;
	private String createdBy;
	private Date createdOn;
	private String lastUpdateBy;
	private Date lastUpdateOn;
}
