package com.axiohelix.pvmo.entity;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Getter
@Setter
@NoArgsConstructor
public class JCB {
    private String id;
	private String pcsId;
	private String code;
	private String objectId;
	private Byte status;
	private Integer sortOrder;
	private List<PanelString> panelStrings;
}