package com.axiohelix.pvmo.entity;

import java.util.Date;

import lombok.*;
import java.util.Date;

@Data
@Getter
@Setter

public class Log {

	private String id;
	private String actionName;
	private String actionState;
	private String accessHost;
	private String remark;
	private Byte status;
	private Integer sortOrder;
	private String createdBy;
	private Date createdOn;
	private String lastUpdateBy;
	private Date lastUpdateOn;
}
