package com.axiohelix.pvmo.entity;

import lombok.*;
import java.util.Date;

@Data
@Getter
@Setter

public class MoniteringCenter extends User {

	private String id;
	private String location;
	private String address;
	private String remark;
	private Byte status;
	private Integer sortOrder;
	private String createdBy;
	private Date createdOn;
	private String lastUpdateBy;
	private Date lastUpdateOn;
}
