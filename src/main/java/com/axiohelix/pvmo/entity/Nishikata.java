package com.axiohelix.pvmo.entity;

public class Nishikata {
    private Integer id;

    private Integer pcs;

    private Integer jcb;

    private Integer str;

    private Integer panel;

    private Boolean errStr;

    private Boolean errAi;

    private Boolean errEtc;

    private String remarks;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPcs() {
        return pcs;
    }

    public void setPcs(Integer pcs) {
        this.pcs = pcs;
    }

    public Integer getJcb() {
        return jcb;
    }

    public void setJcb(Integer jcb) {
        this.jcb = jcb;
    }

    public Integer getStr() {
        return str;
    }

    public void setStr(Integer str) {
        this.str = str;
    }

    public Integer getPanel() {
        return panel;
    }

    public void setPanel(Integer panel) {
        this.panel = panel;
    }

    public Boolean getErrStr() {
        return errStr;
    }

    public void setErrStr(Boolean errStr) {
        this.errStr = errStr;
    }

    public Boolean getErrAi() {
        return errAi;
    }

    public void setErrAi(Boolean errAi) {
        this.errAi = errAi;
    }

    public Boolean getErrEtc() {
        return errEtc;
    }

    public void setErrEtc(Boolean errEtc) {
        this.errEtc = errEtc;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks == null ? null : remarks.trim();
    }
}