package com.axiohelix.pvmo.entity;

import java.util.ArrayList;
import java.util.List;

public class NishikataExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public NishikataExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andPcsIsNull() {
            addCriterion("pcs is null");
            return (Criteria) this;
        }

        public Criteria andPcsIsNotNull() {
            addCriterion("pcs is not null");
            return (Criteria) this;
        }

        public Criteria andPcsEqualTo(Integer value) {
            addCriterion("pcs =", value, "pcs");
            return (Criteria) this;
        }

        public Criteria andPcsNotEqualTo(Integer value) {
            addCriterion("pcs <>", value, "pcs");
            return (Criteria) this;
        }

        public Criteria andPcsGreaterThan(Integer value) {
            addCriterion("pcs >", value, "pcs");
            return (Criteria) this;
        }

        public Criteria andPcsGreaterThanOrEqualTo(Integer value) {
            addCriterion("pcs >=", value, "pcs");
            return (Criteria) this;
        }

        public Criteria andPcsLessThan(Integer value) {
            addCriterion("pcs <", value, "pcs");
            return (Criteria) this;
        }

        public Criteria andPcsLessThanOrEqualTo(Integer value) {
            addCriterion("pcs <=", value, "pcs");
            return (Criteria) this;
        }

        public Criteria andPcsIn(List<Integer> values) {
            addCriterion("pcs in", values, "pcs");
            return (Criteria) this;
        }

        public Criteria andPcsNotIn(List<Integer> values) {
            addCriterion("pcs not in", values, "pcs");
            return (Criteria) this;
        }

        public Criteria andPcsBetween(Integer value1, Integer value2) {
            addCriterion("pcs between", value1, value2, "pcs");
            return (Criteria) this;
        }

        public Criteria andPcsNotBetween(Integer value1, Integer value2) {
            addCriterion("pcs not between", value1, value2, "pcs");
            return (Criteria) this;
        }

        public Criteria andJcbIsNull() {
            addCriterion("jcb is null");
            return (Criteria) this;
        }

        public Criteria andJcbIsNotNull() {
            addCriterion("jcb is not null");
            return (Criteria) this;
        }

        public Criteria andJcbEqualTo(Integer value) {
            addCriterion("jcb =", value, "jcb");
            return (Criteria) this;
        }

        public Criteria andJcbNotEqualTo(Integer value) {
            addCriterion("jcb <>", value, "jcb");
            return (Criteria) this;
        }

        public Criteria andJcbGreaterThan(Integer value) {
            addCriterion("jcb >", value, "jcb");
            return (Criteria) this;
        }

        public Criteria andJcbGreaterThanOrEqualTo(Integer value) {
            addCriterion("jcb >=", value, "jcb");
            return (Criteria) this;
        }

        public Criteria andJcbLessThan(Integer value) {
            addCriterion("jcb <", value, "jcb");
            return (Criteria) this;
        }

        public Criteria andJcbLessThanOrEqualTo(Integer value) {
            addCriterion("jcb <=", value, "jcb");
            return (Criteria) this;
        }

        public Criteria andJcbIn(List<Integer> values) {
            addCriterion("jcb in", values, "jcb");
            return (Criteria) this;
        }

        public Criteria andJcbNotIn(List<Integer> values) {
            addCriterion("jcb not in", values, "jcb");
            return (Criteria) this;
        }

        public Criteria andJcbBetween(Integer value1, Integer value2) {
            addCriterion("jcb between", value1, value2, "jcb");
            return (Criteria) this;
        }

        public Criteria andJcbNotBetween(Integer value1, Integer value2) {
            addCriterion("jcb not between", value1, value2, "jcb");
            return (Criteria) this;
        }

        public Criteria andStrIsNull() {
            addCriterion("str is null");
            return (Criteria) this;
        }

        public Criteria andStrIsNotNull() {
            addCriterion("str is not null");
            return (Criteria) this;
        }

        public Criteria andStrEqualTo(Integer value) {
            addCriterion("str =", value, "str");
            return (Criteria) this;
        }

        public Criteria andStrNotEqualTo(Integer value) {
            addCriterion("str <>", value, "str");
            return (Criteria) this;
        }

        public Criteria andStrGreaterThan(Integer value) {
            addCriterion("str >", value, "str");
            return (Criteria) this;
        }

        public Criteria andStrGreaterThanOrEqualTo(Integer value) {
            addCriterion("str >=", value, "str");
            return (Criteria) this;
        }

        public Criteria andStrLessThan(Integer value) {
            addCriterion("str <", value, "str");
            return (Criteria) this;
        }

        public Criteria andStrLessThanOrEqualTo(Integer value) {
            addCriterion("str <=", value, "str");
            return (Criteria) this;
        }

        public Criteria andStrIn(List<Integer> values) {
            addCriterion("str in", values, "str");
            return (Criteria) this;
        }

        public Criteria andStrNotIn(List<Integer> values) {
            addCriterion("str not in", values, "str");
            return (Criteria) this;
        }

        public Criteria andStrBetween(Integer value1, Integer value2) {
            addCriterion("str between", value1, value2, "str");
            return (Criteria) this;
        }

        public Criteria andStrNotBetween(Integer value1, Integer value2) {
            addCriterion("str not between", value1, value2, "str");
            return (Criteria) this;
        }

        public Criteria andPanelIsNull() {
            addCriterion("panel is null");
            return (Criteria) this;
        }

        public Criteria andPanelIsNotNull() {
            addCriterion("panel is not null");
            return (Criteria) this;
        }

        public Criteria andPanelEqualTo(Integer value) {
            addCriterion("panel =", value, "panel");
            return (Criteria) this;
        }

        public Criteria andPanelNotEqualTo(Integer value) {
            addCriterion("panel <>", value, "panel");
            return (Criteria) this;
        }

        public Criteria andPanelGreaterThan(Integer value) {
            addCriterion("panel >", value, "panel");
            return (Criteria) this;
        }

        public Criteria andPanelGreaterThanOrEqualTo(Integer value) {
            addCriterion("panel >=", value, "panel");
            return (Criteria) this;
        }

        public Criteria andPanelLessThan(Integer value) {
            addCriterion("panel <", value, "panel");
            return (Criteria) this;
        }

        public Criteria andPanelLessThanOrEqualTo(Integer value) {
            addCriterion("panel <=", value, "panel");
            return (Criteria) this;
        }

        public Criteria andPanelIn(List<Integer> values) {
            addCriterion("panel in", values, "panel");
            return (Criteria) this;
        }

        public Criteria andPanelNotIn(List<Integer> values) {
            addCriterion("panel not in", values, "panel");
            return (Criteria) this;
        }

        public Criteria andPanelBetween(Integer value1, Integer value2) {
            addCriterion("panel between", value1, value2, "panel");
            return (Criteria) this;
        }

        public Criteria andPanelNotBetween(Integer value1, Integer value2) {
            addCriterion("panel not between", value1, value2, "panel");
            return (Criteria) this;
        }

        public Criteria andErrStrIsNull() {
            addCriterion("err_str is null");
            return (Criteria) this;
        }

        public Criteria andErrStrIsNotNull() {
            addCriterion("err_str is not null");
            return (Criteria) this;
        }

        public Criteria andErrStrEqualTo(Boolean value) {
            addCriterion("err_str =", value, "errStr");
            return (Criteria) this;
        }

        public Criteria andErrStrNotEqualTo(Boolean value) {
            addCriterion("err_str <>", value, "errStr");
            return (Criteria) this;
        }

        public Criteria andErrStrGreaterThan(Boolean value) {
            addCriterion("err_str >", value, "errStr");
            return (Criteria) this;
        }

        public Criteria andErrStrGreaterThanOrEqualTo(Boolean value) {
            addCriterion("err_str >=", value, "errStr");
            return (Criteria) this;
        }

        public Criteria andErrStrLessThan(Boolean value) {
            addCriterion("err_str <", value, "errStr");
            return (Criteria) this;
        }

        public Criteria andErrStrLessThanOrEqualTo(Boolean value) {
            addCriterion("err_str <=", value, "errStr");
            return (Criteria) this;
        }

        public Criteria andErrStrIn(List<Boolean> values) {
            addCriterion("err_str in", values, "errStr");
            return (Criteria) this;
        }

        public Criteria andErrStrNotIn(List<Boolean> values) {
            addCriterion("err_str not in", values, "errStr");
            return (Criteria) this;
        }

        public Criteria andErrStrBetween(Boolean value1, Boolean value2) {
            addCriterion("err_str between", value1, value2, "errStr");
            return (Criteria) this;
        }

        public Criteria andErrStrNotBetween(Boolean value1, Boolean value2) {
            addCriterion("err_str not between", value1, value2, "errStr");
            return (Criteria) this;
        }

        public Criteria andErrAiIsNull() {
            addCriterion("err_ai is null");
            return (Criteria) this;
        }

        public Criteria andErrAiIsNotNull() {
            addCriterion("err_ai is not null");
            return (Criteria) this;
        }

        public Criteria andErrAiEqualTo(Boolean value) {
            addCriterion("err_ai =", value, "errAi");
            return (Criteria) this;
        }

        public Criteria andErrAiNotEqualTo(Boolean value) {
            addCriterion("err_ai <>", value, "errAi");
            return (Criteria) this;
        }

        public Criteria andErrAiGreaterThan(Boolean value) {
            addCriterion("err_ai >", value, "errAi");
            return (Criteria) this;
        }

        public Criteria andErrAiGreaterThanOrEqualTo(Boolean value) {
            addCriterion("err_ai >=", value, "errAi");
            return (Criteria) this;
        }

        public Criteria andErrAiLessThan(Boolean value) {
            addCriterion("err_ai <", value, "errAi");
            return (Criteria) this;
        }

        public Criteria andErrAiLessThanOrEqualTo(Boolean value) {
            addCriterion("err_ai <=", value, "errAi");
            return (Criteria) this;
        }

        public Criteria andErrAiIn(List<Boolean> values) {
            addCriterion("err_ai in", values, "errAi");
            return (Criteria) this;
        }

        public Criteria andErrAiNotIn(List<Boolean> values) {
            addCriterion("err_ai not in", values, "errAi");
            return (Criteria) this;
        }

        public Criteria andErrAiBetween(Boolean value1, Boolean value2) {
            addCriterion("err_ai between", value1, value2, "errAi");
            return (Criteria) this;
        }

        public Criteria andErrAiNotBetween(Boolean value1, Boolean value2) {
            addCriterion("err_ai not between", value1, value2, "errAi");
            return (Criteria) this;
        }

        public Criteria andErrEtcIsNull() {
            addCriterion("err_etc is null");
            return (Criteria) this;
        }

        public Criteria andErrEtcIsNotNull() {
            addCriterion("err_etc is not null");
            return (Criteria) this;
        }

        public Criteria andErrEtcEqualTo(Boolean value) {
            addCriterion("err_etc =", value, "errEtc");
            return (Criteria) this;
        }

        public Criteria andErrEtcNotEqualTo(Boolean value) {
            addCriterion("err_etc <>", value, "errEtc");
            return (Criteria) this;
        }

        public Criteria andErrEtcGreaterThan(Boolean value) {
            addCriterion("err_etc >", value, "errEtc");
            return (Criteria) this;
        }

        public Criteria andErrEtcGreaterThanOrEqualTo(Boolean value) {
            addCriterion("err_etc >=", value, "errEtc");
            return (Criteria) this;
        }

        public Criteria andErrEtcLessThan(Boolean value) {
            addCriterion("err_etc <", value, "errEtc");
            return (Criteria) this;
        }

        public Criteria andErrEtcLessThanOrEqualTo(Boolean value) {
            addCriterion("err_etc <=", value, "errEtc");
            return (Criteria) this;
        }

        public Criteria andErrEtcIn(List<Boolean> values) {
            addCriterion("err_etc in", values, "errEtc");
            return (Criteria) this;
        }

        public Criteria andErrEtcNotIn(List<Boolean> values) {
            addCriterion("err_etc not in", values, "errEtc");
            return (Criteria) this;
        }

        public Criteria andErrEtcBetween(Boolean value1, Boolean value2) {
            addCriterion("err_etc between", value1, value2, "errEtc");
            return (Criteria) this;
        }

        public Criteria andErrEtcNotBetween(Boolean value1, Boolean value2) {
            addCriterion("err_etc not between", value1, value2, "errEtc");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}