package com.axiohelix.pvmo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Getter
@Setter
@NoArgsConstructor
public class Panel {
    private String id;
	private String code;
	private String objectId;
	private String panelStringId;
	private String deviceId;
	private Byte status;
	private Integer sortOrder;
}