package com.axiohelix.pvmo.entity;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Getter
@Setter
@NoArgsConstructor
public class PanelString {
    private String id;
	private String jcbId;
	private String code;
	private String objectId;
	private Byte status;
	private Integer sortOrder;
	private List<Panel> panels;
}