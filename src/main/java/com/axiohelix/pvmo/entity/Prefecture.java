package com.axiohelix.pvmo.entity;

import java.util.Date;

public class Prefecture extends GenericEntity {
    
	private String id;

	private String regionId;

    private String prefecture;

    private Byte status;

    private Integer sortOrder;

    private String createdBy;

    private Date createdOn;

    private String lastUpdateBy;

    private Date lastUpdateOn;

    private String remark;

    public String getId() {
        return id;
    }

   public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId == null ? null : regionId.trim();
    }

    public String getPrefecture() {
        return prefecture;
    }

    public void setPrefecture(String prefecture) {
        this.prefecture = prefecture == null ? null : prefecture.trim();
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy == null ? null : createdBy.trim();
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getLastUpdateBy() {
        return lastUpdateBy;
    }

    
    public void setLastUpdateBy(String lastUpdateBy) {
        this.lastUpdateBy = lastUpdateBy == null ? null : lastUpdateBy.trim();
    }

    
    public Date getLastUpdateOn() {
        return lastUpdateOn;
    }

    
    public void setLastUpdateOn(Date lastUpdateOn) {
        this.lastUpdateOn = lastUpdateOn;
    }

   
    public String getRemark() {
        return remark;
    }

    
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }
}