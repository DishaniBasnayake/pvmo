package com.axiohelix.pvmo.entity;

import java.util.Date;

public class Schedule {

	private String id;
	private String inspectorId;
	private String title;
	private String content;
	private String location;
	private Date scheduleDate;
	private String startTime;
	private String endTime;
	private String remark;
	private Byte status;
	private Integer sortOrder;
	private String createdBy;
	private Date createdOn;
	private String lastUpdateBy;
	private Date lastUpdateOn;
}