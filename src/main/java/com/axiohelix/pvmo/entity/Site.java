package com.axiohelix.pvmo.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Site extends GenericEntity{
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_site.ID
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	private String id;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_site.COMPANY_ID
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	private String companyId;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_site.CODE
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	private String code;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_site.URL
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	private String url;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_site.CAMERA_URL
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	private String cameraUrl;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_site.USERNAME
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	private String username;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_site.PASSWORD
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	private String password;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_site.NAME
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	private String name;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_site.KANA_NAME
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	private String kanaName;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_site.SPN
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	private String spn;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_site.POST_CODE
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	private String postCode;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_site.CONTACT_NUMBER
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	private String contactNumber;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_site.EMAIL
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	private String email;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_site.REGION
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	private String region;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_site.PREFECTURE
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	private String prefecture;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_site.STATUS
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	private Byte status;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_site.SORT_ORDER
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	private Integer sortOrder;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_site.CREATED_BY
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	private String createdBy;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_site.CREATED_ON
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	private Date createdOn;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_site.LAST_UPDATE_BY
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	private String lastUpdateBy;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_site.LAST_UPDATE_ON
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	private Date lastUpdateOn;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_site.LATITUDE
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	private Double latitude;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_site.LONGITUDE
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	private Double longitude;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_site.ID
	 * @return  the value of tbl_site.ID
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public String getId() {
		return id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_site.ID
	 * @param id  the value for tbl_site.ID
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public void setId(String id) {
		this.id = id == null ? null : id.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_site.COMPANY_ID
	 * @return  the value of tbl_site.COMPANY_ID
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public String getCompanyId() {
		return companyId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_site.COMPANY_ID
	 * @param companyId  the value for tbl_site.COMPANY_ID
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public void setCompanyId(String companyId) {
		this.companyId = companyId == null ? null : companyId.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_site.CODE
	 * @return  the value of tbl_site.CODE
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public String getCode() {
		return code;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_site.CODE
	 * @param code  the value for tbl_site.CODE
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public void setCode(String code) {
		this.code = code == null ? null : code.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_site.URL
	 * @return  the value of tbl_site.URL
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_site.URL
	 * @param url  the value for tbl_site.URL
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public void setUrl(String url) {
		this.url = url == null ? null : url.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_site.CAMERA_URL
	 * @return  the value of tbl_site.CAMERA_URL
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public String getCameraUrl() {
		return cameraUrl;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_site.CAMERA_URL
	 * @param cameraUrl  the value for tbl_site.CAMERA_URL
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public void setCameraUrl(String cameraUrl) {
		this.cameraUrl = cameraUrl == null ? null : cameraUrl.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_site.USERNAME
	 * @return  the value of tbl_site.USERNAME
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_site.USERNAME
	 * @param username  the value for tbl_site.USERNAME
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public void setUsername(String username) {
		this.username = username == null ? null : username.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_site.PASSWORD
	 * @return  the value of tbl_site.PASSWORD
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_site.PASSWORD
	 * @param password  the value for tbl_site.PASSWORD
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public void setPassword(String password) {
		this.password = password == null ? null : password.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_site.NAME
	 * @return  the value of tbl_site.NAME
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public String getName() {
		return name;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_site.NAME
	 * @param name  the value for tbl_site.NAME
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public void setName(String name) {
		this.name = name == null ? null : name.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_site.KANA_NAME
	 * @return  the value of tbl_site.KANA_NAME
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public String getKanaName() {
		return kanaName;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_site.KANA_NAME
	 * @param kanaName  the value for tbl_site.KANA_NAME
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public void setKanaName(String kanaName) {
		this.kanaName = kanaName == null ? null : kanaName.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_site.SPN
	 * @return  the value of tbl_site.SPN
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public String getSpn() {
		return spn;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_site.SPN
	 * @param spn  the value for tbl_site.SPN
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public void setSpn(String spn) {
		this.spn = spn == null ? null : spn.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_site.POST_CODE
	 * @return  the value of tbl_site.POST_CODE
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public String getPostCode() {
		return postCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_site.POST_CODE
	 * @param postCode  the value for tbl_site.POST_CODE
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public void setPostCode(String postCode) {
		this.postCode = postCode == null ? null : postCode.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_site.CONTACT_NUMBER
	 * @return  the value of tbl_site.CONTACT_NUMBER
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public String getContactNumber() {
		return contactNumber;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_site.CONTACT_NUMBER
	 * @param contactNumber  the value for tbl_site.CONTACT_NUMBER
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber == null ? null : contactNumber.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_site.EMAIL
	 * @return  the value of tbl_site.EMAIL
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_site.EMAIL
	 * @param email  the value for tbl_site.EMAIL
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public void setEmail(String email) {
		this.email = email == null ? null : email.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_site.REGION
	 * @return  the value of tbl_site.REGION
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public String getRegion() {
		return region;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_site.REGION
	 * @param region  the value for tbl_site.REGION
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public void setRegion(String region) {
		this.region = region == null ? null : region.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_site.PREFECTURE
	 * @return  the value of tbl_site.PREFECTURE
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public String getPrefecture() {
		return prefecture;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_site.PREFECTURE
	 * @param prefecture  the value for tbl_site.PREFECTURE
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public void setPrefecture(String prefecture) {
		this.prefecture = prefecture == null ? null : prefecture.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_site.STATUS
	 * @return  the value of tbl_site.STATUS
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public Byte getStatus() {
		return status;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_site.STATUS
	 * @param status  the value for tbl_site.STATUS
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public void setStatus(Byte status) {
		this.status = status;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_site.SORT_ORDER
	 * @return  the value of tbl_site.SORT_ORDER
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public Integer getSortOrder() {
		return sortOrder;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_site.SORT_ORDER
	 * @param sortOrder  the value for tbl_site.SORT_ORDER
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_site.CREATED_BY
	 * @return  the value of tbl_site.CREATED_BY
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_site.CREATED_BY
	 * @param createdBy  the value for tbl_site.CREATED_BY
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy == null ? null : createdBy.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_site.CREATED_ON
	 * @return  the value of tbl_site.CREATED_ON
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_site.CREATED_ON
	 * @param createdOn  the value for tbl_site.CREATED_ON
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_site.LAST_UPDATE_BY
	 * @return  the value of tbl_site.LAST_UPDATE_BY
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public String getLastUpdateBy() {
		return lastUpdateBy;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_site.LAST_UPDATE_BY
	 * @param lastUpdateBy  the value for tbl_site.LAST_UPDATE_BY
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public void setLastUpdateBy(String lastUpdateBy) {
		this.lastUpdateBy = lastUpdateBy == null ? null : lastUpdateBy.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_site.LAST_UPDATE_ON
	 * @return  the value of tbl_site.LAST_UPDATE_ON
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public Date getLastUpdateOn() {
		return lastUpdateOn;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_site.LAST_UPDATE_ON
	 * @param lastUpdateOn  the value for tbl_site.LAST_UPDATE_ON
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public void setLastUpdateOn(Date lastUpdateOn) {
		this.lastUpdateOn = lastUpdateOn;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_site.LATITUDE
	 * @return  the value of tbl_site.LATITUDE
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public Double getLatitude() {
		return latitude;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_site.LATITUDE
	 * @param latitude  the value for tbl_site.LATITUDE
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_site.LONGITUDE
	 * @return  the value of tbl_site.LONGITUDE
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public Double getLongitude() {
		return longitude;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_site.LONGITUDE
	 * @param longitude  the value for tbl_site.LONGITUDE
	 * @mbg.generated  Fri Apr 29 11:46:08 IST 2022
	 */
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	private List<ServiceCenter> serviceCenters = new ArrayList<ServiceCenter>();
	private Company company;
	private List<User> companions = new ArrayList<User>();
	private List<Ticket> tickets = new ArrayList<Ticket>();
	private Region siteRegion;

	public List<ServiceCenter> getServiceCenters() {
		return serviceCenters;
	}

	public void setServiceCenters(List<ServiceCenter> serviceCenters) {
		this.serviceCenters = serviceCenters;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public String getDisplayName() {
		return this.code + "-" + this.name;
	}

	public List<User> getCompanions() {
		return companions;
	}

	public void setCompanions(List<User> companions) {
		this.companions = companions;
	}

	public List<Ticket> getTickets() {
		return tickets;
	}

	public void setTickets(List<Ticket> tickets) {
		this.tickets = tickets;
	}

	public Region getSiteRegion() {
		return siteRegion;
	}

	public void setSiteRegion(Region siteRegion) {
		this.siteRegion = siteRegion;
	}
}