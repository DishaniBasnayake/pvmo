package com.axiohelix.pvmo.entity;

public class SiteMonitoringStructureWithBLOBs extends SiteMonitoringStructure {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbl_site_monitoring_structure.FACILITY_MAINTENANCE
     *
     * @mbg.generated Tue Nov 16 10:53:14 IST 2021
     */
    private String facilityMaintenance;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbl_site_monitoring_structure.REMARK
     *
     * @mbg.generated Tue Nov 16 10:53:14 IST 2021
     */
    private String remark;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbl_site_monitoring_structure.FACILITY_MAINTENANCE
     *
     * @return the value of tbl_site_monitoring_structure.FACILITY_MAINTENANCE
     *
     * @mbg.generated Tue Nov 16 10:53:14 IST 2021
     */
    public String getFacilityMaintenance() {
        return facilityMaintenance;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbl_site_monitoring_structure.FACILITY_MAINTENANCE
     *
     * @param facilityMaintenance the value for tbl_site_monitoring_structure.FACILITY_MAINTENANCE
     *
     * @mbg.generated Tue Nov 16 10:53:14 IST 2021
     */
    public void setFacilityMaintenance(String facilityMaintenance) {
        this.facilityMaintenance = facilityMaintenance == null ? null : facilityMaintenance.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbl_site_monitoring_structure.REMARK
     *
     * @return the value of tbl_site_monitoring_structure.REMARK
     *
     * @mbg.generated Tue Nov 16 10:53:14 IST 2021
     */
    public String getRemark() {
        return remark;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbl_site_monitoring_structure.REMARK
     *
     * @param remark the value for tbl_site_monitoring_structure.REMARK
     *
     * @mbg.generated Tue Nov 16 10:53:14 IST 2021
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }
}