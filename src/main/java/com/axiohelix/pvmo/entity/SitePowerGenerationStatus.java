package com.axiohelix.pvmo.entity;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class SitePowerGenerationStatus extends GenericEntity {
  
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_site_power_generation_status.ID
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	private String id;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_site_power_generation_status.SITE_ID
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	private String siteId;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_site_power_generation_status.ACTUAL_GENERATION_AMOUNT
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	private String actualGenerationAmount;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_site_power_generation_status.AI_PREDICTION
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	private String aiPrediction;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_site_power_generation_status.SUSPENDED_PCS
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	private String suspendedPcs;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_site_power_generation_status.PCS_LINK
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	private String pcsLink;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_site_power_generation_status.STATUS
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	private Byte status;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_site_power_generation_status.SORT_ORDER
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	private Integer sortOrder;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_site_power_generation_status.CREATED_BY
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	private String createdBy;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_site_power_generation_status.CREATED_ON
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	private Date createdOn;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_site_power_generation_status.LAST_UPDATE_BY
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	private String lastUpdateBy;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_site_power_generation_status.LAST_UPDATE_ON
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	private Date lastUpdateOn;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_site_power_generation_status.REMARK
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	private String remark;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbl_site_power_generation_status.DATE
     *
     * @mbg.generated Sun Nov 07 20:54:38 IST 2021
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date date;

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_site_power_generation_status.ID
	 * @param id  the value for tbl_site_power_generation_status.ID
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	public void setId(String id) {
		this.id = id == null ? null : id.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_site_power_generation_status.SITE_ID
	 * @return  the value of tbl_site_power_generation_status.SITE_ID
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	public String getSiteId() {
		return siteId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_site_power_generation_status.SITE_ID
	 * @param siteId  the value for tbl_site_power_generation_status.SITE_ID
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	public void setSiteId(String siteId) {
		this.siteId = siteId == null ? null : siteId.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_site_power_generation_status.DATE
	 * @return  the value of tbl_site_power_generation_status.DATE
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_site_power_generation_status.DATE
	 * @param date  the value for tbl_site_power_generation_status.DATE
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_site_power_generation_status.ACTUAL_GENERATION_AMOUNT
	 * @return  the value of tbl_site_power_generation_status.ACTUAL_GENERATION_AMOUNT
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	public String getActualGenerationAmount() {
		return actualGenerationAmount;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_site_power_generation_status.ACTUAL_GENERATION_AMOUNT
	 * @param actualGenerationAmount  the value for tbl_site_power_generation_status.ACTUAL_GENERATION_AMOUNT
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	public void setActualGenerationAmount(String actualGenerationAmount) {
		this.actualGenerationAmount = actualGenerationAmount == null ? null : actualGenerationAmount.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_site_power_generation_status.AI_PREDICTION
	 * @return  the value of tbl_site_power_generation_status.AI_PREDICTION
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	public String getAiPrediction() {
		return aiPrediction;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_site_power_generation_status.AI_PREDICTION
	 * @param aiPrediction  the value for tbl_site_power_generation_status.AI_PREDICTION
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	public void setAiPrediction(String aiPrediction) {
		this.aiPrediction = aiPrediction == null ? null : aiPrediction.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_site_power_generation_status.SUSPENDED_PCS
	 * @return  the value of tbl_site_power_generation_status.SUSPENDED_PCS
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	public String getSuspendedPcs() {
		return suspendedPcs;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_site_power_generation_status.SUSPENDED_PCS
	 * @param suspendedPcs  the value for tbl_site_power_generation_status.SUSPENDED_PCS
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	public void setSuspendedPcs(String suspendedPcs) {
		this.suspendedPcs = suspendedPcs == null ? null : suspendedPcs.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_site_power_generation_status.PCS_LINK
	 * @return  the value of tbl_site_power_generation_status.PCS_LINK
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	public String getPcsLink() {
		return pcsLink;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_site_power_generation_status.PCS_LINK
	 * @param pcsLink  the value for tbl_site_power_generation_status.PCS_LINK
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	public void setPcsLink(String pcsLink) {
		this.pcsLink = pcsLink == null ? null : pcsLink.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_site_power_generation_status.STATUS
	 * @return  the value of tbl_site_power_generation_status.STATUS
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	public Byte getStatus() {
		return status;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_site_power_generation_status.STATUS
	 * @param status  the value for tbl_site_power_generation_status.STATUS
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	public void setStatus(Byte status) {
		this.status = status;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_site_power_generation_status.SORT_ORDER
	 * @return  the value of tbl_site_power_generation_status.SORT_ORDER
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	public Integer getSortOrder() {
		return sortOrder;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_site_power_generation_status.SORT_ORDER
	 * @param sortOrder  the value for tbl_site_power_generation_status.SORT_ORDER
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_site_power_generation_status.CREATED_BY
	 * @return  the value of tbl_site_power_generation_status.CREATED_BY
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_site_power_generation_status.CREATED_BY
	 * @param createdBy  the value for tbl_site_power_generation_status.CREATED_BY
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy == null ? null : createdBy.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_site_power_generation_status.CREATED_ON
	 * @return  the value of tbl_site_power_generation_status.CREATED_ON
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_site_power_generation_status.CREATED_ON
	 * @param createdOn  the value for tbl_site_power_generation_status.CREATED_ON
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_site_power_generation_status.LAST_UPDATE_BY
	 * @return  the value of tbl_site_power_generation_status.LAST_UPDATE_BY
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	public String getLastUpdateBy() {
		return lastUpdateBy;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_site_power_generation_status.LAST_UPDATE_BY
	 * @param lastUpdateBy  the value for tbl_site_power_generation_status.LAST_UPDATE_BY
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	public void setLastUpdateBy(String lastUpdateBy) {
		this.lastUpdateBy = lastUpdateBy == null ? null : lastUpdateBy.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_site_power_generation_status.LAST_UPDATE_ON
	 * @return  the value of tbl_site_power_generation_status.LAST_UPDATE_ON
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	public Date getLastUpdateOn() {
		return lastUpdateOn;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_site_power_generation_status.LAST_UPDATE_ON
	 * @param lastUpdateOn  the value for tbl_site_power_generation_status.LAST_UPDATE_ON
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	public void setLastUpdateOn(Date lastUpdateOn) {
		this.lastUpdateOn = lastUpdateOn;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_site_power_generation_status.REMARK
	 * @return  the value of tbl_site_power_generation_status.REMARK
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_site_power_generation_status.REMARK
	 * @param remark  the value for tbl_site_power_generation_status.REMARK
	 * @mbg.generated  Tue Nov 16 14:29:13 IST 2021
	 */
	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}
}