package com.axiohelix.pvmo.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TicketExample extends GenericEntity{
    /**
	 * This field was generated by MyBatis Generator. This field corresponds to the database table tbl_ticket
	 * @mbg.generated  Mon Jan 17 12:05:40 IST 2022
	 */
	protected String orderByClause;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database table tbl_ticket
	 * @mbg.generated  Mon Jan 17 12:05:40 IST 2022
	 */
	protected boolean distinct;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database table tbl_ticket
	 * @mbg.generated  Mon Jan 17 12:05:40 IST 2022
	 */
	protected List<Criteria> oredCriteria;

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_ticket
	 * @mbg.generated  Mon Jan 17 12:05:40 IST 2022
	 */
	public TicketExample() {
		oredCriteria = new ArrayList<>();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_ticket
	 * @mbg.generated  Mon Jan 17 12:05:40 IST 2022
	 */
	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_ticket
	 * @mbg.generated  Mon Jan 17 12:05:40 IST 2022
	 */
	public String getOrderByClause() {
		return orderByClause;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_ticket
	 * @mbg.generated  Mon Jan 17 12:05:40 IST 2022
	 */
	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_ticket
	 * @mbg.generated  Mon Jan 17 12:05:40 IST 2022
	 */
	public boolean isDistinct() {
		return distinct;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_ticket
	 * @mbg.generated  Mon Jan 17 12:05:40 IST 2022
	 */
	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_ticket
	 * @mbg.generated  Mon Jan 17 12:05:40 IST 2022
	 */
	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_ticket
	 * @mbg.generated  Mon Jan 17 12:05:40 IST 2022
	 */
	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_ticket
	 * @mbg.generated  Mon Jan 17 12:05:40 IST 2022
	 */
	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_ticket
	 * @mbg.generated  Mon Jan 17 12:05:40 IST 2022
	 */
	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_ticket
	 * @mbg.generated  Mon Jan 17 12:05:40 IST 2022
	 */
	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the database table tbl_ticket
	 * @mbg.generated  Mon Jan 17 12:05:40 IST 2022
	 */
	protected abstract static class GeneratedCriteria {
		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("ID is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("ID is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(String value) {
			addCriterion("ID =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(String value) {
			addCriterion("ID <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(String value) {
			addCriterion("ID >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(String value) {
			addCriterion("ID >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(String value) {
			addCriterion("ID <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(String value) {
			addCriterion("ID <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLike(String value) {
			addCriterion("ID like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotLike(String value) {
			addCriterion("ID not like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<String> values) {
			addCriterion("ID in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<String> values) {
			addCriterion("ID not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(String value1, String value2) {
			addCriterion("ID between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(String value1, String value2) {
			addCriterion("ID not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andSiteIdIsNull() {
			addCriterion("SITE_ID is null");
			return (Criteria) this;
		}

		public Criteria andSiteIdIsNotNull() {
			addCriterion("SITE_ID is not null");
			return (Criteria) this;
		}

		public Criteria andSiteIdEqualTo(String value) {
			addCriterion("SITE_ID =", value, "siteId");
			return (Criteria) this;
		}

		public Criteria andSiteIdNotEqualTo(String value) {
			addCriterion("SITE_ID <>", value, "siteId");
			return (Criteria) this;
		}

		public Criteria andSiteIdGreaterThan(String value) {
			addCriterion("SITE_ID >", value, "siteId");
			return (Criteria) this;
		}

		public Criteria andSiteIdGreaterThanOrEqualTo(String value) {
			addCriterion("SITE_ID >=", value, "siteId");
			return (Criteria) this;
		}

		public Criteria andSiteIdLessThan(String value) {
			addCriterion("SITE_ID <", value, "siteId");
			return (Criteria) this;
		}

		public Criteria andSiteIdLessThanOrEqualTo(String value) {
			addCriterion("SITE_ID <=", value, "siteId");
			return (Criteria) this;
		}

		public Criteria andSiteIdLike(String value) {
			addCriterion("SITE_ID like", value, "siteId");
			return (Criteria) this;
		}

		public Criteria andSiteIdNotLike(String value) {
			addCriterion("SITE_ID not like", value, "siteId");
			return (Criteria) this;
		}

		public Criteria andSiteIdIn(List<String> values) {
			addCriterion("SITE_ID in", values, "siteId");
			return (Criteria) this;
		}

		public Criteria andSiteIdNotIn(List<String> values) {
			addCriterion("SITE_ID not in", values, "siteId");
			return (Criteria) this;
		}

		public Criteria andSiteIdBetween(String value1, String value2) {
			addCriterion("SITE_ID between", value1, value2, "siteId");
			return (Criteria) this;
		}

		public Criteria andSiteIdNotBetween(String value1, String value2) {
			addCriterion("SITE_ID not between", value1, value2, "siteId");
			return (Criteria) this;
		}

		public Criteria andTemplateIdIsNull() {
			addCriterion("TEMPLATE_ID is null");
			return (Criteria) this;
		}

		public Criteria andTemplateIdIsNotNull() {
			addCriterion("TEMPLATE_ID is not null");
			return (Criteria) this;
		}

		public Criteria andTemplateIdEqualTo(String value) {
			addCriterion("TEMPLATE_ID =", value, "templateId");
			return (Criteria) this;
		}

		public Criteria andTemplateIdNotEqualTo(String value) {
			addCriterion("TEMPLATE_ID <>", value, "templateId");
			return (Criteria) this;
		}

		public Criteria andTemplateIdGreaterThan(String value) {
			addCriterion("TEMPLATE_ID >", value, "templateId");
			return (Criteria) this;
		}

		public Criteria andTemplateIdGreaterThanOrEqualTo(String value) {
			addCriterion("TEMPLATE_ID >=", value, "templateId");
			return (Criteria) this;
		}

		public Criteria andTemplateIdLessThan(String value) {
			addCriterion("TEMPLATE_ID <", value, "templateId");
			return (Criteria) this;
		}

		public Criteria andTemplateIdLessThanOrEqualTo(String value) {
			addCriterion("TEMPLATE_ID <=", value, "templateId");
			return (Criteria) this;
		}

		public Criteria andTemplateIdLike(String value) {
			addCriterion("TEMPLATE_ID like", value, "templateId");
			return (Criteria) this;
		}

		public Criteria andTemplateIdNotLike(String value) {
			addCriterion("TEMPLATE_ID not like", value, "templateId");
			return (Criteria) this;
		}

		public Criteria andTemplateIdIn(List<String> values) {
			addCriterion("TEMPLATE_ID in", values, "templateId");
			return (Criteria) this;
		}

		public Criteria andTemplateIdNotIn(List<String> values) {
			addCriterion("TEMPLATE_ID not in", values, "templateId");
			return (Criteria) this;
		}

		public Criteria andTemplateIdBetween(String value1, String value2) {
			addCriterion("TEMPLATE_ID between", value1, value2, "templateId");
			return (Criteria) this;
		}

		public Criteria andTemplateIdNotBetween(String value1, String value2) {
			addCriterion("TEMPLATE_ID not between", value1, value2, "templateId");
			return (Criteria) this;
		}

		public Criteria andTicketIdIsNull() {
			addCriterion("TICKET_ID is null");
			return (Criteria) this;
		}

		public Criteria andTicketIdIsNotNull() {
			addCriterion("TICKET_ID is not null");
			return (Criteria) this;
		}

		public Criteria andTicketIdEqualTo(String value) {
			addCriterion("TICKET_ID =", value, "ticketId");
			return (Criteria) this;
		}

		public Criteria andTicketIdNotEqualTo(String value) {
			addCriterion("TICKET_ID <>", value, "ticketId");
			return (Criteria) this;
		}

		public Criteria andTicketIdGreaterThan(String value) {
			addCriterion("TICKET_ID >", value, "ticketId");
			return (Criteria) this;
		}

		public Criteria andTicketIdGreaterThanOrEqualTo(String value) {
			addCriterion("TICKET_ID >=", value, "ticketId");
			return (Criteria) this;
		}

		public Criteria andTicketIdLessThan(String value) {
			addCriterion("TICKET_ID <", value, "ticketId");
			return (Criteria) this;
		}

		public Criteria andTicketIdLessThanOrEqualTo(String value) {
			addCriterion("TICKET_ID <=", value, "ticketId");
			return (Criteria) this;
		}

		public Criteria andTicketIdLike(String value) {
			addCriterion("TICKET_ID like", value, "ticketId");
			return (Criteria) this;
		}

		public Criteria andTicketIdNotLike(String value) {
			addCriterion("TICKET_ID not like", value, "ticketId");
			return (Criteria) this;
		}

		public Criteria andTicketIdIn(List<String> values) {
			addCriterion("TICKET_ID in", values, "ticketId");
			return (Criteria) this;
		}

		public Criteria andTicketIdNotIn(List<String> values) {
			addCriterion("TICKET_ID not in", values, "ticketId");
			return (Criteria) this;
		}

		public Criteria andTicketIdBetween(String value1, String value2) {
			addCriterion("TICKET_ID between", value1, value2, "ticketId");
			return (Criteria) this;
		}

		public Criteria andTicketIdNotBetween(String value1, String value2) {
			addCriterion("TICKET_ID not between", value1, value2, "ticketId");
			return (Criteria) this;
		}

		public Criteria andTicketTitleIsNull() {
			addCriterion("TICKET_TITLE is null");
			return (Criteria) this;
		}

		public Criteria andTicketTitleIsNotNull() {
			addCriterion("TICKET_TITLE is not null");
			return (Criteria) this;
		}

		public Criteria andTicketTitleEqualTo(String value) {
			addCriterion("TICKET_TITLE =", value, "ticketTitle");
			return (Criteria) this;
		}

		public Criteria andTicketTitleNotEqualTo(String value) {
			addCriterion("TICKET_TITLE <>", value, "ticketTitle");
			return (Criteria) this;
		}

		public Criteria andTicketTitleGreaterThan(String value) {
			addCriterion("TICKET_TITLE >", value, "ticketTitle");
			return (Criteria) this;
		}

		public Criteria andTicketTitleGreaterThanOrEqualTo(String value) {
			addCriterion("TICKET_TITLE >=", value, "ticketTitle");
			return (Criteria) this;
		}

		public Criteria andTicketTitleLessThan(String value) {
			addCriterion("TICKET_TITLE <", value, "ticketTitle");
			return (Criteria) this;
		}

		public Criteria andTicketTitleLessThanOrEqualTo(String value) {
			addCriterion("TICKET_TITLE <=", value, "ticketTitle");
			return (Criteria) this;
		}

		public Criteria andTicketTitleLike(String value) {
			addCriterion("TICKET_TITLE like", value, "ticketTitle");
			return (Criteria) this;
		}

		public Criteria andTicketTitleNotLike(String value) {
			addCriterion("TICKET_TITLE not like", value, "ticketTitle");
			return (Criteria) this;
		}

		public Criteria andTicketTitleIn(List<String> values) {
			addCriterion("TICKET_TITLE in", values, "ticketTitle");
			return (Criteria) this;
		}

		public Criteria andTicketTitleNotIn(List<String> values) {
			addCriterion("TICKET_TITLE not in", values, "ticketTitle");
			return (Criteria) this;
		}

		public Criteria andTicketTitleBetween(String value1, String value2) {
			addCriterion("TICKET_TITLE between", value1, value2, "ticketTitle");
			return (Criteria) this;
		}

		public Criteria andTicketTitleNotBetween(String value1, String value2) {
			addCriterion("TICKET_TITLE not between", value1, value2, "ticketTitle");
			return (Criteria) this;
		}

		public Criteria andTicketTypeIsNull() {
			addCriterion("TICKET_TYPE is null");
			return (Criteria) this;
		}

		public Criteria andTicketTypeIsNotNull() {
			addCriterion("TICKET_TYPE is not null");
			return (Criteria) this;
		}

		public Criteria andTicketTypeEqualTo(String value) {
			addCriterion("TICKET_TYPE =", value, "ticketType");
			return (Criteria) this;
		}

		public Criteria andTicketTypeNotEqualTo(String value) {
			addCriterion("TICKET_TYPE <>", value, "ticketType");
			return (Criteria) this;
		}

		public Criteria andTicketTypeGreaterThan(String value) {
			addCriterion("TICKET_TYPE >", value, "ticketType");
			return (Criteria) this;
		}

		public Criteria andTicketTypeGreaterThanOrEqualTo(String value) {
			addCriterion("TICKET_TYPE >=", value, "ticketType");
			return (Criteria) this;
		}

		public Criteria andTicketTypeLessThan(String value) {
			addCriterion("TICKET_TYPE <", value, "ticketType");
			return (Criteria) this;
		}

		public Criteria andTicketTypeLessThanOrEqualTo(String value) {
			addCriterion("TICKET_TYPE <=", value, "ticketType");
			return (Criteria) this;
		}

		public Criteria andTicketTypeLike(String value) {
			addCriterion("TICKET_TYPE like", value, "ticketType");
			return (Criteria) this;
		}

		public Criteria andTicketTypeNotLike(String value) {
			addCriterion("TICKET_TYPE not like", value, "ticketType");
			return (Criteria) this;
		}

		public Criteria andTicketTypeIn(List<String> values) {
			addCriterion("TICKET_TYPE in", values, "ticketType");
			return (Criteria) this;
		}

		public Criteria andTicketTypeNotIn(List<String> values) {
			addCriterion("TICKET_TYPE not in", values, "ticketType");
			return (Criteria) this;
		}

		public Criteria andTicketTypeBetween(String value1, String value2) {
			addCriterion("TICKET_TYPE between", value1, value2, "ticketType");
			return (Criteria) this;
		}

		public Criteria andTicketTypeNotBetween(String value1, String value2) {
			addCriterion("TICKET_TYPE not between", value1, value2, "ticketType");
			return (Criteria) this;
		}

		public Criteria andTicketStatusIsNull() {
			addCriterion("TICKET_STATUS is null");
			return (Criteria) this;
		}

		public Criteria andTicketStatusIsNotNull() {
			addCriterion("TICKET_STATUS is not null");
			return (Criteria) this;
		}

		public Criteria andTicketStatusEqualTo(Integer value) {
			addCriterion("TICKET_STATUS =", value, "ticketStatus");
			return (Criteria) this;
		}

		public Criteria andTicketStatusNotEqualTo(Integer value) {
			addCriterion("TICKET_STATUS <>", value, "ticketStatus");
			return (Criteria) this;
		}

		public Criteria andTicketStatusGreaterThan(Integer value) {
			addCriterion("TICKET_STATUS >", value, "ticketStatus");
			return (Criteria) this;
		}

		public Criteria andTicketStatusGreaterThanOrEqualTo(Integer value) {
			addCriterion("TICKET_STATUS >=", value, "ticketStatus");
			return (Criteria) this;
		}

		public Criteria andTicketStatusLessThan(Integer value) {
			addCriterion("TICKET_STATUS <", value, "ticketStatus");
			return (Criteria) this;
		}

		public Criteria andTicketStatusLessThanOrEqualTo(Integer value) {
			addCriterion("TICKET_STATUS <=", value, "ticketStatus");
			return (Criteria) this;
		}

		public Criteria andTicketStatusIn(List<Integer> values) {
			addCriterion("TICKET_STATUS in", values, "ticketStatus");
			return (Criteria) this;
		}

		public Criteria andTicketStatusNotIn(List<Integer> values) {
			addCriterion("TICKET_STATUS not in", values, "ticketStatus");
			return (Criteria) this;
		}

		public Criteria andTicketStatusBetween(Integer value1, Integer value2) {
			addCriterion("TICKET_STATUS between", value1, value2, "ticketStatus");
			return (Criteria) this;
		}

		public Criteria andTicketStatusNotBetween(Integer value1, Integer value2) {
			addCriterion("TICKET_STATUS not between", value1, value2, "ticketStatus");
			return (Criteria) this;
		}

		public Criteria andStatusIsNull() {
			addCriterion("STATUS is null");
			return (Criteria) this;
		}

		public Criteria andStatusIsNotNull() {
			addCriterion("STATUS is not null");
			return (Criteria) this;
		}

		public Criteria andStatusEqualTo(Byte value) {
			addCriterion("STATUS =", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusNotEqualTo(Byte value) {
			addCriterion("STATUS <>", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusGreaterThan(Byte value) {
			addCriterion("STATUS >", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusGreaterThanOrEqualTo(Byte value) {
			addCriterion("STATUS >=", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusLessThan(Byte value) {
			addCriterion("STATUS <", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusLessThanOrEqualTo(Byte value) {
			addCriterion("STATUS <=", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusIn(List<Byte> values) {
			addCriterion("STATUS in", values, "status");
			return (Criteria) this;
		}

		public Criteria andStatusNotIn(List<Byte> values) {
			addCriterion("STATUS not in", values, "status");
			return (Criteria) this;
		}

		public Criteria andStatusBetween(Byte value1, Byte value2) {
			addCriterion("STATUS between", value1, value2, "status");
			return (Criteria) this;
		}

		public Criteria andStatusNotBetween(Byte value1, Byte value2) {
			addCriterion("STATUS not between", value1, value2, "status");
			return (Criteria) this;
		}

		public Criteria andSortOrderIsNull() {
			addCriterion("SORT_ORDER is null");
			return (Criteria) this;
		}

		public Criteria andSortOrderIsNotNull() {
			addCriterion("SORT_ORDER is not null");
			return (Criteria) this;
		}

		public Criteria andSortOrderEqualTo(Integer value) {
			addCriterion("SORT_ORDER =", value, "sortOrder");
			return (Criteria) this;
		}

		public Criteria andSortOrderNotEqualTo(Integer value) {
			addCriterion("SORT_ORDER <>", value, "sortOrder");
			return (Criteria) this;
		}

		public Criteria andSortOrderGreaterThan(Integer value) {
			addCriterion("SORT_ORDER >", value, "sortOrder");
			return (Criteria) this;
		}

		public Criteria andSortOrderGreaterThanOrEqualTo(Integer value) {
			addCriterion("SORT_ORDER >=", value, "sortOrder");
			return (Criteria) this;
		}

		public Criteria andSortOrderLessThan(Integer value) {
			addCriterion("SORT_ORDER <", value, "sortOrder");
			return (Criteria) this;
		}

		public Criteria andSortOrderLessThanOrEqualTo(Integer value) {
			addCriterion("SORT_ORDER <=", value, "sortOrder");
			return (Criteria) this;
		}

		public Criteria andSortOrderIn(List<Integer> values) {
			addCriterion("SORT_ORDER in", values, "sortOrder");
			return (Criteria) this;
		}

		public Criteria andSortOrderNotIn(List<Integer> values) {
			addCriterion("SORT_ORDER not in", values, "sortOrder");
			return (Criteria) this;
		}

		public Criteria andSortOrderBetween(Integer value1, Integer value2) {
			addCriterion("SORT_ORDER between", value1, value2, "sortOrder");
			return (Criteria) this;
		}

		public Criteria andSortOrderNotBetween(Integer value1, Integer value2) {
			addCriterion("SORT_ORDER not between", value1, value2, "sortOrder");
			return (Criteria) this;
		}

		public Criteria andCreatedByIsNull() {
			addCriterion("CREATED_BY is null");
			return (Criteria) this;
		}

		public Criteria andCreatedByIsNotNull() {
			addCriterion("CREATED_BY is not null");
			return (Criteria) this;
		}

		public Criteria andCreatedByEqualTo(String value) {
			addCriterion("CREATED_BY =", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByNotEqualTo(String value) {
			addCriterion("CREATED_BY <>", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByGreaterThan(String value) {
			addCriterion("CREATED_BY >", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByGreaterThanOrEqualTo(String value) {
			addCriterion("CREATED_BY >=", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByLessThan(String value) {
			addCriterion("CREATED_BY <", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByLessThanOrEqualTo(String value) {
			addCriterion("CREATED_BY <=", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByLike(String value) {
			addCriterion("CREATED_BY like", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByNotLike(String value) {
			addCriterion("CREATED_BY not like", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByIn(List<String> values) {
			addCriterion("CREATED_BY in", values, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByNotIn(List<String> values) {
			addCriterion("CREATED_BY not in", values, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByBetween(String value1, String value2) {
			addCriterion("CREATED_BY between", value1, value2, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByNotBetween(String value1, String value2) {
			addCriterion("CREATED_BY not between", value1, value2, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedOnIsNull() {
			addCriterion("CREATED_ON is null");
			return (Criteria) this;
		}

		public Criteria andCreatedOnIsNotNull() {
			addCriterion("CREATED_ON is not null");
			return (Criteria) this;
		}

		public Criteria andCreatedOnEqualTo(Date value) {
			addCriterion("CREATED_ON =", value, "createdOn");
			return (Criteria) this;
		}

		public Criteria andCreatedOnNotEqualTo(Date value) {
			addCriterion("CREATED_ON <>", value, "createdOn");
			return (Criteria) this;
		}

		public Criteria andCreatedOnGreaterThan(Date value) {
			addCriterion("CREATED_ON >", value, "createdOn");
			return (Criteria) this;
		}

		public Criteria andCreatedOnGreaterThanOrEqualTo(Date value) {
			addCriterion("CREATED_ON >=", value, "createdOn");
			return (Criteria) this;
		}

		public Criteria andCreatedOnLessThan(Date value) {
			addCriterion("CREATED_ON <", value, "createdOn");
			return (Criteria) this;
		}

		public Criteria andCreatedOnLessThanOrEqualTo(Date value) {
			addCriterion("CREATED_ON <=", value, "createdOn");
			return (Criteria) this;
		}

		public Criteria andCreatedOnIn(List<Date> values) {
			addCriterion("CREATED_ON in", values, "createdOn");
			return (Criteria) this;
		}

		public Criteria andCreatedOnNotIn(List<Date> values) {
			addCriterion("CREATED_ON not in", values, "createdOn");
			return (Criteria) this;
		}

		public Criteria andCreatedOnBetween(Date value1, Date value2) {
			addCriterion("CREATED_ON between", value1, value2, "createdOn");
			return (Criteria) this;
		}

		public Criteria andCreatedOnNotBetween(Date value1, Date value2) {
			addCriterion("CREATED_ON not between", value1, value2, "createdOn");
			return (Criteria) this;
		}

		public Criteria andLastUpdateByIsNull() {
			addCriterion("LAST_UPDATE_BY is null");
			return (Criteria) this;
		}

		public Criteria andLastUpdateByIsNotNull() {
			addCriterion("LAST_UPDATE_BY is not null");
			return (Criteria) this;
		}

		public Criteria andLastUpdateByEqualTo(String value) {
			addCriterion("LAST_UPDATE_BY =", value, "lastUpdateBy");
			return (Criteria) this;
		}

		public Criteria andLastUpdateByNotEqualTo(String value) {
			addCriterion("LAST_UPDATE_BY <>", value, "lastUpdateBy");
			return (Criteria) this;
		}

		public Criteria andLastUpdateByGreaterThan(String value) {
			addCriterion("LAST_UPDATE_BY >", value, "lastUpdateBy");
			return (Criteria) this;
		}

		public Criteria andLastUpdateByGreaterThanOrEqualTo(String value) {
			addCriterion("LAST_UPDATE_BY >=", value, "lastUpdateBy");
			return (Criteria) this;
		}

		public Criteria andLastUpdateByLessThan(String value) {
			addCriterion("LAST_UPDATE_BY <", value, "lastUpdateBy");
			return (Criteria) this;
		}

		public Criteria andLastUpdateByLessThanOrEqualTo(String value) {
			addCriterion("LAST_UPDATE_BY <=", value, "lastUpdateBy");
			return (Criteria) this;
		}

		public Criteria andLastUpdateByLike(String value) {
			addCriterion("LAST_UPDATE_BY like", value, "lastUpdateBy");
			return (Criteria) this;
		}

		public Criteria andLastUpdateByNotLike(String value) {
			addCriterion("LAST_UPDATE_BY not like", value, "lastUpdateBy");
			return (Criteria) this;
		}

		public Criteria andLastUpdateByIn(List<String> values) {
			addCriterion("LAST_UPDATE_BY in", values, "lastUpdateBy");
			return (Criteria) this;
		}

		public Criteria andLastUpdateByNotIn(List<String> values) {
			addCriterion("LAST_UPDATE_BY not in", values, "lastUpdateBy");
			return (Criteria) this;
		}

		public Criteria andLastUpdateByBetween(String value1, String value2) {
			addCriterion("LAST_UPDATE_BY between", value1, value2, "lastUpdateBy");
			return (Criteria) this;
		}

		public Criteria andLastUpdateByNotBetween(String value1, String value2) {
			addCriterion("LAST_UPDATE_BY not between", value1, value2, "lastUpdateBy");
			return (Criteria) this;
		}

		public Criteria andLastUpdateOnIsNull() {
			addCriterion("LAST_UPDATE_ON is null");
			return (Criteria) this;
		}

		public Criteria andLastUpdateOnIsNotNull() {
			addCriterion("LAST_UPDATE_ON is not null");
			return (Criteria) this;
		}

		public Criteria andLastUpdateOnEqualTo(Date value) {
			addCriterion("LAST_UPDATE_ON =", value, "lastUpdateOn");
			return (Criteria) this;
		}

		public Criteria andLastUpdateOnNotEqualTo(Date value) {
			addCriterion("LAST_UPDATE_ON <>", value, "lastUpdateOn");
			return (Criteria) this;
		}

		public Criteria andLastUpdateOnGreaterThan(Date value) {
			addCriterion("LAST_UPDATE_ON >", value, "lastUpdateOn");
			return (Criteria) this;
		}

		public Criteria andLastUpdateOnGreaterThanOrEqualTo(Date value) {
			addCriterion("LAST_UPDATE_ON >=", value, "lastUpdateOn");
			return (Criteria) this;
		}

		public Criteria andLastUpdateOnLessThan(Date value) {
			addCriterion("LAST_UPDATE_ON <", value, "lastUpdateOn");
			return (Criteria) this;
		}

		public Criteria andLastUpdateOnLessThanOrEqualTo(Date value) {
			addCriterion("LAST_UPDATE_ON <=", value, "lastUpdateOn");
			return (Criteria) this;
		}

		public Criteria andLastUpdateOnIn(List<Date> values) {
			addCriterion("LAST_UPDATE_ON in", values, "lastUpdateOn");
			return (Criteria) this;
		}

		public Criteria andLastUpdateOnNotIn(List<Date> values) {
			addCriterion("LAST_UPDATE_ON not in", values, "lastUpdateOn");
			return (Criteria) this;
		}

		public Criteria andLastUpdateOnBetween(Date value1, Date value2) {
			addCriterion("LAST_UPDATE_ON between", value1, value2, "lastUpdateOn");
			return (Criteria) this;
		}

		public Criteria andLastUpdateOnNotBetween(Date value1, Date value2) {
			addCriterion("LAST_UPDATE_ON not between", value1, value2, "lastUpdateOn");
			return (Criteria) this;
		}
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the database table tbl_ticket
	 * @mbg.generated  Mon Jan 17 12:05:40 IST 2022
	 */
	public static class Criterion {
		private String condition;
		private Object value;
		private Object secondValue;
		private boolean noValue;
		private boolean singleValue;
		private boolean betweenValue;
		private boolean listValue;
		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			} else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}
	}

	/**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table tbl_ticket
     *
     * @mbg.generated do_not_delete_during_merge Mon Dec 06 13:29:48 IST 2021
     */
    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }
}