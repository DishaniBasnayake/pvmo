package com.axiohelix.pvmo.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TicketUserDTO extends GenericEntity {

	private String remark;
	
	private String ticketId;
	private String userId;
	private String userType;
	private String content;
	
	private String ticketTitle;

}