package com.axiohelix.pvmo.entity.custom;

import com.axiohelix.pvmo.entity.SiteUserGPSLogWithBLOBs;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SiteUserGPSLogExtended extends SiteUserGPSLogWithBLOBs {
	
	private String fullName;
	private String SiteName;
	private String inTime;
	private String outTime;
	private String timeSpent; 
	
}
