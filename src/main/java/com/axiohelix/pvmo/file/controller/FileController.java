package com.axiohelix.pvmo.file.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.imgscalr.Scalr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.axiohelix.pvmo.file.payload.UploadFileResponse;
import com.axiohelix.pvmo.file.service.FileStorageService;
import com.axiohelix.pvmo.util.CommonConstant;

@Controller
@RequestMapping({CommonConstant.CONTROLLER_URL_FILE, CommonConstant.API_CONTROLLER_URL_FILE})
public class FileController {

    private static final Logger logger = LoggerFactory.getLogger(FileController.class);

    @Autowired
    private FileStorageService fileStorageService;

    @ResponseBody
    @PostMapping("/upload")
    public UploadFileResponse uploadFile(@RequestParam("file") MultipartFile file) {
        String fileName = fileStorageService.storeFile(file);

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/downloadFile/")
                .path(fileName)
                .toUriString();
               
        return new UploadFileResponse(fileName, fileDownloadUri,
                file.getContentType(), file.getSize());
    }

    @ResponseBody
    @PostMapping("/upload-multiple")
    public List<UploadFileResponse> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) {
        return Arrays.asList(files)
                .stream()
                .map(file -> uploadFile(file))
                .collect(Collectors.toList());
    }


    @ResponseBody
    @GetMapping("/download/{fileName:.+}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) {
    	// Load file as Resource
    	Resource resource = fileStorageService.loadFileAsResource(fileName);
    	
    	
    	// Try to determine file's content type
    	String contentType = null;
    	try {
    		contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
    	} catch (IOException ex) {
    		logger.info("Could not determine file type.");
    	}
    	
    	// Fallback to the default content type if type could not be determined
    	if(contentType == null) {
    		contentType = "application/octet-stream";
    	}
    	
    	return ResponseEntity.ok()
    			.contentType(MediaType.parseMediaType(contentType))
    			.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
    			.body(resource);
    }
    
    @ResponseBody
    @PostMapping("/download-multiple")
    public void downloadZipFile(HttpServletResponse response,@RequestParam List<String> listOfFileNames) {
        response.setContentType("application/zip");
        response.setHeader("Content-Disposition", "attachment; filename=download.zip");
        try(ZipOutputStream zipOutputStream = new ZipOutputStream(response.getOutputStream())) {
            for(String fileName : listOfFileNames) {
                Resource fileSystemResource = fileStorageService.loadFileAsResource(fileName);
                ZipEntry zipEntry = new ZipEntry(fileSystemResource.getFilename());
                zipEntry.setSize(fileSystemResource.contentLength());
                zipEntry.setTime(System.currentTimeMillis());

                zipOutputStream.putNextEntry(zipEntry);

                StreamUtils.copy(fileSystemResource.getInputStream(), zipOutputStream);
                zipOutputStream.closeEntry();
            }
            zipOutputStream.finish();
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
    }
    
    @ResponseBody
    @GetMapping("/view/thumb/{fileName:.+}")
    public ResponseEntity<byte[]> viewThumb(@PathVariable String fileName, HttpServletRequest request) throws IOException {
    	// Load file as Resource
    	Resource resource = fileStorageService.loadFileAsResource(fileName);
    	BufferedImage bufferedImage = ImageIO.read(resource.getFile());
    	bufferedImage = Scalr.resize(bufferedImage, 100);
    	
    	String contentType = null;
    	String mimeType = "jpeg";
    	try {
    		contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
    		mimeType = contentType.split("/")[1];
    	} catch (IOException ex) {
    		logger.info("Could not determine file type.");
    	}
    	
    	ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    	ImageIO.write(bufferedImage ,mimeType, byteArrayOutputStream);
    	System.err.println(mimeType);

    	byte[] imageInByte = byteArrayOutputStream.toByteArray();
    	
    	return ResponseEntity.ok()
    			.contentType(MediaType.parseMediaType(contentType))
    			.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(imageInByte);
    }
    
}
