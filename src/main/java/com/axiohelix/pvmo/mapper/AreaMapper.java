package com.axiohelix.pvmo.mapper;

import com.axiohelix.pvmo.entity.Area;
import com.axiohelix.pvmo.entity.AreaExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface AreaMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_area
     *
     * @mbg.generated Mon Jun 13 10:17:39 IST 2022
     */
    long countByExample(AreaExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_area
     *
     * @mbg.generated Mon Jun 13 10:17:39 IST 2022
     */
    int deleteByExample(AreaExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_area
     *
     * @mbg.generated Mon Jun 13 10:17:39 IST 2022
     */
    int deleteByPrimaryKey(String id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_area
     *
     * @mbg.generated Mon Jun 13 10:17:39 IST 2022
     */
    int insert(Area record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_area
     *
     * @mbg.generated Mon Jun 13 10:17:39 IST 2022
     */
    int insertSelective(Area record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_area
     *
     * @mbg.generated Mon Jun 13 10:17:39 IST 2022
     */
    List<Area> selectByExample(AreaExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_area
     *
     * @mbg.generated Mon Jun 13 10:17:39 IST 2022
     */
    Area selectByPrimaryKey(String id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_area
     *
     * @mbg.generated Mon Jun 13 10:17:39 IST 2022
     */
    int updateByExampleSelective(@Param("record") Area record, @Param("example") AreaExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_area
     *
     * @mbg.generated Mon Jun 13 10:17:39 IST 2022
     */
    int updateByExample(@Param("record") Area record, @Param("example") AreaExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_area
     *
     * @mbg.generated Mon Jun 13 10:17:39 IST 2022
     */
    int updateByPrimaryKeySelective(Area record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_area
     *
     * @mbg.generated Mon Jun 13 10:17:39 IST 2022
     */
    int updateByPrimaryKey(Area record);
}