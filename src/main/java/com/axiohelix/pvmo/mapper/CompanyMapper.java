package com.axiohelix.pvmo.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.axiohelix.pvmo.entity.Company;
import com.axiohelix.pvmo.entity.CompanyExample;
import com.axiohelix.pvmo.entity.CompanyWithBLOBs;

@Mapper
public interface CompanyMapper extends GenericMapper<Company, CompanyExample, CompanyWithBLOBs> {

}