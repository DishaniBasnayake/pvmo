package com.axiohelix.pvmo.mapper;

import org.apache.ibatis.annotations.Mapper;


import com.axiohelix.pvmo.entity.Consumer;
import com.axiohelix.pvmo.entity.ConsumerExample;
import com.axiohelix.pvmo.entity.ConsumerWithBLOBs;

@Mapper
public interface ConsumerMapper extends GenericMapper<Consumer, ConsumerExample, ConsumerWithBLOBs> {

}