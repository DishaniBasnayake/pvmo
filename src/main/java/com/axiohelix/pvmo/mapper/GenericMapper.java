package com.axiohelix.pvmo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface GenericMapper<T,TExample,TWithBLOBs> {

	long countByExample(TExample example);

	int deleteByExample(TExample example);

	List<TWithBLOBs> selectByExampleWithBLOBs(TExample example);
	
	List<T> selectByExample(TExample example);

	int updateByExample(@Param("record") T record, @Param("example") TExample example);
	
	int deleteByPrimaryKey(String id);

	int insert(TWithBLOBs record);

	int insertSelective(TWithBLOBs record);

	TWithBLOBs selectByPrimaryKey(String id);

	int updateByExampleSelective(@Param("record") TWithBLOBs record, @Param("example") TExample example);

	int updateByExampleWithBLOBs(@Param("record") TWithBLOBs record, @Param("example") TExample example);

	int updateByPrimaryKeySelective(TWithBLOBs record);

	int updateByPrimaryKeyWithBLOBs(TWithBLOBs record);

	int updateByPrimaryKey(T record);
}