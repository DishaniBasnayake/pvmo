package com.axiohelix.pvmo.mapper;

import com.axiohelix.pvmo.entity.JCB;
import com.axiohelix.pvmo.entity.JCBExample;
import com.axiohelix.pvmo.entity.PCS;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface JCBMapper {

	long countByExample(JCBExample example);

	int deleteByExample(JCBExample example);

	int deleteByPrimaryKey(String id);

	int insert(JCB record);

	int insertSelective(JCB record);

	List<JCB> selectByExample(JCBExample example);

	JCB selectByPrimaryKey(String id);

	int updateByExampleSelective(@Param("record") JCB record, @Param("example") JCBExample example);

	int updateByExample(@Param("record") JCB record, @Param("example") JCBExample example);

	int updateByPrimaryKeySelective(JCB record);

	int updateByPrimaryKey(JCB record);

	@Select("select * from tbl_jcb where object_id=#{objectId} and status=0")
	JCB findByObjectId(String objectId);

	@Select("select * from tbl_jcb where object_id=#{objectId} and pcs_id=#{pcsId} and status=0")
	JCB findByObjectIdPcsId(@Param("objectId")String objectId, @Param("pcsId")String pcsId);
	
	@Select("select * from tbl_jcb where pcs_id=#{pcsId} and status=0 order by abs(object_id) asc")
	@ResultMap("ResultMapWithComponents")
	List<JCB> selectByPCSId(@Param("pcsId")String pcsId);

	void insertMultiple(@Param("jcbList")List<JCB> jcbList);
	
	

}