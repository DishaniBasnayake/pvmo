package com.axiohelix.pvmo.mapper;

import com.axiohelix.pvmo.entity.Nishikata;
import com.axiohelix.pvmo.entity.NishikataExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface NishikataMapper {
    long countByExample(NishikataExample example);

    int deleteByExample(NishikataExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Nishikata record);

    int insertSelective(Nishikata record);

    List<Nishikata> selectByExampleWithBLOBs(NishikataExample example);

    List<Nishikata> selectByExample(NishikataExample example);

    Nishikata selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Nishikata record, @Param("example") NishikataExample example);

    int updateByExampleWithBLOBs(@Param("record") Nishikata record, @Param("example") NishikataExample example);

    int updateByExample(@Param("record") Nishikata record, @Param("example") NishikataExample example);

    int updateByPrimaryKeySelective(Nishikata record);

    int updateByPrimaryKeyWithBLOBs(Nishikata record);

    int updateByPrimaryKey(Nishikata record);
}