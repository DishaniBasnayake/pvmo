package com.axiohelix.pvmo.mapper;

import com.axiohelix.pvmo.entity.PCS;
import com.axiohelix.pvmo.entity.PCSExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface PCSMapper {

	long countByExample(PCSExample example);

	int deleteByExample(PCSExample example);

	int deleteByPrimaryKey(String id);

	int insert(PCS record);

	int insertSelective(PCS record);

	List<PCS> selectByExample(PCSExample example);

	PCS selectByPrimaryKey(String id);

	int updateByExampleSelective(@Param("record") PCS record, @Param("example") PCSExample example);

	int updateByExample(@Param("record") PCS record, @Param("example") PCSExample example);

	int updateByPrimaryKeySelective(PCS record);

	int updateByPrimaryKey(PCS record);

	@Select("select * from tbl_pcs where object_id=#{objectId} and status=0 limit 1")
	PCS findByObjectId(String objectId);

	@Select("select * from tbl_pcs where object_id=#{objectId} and device_id=#{deviceId} and status=0 limit 1")
	PCS findByObjectIdDeviceId(@Param("objectId")String objectId, @Param("deviceId")String deviceId);
	
	@Select("select * from tbl_pcs where device_id=#{deviceId} and status=0")
	@ResultMap("ResultMapWithComponents")
	List<PCS> selectByDeviceId(String deviceId);

	void insertMultiple(@Param("pcsList")List<PCS> pcsList);

	@Select("select * from tbl_pcs where site_id=#{siteId} and status=0 order by abs(object_id) asc limit #{from}, #{to}")
	@ResultMap("ResultMapWithComponents")
	List<PCS> findBySiteIdWithComponents(@Param("siteId")String siteId, @Param("from")Integer from, @Param("to")Integer to);

}