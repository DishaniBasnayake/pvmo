package com.axiohelix.pvmo.mapper;

import com.axiohelix.pvmo.entity.PCS;
import com.axiohelix.pvmo.entity.Panel;
import com.axiohelix.pvmo.entity.PanelExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface PanelMapper {

	long countByExample(PanelExample example);

	int deleteByExample(PanelExample example);

	int deleteByPrimaryKey(String id);

	int insert(Panel record);

	int insertSelective(Panel record);

	List<Panel> selectByExample(PanelExample example);

	Panel selectByPrimaryKey(String id);

	int updateByExampleSelective(@Param("record") Panel record, @Param("example") PanelExample example);

	int updateByExample(@Param("record") Panel record, @Param("example") PanelExample example);

	int updateByPrimaryKeySelective(Panel record);

	int updateByPrimaryKey(Panel record);

	@Select("select * from tbl_panel where object_id=#{objectId} and status=0")
	Panel findByObjectId(String object_id);

	@Select("select * from tbl_panel where object_id=#{objectId} and panel_string_id=#{panelStringId} and status=0")
	Panel findByObjectIdPanelStringId(@Param("objectId") String objectId, @Param("panelStringId") String panelStringId);
	
	@Select("select * from tbl_panel where panel_string_id=#{panelStringId} and status=0 order by abs(object_id) asc")
	List<Panel> selectByPanelStringId(@Param("panelStringId") String panelStringId);

	void insertMultiple(@Param("panelList")List<Panel> panelList);
	
}