package com.axiohelix.pvmo.mapper;

import com.axiohelix.pvmo.entity.PCS;
import com.axiohelix.pvmo.entity.PanelString;
import com.axiohelix.pvmo.entity.PanelStringExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface PanelStringMapper {

	long countByExample(PanelStringExample example);

	int deleteByExample(PanelStringExample example);

	int deleteByPrimaryKey(String id);

	int insert(PanelString record);

	int insertSelective(PanelString record);

	List<PanelString> selectByExample(PanelStringExample example);

	PanelString selectByPrimaryKey(String id);

	int updateByExampleSelective(@Param("record") PanelString record, @Param("example") PanelStringExample example);

	int updateByExample(@Param("record") PanelString record, @Param("example") PanelStringExample example);

	int updateByPrimaryKeySelective(PanelString record);

	int updateByPrimaryKey(PanelString record);

	@Select("select * from tbl_panel_string where object_id=#{objectId} and jcb_id=#{jcbId} and status=0")
	PanelString findByObjectId(@Param("objectId")String objectId, @Param("jcbId")String jcbId);
	
	@Select("select * from tbl_panel_string where jcb_id=#{jcbId} and status=0 order by abs(object_id) asc")
	@ResultMap("ResultMapWithComponents")
	List<PanelString> selectByJCBId(@Param("jcbId")String jcbId);

	void insertMultiple(@Param("panelStringList")List<PanelString> panelStringList);
	

}