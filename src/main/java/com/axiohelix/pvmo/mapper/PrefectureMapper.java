package com.axiohelix.pvmo.mapper;

import com.axiohelix.pvmo.entity.Prefecture;
import com.axiohelix.pvmo.entity.PrefectureExample;
import com.axiohelix.pvmo.entity.PrefectureWithBLOBs;
import com.axiohelix.pvmo.entity.Region;
import com.axiohelix.pvmo.entity.RegionExample;
import com.axiohelix.pvmo.entity.RegionWithBLOBs;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface PrefectureMapper extends GenericMapper<Prefecture, PrefectureExample, PrefectureWithBLOBs> {

	
}
