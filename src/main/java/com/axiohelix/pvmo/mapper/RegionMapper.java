package com.axiohelix.pvmo.mapper;


import com.axiohelix.pvmo.entity.Region;
import com.axiohelix.pvmo.entity.RegionExample;
import com.axiohelix.pvmo.entity.RegionWithBLOBs;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


	@Mapper
	public interface RegionMapper extends GenericMapper<Region, RegionExample, RegionWithBLOBs> {

		
	}
