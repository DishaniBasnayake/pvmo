package com.axiohelix.pvmo.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.axiohelix.pvmo.entity.ServiceCenter;
import com.axiohelix.pvmo.entity.ServiceCenterExample;
import com.axiohelix.pvmo.entity.ServiceCenterWithBLOBs;

@Mapper
public interface ServiceCenterMapper
		extends GenericMapper<ServiceCenter, ServiceCenterExample, ServiceCenterWithBLOBs> {

}