package com.axiohelix.pvmo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.axiohelix.pvmo.entity.ServiceCenter;
import com.axiohelix.pvmo.entity.Site;
import com.axiohelix.pvmo.entity.SiteWithBLOBs;

@Mapper
public interface ServiceCenterSiteMapper {

	int deleteServiceCenterSite(String seviceCenterId, String siteId);

	int addServiceCenterSite(String id, String seviceCenterId, String siteId);

	List<ServiceCenter> getServiceCenters(String siteId);

	List<SiteWithBLOBs> getSites(String serviceCenterId);
	
	List<Site> getNotAssignedSites(String companyId,String serviceCenterId);

}
