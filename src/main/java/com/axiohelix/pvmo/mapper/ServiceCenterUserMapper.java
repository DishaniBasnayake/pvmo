package com.axiohelix.pvmo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.axiohelix.pvmo.entity.ServiceCenter;
import com.axiohelix.pvmo.entity.ServiceCenterWithBLOBs;
import com.axiohelix.pvmo.entity.User;
import com.axiohelix.pvmo.entity.UserWithBLOBs;

@Mapper
public interface ServiceCenterUserMapper {

	int deleteServiceCenterUser(String seviceCenterId, String userId);

	int addServiceCenterUser(String id, String seviceCenterId, String userId, String userType);

	List<ServiceCenterWithBLOBs> getServiceCenters(String userId);

	List<UserWithBLOBs> getUsers(String serviceCenterId);
	
	List<UserWithBLOBs> getNotAssignedUsers(String companyId,String serviceCenterId);

}
