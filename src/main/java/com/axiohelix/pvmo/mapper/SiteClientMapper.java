package com.axiohelix.pvmo.mapper;

import com.axiohelix.pvmo.entity.SiteClient;
import com.axiohelix.pvmo.entity.SiteClientExample;
import com.axiohelix.pvmo.entity.SiteClientWithBLOBs;
import com.axiohelix.pvmo.entity.SiteStatus;
import com.axiohelix.pvmo.entity.SiteStatusExample;
import com.axiohelix.pvmo.entity.SiteStatusWithBLOBs;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface SiteClientMapper extends GenericMapper<SiteClient, SiteClientExample, SiteClientWithBLOBs> {
   
}