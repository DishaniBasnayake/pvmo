package com.axiohelix.pvmo.mapper;


import org.apache.ibatis.annotations.Mapper;

import com.axiohelix.pvmo.entity.SiteConstructionCompany;
import com.axiohelix.pvmo.entity.SiteConstructionCompanyExample;
import com.axiohelix.pvmo.entity.SiteConstructionCompanyWithBLOBs;



@Mapper
public interface SiteConstructionCompanyMapper extends GenericMapper<SiteConstructionCompany, 
	SiteConstructionCompanyExample, SiteConstructionCompanyWithBLOBs>{
   
}