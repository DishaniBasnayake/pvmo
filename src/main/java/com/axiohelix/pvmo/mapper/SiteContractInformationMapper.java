package com.axiohelix.pvmo.mapper;


import org.apache.ibatis.annotations.Mapper;

import com.axiohelix.pvmo.entity.SiteContractInformation;
import com.axiohelix.pvmo.entity.SiteContractInformationExample;
import com.axiohelix.pvmo.entity.SiteContractInformationWithBLOBs;

@Mapper
public interface SiteContractInformationMapper extends GenericMapper<SiteContractInformation, 
SiteContractInformationExample, SiteContractInformationWithBLOBs>{
	   
}