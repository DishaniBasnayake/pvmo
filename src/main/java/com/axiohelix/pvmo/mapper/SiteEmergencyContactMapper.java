package com.axiohelix.pvmo.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.axiohelix.pvmo.entity.SiteEmergencyContact;
import com.axiohelix.pvmo.entity.SiteEmergencyContactExample;
import com.axiohelix.pvmo.entity.SiteEmergencyContactWithBLOBs;

@Mapper
public interface SiteEmergencyContactMapper
		extends GenericMapper<SiteEmergencyContact, SiteEmergencyContactExample, SiteEmergencyContactWithBLOBs> {

}