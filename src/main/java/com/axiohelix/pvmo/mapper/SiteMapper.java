package com.axiohelix.pvmo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.axiohelix.pvmo.entity.Site;
import com.axiohelix.pvmo.entity.SiteExample;
import com.axiohelix.pvmo.entity.SiteWithBLOBs;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface SiteMapper extends GenericMapper<Site, SiteExample, SiteWithBLOBs> {

	@Select("select tp.id from tbl_panel tp "
			+ "left join nishikata n on n.id = tp.device_id "
			+ "where "
			+ "n.err_str = 1")
	List<String> getNishikataPanelIdsWithErrors();

	@Select("select * from tbl_site where spn = #{spn} limit 1")
	@ResultMap("ResultMapWithBLOBs")
	SiteWithBLOBs getSiteBySPN(String spn);

}