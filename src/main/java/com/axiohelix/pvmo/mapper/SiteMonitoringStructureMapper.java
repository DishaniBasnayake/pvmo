package com.axiohelix.pvmo.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.axiohelix.pvmo.entity.SiteMonitoringStructure;
import com.axiohelix.pvmo.entity.SiteMonitoringStructureExample;
import com.axiohelix.pvmo.entity.SiteMonitoringStructureWithBLOBs;

@Mapper
public interface SiteMonitoringStructureMapper extends
		GenericMapper<SiteMonitoringStructure, SiteMonitoringStructureExample, SiteMonitoringStructureWithBLOBs> {

}