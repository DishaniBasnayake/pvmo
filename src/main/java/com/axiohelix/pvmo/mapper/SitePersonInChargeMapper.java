package com.axiohelix.pvmo.mapper;


import org.apache.ibatis.annotations.Mapper;

import com.axiohelix.pvmo.entity.SitePersonInCharge;
import com.axiohelix.pvmo.entity.SitePersonInChargeExample;
import com.axiohelix.pvmo.entity.SitePersonInChargeWithBLOBs;

@Mapper
public interface SitePersonInChargeMapper extends
GenericMapper<SitePersonInCharge, SitePersonInChargeExample, SitePersonInChargeWithBLOBs> {

}