package com.axiohelix.pvmo.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.axiohelix.pvmo.entity.SitePowerGenerationCapacity;
import com.axiohelix.pvmo.entity.SitePowerGenerationCapacityExample;
import com.axiohelix.pvmo.entity.SitePowerGenerationCapacityWithBLOBs;



@Mapper
public interface SitePowerGenerationCapacityMapper extends 
	GenericMapper<SitePowerGenerationCapacity, SitePowerGenerationCapacityExample, SitePowerGenerationCapacityWithBLOBs> {

	

}