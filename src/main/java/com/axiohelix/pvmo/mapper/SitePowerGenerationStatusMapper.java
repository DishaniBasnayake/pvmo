package com.axiohelix.pvmo.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.axiohelix.pvmo.entity.SitePowerGenerationStatus;
import com.axiohelix.pvmo.entity.SitePowerGenerationStatusExample;
import com.axiohelix.pvmo.entity.SitePowerGenerationStatusWithBLOBs;

@Mapper
public interface SitePowerGenerationStatusMapper extends
		GenericMapper<SitePowerGenerationStatus, SitePowerGenerationStatusExample, SitePowerGenerationStatusWithBLOBs> {

	

}