package com.axiohelix.pvmo.mapper;


import org.apache.ibatis.annotations.Mapper;

import com.axiohelix.pvmo.entity.SitePowerSupplySystem;
import com.axiohelix.pvmo.entity.SitePowerSupplySystemExample;
import com.axiohelix.pvmo.entity.SitePowerSupplySystemWithBLOBs;


@Mapper
public interface SitePowerSupplySystemMapper extends
GenericMapper<SitePowerSupplySystem, SitePowerSupplySystemExample, SitePowerSupplySystemWithBLOBs> {

}