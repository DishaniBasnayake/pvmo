package com.axiohelix.pvmo.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.axiohelix.pvmo.entity.SiteStatus;
import com.axiohelix.pvmo.entity.SiteStatusExample;
import com.axiohelix.pvmo.entity.SiteStatusWithBLOBs;
import java.util.List;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface SiteStatusMapper extends GenericMapper<SiteStatus, SiteStatusExample, SiteStatusWithBLOBs> {

}