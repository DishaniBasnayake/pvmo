package com.axiohelix.pvmo.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.axiohelix.pvmo.entity.SiteUrgentResponse;
import com.axiohelix.pvmo.entity.SiteUrgentResponseExample;
import com.axiohelix.pvmo.entity.SiteUrgentResponseWithBLOBs;

@Mapper
public interface SiteUrgentResponseMapper
		extends GenericMapper<SiteUrgentResponse, SiteUrgentResponseExample, SiteUrgentResponseWithBLOBs> {

}