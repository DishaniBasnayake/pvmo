package com.axiohelix.pvmo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.axiohelix.pvmo.entity.TicketWithBLOBs;
import com.axiohelix.pvmo.entity.Document;
import com.axiohelix.pvmo.entity.DocumentWithBLOBs;
import com.axiohelix.pvmo.entity.Ticket;

@Mapper
public interface TicketDocumentMapper {
	int deleteTicketDocument(String ticketId, String documentId);

	int addTicketDocument(String id, String ticketId, String documentId);

	List<Ticket> getTickets(String documentId);

	List<Document> getDocuments(String ticketId);
}
