package com.axiohelix.pvmo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.axiohelix.pvmo.entity.TicketWithBLOBs;
import com.axiohelix.pvmo.entity.UserWithBLOBs;

@Mapper
public interface TicketUserMapper {
	int deleteTicketUser(String ticketId, String userId);

	int addTicketUser(String id, String ticketId, String userId, String userType);

	List<TicketWithBLOBs> getTickets(String userId);

	List<UserWithBLOBs> getUsers(String ticketId);
	
	List<UserWithBLOBs> getNotAssignedUsers(String siteId,String ticketId);

	int updateTicketUser(String ticketId, String userId, String content);
}
