package com.axiohelix.pvmo.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.axiohelix.pvmo.entity.User;
import com.axiohelix.pvmo.entity.UserExample;
import com.axiohelix.pvmo.entity.UserWithBLOBs;

@Mapper
public interface UserMapper extends GenericMapper<User, UserExample, UserWithBLOBs> {

}