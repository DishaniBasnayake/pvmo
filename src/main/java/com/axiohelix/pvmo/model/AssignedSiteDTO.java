package com.axiohelix.pvmo.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AssignedSiteDTO {
	private String displayName;
	private String id;
}
