package com.axiohelix.pvmo.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AssignedUserDTO {
	private String name;
	private String id;
	private String userType;
}
