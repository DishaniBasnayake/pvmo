package com.axiohelix.pvmo.model;

import com.opencsv.bean.CsvBindByName;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DeviceCSV {
	@CsvBindByName
	private String id;
	
	@CsvBindByName
	private String pcs;
	
	@CsvBindByName
	private String jcb;
	
	@CsvBindByName
	private String str;
	
	@CsvBindByName
	private String panel;
}
