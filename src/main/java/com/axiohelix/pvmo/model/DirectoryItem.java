package com.axiohelix.pvmo.model;

import java.nio.file.Path;

import com.axiohelix.pvmo.util.DirectoryItemType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DirectoryItem {
	public static final String DUMMYNAME = "-99";
	private String name;
	private Long lastUpdatedOn;
	private String lastUpdatedBy;
	private Long size;
	private DirectoryItemType type;
	private Path path;
	private DirectoryItem parent;
	private boolean root = false;

	public DirectoryItem(String string, long millis, String string2, long size2, DirectoryItemType file, Path path2) {
		this.name = string;
		this.lastUpdatedOn = millis;
		this.lastUpdatedBy = string2;
		this.size = size2;
		this.type = file;
		this.path = path2;
	}
}
