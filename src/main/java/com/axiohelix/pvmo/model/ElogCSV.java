package com.axiohelix.pvmo.model;

import java.time.LocalDateTime;

import com.opencsv.bean.CsvBindByName;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ElogCSV {
	
	//@CsvBindByName(column = "Id")
	//private String id;
	
	@CsvBindByName(column = "Latitude")
	private Double latitude;
	
	@CsvBindByName(column = "Longitude")
	private Double longitude;
	
	@CsvBindByName(column = "Action")
	private String logType;
	
	@CsvBindByName(column = "Timestamp")
	private LocalDateTime logTime;
	
	@CsvBindByName(column = "Host")
	private String ip;
	
	@CsvBindByName(column = "User")
	private String userName;
	
	@CsvBindByName(column = "SiteName")
	private String siteName;

}
