package com.axiohelix.pvmo.model;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EntryExitSearch {
	
	private String site;
	private String user;
	private Date date;
	
}
