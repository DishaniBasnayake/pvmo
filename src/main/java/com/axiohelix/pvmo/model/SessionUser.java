package com.axiohelix.pvmo.model;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SessionUser extends User {
	
	private static final long serialVersionUID = 1L;

	public SessionUser(String username, String password, Collection<? extends GrantedAuthority> authorities, String userId, String userType) {
		super(username, password, authorities);
		this.userType = userType;
		this.userId = userId;
	}
	
	private String userId;

	private String userType;
	
}
