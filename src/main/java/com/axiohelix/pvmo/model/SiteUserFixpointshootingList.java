package com.axiohelix.pvmo.model;

import java.util.Set;

import com.axiohelix.pvmo.entity.SiteUserFixpointshootingWithBLOBs;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SiteUserFixpointshootingList {

	private Set<SiteUserFixpointshootingWithBLOBs> siteUserFixpointshootingList;
	
}
