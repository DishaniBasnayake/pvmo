package com.axiohelix.pvmo.model;

import com.opencsv.bean.CsvBindByName;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TlogCSV {
	@CsvBindByName
	private String id;
	
	@CsvBindByName
	private String spn;
	
	@CsvBindByName
	private String spnSub;
	
	@CsvBindByName
	private String siteName;
	
	@CsvBindByName
	private String ticketNumber;
	
	@CsvBindByName
	private String startDate;
	
	@CsvBindByName
	private String startTime;
	
	@CsvBindByName
	private String ticketOwner;
	
	@CsvBindByName
	private String status;
}
