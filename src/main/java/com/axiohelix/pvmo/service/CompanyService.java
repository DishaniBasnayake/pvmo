package com.axiohelix.pvmo.service;

import java.util.List;

import com.axiohelix.pvmo.entity.Company;
import com.axiohelix.pvmo.entity.CompanyExample;
import com.axiohelix.pvmo.entity.CompanyWithBLOBs;
import com.axiohelix.pvmo.util.CommonResponse;

public interface CompanyService extends GenericService<Company,CompanyExample,CompanyWithBLOBs>{

	

}
