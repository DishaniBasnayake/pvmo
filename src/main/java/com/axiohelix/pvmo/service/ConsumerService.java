package com.axiohelix.pvmo.service;



import com.axiohelix.pvmo.entity.Consumer;
import com.axiohelix.pvmo.entity.ConsumerExample;
import com.axiohelix.pvmo.entity.ConsumerWithBLOBs;

public interface ConsumerService extends GenericService<Consumer,ConsumerExample,ConsumerWithBLOBs> {

}
