package com.axiohelix.pvmo.service;

import java.util.List;
import java.util.Map;

import com.axiohelix.pvmo.entity.Device;
import com.axiohelix.pvmo.entity.PCS;
import com.axiohelix.pvmo.entity.Panel;
import com.axiohelix.pvmo.entity.Site;
import com.axiohelix.pvmo.entity.SiteExample;
import com.axiohelix.pvmo.entity.SiteWithBLOBs;

public interface DeviceService {

	void saveDeviceWithComponents(Map<String, Map<String, Map<String, Map<String, Panel>>>> pcsMap, String siteId);

	List<PCS> findBySiteIdWithComponents(String id, String from, String to);

}
