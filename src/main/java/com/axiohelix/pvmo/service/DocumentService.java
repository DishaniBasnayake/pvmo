package com.axiohelix.pvmo.service;

import com.axiohelix.pvmo.entity.Document;
import com.axiohelix.pvmo.entity.DocumentExample;
import com.axiohelix.pvmo.entity.DocumentWithBLOBs;

public interface DocumentService extends GenericService<Document, DocumentExample, DocumentWithBLOBs> {

}
