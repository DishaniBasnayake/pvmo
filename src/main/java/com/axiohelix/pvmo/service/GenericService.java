package com.axiohelix.pvmo.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.axiohelix.pvmo.entity.Company;
import com.axiohelix.pvmo.entity.CompanyWithBLOBs;
import com.axiohelix.pvmo.util.CommonResponse;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;

public interface GenericService<T,TExample,TWithBLOBs> {

	long countByExample(TExample example);

	int deleteByExample(TExample example);

	List<TWithBLOBs> selectByExampleWithBLOBs(TExample example);
	
	List<T> selectByExample(TExample example);

	int updateByExample(@Param("record") T record, @Param("example") TExample example);
	
	int deleteByPrimaryKey(String id);

	int insert(TWithBLOBs record);

	int insertSelective(TWithBLOBs record);

	T selectByPrimaryKey(String id);

	TWithBLOBs selectByPrimaryKeyWithBLOBs(String id);

	int updateByExampleSelective(@Param("record") TWithBLOBs record, @Param("example") TExample example);

	int updateByExampleWithBLOBs(@Param("record") TWithBLOBs record, @Param("example") TExample example);

	int updateByPrimaryKeySelective(TWithBLOBs record);

	int updateByPrimaryKeyWithBLOBs(TWithBLOBs record);

	int updateByPrimaryKey(T record);
	
	PageInfo<T> selectByExampleWithBLOBs(TExample example,int pageNo, int pageSize);

	PageInfo<T> selectByExample(TExample example,int pageNo, int pageSize);	

	CommonResponse saveUpdate(TWithBLOBs record);
	
	

}
