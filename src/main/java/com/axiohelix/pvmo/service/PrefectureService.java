package com.axiohelix.pvmo.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.axiohelix.pvmo.entity.Prefecture;
import com.axiohelix.pvmo.entity.PrefectureExample;
import com.axiohelix.pvmo.entity.PrefectureWithBLOBs;
import com.axiohelix.pvmo.util.CommonResponse;
import com.github.pagehelper.PageInfo;


  public interface PrefectureService extends
  GenericService<Prefecture,PrefectureExample,PrefectureWithBLOBs> {
  
  
  }
 