package com.axiohelix.pvmo.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.axiohelix.pvmo.entity.Region;
import com.axiohelix.pvmo.entity.RegionExample;
import com.axiohelix.pvmo.entity.RegionWithBLOBs;
import com.axiohelix.pvmo.util.CommonResponse;
import com.github.pagehelper.PageInfo;

public interface RegionService extends GenericService<Region,RegionExample,RegionWithBLOBs> {
	

}
