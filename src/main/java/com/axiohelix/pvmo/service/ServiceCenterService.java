package com.axiohelix.pvmo.service;

import com.axiohelix.pvmo.entity.ServiceCenter;
import com.axiohelix.pvmo.entity.ServiceCenterExample;
import com.axiohelix.pvmo.entity.ServiceCenterWithBLOBs;

public interface ServiceCenterService  extends GenericService<ServiceCenter,ServiceCenterExample,ServiceCenterWithBLOBs>{

}
