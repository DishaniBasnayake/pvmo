package com.axiohelix.pvmo.service;

import java.util.List;

import com.axiohelix.pvmo.entity.ServiceCenter;
import com.axiohelix.pvmo.entity.ServiceCenterWithBLOBs;
import com.axiohelix.pvmo.entity.User;
import com.axiohelix.pvmo.entity.UserWithBLOBs;

public interface ServiceCenterUserService {

	int deleteServiceCenterUser(String seviceCenterId, String userId);

	int addServiceCenterUser(String id, String seviceCenterId, String userId, String userType);

	List<ServiceCenterWithBLOBs> getServiceCenters(String userId);

	List<UserWithBLOBs> getUsers(String serviceCenterId);
	
	List<UserWithBLOBs> getNotAssignedUsers(String companyId, String serviceCenterId);
	
}
