package com.axiohelix.pvmo.service;

import com.axiohelix.pvmo.entity.SiteClient;
import com.axiohelix.pvmo.entity.SiteClientExample;
import com.axiohelix.pvmo.entity.SiteClientWithBLOBs;


public interface SiteClientService extends GenericService<SiteClient, SiteClientExample, SiteClientWithBLOBs> {

}
