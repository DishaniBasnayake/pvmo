package com.axiohelix.pvmo.service;

import com.axiohelix.pvmo.entity.SiteConstructionCompany;
import com.axiohelix.pvmo.entity.SiteConstructionCompanyExample;
import com.axiohelix.pvmo.entity.SiteConstructionCompanyWithBLOBs;

public interface SiteConstructionCompanyService extends
		GenericService<SiteConstructionCompany, SiteConstructionCompanyExample, SiteConstructionCompanyWithBLOBs> {

}
