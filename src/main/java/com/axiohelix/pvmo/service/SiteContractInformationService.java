package com.axiohelix.pvmo.service;

import com.axiohelix.pvmo.entity.SiteContractInformation;
import com.axiohelix.pvmo.entity.SiteContractInformationExample;
import com.axiohelix.pvmo.entity.SiteContractInformationWithBLOBs;

public interface SiteContractInformationService extends
		GenericService<SiteContractInformation, SiteContractInformationExample, SiteContractInformationWithBLOBs> {

}
