package com.axiohelix.pvmo.service;

import com.axiohelix.pvmo.entity.SiteDocument;
import com.axiohelix.pvmo.entity.SiteDocumentExample;
import com.axiohelix.pvmo.entity.SiteDocumentWithBLOBs;

public interface SiteDocumentService extends GenericService<SiteDocument, SiteDocumentExample, SiteDocumentWithBLOBs> {

	
}
