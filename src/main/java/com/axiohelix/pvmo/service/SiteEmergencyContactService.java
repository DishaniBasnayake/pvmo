package com.axiohelix.pvmo.service;

import com.axiohelix.pvmo.entity.SiteEmergencyContact;
import com.axiohelix.pvmo.entity.SiteEmergencyContactExample;
import com.axiohelix.pvmo.entity.SiteEmergencyContactWithBLOBs;

public interface SiteEmergencyContactService extends
		GenericService<SiteEmergencyContact, SiteEmergencyContactExample, SiteEmergencyContactWithBLOBs> {

}