package com.axiohelix.pvmo.service;

import com.axiohelix.pvmo.entity.SiteMonitoringStructure;
import com.axiohelix.pvmo.entity.SiteMonitoringStructureExample;
import com.axiohelix.pvmo.entity.SiteMonitoringStructureWithBLOBs;

public interface SiteMonitoringStructureService extends
		GenericService<SiteMonitoringStructure, SiteMonitoringStructureExample, SiteMonitoringStructureWithBLOBs> {

}