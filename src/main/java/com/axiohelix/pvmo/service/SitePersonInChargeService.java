package com.axiohelix.pvmo.service;

import com.axiohelix.pvmo.entity.SitePersonInCharge;
import com.axiohelix.pvmo.entity.SitePersonInChargeExample;
import com.axiohelix.pvmo.entity.SitePersonInChargeWithBLOBs;

public interface SitePersonInChargeService
		extends GenericService<SitePersonInCharge, SitePersonInChargeExample, SitePersonInChargeWithBLOBs> {

}