package com.axiohelix.pvmo.service;


import com.axiohelix.pvmo.entity.SitePowerGenerationCapacity;
import com.axiohelix.pvmo.entity.SitePowerGenerationCapacityExample;
import com.axiohelix.pvmo.entity.SitePowerGenerationCapacityWithBLOBs;


public interface SitePowerGenerationCapacityService extends GenericService<SitePowerGenerationCapacity, 
	SitePowerGenerationCapacityExample, SitePowerGenerationCapacityWithBLOBs> {

}
