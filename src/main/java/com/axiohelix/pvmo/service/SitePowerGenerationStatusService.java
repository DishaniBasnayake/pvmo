package com.axiohelix.pvmo.service;

import com.axiohelix.pvmo.entity.SitePowerGenerationStatus;
import com.axiohelix.pvmo.entity.SitePowerGenerationStatusExample;
import com.axiohelix.pvmo.entity.SitePowerGenerationStatusWithBLOBs;

public interface SitePowerGenerationStatusService extends
		GenericService<SitePowerGenerationStatus, SitePowerGenerationStatusExample, SitePowerGenerationStatusWithBLOBs> {

}
