package com.axiohelix.pvmo.service;

import com.axiohelix.pvmo.entity.SitePowerSupplySystem;
import com.axiohelix.pvmo.entity.SitePowerSupplySystemExample;
import com.axiohelix.pvmo.entity.SitePowerSupplySystemWithBLOBs;


public interface SitePowerSupplySystemService extends GenericService<SitePowerSupplySystem, SitePowerSupplySystemExample,
SitePowerSupplySystemWithBLOBs> {

}
