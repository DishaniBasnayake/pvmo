package com.axiohelix.pvmo.service;

import java.util.List;

import com.axiohelix.pvmo.entity.Site;
import com.axiohelix.pvmo.entity.SiteExample;
import com.axiohelix.pvmo.entity.SiteWithBLOBs;

public interface SiteService  extends GenericService<Site,SiteExample,SiteWithBLOBs>{
	
	List<String> getNishikataPanelIdsWithErrors();
	
	SiteWithBLOBs getSiteBySPN(String spn);

}
