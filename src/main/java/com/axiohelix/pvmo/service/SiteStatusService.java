package com.axiohelix.pvmo.service;

import com.axiohelix.pvmo.entity.SiteStatus;
import com.axiohelix.pvmo.entity.SiteStatusExample;
import com.axiohelix.pvmo.entity.SiteStatusWithBLOBs;

public interface SiteStatusService extends GenericService<SiteStatus, SiteStatusExample, SiteStatusWithBLOBs> {

}
