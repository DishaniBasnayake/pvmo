package com.axiohelix.pvmo.service;

import com.axiohelix.pvmo.entity.SiteUrgentResponse;
import com.axiohelix.pvmo.entity.SiteUrgentResponseExample;
import com.axiohelix.pvmo.entity.SiteUrgentResponseWithBLOBs;

public interface SiteUrgentResponseService
		extends GenericService<SiteUrgentResponse, SiteUrgentResponseExample, SiteUrgentResponseWithBLOBs> {

}