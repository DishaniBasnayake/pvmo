package com.axiohelix.pvmo.service;

import com.axiohelix.pvmo.entity.SiteUserFixpointshooting;
import com.axiohelix.pvmo.entity.SiteUserFixpointshootingExample;
import com.axiohelix.pvmo.entity.SiteUserFixpointshootingWithBLOBs;

public interface SiteUserFixpointshootingService extends GenericService<SiteUserFixpointshooting, SiteUserFixpointshootingExample, SiteUserFixpointshootingWithBLOBs> {

	
}
