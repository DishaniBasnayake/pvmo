package com.axiohelix.pvmo.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.axiohelix.pvmo.entity.SiteUserGPSLog;
import com.axiohelix.pvmo.entity.SiteUserGPSLogExample;
import com.axiohelix.pvmo.entity.SiteUserGPSLogWithBLOBs;
import com.axiohelix.pvmo.entity.custom.SiteUserGPSLogExtended;
import com.axiohelix.pvmo.model.EntryExitSearch;

public interface SiteUserGPSLogService extends GenericService<SiteUserGPSLog, SiteUserGPSLogExample, SiteUserGPSLogWithBLOBs> {

	List<SiteUserGPSLogExtended> latest(String siteId);
	
	List<SiteUserGPSLogExtended> findAll();
	
	Page<SiteUserGPSLogExtended> searchEntryExit(EntryExitSearch search, Pageable pageable);
}
