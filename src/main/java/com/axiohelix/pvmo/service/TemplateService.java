package com.axiohelix.pvmo.service;

import com.axiohelix.pvmo.entity.Template;
import com.axiohelix.pvmo.entity.TemplateExample;
import com.axiohelix.pvmo.entity.TemplateWithBLOBs;

public interface TemplateService  extends GenericService<Template,TemplateExample,TemplateWithBLOBs>{

}
