package com.axiohelix.pvmo.service;

import java.util.List;

import com.axiohelix.pvmo.entity.Document;
import com.axiohelix.pvmo.entity.Ticket;

public interface TicketDocumentService {
	int deleteTicketDocument(String ticketId, String DocumentId);

	int addTicketDocument(String id, String ticketId, String documentId);

	List<Ticket> getTickets(String DocumentId);

	List<Document> getDocuments(String ticketId);
	
}
