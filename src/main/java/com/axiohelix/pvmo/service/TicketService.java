package com.axiohelix.pvmo.service;


import java.util.List;

import com.axiohelix.pvmo.entity.Ticket;
import com.axiohelix.pvmo.entity.TicketExample;
import com.axiohelix.pvmo.entity.TicketWithBLOBs;

public interface TicketService  extends GenericService<Ticket,TicketExample,TicketWithBLOBs>{

	void insertMultiple(List<TicketWithBLOBs> tickets);

	Integer getExistingTicketCount();

}
