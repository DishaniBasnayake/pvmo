package com.axiohelix.pvmo.service;

import java.util.List;

import com.axiohelix.pvmo.entity.TicketUser;
import com.axiohelix.pvmo.entity.TicketWithBLOBs;
import com.axiohelix.pvmo.entity.User;
import com.axiohelix.pvmo.entity.UserWithBLOBs;

public interface TicketUserService {
	int deleteTicketUser(String ticketId, String userId);

	int addTicketUser(String id, String ticketId, String userId, String userType);

	List<TicketWithBLOBs> getTickets(String userId);

	List<UserWithBLOBs> getUsers(String ticketId);
	
	List<UserWithBLOBs> getNotAssignedUsers(String siteId, String ticketId);

	int updateTicketUser(String ticketId, String userId, String content);
}
