package com.axiohelix.pvmo.service;

import com.axiohelix.pvmo.entity.User;
import com.axiohelix.pvmo.entity.UserExample;
import com.axiohelix.pvmo.entity.UserWithBLOBs;

public interface UserService extends GenericService<User,UserExample,UserWithBLOBs>{

	User findByUsername(String username);

}
