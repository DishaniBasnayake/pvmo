package com.axiohelix.pvmo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.axiohelix.pvmo.entity.Company;
import com.axiohelix.pvmo.entity.CompanyExample;
import com.axiohelix.pvmo.entity.CompanyWithBLOBs;
import com.axiohelix.pvmo.mapper.CompanyMapper;
import com.axiohelix.pvmo.mapper.GenericMapper;
import com.axiohelix.pvmo.service.CompanyService;

@Service
public class CompanyServiceImpl extends GenericServiceImpl<Company, CompanyExample, CompanyWithBLOBs>
		implements CompanyService {

	@Autowired CompanyMapper mapper;
	
	public CompanyServiceImpl(GenericMapper<Company, CompanyExample, CompanyWithBLOBs> mapper) {
		super(mapper);
	}


}
