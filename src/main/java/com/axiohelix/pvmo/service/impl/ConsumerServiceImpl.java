package com.axiohelix.pvmo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.axiohelix.pvmo.entity.Consumer;
import com.axiohelix.pvmo.entity.ConsumerExample;
import com.axiohelix.pvmo.entity.ConsumerWithBLOBs;
import com.axiohelix.pvmo.mapper.ConsumerMapper;
import com.axiohelix.pvmo.mapper.GenericMapper;
import com.axiohelix.pvmo.service.ConsumerService;


@Service
public class ConsumerServiceImpl extends GenericServiceImpl<Consumer, ConsumerExample, ConsumerWithBLOBs>
		implements ConsumerService {

@Autowired ConsumerMapper mapper;
	
	public ConsumerServiceImpl(GenericMapper<Consumer, ConsumerExample, ConsumerWithBLOBs> mapper) {
		super(mapper);
	}

	
	

}
