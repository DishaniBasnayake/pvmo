package com.axiohelix.pvmo.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.axiohelix.pvmo.entity.Device;
import com.axiohelix.pvmo.entity.JCB;
import com.axiohelix.pvmo.entity.PCS;
import com.axiohelix.pvmo.entity.Panel;
import com.axiohelix.pvmo.entity.PanelString;
import com.axiohelix.pvmo.entity.Site;
import com.axiohelix.pvmo.entity.SiteExample;
import com.axiohelix.pvmo.entity.SiteWithBLOBs;
import com.axiohelix.pvmo.entity.Ticket;
import com.axiohelix.pvmo.entity.TicketExample;
import com.axiohelix.pvmo.entity.TicketWithBLOBs;
import com.axiohelix.pvmo.mapper.GenericMapper;
import com.axiohelix.pvmo.mapper.JCBMapper;
import com.axiohelix.pvmo.mapper.PCSMapper;
import com.axiohelix.pvmo.mapper.PanelMapper;
import com.axiohelix.pvmo.mapper.PanelStringMapper;
import com.axiohelix.pvmo.mapper.SiteMapper;
import com.axiohelix.pvmo.mapper.TicketMapper;
import com.axiohelix.pvmo.service.DeviceService;
import com.axiohelix.pvmo.service.SiteService;
import com.axiohelix.pvmo.service.TicketService;
import com.axiohelix.pvmo.type.Status;
import com.axiohelix.pvmo.util.Snowflake;

@Service
public class DeviceServiceImpl implements DeviceService {

	@Autowired PCSMapper pcsMapper;
	@Autowired JCBMapper jcbMapper;
	@Autowired PanelStringMapper panelStringMapper;
	@Autowired PanelMapper panelMapper;
	
	@Override
	@Transactional
	public void saveDeviceWithComponents(
			Map<String, Map<String, Map<String, Map<String, Panel>>>> pcsMap, String siteId) {
		
		//List<Device> deviceList = new ArrayList<Device>();
		List<PCS> pcsList = new ArrayList<PCS>();
		List<JCB> jcbList = new ArrayList<JCB>();
		List<PanelString> panelStringList = new ArrayList<PanelString>();
		List<Panel> panelList = new ArrayList<Panel>();
		
		//for(Entry<String, Map<String, Map<String, Map<String, Map<String, Panel>>>>> deviceItem : deviceMap.entrySet()) {
			//check whether device exists
			//Device device = deviceMapper.findByObjectId(deviceItem.getKey());
			/*if(device==null) {
				device = new Device();
				device.setId(String.valueOf(Snowflake.newId()));
				device.setObjectId(deviceItem.getKey());
				device.setSiteId(siteId);
				device.setStatus(Status.ACTIVE.getDbValue().byteValue());
				//deviceMapper.insert(device);
				deviceList.add(device);
			}	*/
			
			for(Entry<String, Map<String, Map<String, Map<String, Panel>>>> pcsItem : pcsMap.entrySet()) {
				PCS pcs = pcsMapper.findByObjectId(pcsItem.getKey());
				if(pcs==null) {
					pcs = new PCS();
					pcs.setId(String.valueOf(Snowflake.newId()));
					pcs.setObjectId(pcsItem.getKey());
					pcs.setSiteId(siteId);
					pcs.setStatus(Status.ACTIVE.getDbValue().byteValue());
					//pcsMapper.insert(pcs);
					pcsList.add(pcs);
				}
				for(Entry<String, Map<String, Map<String, Panel>>> jcbItem : pcsItem.getValue().entrySet()) {
					JCB jcb = jcbMapper.findByObjectIdPcsId(jcbItem.getKey(),pcs.getId());
					if(jcb==null) {
						jcb = new JCB();
						jcb.setId(String.valueOf(Snowflake.newId()));
						jcb.setObjectId(jcbItem.getKey());
						jcb.setPcsId(pcs.getId());
						jcb.setStatus(Status.ACTIVE.getDbValue().byteValue());
						//jcbMapper.insert(jcb);
						jcbList.add(jcb);
					}
					for(Entry<String, Map<String, Panel>> panelStringItem : jcbItem.getValue().entrySet()) {
						PanelString panelString = panelStringMapper.findByObjectId(panelStringItem.getKey(),jcb.getId());
						if(panelString==null) {
							panelString = new PanelString();
							panelString.setId(String.valueOf(Snowflake.newId()));
							panelString.setObjectId(panelStringItem.getKey());
							panelString.setJcbId(jcb.getId());
							panelString.setStatus(Status.ACTIVE.getDbValue().byteValue());
							//panelStringMapper.insert(panelString);
							panelStringList.add(panelString);
						}
						for(Entry<String, Panel> panelItem : panelStringItem.getValue().entrySet()) {
							Panel panel = panelMapper.findByObjectIdPanelStringId(panelItem.getKey(),panelString.getId());
							if(panel==null) {
								panel = panelItem.getValue();
								panel.setId(String.valueOf(Snowflake.newId()));
								panel.setPanelStringId(panelString.getId());
								panel.setStatus(Status.ACTIVE.getDbValue().byteValue());
								//panelMapper.insert(panel);
								panelList.add(panel);
							}
						}
					}
					
				}
				
			}
				
//			device.getValue()
		//}
		
/*		if (deviceList.size() > 0) {
			//deviceMapper.insertMultiple(deviceList);
			deviceList.forEach(i -> deviceMapper.insert(i));
		}
*/
		if (pcsList.size() > 0) {
			//pcsMapper.insertMultiple(pcsList);
			pcsList.forEach(i -> pcsMapper.insert(i));
		}
		if (jcbList.size() > 0) {
			//jcbMapper.insertMultiple(jcbList);
			jcbList.forEach(i -> jcbMapper.insert(i));
		}
		if (panelStringList.size() > 0) {
			//panelStringMapper.insertMultiple(panelStringList);
			panelStringList.forEach(i -> panelStringMapper.insert(i));
		}
		if (panelList.size() > 0) {
			//panelMapper.insertMultiple(panelList);
			panelList.forEach(i -> panelMapper.insert(i));
		}
		
	}

	@Override
	public List<PCS> findBySiteIdWithComponents(String id, String from, String to) {
		return pcsMapper.findBySiteIdWithComponents(id, Integer.parseInt(from), Integer.parseInt(to));
	}
	
	


}
