package com.axiohelix.pvmo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.axiohelix.pvmo.entity.Document;
import com.axiohelix.pvmo.entity.DocumentExample;
import com.axiohelix.pvmo.entity.DocumentWithBLOBs;
import com.axiohelix.pvmo.mapper.DocumentMapper;
import com.axiohelix.pvmo.mapper.GenericMapper;
import com.axiohelix.pvmo.service.DocumentService;

@Service
public class DocumentServiceImpl extends GenericServiceImpl<Document,DocumentExample,DocumentWithBLOBs>
		implements DocumentService {

	@Autowired DocumentMapper mapper;
	
	public DocumentServiceImpl(GenericMapper<Document,DocumentExample,DocumentWithBLOBs>mapper) {
		super(mapper);
	}


}
