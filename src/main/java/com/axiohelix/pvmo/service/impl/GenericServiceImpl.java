package com.axiohelix.pvmo.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.axiohelix.pvmo.controller.CompanyController;
import com.axiohelix.pvmo.entity.CompanyExample;
import com.axiohelix.pvmo.entity.GenericEntity;
import com.axiohelix.pvmo.mapper.CompanyMapper;
import com.axiohelix.pvmo.mapper.GenericMapper;
import com.axiohelix.pvmo.service.GenericService;
import com.axiohelix.pvmo.type.Status;
import com.axiohelix.pvmo.util.CommonResponse;
import com.axiohelix.pvmo.util.CommonValidation;
import com.axiohelix.pvmo.util.DateTimeUtil;
import com.axiohelix.pvmo.util.Snowflake;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class GenericServiceImpl<T extends GenericEntity, TExample extends GenericEntity, TWithBLOBs extends GenericEntity>
		implements GenericService<T, TExample, TWithBLOBs> {

	private static final Logger LOGGER = LogManager.getLogger(GenericServiceImpl.class);
	private final GenericMapper<T, TExample, TWithBLOBs> genericMapper;

	@Override
	public long countByExample(TExample example) {
		return genericMapper.countByExample(example);
	}

	@Override
	public int deleteByExample(TExample example) {
		return genericMapper.deleteByExample(example);
	}

	@Override
	public int insert(TWithBLOBs record) {
		return genericMapper.insert(record);
	}

	@Override
	public int insertSelective(TWithBLOBs record) {
		return genericMapper.insertSelective(record);
	}

	@Override
	public List<TWithBLOBs> selectByExampleWithBLOBs(TExample example) {
		return genericMapper.selectByExampleWithBLOBs(example);
	}

	@Override
	public List<T> selectByExample(TExample example) {
		return genericMapper.selectByExample(example);
	}

	@Override
	public int updateByExampleWithBLOBs(TWithBLOBs record, TExample example) {
		return genericMapper.updateByExampleWithBLOBs(record, example);
	}

	@Override
	public int updateByExample(T record, TExample example) {
		return genericMapper.updateByExample(record, example);
	}

	@Override
	public int updateByExampleSelective(TWithBLOBs record, TExample example) {
		return genericMapper.updateByExampleSelective(record, example);
	}

	@Override
	public int updateByPrimaryKeySelective(TWithBLOBs record) {
		return genericMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKeyWithBLOBs(TWithBLOBs record) {
		return genericMapper.updateByPrimaryKeyWithBLOBs(record);
	}

	@Override
	public int updateByPrimaryKey(T record) {
		return genericMapper.updateByPrimaryKey(record);
	}

	@Override
	public int deleteByPrimaryKey(String id) {
		return genericMapper.deleteByPrimaryKey(id);
	}

	@Override
	public TWithBLOBs selectByPrimaryKeyWithBLOBs(String id) {
		return genericMapper.selectByPrimaryKey(id);
	}
	
	@Override
	public T selectByPrimaryKey(String id) {
		return (T) genericMapper.selectByPrimaryKey(id);
	}

	/* User defined */

	@Override
	public PageInfo<T> selectByExampleWithBLOBs(TExample example, int pageNo, int pageSize) {
		PageHelper.startPage(pageNo, pageSize);
		List<T> list = genericMapper.selectByExample(example);
		return new PageInfo<T>(list);
	}

	@Override
	public PageInfo<T> selectByExample(TExample example, int pageNo, int pageSize) {
		PageHelper.startPage(pageNo, pageSize);
		List<T> list = genericMapper.selectByExample(example);
		return new PageInfo<T>(list);
	}

	@Override
	@Transactional
	public CommonResponse saveUpdate(TWithBLOBs record) {
		CommonResponse commonResponse = new CommonResponse();
		try {
			if (CommonValidation.stringNullValidation(record.getId())) {
				record.setId(String.valueOf(Snowflake.newId()));
				record.setCreatedOn(DateTimeUtil.getUtilTime());
				record.setLastUpdateOn(DateTimeUtil.getUtilTime());
				record.setStatus(Status.ACTIVE.getDbValue().byteValue());
				genericMapper.insert(record);
			} else {
				record.setLastUpdateOn(DateTimeUtil.getUtilTime());
				genericMapper.updateByPrimaryKeySelective(record);
			}
			commonResponse.getPayload().add(record);
			commonResponse.setStatus(true);
		} catch (Exception e) {
			commonResponse.getErrorMessages().add(e.getMessage());
			LOGGER.warn("/**************** Exception in insert record -> save()" + e);
		}
		return commonResponse;
	}
}
