package com.axiohelix.pvmo.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import com.axiohelix.pvmo.entity.Region;
import com.axiohelix.pvmo.entity.RegionExample;
import com.axiohelix.pvmo.entity.RegionWithBLOBs;
import com.axiohelix.pvmo.mapper.GenericMapper;
import com.axiohelix.pvmo.mapper.RegionMapper;
import com.axiohelix.pvmo.service.RegionService;
import com.axiohelix.pvmo.type.Status;
import com.axiohelix.pvmo.util.CommonResponse;
import com.axiohelix.pvmo.util.CommonValidation;
import com.axiohelix.pvmo.util.DateTimeUtil;
import com.axiohelix.pvmo.util.Snowflake;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@Service
public class RegionServiceImpl extends GenericServiceImpl<Region, RegionExample, RegionWithBLOBs>
		implements RegionService {

	@Autowired
	RegionMapper mapper;
	
	public RegionServiceImpl(GenericMapper<Region, RegionExample, RegionWithBLOBs> mapper) {
		super(mapper);

	}

}
