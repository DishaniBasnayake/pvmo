package com.axiohelix.pvmo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.axiohelix.pvmo.entity.Company;
import com.axiohelix.pvmo.entity.CompanyExample;
import com.axiohelix.pvmo.entity.CompanyWithBLOBs;
import com.axiohelix.pvmo.entity.ServiceCenter;
import com.axiohelix.pvmo.entity.ServiceCenterExample;
import com.axiohelix.pvmo.entity.ServiceCenterWithBLOBs;
import com.axiohelix.pvmo.mapper.CompanyMapper;
import com.axiohelix.pvmo.mapper.GenericMapper;
import com.axiohelix.pvmo.mapper.ServiceCenterMapper;
import com.axiohelix.pvmo.service.CompanyService;
import com.axiohelix.pvmo.service.GenericService;
import com.axiohelix.pvmo.service.ServiceCenterService;

@Service
public class ServiceCenterServiceImpl extends GenericServiceImpl<ServiceCenter,ServiceCenterExample,ServiceCenterWithBLOBs>
		implements ServiceCenterService {

	@Autowired ServiceCenterMapper mapper;
	
	public ServiceCenterServiceImpl(GenericMapper<ServiceCenter,ServiceCenterExample,ServiceCenterWithBLOBs> mapper) {
		super(mapper);
	}


}
