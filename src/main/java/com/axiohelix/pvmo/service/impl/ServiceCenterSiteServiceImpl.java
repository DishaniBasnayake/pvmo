package com.axiohelix.pvmo.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.axiohelix.pvmo.entity.ServiceCenter;
import com.axiohelix.pvmo.entity.Site;
import com.axiohelix.pvmo.entity.SiteWithBLOBs;
import com.axiohelix.pvmo.mapper.ServiceCenterSiteMapper;
import com.axiohelix.pvmo.service.ServiceCenterSiteService;

@Service
public class ServiceCenterSiteServiceImpl implements ServiceCenterSiteService {

	private static final Logger LOGGER = LogManager.getLogger(ServiceCenterSiteServiceImpl.class);
	@Autowired
	ServiceCenterSiteMapper mapper;

	@Override
	public int deleteServiceCenterSite(String seviceCenterId, String siteId) {
		return mapper.deleteServiceCenterSite(seviceCenterId, siteId);
	}

	@Override
	public int addServiceCenterSite(String id, String seviceCenterId, String siteId) {
		return mapper.addServiceCenterSite(id, seviceCenterId, siteId);
	}

	@Override
	public List<ServiceCenter> getServiceCenters(String siteId) {
		return mapper.getServiceCenters(siteId);
	}

	@Override
	public List<SiteWithBLOBs> getSites(String serviceCenterId) {
		return mapper.getSites(serviceCenterId);
	}
	
	@Override
	public List<Site> getNotAssignedSites(String companyId,String serviceCenterId) {
		return mapper.getNotAssignedSites(companyId,serviceCenterId);
	}

}
