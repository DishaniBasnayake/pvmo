package com.axiohelix.pvmo.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.axiohelix.pvmo.entity.ServiceCenter;
import com.axiohelix.pvmo.entity.ServiceCenterWithBLOBs;
import com.axiohelix.pvmo.entity.User;
import com.axiohelix.pvmo.entity.UserWithBLOBs;
import com.axiohelix.pvmo.mapper.ServiceCenterUserMapper;
import com.axiohelix.pvmo.service.ServiceCenterUserService;

@Service
public class ServiceCenterUserServiceImpl implements ServiceCenterUserService {

	private static final Logger LOGGER = LogManager.getLogger(ServiceCenterUserServiceImpl.class);
	@Autowired
	ServiceCenterUserMapper mapper;

	@Override
	public int deleteServiceCenterUser(String seviceCenterId, String userId) {
		return mapper.deleteServiceCenterUser(seviceCenterId, userId);
	}

	@Override
	public int addServiceCenterUser(String id, String seviceCenterId, String userId, String userType) {
		return mapper.addServiceCenterUser(id, seviceCenterId, userId, userType);
	}

	@Override
	public List<ServiceCenterWithBLOBs> getServiceCenters(String userId) {
		return mapper.getServiceCenters(userId);
	}

	@Override
	public List<UserWithBLOBs> getUsers(String serviceCenterId) {
		return mapper.getUsers(serviceCenterId);
	}
	
	@Override
	public List<UserWithBLOBs> getNotAssignedUsers(String companyId,String serviceCenterId) {
		return mapper.getNotAssignedUsers(companyId,serviceCenterId);
	}


}
