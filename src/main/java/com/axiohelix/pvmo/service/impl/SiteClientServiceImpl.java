package com.axiohelix.pvmo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.axiohelix.pvmo.entity.SiteClient;
import com.axiohelix.pvmo.entity.SiteClientExample;
import com.axiohelix.pvmo.entity.SiteClientWithBLOBs;

import com.axiohelix.pvmo.mapper.GenericMapper;
import com.axiohelix.pvmo.mapper.SiteClientMapper;
import com.axiohelix.pvmo.service.SiteClientService;


@Service
public class SiteClientServiceImpl extends GenericServiceImpl<SiteClient,SiteClientExample,SiteClientWithBLOBs>
		implements SiteClientService {

	@Autowired SiteClientMapper clientMapper;
	
	public SiteClientServiceImpl(GenericMapper<SiteClient,SiteClientExample,SiteClientWithBLOBs> clientMapper) {
		super(clientMapper);
	}


}
