package com.axiohelix.pvmo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.axiohelix.pvmo.entity.SiteConstructionCompany;
import com.axiohelix.pvmo.entity.SiteConstructionCompanyExample;
import com.axiohelix.pvmo.entity.SiteConstructionCompanyWithBLOBs;
import com.axiohelix.pvmo.entity.SitePowerGenerationCapacity;
import com.axiohelix.pvmo.entity.SitePowerGenerationCapacityExample;
import com.axiohelix.pvmo.entity.SitePowerGenerationCapacityWithBLOBs;
import com.axiohelix.pvmo.mapper.GenericMapper;
import com.axiohelix.pvmo.mapper.SiteConstructionCompanyMapper;
import com.axiohelix.pvmo.mapper.SitePowerGenerationCapacityMapper;
import com.axiohelix.pvmo.service.SiteConstructionCompanyService;
import com.axiohelix.pvmo.service.SitePowerGenerationCapacityService;

@Service
public class SiteConstructionCompanyServiceImpl extends
		GenericServiceImpl<SiteConstructionCompany, SiteConstructionCompanyExample, SiteConstructionCompanyWithBLOBs>
		implements SiteConstructionCompanyService {

	@Autowired
	SiteConstructionCompanyMapper mapper;

	public SiteConstructionCompanyServiceImpl(
			GenericMapper<SiteConstructionCompany, SiteConstructionCompanyExample, SiteConstructionCompanyWithBLOBs> mapper) {
		super(mapper);
	}

}
