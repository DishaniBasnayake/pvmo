package com.axiohelix.pvmo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.axiohelix.pvmo.entity.SiteContractInformation;
import com.axiohelix.pvmo.entity.SiteContractInformationExample;
import com.axiohelix.pvmo.entity.SiteContractInformationWithBLOBs;
import com.axiohelix.pvmo.mapper.GenericMapper;
import com.axiohelix.pvmo.mapper.SiteContractInformationMapper;
import com.axiohelix.pvmo.service.SiteContractInformationService;


@Service
public class SiteContractInformationServiceImpl extends
		GenericServiceImpl<SiteContractInformation, SiteContractInformationExample, SiteContractInformationWithBLOBs>
		implements SiteContractInformationService {

	@Autowired
	SiteContractInformationMapper mapper;

	public SiteContractInformationServiceImpl(
			GenericMapper<SiteContractInformation, SiteContractInformationExample, SiteContractInformationWithBLOBs> mapper) {
		super(mapper);
	}

}
