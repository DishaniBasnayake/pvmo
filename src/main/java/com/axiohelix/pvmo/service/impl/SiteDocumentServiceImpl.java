package com.axiohelix.pvmo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.axiohelix.pvmo.entity.SiteDocument;
import com.axiohelix.pvmo.entity.SiteDocumentExample;
import com.axiohelix.pvmo.entity.SiteDocumentWithBLOBs;
import com.axiohelix.pvmo.mapper.GenericMapper;
import com.axiohelix.pvmo.mapper.SiteDocumentMapper;
import com.axiohelix.pvmo.service.SiteDocumentService;

@Service
public class SiteDocumentServiceImpl extends GenericServiceImpl<SiteDocument,SiteDocumentExample,SiteDocumentWithBLOBs>
		implements SiteDocumentService {

	@Autowired SiteDocumentMapper mapper;
	
	public SiteDocumentServiceImpl(GenericMapper<SiteDocument,SiteDocumentExample,SiteDocumentWithBLOBs> mapper) {
		super(mapper);
	}

}
