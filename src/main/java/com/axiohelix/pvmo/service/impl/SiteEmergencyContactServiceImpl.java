package com.axiohelix.pvmo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.axiohelix.pvmo.entity.SiteEmergencyContact;
import com.axiohelix.pvmo.entity.SiteEmergencyContactExample;
import com.axiohelix.pvmo.entity.SiteEmergencyContactWithBLOBs;
import com.axiohelix.pvmo.mapper.GenericMapper;
import com.axiohelix.pvmo.mapper.SiteEmergencyContactMapper;
import com.axiohelix.pvmo.service.SiteEmergencyContactService;


@Service
public class SiteEmergencyContactServiceImpl extends
		GenericServiceImpl<SiteEmergencyContact, SiteEmergencyContactExample, SiteEmergencyContactWithBLOBs>
		implements SiteEmergencyContactService {

	@Autowired
	SiteEmergencyContactMapper mapper;

	public SiteEmergencyContactServiceImpl(
			GenericMapper<SiteEmergencyContact, SiteEmergencyContactExample, SiteEmergencyContactWithBLOBs> mapper) {
		super(mapper);
	}

}
