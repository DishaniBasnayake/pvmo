package com.axiohelix.pvmo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.axiohelix.pvmo.entity.SiteMonitoringStructure;
import com.axiohelix.pvmo.entity.SiteMonitoringStructureExample;
import com.axiohelix.pvmo.entity.SiteMonitoringStructureWithBLOBs;
import com.axiohelix.pvmo.mapper.GenericMapper;
import com.axiohelix.pvmo.mapper.SiteMonitoringStructureMapper;
import com.axiohelix.pvmo.service.SiteMonitoringStructureService;


@Service
public class SiteMonitoringStructureServiceImpl extends
		GenericServiceImpl<SiteMonitoringStructure, SiteMonitoringStructureExample, SiteMonitoringStructureWithBLOBs>
		implements SiteMonitoringStructureService {

	@Autowired
	SiteMonitoringStructureMapper mapper;

	public SiteMonitoringStructureServiceImpl(
			GenericMapper<SiteMonitoringStructure, SiteMonitoringStructureExample, SiteMonitoringStructureWithBLOBs> mapper) {
		super(mapper);
	}

}
