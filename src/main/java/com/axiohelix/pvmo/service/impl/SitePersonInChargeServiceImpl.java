package com.axiohelix.pvmo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.axiohelix.pvmo.entity.SitePersonInCharge;
import com.axiohelix.pvmo.entity.SitePersonInChargeExample;
import com.axiohelix.pvmo.entity.SitePersonInChargeWithBLOBs;
import com.axiohelix.pvmo.mapper.GenericMapper;
import com.axiohelix.pvmo.mapper.SitePersonInChargeMapper;
import com.axiohelix.pvmo.service.SitePersonInChargeService;



@Service
public class SitePersonInChargeServiceImpl extends GenericServiceImpl<SitePersonInCharge,SitePersonInChargeExample,SitePersonInChargeWithBLOBs>
		implements SitePersonInChargeService {

	@Autowired SitePersonInChargeMapper mapper;
	
	public SitePersonInChargeServiceImpl(GenericMapper<SitePersonInCharge,SitePersonInChargeExample,SitePersonInChargeWithBLOBs> mapper) {
		super(mapper);
	}


}
