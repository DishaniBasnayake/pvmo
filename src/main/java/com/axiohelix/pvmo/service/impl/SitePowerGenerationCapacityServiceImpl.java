package com.axiohelix.pvmo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.axiohelix.pvmo.entity.SitePowerGenerationCapacity;
import com.axiohelix.pvmo.entity.SitePowerGenerationCapacityExample;
import com.axiohelix.pvmo.entity.SitePowerGenerationCapacityWithBLOBs;
import com.axiohelix.pvmo.mapper.GenericMapper;
import com.axiohelix.pvmo.mapper.SitePowerGenerationCapacityMapper;
import com.axiohelix.pvmo.service.SitePowerGenerationCapacityService;

@Service
public class SitePowerGenerationCapacityServiceImpl extends
		GenericServiceImpl<SitePowerGenerationCapacity, SitePowerGenerationCapacityExample, SitePowerGenerationCapacityWithBLOBs>
		implements SitePowerGenerationCapacityService {

	@Autowired
	SitePowerGenerationCapacityMapper mapper;

	public SitePowerGenerationCapacityServiceImpl(
			GenericMapper<SitePowerGenerationCapacity, SitePowerGenerationCapacityExample, SitePowerGenerationCapacityWithBLOBs> mapper) {
		super(mapper);
	}

}
