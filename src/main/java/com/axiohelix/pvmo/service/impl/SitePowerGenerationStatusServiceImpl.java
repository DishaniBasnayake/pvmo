package com.axiohelix.pvmo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.axiohelix.pvmo.entity.SitePowerGenerationStatus;
import com.axiohelix.pvmo.entity.SitePowerGenerationStatusExample;
import com.axiohelix.pvmo.entity.SitePowerGenerationStatusWithBLOBs;
import com.axiohelix.pvmo.mapper.GenericMapper;
import com.axiohelix.pvmo.mapper.SitePowerGenerationStatusMapper;
import com.axiohelix.pvmo.service.SitePowerGenerationStatusService;

@Service
public class SitePowerGenerationStatusServiceImpl extends
		GenericServiceImpl<SitePowerGenerationStatus, SitePowerGenerationStatusExample, SitePowerGenerationStatusWithBLOBs>
		implements SitePowerGenerationStatusService {

	@Autowired
	SitePowerGenerationStatusMapper mapper;

	public SitePowerGenerationStatusServiceImpl(
			GenericMapper<SitePowerGenerationStatus, SitePowerGenerationStatusExample, SitePowerGenerationStatusWithBLOBs> mapper) {
		super(mapper);
	}

}
