package com.axiohelix.pvmo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.axiohelix.pvmo.entity.SitePowerSupplySystem;
import com.axiohelix.pvmo.entity.SitePowerSupplySystemExample;
import com.axiohelix.pvmo.entity.SitePowerSupplySystemWithBLOBs;
import com.axiohelix.pvmo.mapper.GenericMapper;
import com.axiohelix.pvmo.mapper.SitePowerSupplySystemMapper;
import com.axiohelix.pvmo.service.SitePowerSupplySystemService;


@Service
public class SitePowerSupplySystemServiceImpl extends GenericServiceImpl<SitePowerSupplySystem,SitePowerSupplySystemExample,SitePowerSupplySystemWithBLOBs>
		implements SitePowerSupplySystemService {

	@Autowired SitePowerSupplySystemMapper mapper;
	
	public SitePowerSupplySystemServiceImpl(GenericMapper<SitePowerSupplySystem,SitePowerSupplySystemExample,SitePowerSupplySystemWithBLOBs> mapper) {
		super(mapper);
	}


}
