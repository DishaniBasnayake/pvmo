package com.axiohelix.pvmo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.axiohelix.pvmo.entity.Site;
import com.axiohelix.pvmo.entity.SiteExample;
import com.axiohelix.pvmo.entity.SiteWithBLOBs;
import com.axiohelix.pvmo.mapper.GenericMapper;
import com.axiohelix.pvmo.mapper.SiteMapper;
import com.axiohelix.pvmo.service.SiteService;

@Service
public class SiteServiceImpl extends GenericServiceImpl<Site,SiteExample,SiteWithBLOBs>
		implements SiteService {

	@Autowired SiteMapper mapper;
	
	public SiteServiceImpl(GenericMapper<Site,SiteExample,SiteWithBLOBs> mapper) {
		super(mapper);
	}

	@Override
	public List<String> getNishikataPanelIdsWithErrors() {
		return mapper.getNishikataPanelIdsWithErrors();
	}

	@Override
	public SiteWithBLOBs getSiteBySPN(String spn) {
		return mapper.getSiteBySPN(spn);
	}


}
