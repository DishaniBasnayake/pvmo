package com.axiohelix.pvmo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.axiohelix.pvmo.entity.Site;
import com.axiohelix.pvmo.entity.SiteExample;
import com.axiohelix.pvmo.entity.SiteStatus;
import com.axiohelix.pvmo.entity.SiteStatusExample;
import com.axiohelix.pvmo.entity.SiteStatusWithBLOBs;
import com.axiohelix.pvmo.entity.SiteWithBLOBs;
import com.axiohelix.pvmo.mapper.GenericMapper;
import com.axiohelix.pvmo.mapper.SiteMapper;
import com.axiohelix.pvmo.mapper.SiteStatusMapper;
import com.axiohelix.pvmo.service.SiteService;
import com.axiohelix.pvmo.service.SiteStatusService;

@Service
public class SiteStatusServiceImpl extends GenericServiceImpl<SiteStatus,SiteStatusExample,SiteStatusWithBLOBs>
		implements SiteStatusService {

	@Autowired SiteStatusMapper mapper;
	
	public SiteStatusServiceImpl(GenericMapper<SiteStatus,SiteStatusExample,SiteStatusWithBLOBs> mapper) {
		super(mapper);
	}


}
