package com.axiohelix.pvmo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.axiohelix.pvmo.entity.SiteUrgentResponse;
import com.axiohelix.pvmo.entity.SiteUrgentResponseExample;
import com.axiohelix.pvmo.entity.SiteUrgentResponseWithBLOBs;
import com.axiohelix.pvmo.mapper.GenericMapper;
import com.axiohelix.pvmo.mapper.SiteUrgentResponseMapper;
import com.axiohelix.pvmo.service.SiteUrgentResponseService;


@Service
public class SiteUrgentResponseServiceImpl extends GenericServiceImpl<SiteUrgentResponse,SiteUrgentResponseExample,SiteUrgentResponseWithBLOBs>
		implements SiteUrgentResponseService {

	@Autowired SiteUrgentResponseMapper mapper;
	
	public SiteUrgentResponseServiceImpl(GenericMapper<SiteUrgentResponse,SiteUrgentResponseExample,SiteUrgentResponseWithBLOBs> mapper) {
		super(mapper);
	}


}
