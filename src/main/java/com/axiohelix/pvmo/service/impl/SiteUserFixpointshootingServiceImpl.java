package com.axiohelix.pvmo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.axiohelix.pvmo.entity.SiteUserFixpointshooting;
import com.axiohelix.pvmo.entity.SiteUserFixpointshootingExample;
import com.axiohelix.pvmo.entity.SiteUserFixpointshootingWithBLOBs;
import com.axiohelix.pvmo.mapper.GenericMapper;
import com.axiohelix.pvmo.mapper.SiteUserFixpointshootingMapper;
import com.axiohelix.pvmo.service.SiteUserFixpointshootingService;

@Service
public class SiteUserFixpointshootingServiceImpl extends GenericServiceImpl<SiteUserFixpointshooting,SiteUserFixpointshootingExample,SiteUserFixpointshootingWithBLOBs>
		implements SiteUserFixpointshootingService {

	@Autowired SiteUserFixpointshootingMapper mapper;
	
	public SiteUserFixpointshootingServiceImpl(GenericMapper<SiteUserFixpointshooting,SiteUserFixpointshootingExample,SiteUserFixpointshootingWithBLOBs> mapper) {
		super(mapper);
	}

}
