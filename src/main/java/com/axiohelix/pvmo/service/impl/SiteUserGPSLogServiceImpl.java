package com.axiohelix.pvmo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.axiohelix.pvmo.entity.SiteUserGPSLog;
import com.axiohelix.pvmo.entity.SiteUserGPSLogExample;
import com.axiohelix.pvmo.entity.SiteUserGPSLogWithBLOBs;
import com.axiohelix.pvmo.entity.custom.SiteUserGPSLogExtended;
import com.axiohelix.pvmo.mapper.GenericMapper;
import com.axiohelix.pvmo.mapper.SiteUserGPSLogMapper;
import com.axiohelix.pvmo.model.EntryExitSearch;
import com.axiohelix.pvmo.service.SiteUserGPSLogService;

@Service
public class SiteUserGPSLogServiceImpl extends GenericServiceImpl<SiteUserGPSLog,SiteUserGPSLogExample,SiteUserGPSLogWithBLOBs>
		implements SiteUserGPSLogService {

	@Autowired SiteUserGPSLogMapper mapper;
	
	public SiteUserGPSLogServiceImpl(GenericMapper<SiteUserGPSLog,SiteUserGPSLogExample,SiteUserGPSLogWithBLOBs>mapper) {
		super(mapper);
	}

	@Override
	public List<SiteUserGPSLogExtended> latest(String siteId) {
		 return mapper.latest(siteId);        
	}

	@Override
	public List<SiteUserGPSLogExtended> findAll() {
		return mapper.selectAll();
	}
	
	@Override
	public Page<SiteUserGPSLogExtended> searchEntryExit(EntryExitSearch search, Pageable pageable) {
        List<SiteUserGPSLogExtended> content = mapper.searchEntryExit(search, pageable);
        Long total = mapper.countEntryExit(search);
        return new PageImpl<>(content, pageable, total);
    }

}
