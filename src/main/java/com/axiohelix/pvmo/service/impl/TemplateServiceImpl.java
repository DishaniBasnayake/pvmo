package com.axiohelix.pvmo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.axiohelix.pvmo.entity.Template;
import com.axiohelix.pvmo.entity.TemplateExample;
import com.axiohelix.pvmo.entity.TemplateWithBLOBs;
import com.axiohelix.pvmo.mapper.GenericMapper;
import com.axiohelix.pvmo.mapper.TemplateMapper;
import com.axiohelix.pvmo.service.TemplateService;

@Service
public class TemplateServiceImpl extends GenericServiceImpl<Template,TemplateExample,TemplateWithBLOBs>
		implements TemplateService {

	@Autowired TemplateMapper mapper;
	
	public TemplateServiceImpl(GenericMapper<Template,TemplateExample,TemplateWithBLOBs>mapper) {
		super(mapper);
	}


}
