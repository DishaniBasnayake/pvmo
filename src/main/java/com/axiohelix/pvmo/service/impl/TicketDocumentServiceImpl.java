package com.axiohelix.pvmo.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.axiohelix.pvmo.entity.Document;
import com.axiohelix.pvmo.entity.DocumentWithBLOBs;
import com.axiohelix.pvmo.entity.Ticket;
import com.axiohelix.pvmo.entity.TicketWithBLOBs;
import com.axiohelix.pvmo.mapper.TicketDocumentMapper;
import com.axiohelix.pvmo.service.TicketDocumentService;

@Service
public class TicketDocumentServiceImpl implements TicketDocumentService {

	private static final Logger LOGGER = LogManager.getLogger(TicketDocumentServiceImpl.class);
	@Autowired
	TicketDocumentMapper mapper;

	@Override
	public int deleteTicketDocument(String ticketId, String DocumentId) {
		return mapper.deleteTicketDocument(ticketId, DocumentId);
	}

	@Override
	public int addTicketDocument(String id, String ticketId, String documentId) {
		return mapper.addTicketDocument(id,ticketId,documentId);
	}

	@Override
	public List<Ticket> getTickets(String DocumentId) {
		return mapper.getTickets(DocumentId);
	}

	@Override
	public List<Document> getDocuments(String ticketId) {
		return mapper.getDocuments(ticketId);
	}


}
