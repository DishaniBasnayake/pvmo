package com.axiohelix.pvmo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.axiohelix.pvmo.entity.Site;
import com.axiohelix.pvmo.entity.SiteExample;
import com.axiohelix.pvmo.entity.SiteWithBLOBs;
import com.axiohelix.pvmo.entity.Ticket;
import com.axiohelix.pvmo.entity.TicketExample;
import com.axiohelix.pvmo.entity.TicketWithBLOBs;
import com.axiohelix.pvmo.mapper.GenericMapper;
import com.axiohelix.pvmo.mapper.SiteMapper;
import com.axiohelix.pvmo.mapper.TicketMapper;
import com.axiohelix.pvmo.service.SiteService;
import com.axiohelix.pvmo.service.TicketService;

@Service
public class TicketServiceImpl extends GenericServiceImpl<Ticket,TicketExample,TicketWithBLOBs>
		implements TicketService {

	@Autowired TicketMapper mapper;
	
	public TicketServiceImpl(GenericMapper<Ticket,TicketExample,TicketWithBLOBs>mapper) {
		super(mapper);
	}

	@Override
	public void insertMultiple(List<TicketWithBLOBs> tickets) {
		if (tickets != null && tickets.size() > 0) {
			tickets.forEach(t -> mapper.insert(t));
		}
	}

	@Override
	public Integer getExistingTicketCount() {
		return mapper.getExistingTicketCount();
	}


}
