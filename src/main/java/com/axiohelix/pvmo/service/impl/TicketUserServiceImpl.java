package com.axiohelix.pvmo.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.axiohelix.pvmo.entity.TicketWithBLOBs;
import com.axiohelix.pvmo.entity.User;
import com.axiohelix.pvmo.entity.UserWithBLOBs;
import com.axiohelix.pvmo.mapper.TicketUserMapper;
import com.axiohelix.pvmo.service.TicketUserService;

@Service
public class TicketUserServiceImpl implements TicketUserService {

	private static final Logger LOGGER = LogManager.getLogger(TicketUserServiceImpl.class);
	@Autowired
	TicketUserMapper mapper;

	@Override
	public int deleteTicketUser(String ticketId, String userId) {
		return mapper.deleteTicketUser(ticketId, userId);
	}

	@Override
	public int addTicketUser(String id, String ticketId, String userId, String userType) {
		return mapper.addTicketUser(id, ticketId, userId, userType);
	}

	@Override
	public List<TicketWithBLOBs> getTickets(String userId) {
		return mapper.getTickets(userId);
	}

	@Override
	public List<UserWithBLOBs> getUsers(String ticketId) {
		return mapper.getUsers(ticketId);
	}
	
	@Override
	public List<UserWithBLOBs> getNotAssignedUsers(String siteId,String ticketId) {
		return mapper.getNotAssignedUsers(siteId,ticketId);
	}

	@Override
	public int updateTicketUser(String ticketId, String userId, String content) {
		return mapper.updateTicketUser(ticketId, userId, content);
	}

}
