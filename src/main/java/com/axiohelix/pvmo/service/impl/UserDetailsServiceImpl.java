package com.axiohelix.pvmo.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.SpringSecurityMessageSource;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.axiohelix.pvmo.model.SessionUser;
import com.axiohelix.pvmo.service.UserService;
import com.axiohelix.pvmo.type.Status;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UserService userService;

	@Transactional
	@Override
	public SessionUser loadUserByUsername(String username) throws UsernameNotFoundException {
		com.axiohelix.pvmo.entity.User user = userService.findByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException(SpringSecurityMessageSource.getAccessor()
					.getMessage("AbstractUserDetailsAuthenticationProvider.badCredentials", "Invalid Credentials."));
		} else if (user.getStatus() != Status.ACTIVE.getDbValue().byteValue()
				&& user.getStatus() != Status.HIDDEN.getDbValue().byteValue()) {
			throw new UsernameNotFoundException(SpringSecurityMessageSource.getAccessor()
					.getMessage("AbstractUserDetailsAuthenticationProvider.disabled ", "Accout Deactivated."));
		}
		List<GrantedAuthority> authorities = new ArrayList<>();
		Set<GrantedAuthority> ga = new HashSet<>();
		ga.add(new SimpleGrantedAuthority(com.axiohelix.pvmo.util.CommonConstant.ROLE_PREFIX+user.getUserType()));
		SessionUser userDetails = new SessionUser(user.getEmail(),user.getPassword(),ga,user.getId(),user.getUserType());
		return userDetails;
	}
	
	

}
