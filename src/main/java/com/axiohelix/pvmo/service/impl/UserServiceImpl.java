package com.axiohelix.pvmo.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.axiohelix.pvmo.entity.User;
import com.axiohelix.pvmo.entity.UserExample;
import com.axiohelix.pvmo.entity.UserWithBLOBs;
import com.axiohelix.pvmo.mapper.GenericMapper;
import com.axiohelix.pvmo.mapper.UserMapper;
import com.axiohelix.pvmo.service.UserService;


@Service
public class UserServiceImpl extends GenericServiceImpl<User, UserExample, UserWithBLOBs>
		implements UserService {

	@Autowired UserMapper mapper;
	
	public UserServiceImpl(GenericMapper<User, UserExample, UserWithBLOBs> mapper) {
		super(mapper);
	}

	@Override
	public User findByUsername(String username) {
		UserExample example = new UserExample();
		example.createCriteria().andEmailEqualTo(username);
		
		Optional<User> user =mapper.selectByExample(example).stream().findFirst();
		return user.isPresent() ? user.get() : null;
	}


}
