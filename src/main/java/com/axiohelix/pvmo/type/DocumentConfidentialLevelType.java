package com.axiohelix.pvmo.type;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum DocumentConfidentialLevelType {
	ENGINEER("CON124561", "ENGINEER", 0),
	GUEST("CON124562", "GUEST", 0),
	SERVICE_CENTER_MANAGER("CON124563", "SERVICE CENTER MANAGER", 1),
	AREA_MANAGER("CON124564", "AREA MANAGER", 2),
	MONITERING_CENTER("CON124565", "MONITERING CENTER", 3),
	SYSTEM_MANAGER("CON124566", "SYSTEM MANAGER", 4),
	HEAD_OFFICE_STAFF("CON124567", "HEAD OFFICE STAFF", 4);

	private String dbValue;

	private String displayName;

	private int value;

}
