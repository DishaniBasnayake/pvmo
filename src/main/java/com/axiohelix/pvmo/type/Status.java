package com.axiohelix.pvmo.type;

public enum Status {
	ACTIVE(0,"Active"),DELETED(1,"Deleted"),PERMANENT_DELETED(2,"Permanent-deleted"),HIDDEN(9,"Hidden"),MAP(5,"Map"), PUBLIC(8,"Public"), PRIVATE(7,"Private");
	
	
	private Integer dbValue;
	
	private String displayName;
	
	private Status(Integer dbValue,String displayName){
		this.dbValue=dbValue;
		this.displayName = displayName;
	}

	public Integer getDbValue() {
		return dbValue;
	}

	public void setDbValue(Integer dbValue) {
		this.dbValue = dbValue;
	}
	
	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	
	
}
