package com.axiohelix.pvmo.type;

public enum TemplateType {
	AXIO_AUTOFILL("5", "AXIO自動入力");

	private String dbValue;

	private String displayName;

	private TemplateType(String dbValue, String displayName) {
		this.dbValue = dbValue;
		this.displayName = displayName;
	}

	public String getDbValue() {
		return dbValue;
	}

	public void setDbValue(String dbValue) {
		this.dbValue = dbValue;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

}
