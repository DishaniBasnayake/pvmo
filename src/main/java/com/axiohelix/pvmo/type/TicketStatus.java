package com.axiohelix.pvmo.type;

public enum TicketStatus {
	PENDING(0,"Pending"),
	READY_FOR_APPROVAL(1,"Ready for Approval"),
	COMPLETED(2,"Completed"),
	RECENTLY_COMPLETED(3,"Recently Completed"),
	REJECTED(4,"Rejected");
	
	
	private Integer dbValue;
	
	private String displayName;
	
	private TicketStatus(Integer dbValue,String displayName){
		this.dbValue=dbValue;
		this.displayName = displayName;
	}

	public Integer getDbValue() {
		return dbValue;
	}

	public void setDbValue(Integer dbValue) {
		this.dbValue = dbValue;
	}
	
	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	public static TicketStatus getByDBValue(Integer dbValue) {
	    for (TicketStatus ts : values()) {
	    	if(ts.dbValue == dbValue) {
	    		return ts;
	    	}
	    }
	    return null;
	}
}
