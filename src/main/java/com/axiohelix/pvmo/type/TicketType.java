package com.axiohelix.pvmo.type;

public enum TicketType {
	MANUAL("MANUAL", "MANUAL", "MANUAL"), AI("AI", "AI", "AI"), EMERGENCY("EMERGENCY", "EMERGENCY", "EMERGENCY"),
	AXIO_AUTO_FILL("AXIO自動入力", "AXIO自動入力", "AXIO自動入力");

	private String dbValue;

	private String displayName;

	private String dtoValue;

	private TicketType(String dbValue, String displayName, String dtoValue) {
		this.dbValue = dbValue;
		this.displayName = displayName;
		this.dtoValue = dtoValue;
	}

	public String getDbValue() {
		return dbValue;
	}

	public void setDbValue(String dbValue) {
		this.dbValue = dbValue;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getDtoValue() {
		return dtoValue;
	}

	public void setDtoValue(String dtoValue) {
		this.dtoValue = dtoValue;
	}

}
