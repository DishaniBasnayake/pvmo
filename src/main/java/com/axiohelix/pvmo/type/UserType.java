package com.axiohelix.pvmo.type;

public enum UserType {
	ADMIN("ADMIN", "ADMIN", "administrators"), USER("USER", "USER", "users"),
	SUPERVISOR("SUPERVISOR", "SUPERVISOR", "supervisors"), MANAGER("MANAGER", "MANAGER", "managers"),
	EMPLOYEE("EMPLOYEE", "EMPLOYEE", "employees");

	private String dbValue;

	private String displayName;

	private String dtoValue;

	private UserType(String dbValue, String displayName, String dtoValue) {
		this.dbValue = dbValue;
		this.displayName = displayName;
		this.dtoValue = dtoValue;
	}

	public String getDbValue() {
		return dbValue;
	}

	public void setDbValue(String dbValue) {
		this.dbValue = dbValue;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getDtoValue() {
		return dtoValue;
	}

	public void setDtoValue(String dtoValue) {
		this.dtoValue = dtoValue;
	}

}
