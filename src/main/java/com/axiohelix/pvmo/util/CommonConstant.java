package com.axiohelix.pvmo.util;

public class CommonConstant {

	public static final String DEFAULT_PAGINATION_PAGE = "1";
	public static final String DEFAULT_PAGINATION_SIZE = "10";

	public static final String CONTROLLER_URL_COMPANY = "/company";
	public static final String CONTROLLER_URL_SERVICE_CENTER = "/service-center";
	public static final String CONTROLLER_URL_USER = "/user";
	public static final String CONTROLLER_URL_CONSUMER = "/consumer";
	public static final String CONTROLLER_URL_SITE = "/site";
	public static final String CONTROLLER_URL_REGION = "/region";
	public static final String CONTROLLER_URL_PREFECTURE = "/prefecture";
	public static final String CONTROLLER_URL_TICKET = "/ticket";
	public static final String CONTROLLER_URL_TEMPLATE = "/template";
	public static final String CONTROLLER_URL_GPS = "/gps";
	public static final String CONTROLLER_URL_DOCUMENT = "/document";
	public static final String CONTROLLER_URL_ENTRY_EXIT = "/entryexit";
	public static final String CONTROLLER_URL_FIXPOINTSHOOTING = "/fixpointshooting";
	public static final String CONTROLLER_URL_FILE = "/file";

	public static final String MSG_COMPANY_CODE = "Company code";
	public static final String MSG_IS_ALREADY_EXISTS = "is already exists";
	public static final String MSG_RECORD_NOT_FOUND = "Not Found";
	public static final String MSG_SERVICE_CENTER_CODE = "Service Center Code";
	public static final String MSG_USER_CODE = "User Code";
	public static final CharSequence DEFAULT_PASSWORD = "pvmo123";
	public static final String MSG_SITE_CODE = "Site Code";
	public static final String MSG_TICKET_ID = "Ticket ID";

	public static final String API_CONTROLLER_URL_SITE = "/api/site";
	public static final String API_CONTROLLER_URL_TICKET = "/api/ticket";
	public static final String API_CONTROLLER_URL_TEMPLATE = "/api/template";
	public static final String API_CONTROLLER_URL_DOCUMENT = "/api/document";
	public static final String ROLE_PREFIX = "ROLE_";

	public static final String API_CONTROLLER_URL_FILE = "/api/file";
}
