package com.axiohelix.pvmo.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.util.StringUtils;

import com.axiohelix.pvmo.model.CustomOAuth2User;
import com.axiohelix.pvmo.model.SessionUser;

public class CommonFunctions {

	@Autowired
	private static OAuth2AuthorizedClientService authorizedClientService;
	
	private static Random random = new Random(1000);

	private static Authentication auth;

	public static String generatePrimaryKey() {
		return String.valueOf(Calendar.getInstance().getTimeInMillis() + random.nextInt());
	}

	public static boolean containsIgnoreCase(String str, String subString) {
		if (StringUtils.isEmpty(str) || StringUtils.isEmpty(subString)) {
			return false;
		}
		return str.toLowerCase().contains(subString.toLowerCase());
	}

	public static Date parseStringDate(String dateString) {
		try {
			if (!dateString.isEmpty()) {
				return new SimpleDateFormat("yyyy-MM-dd").parse(dateString);
			}
		} catch (Exception e) {
			return null;
		}
		return null;
	}

	private static void initAuth() {
		auth = SecurityContextHolder.getContext().getAuthentication();
	}

	public static boolean isLoggedIn() {
		initAuth();
		if (auth instanceof AnonymousAuthenticationToken) {
			return false;
		} else {
			return true;
		}
	}

	public static SessionUser getCurrentUser() {
		initAuth();
		if (auth instanceof UsernamePasswordAuthenticationToken) {
			String username = (String) auth.getPrincipal();
			return new SessionUser(username, "", Collections.emptyList(), null, null);			
		} else if(auth instanceof AnonymousAuthenticationToken) {
			return null;
		}else {
			return (SessionUser) auth.getPrincipal();
		}
	}
	
	public static String getCurrentUserName() {
		initAuth();
		if (auth instanceof UsernamePasswordAuthenticationToken) {
			if(auth.getPrincipal() instanceof UserDetails) {
				SessionUser sessionUser = (SessionUser) auth.getPrincipal();
				return sessionUser.getUsername();
			}else {
				return (String) auth.getPrincipal();
			}
		}
		else if(auth instanceof OAuth2AuthenticationToken) {
			OidcUser oauthUser = (OidcUser) auth.getPrincipal();
			String uname = oauthUser.getEmail();
			return uname;
		}
		else if(auth instanceof AnonymousAuthenticationToken) {
			return null;
		}else {
			return null;
		}
	}
	
	public static String getCurrentUserId() {
		initAuth();
		SessionUser sessionUser = (SessionUser) auth.getPrincipal();
		return sessionUser.getUserId();
	}

	public static String generateFileName(String ext, String reference) {
		return String.format("%s-%s.%s", getCurrentDateTime(), reference, ext).replace(" ", "");
	}

	private static String getCurrentDateTime() {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
	}


	public static Date addMinutesToDate(Date date, int minutes) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MINUTE, minutes);
		return calendar.getTime();
	}

	public static Date addHoursToDate(Date date, int hours) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.HOUR_OF_DAY, hours);
		return calendar.getTime();
	}

}
