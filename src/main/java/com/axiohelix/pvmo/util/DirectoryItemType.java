package com.axiohelix.pvmo.util;

public enum DirectoryItemType {
	FILE,
	DIRECTORY
}
