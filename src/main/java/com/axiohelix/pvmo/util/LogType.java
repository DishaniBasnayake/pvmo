package com.axiohelix.pvmo.util;

public enum LogType {

	ENTER(0),
	EXIT(1);
	
	private Integer dbValue;
	
	LogType(Integer dbValue) {
		this.dbValue = dbValue;
	}

	public Integer getDbValue() {
		return dbValue;
	}

	public void setDbValue(Integer dbValue) {
		this.dbValue = dbValue;
	}
	
	public static String getValueByDbValue(Integer b) {
		for (LogType l : LogType.values()) {
			if (l.getDbValue() == b) {
				return l.name();
			}
			
		}
		
		return "";
	}
}
