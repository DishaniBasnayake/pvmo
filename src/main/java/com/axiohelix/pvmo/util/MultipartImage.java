package com.axiohelix.pvmo.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.springframework.web.multipart.MultipartFile;

public class MultipartImage implements MultipartFile {

	private byte[] bytes;
	String name;
	String originalFilename;
	String contentType;
	boolean isEmpty;
	long size;

	public MultipartImage(byte[] bytes, String name, String originalFilename, String contentType, long size) {
		this.bytes = bytes;
		this.name = name;
		this.originalFilename = originalFilename;
		this.contentType = contentType;
		this.size = size;
		this.isEmpty = false;
	}

	public MultipartImage(byte[] byteArray, String name, String originalFilename,long size) {
		this.bytes = byteArray;
		this.name = name;
		this.originalFilename = originalFilename;
		this.size = size;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getOriginalFilename() {
		return originalFilename;
	}

	@Override
	public String getContentType() {
		return contentType;
	}

	@Override
	public boolean isEmpty() {
		return isEmpty;
	}

	@Override
	public long getSize() {
		return size;
	}

	@Override
	public byte[] getBytes() throws IOException {
		return bytes;
	}

	@Override
	public InputStream getInputStream() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void transferTo(File arg0) throws IOException, IllegalStateException {
		// TODO Auto-generated method stub

	}
}
