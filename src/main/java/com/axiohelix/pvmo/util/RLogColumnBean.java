package com.axiohelix.pvmo.util;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

import lombok.Getter;
import lombok.Setter;

class CsvBean {
}

@Getter
@Setter
public class RLogColumnBean extends CsvBean {

	@CsvBindByName(column = "ID")
	@CsvBindByPosition(position = 0)
	private String id;

	@CsvBindByName(column = "SPN")
	@CsvBindByPosition(position = 1)
	private String spn;

	@CsvBindByName(column = "SPN-Sub")
	@CsvBindByPosition(position = 2)
	private String spnSub;

	@CsvBindByName(column = "Site Name")
	@CsvBindByPosition(position = 3)
	private String siteName;

	@CsvBindByName(column = "Ticket number")
	@CsvBindByPosition(position = 4)
	private String ticketNumber;

	@CsvBindByName(column = "Start Date")
	@CsvBindByPosition(position = 5)
	private String startDate;

	@CsvBindByName(column = "Start Time")
	@CsvBindByPosition(position = 6)
	private String startTime;

	@CsvBindByName(column = "Ticket Owner")
	@CsvBindByPosition(position = 7)
	private String ticketOwner;

	@CsvBindByName(column = "Status")
	@CsvBindByPosition(position = 8)
	private String status;

	@CsvBindByName(column = "End Date")
	@CsvBindByPosition(position = 9)
	private String endDate;

	@CsvBindByName(column = "End Time")
	@CsvBindByPosition(position = 10)
	private String endTime;

	@CsvBindByName(column = "Event Name")
	@CsvBindByPosition(position = 11)
	private String eventName;

	@CsvBindByName(column = "Event#")
	@CsvBindByPosition(position = 12)
	private String eventNumber;

	@CsvBindByName(column = "Event Detail1")
	@CsvBindByPosition(position = 13)
	private String eventDetail1;

	@CsvBindByName(column = "Event Detail2")
	@CsvBindByPosition(position = 14)
	private String eventDetails2;

	@CsvBindByName(column = "PCS#")
	@CsvBindByPosition(position = 15)
	private String pcsNumber;

	@CsvBindByName(column = "Equipment")
	@CsvBindByPosition(position = 16)
	private String equipment;

	@CsvBindByName(column = "Engineer Name")
	@CsvBindByPosition(position = 17)
	private String engineerName;

	@CsvBindByName(column = "Note")
	@CsvBindByPosition(position = 18)
	private String note;

	@CsvBindByName(column = "Picture")
	@CsvBindByPosition(position = 19)
	private String picture;

}
