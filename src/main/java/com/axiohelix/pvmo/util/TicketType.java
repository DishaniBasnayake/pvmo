package com.axiohelix.pvmo.util;

import lombok.Getter;

@Getter
public enum TicketType {
	
	AXIO_AUTO_FILL("AXIO自動入力"),
	MONITORING_CENTER("Monitoring Center"),
	SITE("Site"),
	ENGINEER("Engineer");
	
	private String displayValue;
	
	private TicketType(String displayValue) {
		this.displayValue = displayValue;
	}

}
