const MSG_DATA_LOADING_FAIL = "Getting data failed.";
const MSG_RECORD_DELETED_SUCCESS = "Record Deleted Successfully.";
const MSG_RECORD_UPDATED_SUCCESS = "Record Updated Successfully.";
const MSG_RECORD_SAVED_SUCCESS = "Record saved Successfully.";
const MSG_RECORDS_FOUND = "Records Found.";

const EDIT = "Edit";
const ADD = "Add";

const COMPANY = "Company";
const SERVICE_CENTER = "Service Center";
const SITE = "Site";
const USER = "User";
const TICKET = "Ticket";
const REGION = "Region";
const PREFECTURE = "Prefecture";
const CONSUMER = "Consumer";

const NAMES = "Names"
const SELECTED_NAMES = "Selected Names"
const SITES = "Sites"
const SELECTED_SITES ="Selected Sites"

const RECORDS_FOUND = "records found!"
const NO_MORE_RECORDS_FOND = "No more records found!"
const NEW_FOLDER_CREATE_SUCCESS = "New folder created successfully!"
const UPLOAD_FILE_SUCCESS = "File uploaded successfully!"