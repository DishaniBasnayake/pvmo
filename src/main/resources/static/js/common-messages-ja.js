const MSG_DATA_LOADING_FAIL = "データの取得に失敗しました。";
const MSG_RECORD_DELETED_SUCCESS = "レコードが正常に削除されました。";
const MSG_RECORD_UPDATED_SUCCESS = "レコードが正常に更新されました。";
const MSG_RECORD_SAVED_SUCCESS = "レコードは正常に保存されました。";
const MSG_RECORDS_FOUND = "レコードが見つかりました。";

const EDIT = "編集";
const ADD = "追加";

const COMPANY = "顧客";
const SERVICE_CENTER = "サービスセンター";
const SITE = "発電所";
const USER = "ユーザー";
const TICKET = "チケット";
const REGION = "領域";
const PREFECTURE = "県";
const CONSUMER = "消費者";

const NAMES = "名前"
const SELECTED_NAMES = "選択した名前"
const SITES = "発電所"
const SELECTED_SITES ="選択された発電所"

const RECORDS_FOUND = "レコードが見つかりました！"
const NO_MORE_RECORDS_FOND = "これ以上のレコードは見つかりませんでした！"
const NEW_FOLDER_CREATE_SUCCESS = "新しいフォルダが正常に作成されました！"
const UPLOAD_FILE_SUCCESS = "ファイルが正常にアップロードされました!"