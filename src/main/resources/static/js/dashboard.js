/*
 * Author: Abdullah A Almsaeed
 * Date: 4 Jan 2014
 * Description:
 *      This is a demo file used only for the main dashboard (index.html)
 **/


/*** 

var w0 = $("#draggable_0");  
var w1 = $("#draggable_1"); 
var w2 = $("#draggable_2"); 
var positions = new Array();
var  saved= JSON.parse(localStorage.positions || "{}");



$(function () {

  'use strict';
     

  // Make the dashboard widgets sortable Using jquery UI
  $('.connectedSortable').sortable({
    placeholder         : 'sort-highlight',
    connectWith         : '.connectedSortable',
    handle              : '.box-header, .nav-tabs',
    forcePlaceholderSize: true,
    zIndex              : 999999,
	update: function() {
            var order = $(this).sortable('toArray');
			positions.push(order);
			console.log(JSON.stringify(order));	
			localStorage.positions = positions;
     },
  });
  $('.connectedSortable .box-header, .connectedSortable .nav-tabs-custom').css('cursor', 'move');

  // jQuery UI sortable for the todo list
  $('.todo-list').sortable({
    placeholder         : 'sort-highlight',
    handle              : '.handle',
    forcePlaceholderSize: true,
    zIndex              : 999999
  });

});

***/

$(function() {
	$('[id^="sortable"]').sortable({
		placeholder: 'sort-highlight',
		forcePlaceholderSize: true,
		zIndex: 999999,
	    handle              : '.box-header, .nav-tabs',
		connectWith: '[id^="sortable"]',
		helper: "clone",
		appendTo: ".primary_container",
		update: function() {
			$('[id^="sortable"]').children().each(function() {
				savePosition($(this).attr("id"));
			});
		}
	});
});
$('.connectedSortable .box-header, .connectedSortable .nav-tabs-custom').css('cursor', 'move');


$('[id^="sortable"]').children().each(function() {
	loadPosition($(this).attr("id"));
});

function savePosition(id) {
	var el = $("#" + id);
	var position = JSON.parse(localStorage.getItem(id));
	var visible = false;
	if (position) {
		var visible = position.visible;
	}
	var container = el.parent().attr("id");
	var index = el.index();
	localStorage.setItem(id, JSON.stringify({ container: container, index: index, visible: visible }));
}

function loadPosition(id) {
	var el = $("#" + id);
	var position = JSON.parse(localStorage.getItem(id));
	if (position) {
		var container = "#" + position.container;
		var visible = position.visible;
		if (!visible) {
			$(el).css("display", "none");
		}
		if (visible) {
			$("#" + id + "cb").prop('checked', true);
		}
		var index = position.index;
		if (index == 0) {
			$(container).prepend(el);
		} else if ($(container).children().eq(index - 1).length == 0) {
			$(container).append(el);
		} else {
			$(container).children().eq(index - 1).after(el);
		}
	} else {
		savePosition(id);
	}
}


$(".widget-close").on('click', function(event) {
	let elementId = $(this).parent().parent().parent().attr('id');
	let element = localStorage.getItem(elementId);

	var element_obj = JSON.parse(element);
	element_obj.visible = false;
	element_string = JSON.stringify(element_obj);
	// Log to console:
	console.log(element_string);
	localStorage.setItem(elementId, element_string);

	console.log("#" + elementId + "cb");
	$("#" + elementId + "cb").prop('checked', true);
});

$("#btnSaveWidgets").on('click', function() {
	$('#tblWidget input:checked').each(function() {
		var elementId = $(this).attr('id').slice(0, -2);
		let element = localStorage.getItem(elementId);

		var element_obj = JSON.parse(element);
		element_obj.visible = true;
		element_string = JSON.stringify(element_obj);
		localStorage.setItem(elementId, element_string);
		//$("#" + elementId + "cb").prop('checked', false);
	});
	$('#tblWidget input:not(:checked)').each(function() {
		var elementId = $(this).attr('id').slice(0, -2);
		let element = localStorage.getItem(elementId);

		var element_obj = JSON.parse(element);
		element_obj.visible = false;
		element_string = JSON.stringify(element_obj);
		localStorage.setItem(elementId, element_string);
		//$("#" + elementId + "cb").prop('checked', true);
	});

	location.reload();
	return false;
});

$("#widget-modal").on('shown.bs.modal', function() {
	$('#tblWidget input').each(function() {
		var elementId = $(this).attr('id').slice(0, -2);
		let element = localStorage.getItem(elementId);

		var element_obj = JSON.parse(element);
		$("#" + elementId + "cb").prop('checked', element_obj.visible);
		console.log(element_obj.visible);
	});
});



