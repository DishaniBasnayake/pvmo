// ResizeObserver to ensure header column widths are adjusted when the containing div resizes.
// Not supported on IE
var observer = window.ResizeObserver ? new ResizeObserver(function(entries) {
	entries.forEach(function(entry) {
		$(entry.target).DataTable().columns.adjust();
	});
}) : null;

// Function to add a datatable to the ResizeObserver entries array
resizeHandler = function($table) {
	if (observer)
		observer.observe($table[0]);
};
