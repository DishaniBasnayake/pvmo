var COLOR_SOLAR_PANEL_INIT='#B8B8B8';
var COLOR_PCS_INIT='#14AAF5';
var COLOR_ANOMALY='RED';

var canvas = this.__canvas = new fabric.Canvas('c');
init();

function init(){
    //adding grid lines
    var gridsize = 5;
    for (var x = 1; x < (canvas.width / gridsize); x++) {
        canvas.add(new fabric.Line([20 * x, 0, 20 * x, canvas.width], { stroke: "#ddd", strokeWidth: 1, selectable: false, strokeDashArray: [2, 2] }));
        canvas.add(new fabric.Line([0, 20 * x, canvas.width, 20 * x], { stroke: "#ddd", strokeWidth: 1, selectable: false, strokeDashArray: [2, 2] }));
    }
    //end adding grid lines

    // create a polygon object
    var points = [
        { x: 1, y: 1 },
        { x: 20, y: 1 },
        { x: 40, y: 1 },
        { x: 60, y: 1 },
        { x: 80, y: 1 },
        { x: 100, y: 1 },
        { x: 100, y: 20 },
        { x: 100, y: 40 },
        { x: 100, y: 60 },
        { x: 100, y: 80 },
        { x: 80, y: 80 },
        { x: 60, y: 80 },
        { x: 40, y: 80 },
        { x: 20, y: 80 },
        { x: 1, y: 80 },
        { x: 1, y: 60 },
        { x: 1, y: 40 },
        { x: 1, y: 20 },
        { x: 1, y: 1 }]

    var polygon = new fabric.Polygon(points, {
        left: 20,
        top: 30,
        fill: '#FFFFFF',
        strokeWidth: 0.5,
        stroke: 'black',
        scaleX: 6,
        scaleY: 6,
        objectCaching: false,
        transparentCorners: false,
        cornerColor: 'gray',
        fill: 'rgba(0,0,0,0)',
        id: 'floor_area',
    });
    //canvas.viewportTransform = [0.7, 0, 0, 0.7, -50, 50];
    canvas.add(polygon);

}


// define a function that can locate the controls.
// this function will be used both for drawing and for interaction.
function polygonPositionHandler(dim, finalMatrix, fabricObject) {
    var x = (fabricObject.points[this.pointIndex].x - fabricObject.pathOffset.x),
        y = (fabricObject.points[this.pointIndex].y - fabricObject.pathOffset.y);
    return fabric.util.transformPoint(
        { x: x, y: y },
        fabric.util.multiplyTransformMatrices(
            fabricObject.canvas.viewportTransform,
            fabricObject.calcTransformMatrix()
        )
    );
}

// define a function that will define what the control does
// this function will be called on every mouse move after a control has been
// clicked and is being dragged.
// The function receive as argument the mouse event, the current trasnform object
// and the current position in canvas coordinate
// transform.target is a reference to the current object being transformed,
function actionHandler(eventData, transform, x, y) {
    var polygon = transform.target,
        currentControl = polygon.controls[polygon.__corner],
        mouseLocalPosition = polygon.toLocalPoint(new fabric.Point(x, y), 'center', 'center'),
        polygonBaseSize = polygon._getNonTransformedDimensions(),
        size = polygon._getTransformedDimensions(0, 0),
        finalPointPosition = {
            x: mouseLocalPosition.x * polygonBaseSize.x / size.x + polygon.pathOffset.x,
            y: mouseLocalPosition.y * polygonBaseSize.y / size.y + polygon.pathOffset.y
        };
    polygon.points[currentControl.pointIndex] = finalPointPosition;
    return true;
}

// define a function that can keep the polygon in the same position when we change its
// width/height/top/left.
function anchorWrapper(anchorIndex, fn) {
    return function (eventData, transform, x, y) {
        var fabricObject = transform.target,
            absolutePoint = fabric.util.transformPoint({
                x: (fabricObject.points[anchorIndex].x - fabricObject.pathOffset.x),
                y: (fabricObject.points[anchorIndex].y - fabricObject.pathOffset.y),
            }, fabricObject.calcTransformMatrix()),
            actionPerformed = fn(eventData, transform, x, y),
            newDim = fabricObject._setPositionDimensions({}),
            polygonBaseSize = fabricObject._getNonTransformedDimensions(),
            newX = (fabricObject.points[anchorIndex].x - fabricObject.pathOffset.x) / polygonBaseSize.x,
            newY = (fabricObject.points[anchorIndex].y - fabricObject.pathOffset.y) / polygonBaseSize.y;
        fabricObject.setPositionByOrigin(absolutePoint, newX + 0.5, newY + 0.5);
        return actionPerformed;
    }
}

function Edit() {
    // clone what are you copying since you
    // may want copy and paste on different moment.
    // and you do not want the changes happened
    // later to reflect on the copy.
    //var poly = canvas.getObjects()[318];
    var poly = canvas.getItemById('floor_area');
    canvas.setActiveObject(poly);
    poly.edit = !poly.edit;
    if (poly.edit) {
        var lastControl = poly.points.length - 1;
        poly.cornerStyle = 'circle';
        poly.cornerColor = 'rgba(0,0,255,0.5)';
        poly.controls = poly.points.reduce(function (acc, point, index) {
            acc['p' + index] = new fabric.Control({
                positionHandler: polygonPositionHandler,
                actionHandler: anchorWrapper(index > 0 ? index - 1 : lastControl, actionHandler),
                actionName: 'modifyPolygon',
                pointIndex: index
            });
            return acc;
        }, {});
    } else {
        poly.cornerColor = 'blue';
        poly.cornerStyle = 'rect';
        poly.controls = fabric.Object.prototype.controls;
    }
    poly.hasBorders = !poly.edit;
    canvas.requestRenderAll();
}


fabric.Canvas.prototype.getItemById = function(id) {
    var object = null,
    objects = this.getObjects();  
    for (var i = 0;  i < objects.length ; i++) {
        if (objects[i].id && objects[i].id === id) {
            object = objects[i];
            break;
      }
    }  
    return object;
};

var lastSolarPanel_left=0;
var lastSolarPanel_top=10;
var lastSolarPanel_scale_x=1;
var lastSolarPanel_scale_y=1;
var nextSolarPanelId=0;
var nextPcsId=0;
function addSolarPanel() {
    var solarPanel = new fabric.Rect({
        top: lastSolarPanel_top, 
        left: lastSolarPanel_left * lastSolarPanel_scale_x + 30, 
        width: 20, 
        height: 20, 
        fill: COLOR_SOLAR_PANEL_INIT, 
        stroke: 'gray',
        lockUniScaling: true,
        scaleX:lastSolarPanel_scale_x,
        scaleY:lastSolarPanel_scale_y,
        type: 'solar_panel', 
        id: 'solar_panel_'+nextSolarPanelId
    });
    canvas.add(solarPanel);
    lastSolarPanel_left = solarPanel.left;
    lastSolarPanel_top = solarPanel.top;
    lastSolarPanel_scale_x = solarPanel.scaleX;
    lastSolarPanel_scale_y = solarPanel.scaleY;
    nextSolarPanelId=nextSolarPanelId+1;
};

function addPcs() {
    var points = [
        {x: 3, y: 3}, 
        {x: 4, y: 3},
        {x: 5, y: 5}, 
        {x: 5, y: 7}, 
        {x: 4, y: 9}, 
        {x: 3, y: 9},
        {x: 2, y: 9},
        {x: 1, y: 7}, 
        {x: 1, y: 5}, 
        {x: 2, y: 3},
        {x: 3, y: 3}
    ]

    var pcs = new fabric.Polygon(points, {
        left: 20,
        top: 30,
        scaleX: 4,
        scaleY: 4,
        fill: COLOR_PCS_INIT,
        strokeWidth: 0.5,
        stroke: 'blue',
        type: 'pcs',
        id: 'pcs_'+nextPcsId,
    });

    canvas.add(pcs);
    nextPcsId = nextPcsId+1;
};

canvas.on({
    'mouse:down': function(e) {
     if(e.target && canvas.getActiveObject() && canvas.getActiveObject().type){
         if(canvas.getActiveObject().type==='solar_panel' || canvas.getActiveObject().type==='pcs'){
            var obj = canvas.getActiveObject();
            var idBox = document.getElementById('object_id').value=obj.id ;      
         }
      }
    },
/*    'mouse:up': function(e) {
      if (e.target) {
        e.target.opacity = 1;
        canvas.renderAll();
      }
    },
*/  
    'object:moved': function (e) {
        if (canvas.getActiveObject().type === 'solar_panel') {
            var obj = canvas.getActiveObject();
            lastSolarPanel_left = obj.left;
            lastSolarPanel_top = obj.top;
        }
    },
    'object:modified': function (e) {
        if (canvas.getActiveObject().type === 'solar_panel') {            
            var obj = canvas.getActiveObject();
            lastSolarPanel_left = obj.left;
            lastSolarPanel_top = obj.top;
            lastSolarPanel_scale_x = obj.scaleX;
            lastSolarPanel_scale_y= obj.scaleY;
        }
    }
  });


  function deleteItem(){
    if(canvas.getActiveObject()){
        if(canvas.getActiveObject().type==='solar_panel' || canvas.getActiveObject().type==='pcs'){
           var obj = canvas.getActiveObject();
           canvas.remove(obj);   

        }
     }
  };

  function demoAnomalies(){
    clearAnomalies();
    var objects = canvas.getObjects();
    var numSolarPanel = 0;
    var numPcs = 0;
    var anomalityObjects = [];
    for (var i = 0; i < objects.length; i++) {
        if (objects[i].type && objects[i].type === 'solar_panel') {
            numSolarPanel++;
            anomalityObjects.push(objects[i]);
        }
    }

    for (var i = 0; i < objects.length; i++) {
        if (objects[i].type && objects[i].type === 'pcs') {
            numPcs++;
            anomalityObjects.push(objects[i]);
        }
    }

    for(var j=0; j<Math.floor(Math.random() * 3) + 1; j++){
        const rndInt = Math.floor(Math.random() * anomalityObjects.length) + 1;
        canvas.getItemById(anomalityObjects[rndInt-1].id).set('fill',COLOR_ANOMALY);
    }
    canvas.renderAll();
  };

  function clearAnomalies(){
    var objects = canvas.getObjects();
    for (var i = 0; i < objects.length; i++) {
        if (objects[i].type && objects[i].type === 'solar_panel') {
            canvas.getItemById(objects[i].id).set('fill',COLOR_SOLAR_PANEL_INIT);
        }
        else if (objects[i].type && objects[i].type === 'pcs') {
            canvas.getItemById(objects[i].id).set('fill',COLOR_PCS_INIT);
        }
    }
  }

  function clearAll(){
      canvas.clear();
      init();
  }

  function exportToJson(){
    var element = document.getElementById('downloadAnchor');
    var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(canvas.toJSON()));
    element.setAttribute("href",dataStr);
    element.setAttribute("download", "scene.json");
    element.click();
  }


  
  
