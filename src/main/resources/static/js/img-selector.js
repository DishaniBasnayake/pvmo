$(document).ready(function() {
	let sort = 1;
	$('.image-checkbox').each(function() {
		$(this).attr('jms-sort', sort);
		sort++;
	});

	var mediaArray = [];
	var selectedMediasId;
	var isMultipleAllowed = false;
	var prevItem = null;

	function clearSelection() {
		isMultipleAllowed = $('#allowmultiple').is(':checked') ? true : false;
		$('.image-checkbox-checked').each(function() {
			$(this).removeClass('image-checkbox-checked');
		});
		mediaArray = [];
		$('#selectedmediapreview').html('');
		prevItem = null;
		setDownloadBtnStatus();
	}

	$('#allowmultiple').click(function() {
		clearSelection();
		if ($(this).is(":checked")) {
			$('#ulPGT').find('a').css("pointer-events", "none");
		} else {
			$('#ulPGT').find('a').css("pointer-events", "auto");			
		}
	});


	$(".image-checkbox").on("click", function(e) {
		var selected = $(this).find('img').attr('su-media-id');

		if ($(this).hasClass('image-checkbox-checked')) {
			$(this).removeClass('image-checkbox-checked');
			// remove deselected item from array
			mediaArray = $.grep(mediaArray, function(value) {
				return value != selected;
			});
		}
		else {
			if (isMultipleAllowed == true) {
				$(this).addClass('image-checkbox-checked');
				prevItem = $(this);
				mediaArray = [];
				$('.image-checkbox-checked').each(function() {
					mediaArray.push(($(this).find('img').attr('su-media-id')));
				});
			}
		}
		//console.log(selected);
		console.log(mediaArray);
		//selectedMediasId = mediaArray.join(",");
		//console.log(selectedMediasId);
		//$('#selectedmediapreview').html('<div class="alert alert-success"><pre lang="js">' + JSON.stringify(mediaArray.join(", "), null, 4) + '</pre></div>');
		//console.log(isMultipleAllowed);
		setDownloadBtnStatus();
		e.preventDefault();
	});

	function setDownloadBtnStatus() {
		if (mediaArray.length == 0) {
			$("#btnDownload").prop('disabled', true);
		} else {
			$("#btnDownload").prop('disabled', false);
		}
	}

	$(document).on("click", "#btnDownload", function() {
		if (mediaArray.length == 1) {
			$('#btnDownload').attr('disabled', true);
			window.location = '/file/download/' + mediaArray[0];
			$('#btnDownload').attr('disabled', false);
		} else {
			var form = $('<form>', { 'method': 'POST', 'action': '/file/download-multiple' }).hide();
			form.append($('<input>', { 'type': 'hidden', 'name': 'listOfFileNames', 'value': mediaArray }));
			$('body').append(form);
			form.submit();
			form.remove();
			//clearSelection();
		}
	});

});