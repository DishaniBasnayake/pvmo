	
	$.validator.setDefaults({
	    highlight: function(element) {
	        $(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
	    errorElement: 'span',
	    errorClass: 'help-block',
	    errorPlacement: function(error, element) {
	        if(element.parent('.input-group').length) {
	            error.insertAfter(element.parent());
	        } else {
	            error.insertAfter(element);
	        }
	    }
	});	
	
	$.validator.addMethod("regx", function(value, element, regexpr) {
	    return this.optional(element) || regexpr.test(value);
	}, "Input is invalid.");
	
	$.validator.addMethod("sl-phone", function(value, element, regexpr) {
	    return this.optional(element) || regexpr.test(value);
	}, "invalid phone number.{format ex: 0xxxxxxxxx}");
	
	$.validator.addMethod("id-number", function(value, element, regexpr) {
	    return this.optional(element) || regexpr.test(value);
	}, "invalid ID number.");
	
	$.validator.addClassRules('phone', {
		"sl-phone" : /^((0)[0-9]{9})$/,
	});
	
	$.validator.addClassRules('id-number', {
		"id-number" : /^([0-9]{9}(v|V)|[0-9]{12}|(PBLKA)[A-Z0-9]{8,9}|[0-9]{10})$/,
	});
	
	$.validator.addMethod('filesize', function (value, element, param) {
	    return this.optional(element) || (element.files[0].size <= param * 1000000)
	}, 'File size must be less than {0} MB');
	
	
	$('#uploadDataFileName').submit(function(e) {
			e.preventDefault();
		}).validate({
		ignore: [],
		rules : {
			"upload_file_name" : {
				required : true
			},
	
		},
		messages : {
			"upload_file_name": {
	        	required: "Please upload a file",
	    	},			
		},		
		submitHandler : function(form) {			
			
			$(form).ajaxSubmit({
				  success: function(data) {
					  var curStep = $("#btnUploadFile").closest(".setup-content");
					  var curStepBtn = curStep.attr("id");
					  var nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a");
					  nextStepWizard.removeAttr('disabled').trigger('click');
				  }
			 });
		  
		}
	});
	
	$('#uploadDataDescription').submit(function(e) {
			e.preventDefault();
		}).validate({
		rules : {
			"title" : {
				required : true
			},
			"keywords" : {
				required : true
			},
			"description" : {
				required : false
			},
	
		},		
		submitHandler : function(form) {			
			
			$(form).ajaxSubmit({
				  success: function(data) {					
					  $("#metadata-view-full").html(data);
					  var curStep = $("#btnDescription").closest(".setup-content");
					  var curStepBtn = curStep.attr("id");
					  var nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a");
					  nextStepWizard.removeAttr('disabled').trigger('click');
				  }
			 });
		  
		}
	});
	
	$("#organizationData").validate({
		rules : {
			"name" : {
				required : true
			},
			"code" : {
				required : true,
			},
			"address" : {
				required : true
			},
			"telephone" : {
				required : true,
				regx : /(0)[0-9]{9}/,
			},
			"fax" : {
				required : false,
				regx : /(0)[0-9]{9}/,
			},
			"email" : {
				email : true,
				required : true
			},
	
		},
		messages : {
			telephone : {
				regx : "invalid phone number.{format ex: 0xxxxxxxxx}",
			},
			
		},
		submitHandler : function(form) {
			form.submit();
		}
	});
	
	$("#employeeData").validate({
		rules : {
			"organization.id" : {
				required : true,
			},
			"name" : {
				required : true
			},
			"code" : {
				required : true,
			},
			"address" : {
				required : false
			},
			"contactNumber1" : {
				required : true,
				regx : /(0)[0-9]{9}/,
			},
			"contactNumber2" : {
				required : false,
				regx : /(0)[0-9]{9}/,
			},
			"email" : {
				email : true,
				required : false
			},
	
		},
		messages : {
			contactNumber1 : {
				regx : "invalid contact  number.{format ex: 0xxxxxxxxx}",
			},
			
		},
		
		submitHandler : function(form) {
			form.submit();
		}
	});
	
	$("#projectData").validate({
		rules : {
			"sponsor.id" : {
				required : true
			},
			"name" : {
				required : true
			},
			/*
			"project_number" : {
				required : true,
				maxlength:128
			},
			"country" : {
				required : true,
				maxlength:128
			},
			"leadInstitution.id" : {
				required : true
			},
			"project_status" : {
				required : true,
				maxlength:128
			},
			"project_type" : {
				required : true,
				maxlength:128
			},
			"source_of_funding" : {
				required : true,
			},
			"budget_amount" : {
				required : true,
				min : 0,
			},
			"web" : {
				required : false,
			},
			"address" : {
				required : true,
			},
			"proposed_date" : {
				required : true,
			},
			"approved_date" : {
				required : true,
			},
			"startDate" : {
				required : true,
			},
			"endDate" : {
				required : true,
			},
			"permanent_address" : {
				required : true,
			},
			"temporary_address" : {
				required : false,
			},
			"email_address" : {
				required : true,
				maxlength:255,
				email: true				
			},
			"contact_person" : {
				required : true,
				maxlength:255
			},
			"contact_number" : {
				required : true,
				maxlength:255
			},
			*/
		},
		messages : {
			
		},
		submitHandler : function(form) {
			form.submit();
		}
	});
	
	$("#templateData").validate({
		rules : {
			"name" : {
				required : true
			},
			"code" : {
				required : true,
			},
	
		},
		messages : {
			
		},
		submitHandler : function(form) {
			form.submit();
		}
	});
	
	$("#taskData").validate({
		rules : {
			"name" : {
				required : true
			},
			"code" : {
				required : true,
			},
			"startDate" : {
				required : true,
			},
			"endDate" : {
				required : true,
			},
			"mitadap" : {
				required : false,
			},
	
		},
		messages : {
			
		},
		submitHandler : function(form) {
			form.submit();
		}
	});
	
	$("#initiationData").validate({
		rules : {
			"organization.id" : {
				required : true
			},
			"name" : {
				required : true,
			},
	
		},
		messages : {
			
		},
		submitHandler : function(form) {
			form.submit();
		}
	});
	
	$("#sponsorData").validate({
		rules : {
			"name" : {
				required : true,
			},
			/*
			"web" : {
				required : true,
				maxlength:128
			},
			"permanentAddress" : {
				required : true,
			},
			"temporary_address" : {
				required : false,
			},
			"email_address" : {
				required : true,
				maxlength:255,
				email: true				
			},
			"contact_person" : {
				required : true,
				maxlength:255
			},
			"contact_number" : {
				required : true,
				maxlength:255
			},
			"country" : {
				required : true,
				maxlength:128
			},
			*/
	
		},
		submitHandler : function(form) {
			form.submit();
		}
	});
	
	$("#articleData").validate({
		rules : {
			"articleType.id" : {
				required : true,
			},
			"name" : {
				required : true,
			},
			/*
			"content_text" : {
				required : true,
			},
			"data_submitted" : {
				required : true,
			},
			"data_accepted" : {
				required : true,
			},
			"email_address" : {
				required : true,		
			},
			"data_published" : {
				required : true,
			},
			"articleAuthor.title" : {
				required : true,
			},
			"articleAuthor.last_name" : {
				required : true,
			},
			"articleAuthor.initials" : {
				required : true,
			},
			"articleAuthor.other_names" : {
				required : true,
			},
			"articleAuthor.affiliation" : {
				required : true,
			},
			"articleAuthor.email_address" : {
				required : true,
			},			
			"articleAuthor.contact_number" : {
				required : true,
			},
			"articleLocation.location" : {
				required : true,
			},
			"articleLocation.latitude" : {
				required : false,
			},
			"articleLocation.longitude" : {
				required : false,
			},
			"articleLocation.altitude" : {
				required : false,
			}
			*/
	
		},
		submitHandler : function(form) {
			form.submit();
		}
	});
	
	$("#publicationData").validate({
		rules : {
			"publicationType.id" : {
				required : true,
			},
			"name" : {
				required : true,
			},
			"data_submitted" : {
				required : true,
			},"name" : {
				required : true,
			},
			"publisher" : {
				required : true,
			},
			"year" : {
				required : true,
			},
			"data_published" : {
				required : true,
			},
	
		},
		submitHandler : function(form) {
			form.submit();
		}
	});

	$("#userData").validate({
		rules : {
			"name" : {
				required : true
			},
			"telephone" : {
				required : false,
				regx : /(0)[0-9]{9}/,
			},
			"contact_number" : {
				required : false,
				regx : /^((0)[0-9]{9})$/,
			},
			"address" : {
				required : false
			},
			"email" : {
				email : true,
				required : true
			},
			"nic" : {
				required : false,
				regx : /^([0-9]{9}(v|V)|[0-9]{12})$/,
			},
			"gender" : {
				required : true
			},
			"userAccount.username" : {
				required : true,
				minlength : 4
			},
			"userAccount.userPassword" : {
				required : true,
				minlength : 6
			},
			"userAccount.confirmPassword" : {
				required : true,
				minlength : 6,
				equalTo : "#userPassword"
			},
			"userAccount.expireDate" : {
				required : false
			},
			"userAccount.userAccountRoles[]" : "required"
		},
		messages : {
			contact_number : {
				regx : "invalid phone number.{format ex: 0xxxxxxxxx}",
			},
			nic : {
				regx : "invalid NID number.{format ex: xxxxxxxxxV or xxxxxxxxxxxx}",
			},
		},
		errorPlacement : function(error, element) {
			if (element.attr("name") == "user.gender") {
				error.insertAfter("#endGender");
			} else {
				error.insertAfter(element);
			}
		},
		submitHandler : function(form) {
			form.submit();
		}
	});
	
	$("#resetPasswordData").validate({
		rules : {
			"email" : {
				email : true,
				required : true
			},
		},
		submitHandler : function(form) {
			form.submit();
		}
	});
	
	$("#loginData").validate({
		rules : {
			"username" : {
				required : true,
				minlength : 4
			},				
			"password" : {
				required : true,
				minlength : 6
			},
		},
		submitHandler : function(form) {
			form.submit();
		}
	});
	
	$("#updatePasswordData").validate({
		rules : {				
			"userPassword" : {
				required : true,
				minlength : 6
			},
			"confirmPassword" : {
				required : true,
				minlength : 6,
				equalTo : "#userPassword"
			},
		},
		submitHandler : function(form) {
			form.submit();
		}
	});

	$("#loginRequestData").validate({
		rules : {
			"username" : {
				required : true,
				minlength : 4
			},				
			"email" : {
				email : true,
				required : true
			},
			"phone" : {
				required : false,
				regx : /^((0)[0-9]{9})$/,
			},
		},
		messages : {
			phone : {
				regx : "invalid phone number.{format ex: 0xxxxxxxxx}",
			},
		},
		submitHandler : function(form) {
			form.submit();
		}
	});
