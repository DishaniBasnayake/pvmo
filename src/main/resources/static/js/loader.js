$('.wrapper').addClass("blur");
$(window).on('load', function () {
	$('.loader').delay(200).fadeOut("slow");
	setTimeout(function(){
    	$('.wrapper').removeClass("blur");
		$(' html, body').css({overflow: 'auto'});
   	}, 500);
});

function showLoader(){
	$('.wrapper').addClass("blur");
	$('.loader').show();
}  

function hideLoader(){
	$('.loader').delay(200).fadeOut("slow");
	setTimeout(function(){
    	$('.wrapper').removeClass("blur");
   	}, 500);	
}