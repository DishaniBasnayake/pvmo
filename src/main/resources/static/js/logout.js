$(function() {
	$.ajaxSetup({
        statusCode: {
            403: function () {
                window.location.reload();
            },
			401: function () {
                window.location.reload();
            }
        }
    });
});
