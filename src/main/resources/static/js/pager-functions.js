$(function() {	
	$.fn.isOnScreen = function(){
	
	    var win = $(window);
	
	    var viewport = {
	        top : win.scrollTop(),
	        left : win.scrollLeft()
	    };
	    viewport.right = viewport.left + win.width();
	    viewport.bottom = viewport.top + win.height();
	
	    var bounds = this.offset();
	    bounds.right = bounds.left + this.outerWidth();
	    bounds.bottom = bounds.top + this.outerHeight();
	
	    return (!( bounds.top == 0 ||  viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));

	};

   var page = 0;
   var buzy = false;

   document.addEventListener('keypress', function(event) {
		if (event.keyCode == 13) {
		   $("#btnSearch").click();
		}
   });		   

   $("#btnSearch").click( function(e){
	   page= 0;
       e.preventDefault();
       loadMoreData();
   });

   if ($('#load-more').isOnScreen() == true) {
	   loadMoreData();
	}
   	
   $("#load-more").click( function(e){
       e.preventDefault();
       loadMoreData();
   });
   
   $(window).scroll(function() {
	   	if ($('#load-more').isOnScreen() == true) {
		   loadMoreData();
		}
   });
       
   	function loadMoreData(){
   	    if(buzy){ return; }
        $.ajax(
        {
            url: url +'&page=' + page++ ,
            type: "get",
            beforeSend: function()
            {
                $('.ajax-load').show();
                $("#load-more").hide();
	   			$("#reach-end").hide();
				$("#recordsCount").text('');
	
                buzy = true;
            }
        })
        .done(function(data)
        {
            var records = data.payload[0].content;
			$("#recordsCount").text(data.payload[0].totalElements + ' ' + RECORDS_FOUND);
            if(records.length === 0){
                $('.ajax-load').hide();
                $('#reach-end').html('<div class="alert bg-gray"> '+NO_MORE_RECORDS_FOND+' </div>');
				$('#reach-end').show();
                $("#load-more").hide();
                buzy = false;
                return;
            }else{
                $('.ajax-load').hide();
                $("#load-more").show();
                var html = '';
                $.each( records, function( index, entry ){
                	html += '<tr>';
            		html += '<td>'+ ((data.payload[0].number * data.payload[0].size) + index + 1 ) +'</td>';
					html += [entry].map(rowMap).join('');
					html += '</tr>';
                });
                $("#"+tableBodyDivId).append(html);
                buzy = false;

         	   if ($('#load-more').isOnScreen() == true) {
         		   loadMoreData();
         		}
            }
        })
        .fail(function(jqXHR, ajaxOptions, thrownError)
        {
              buzy = false;
			  toastr.error(MSG_DATA_LOADING_FAIL);
			  $("#recordsCount").text(MSG_DATA_LOADING_FAIL);
			  $('#reach-end').html(MSG_DATA_LOADING_FAIL);	 
			  $('.ajax-load').hide();
			  $('#reach-end').show();
        });
   	}
});