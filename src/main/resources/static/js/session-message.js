$(document).ready(function(){
    var success = localStorage.getItem("success");
    var error = localStorage.getItem("error");
    if(success)
    {
        toastr.success(success);
        localStorage.clear();
    }
	if(error)
    {
        toastr.error(error);
        localStorage.clear();
    }
});