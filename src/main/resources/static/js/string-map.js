/**
 * 
 */
var COLOR_SOLAR_PANEL_INIT = '#E5E4E2';
var COLOR_PCS_INIT = '#14AAF5';
var COLOR_QBIC_INIT = '#14AAF5';
var COLOR_ANOMALY = 'RED';
var COLOR_CAMERA_INIT = "#008080";
var enableSelectedItemAnimation = false;
var animationTimer = null;
var animatedItem = null;
var initScaleX = 1;
var initScaleY = 1;
var lastSolarPanel_left = 0;
var lastSolarPanel_top = 10;
var lastSolarPanel_scale_x = 1;
var lastSolarPanel_scale_y = 1;
var nextSolarPanelId = 0;
var nextPcsId = 0;
var nextCameraId = 0;
var nextJcbId = 0;
var nextLandAreaId = 0;
var nextGpsId = 0;

var obj_type_panel = "panel";
var obj_type_pcs = "pcs";
var obj_type_jcb = "jcb";
var obj_type_camera = "camera";
var obj_type_floor = "floor_area";
var obj_type_gps = "gps";
var obj_type_string = "str";
var obj_type_textbox = "textbox";

window.addEventListener('resize', resizeCanvas, false);
var strMap = new Object();
var jcbMap = new Object();
var strInMap = [];
var panelStrings = '';
var panels = '';
var objectsInMap = [];

var userGPSMap = new Object();

var canvas = this.__canvas = new fabric.Canvas('canvas');

document.onkeydown = function(e) {
	switch (e.keyCode) {
		case 32:  /* space */
			if(!canvas.getActiveObject()) {
				var elm = $(e.target);

				if (elm.is("input") || elm.is("textarea")) {
					elm.val(elm.val() + ' ');
				}
			} else {
				return false;
			}
		break;

	}
}

resizeCanvas();

function resizeCanvas() {
	console.log("aaa");
	$("#canvas-base").css("overflow","hidden");
	var width = $("#canvas-base").width();
	var height = "";
	
	if ($(window).width() < 992) {
		height = width * 1.2;

	} else {
		height = width * 0.6;
	}

	canvas.setWidth(width - 15);
	canvas.setHeight(height - 15);
	canvas.renderAll();

	$("#dabx").css("height", height);
	$("#dabx2").css("height", height);
}

init();

function init() {
	//addGrid();

}

var gridGroup;

function addGrid() {
	if (gridGroup) return;
	var gridsize = 5;
	var gridoption = {
		stroke: "#ddd",
		strokeWidth: 1,
		strokeDashArray: [2, 2]
	};
	var gridLines = [];
	for (var x = 0; x < (canvas.width); x += 20) {
		gridLines.push(new fabric.Line([x, 0, x, canvas.width], gridoption));
	}
	for (var x = 0; x < (canvas.height); x += 20) {
		gridLines.push(new fabric.Line([0, x, canvas.height + 300, x], gridoption));
	}
	gridGroup = new fabric.Group(gridLines, {
		selectable: false,
		evented: false
	})
	gridGroup.addWithUpdate();
	canvas.add(gridGroup);

	//   var gridsize = 5;
	//     for (var x = 1; x < (canvas.width / gridsize); x++) {
	//         canvas.add(new fabric.Line([20 * x, 0, 20 * x, canvas.width], { stroke: "#ddd", strokeWidth: 1, selectable: false, strokeDashArray: [2, 2] }));
	//         canvas.add(new fabric.Line([0, 20 * x, canvas.width, 20 * x], { stroke: "#ddd", strokeWidth: 1, selectable: false, strokeDashArray: [2, 2] }));
	//     }
}

function removeGrid() {
	gridGroup && canvas.remove(gridGroup);
	gridGroup = null;
}


function manageGrid() {
	if (document.getElementById('chk-grid').checked) {
		addGrid();
	} else {
		removeGrid();
	}
}


// define a function that can locate the controls.
// this function will be used both for drawing and for interaction.
function polygonPositionHandler(dim, finalMatrix, fabricObject) {
	var x = (fabricObject.points[this.pointIndex].x - fabricObject.pathOffset.x),
		y = (fabricObject.points[this.pointIndex].y - fabricObject.pathOffset.y);
	return fabric.util.transformPoint(
		{ x: x, y: y },
		fabric.util.multiplyTransformMatrices(
			fabricObject.canvas.viewportTransform,
			fabricObject.calcTransformMatrix()
		)
	);
}

// define a function that will define what the control does
// this function will be called on every mouse move after a control has been
// clicked and is being dragged.
// The function receive as argument the mouse event, the current trasnform object
// and the current position in canvas coordinate
// transform.target is a reference to the current object being transformed,
function actionHandler(eventData, transform, x, y) {
	var polygon = transform.target,
		currentControl = polygon.controls[polygon.__corner],
		mouseLocalPosition = polygon.toLocalPoint(new fabric.Point(x, y), 'center', 'center'),
		polygonBaseSize = polygon._getNonTransformedDimensions(),
		size = polygon._getTransformedDimensions(0, 0),
		finalPointPosition = {
			x: mouseLocalPosition.x * polygonBaseSize.x / size.x + polygon.pathOffset.x,
			y: mouseLocalPosition.y * polygonBaseSize.y / size.y + polygon.pathOffset.y
		};
	polygon.points[currentControl.pointIndex] = finalPointPosition;
	return true;
}

// define a function that can keep the polygon in the same position when we change its
// width/height/top/left.
function anchorWrapper(anchorIndex, fn) {
	return function(eventData, transform, x, y) {
		var fabricObject = transform.target,
			absolutePoint = fabric.util.transformPoint({
				x: (fabricObject.points[anchorIndex].x - fabricObject.pathOffset.x),
				y: (fabricObject.points[anchorIndex].y - fabricObject.pathOffset.y),
			}, fabricObject.calcTransformMatrix()),
			actionPerformed = fn(eventData, transform, x, y),
			newDim = fabricObject._setPositionDimensions({}),
			polygonBaseSize = fabricObject._getNonTransformedDimensions(),
			newX = (fabricObject.points[anchorIndex].x - fabricObject.pathOffset.x) / polygonBaseSize.x,
			newY = (fabricObject.points[anchorIndex].y - fabricObject.pathOffset.y) / polygonBaseSize.y;
		fabricObject.setPositionByOrigin(absolutePoint, newX + 0.5, newY + 0.5);
		return actionPerformed;
	}
}

function addLandArea() {
	//create land area
	var points = [
		{ x: 1, y: 1 },
		{ x: 20, y: 1 },
		{ x: 40, y: 1 },
		{ x: 60, y: 1 },
		{ x: 80, y: 1 },
		{ x: 100, y: 1 },
		{ x: 100, y: 20 },
		{ x: 100, y: 40 },
		{ x: 100, y: 60 },
		{ x: 100, y: 80 },
		{ x: 80, y: 80 },
		{ x: 60, y: 80 },
		{ x: 40, y: 80 },
		{ x: 20, y: 80 },
		{ x: 1, y: 80 },
		{ x: 1, y: 60 },
		{ x: 1, y: 40 },
		{ x: 1, y: 20 },
		{ x: 1, y: 1 }]

	var polygon = new fabric.Polygon(points, {
		left: 20,
		top: 30,
		//fill: '#FFFFFF',
		strokeWidth: 0.5,
		stroke: 'black',
		scaleX: 6,
		scaleY: 4,
		objectCaching: false,
		transparentCorners: false,
		cornerColor: 'gray',
		fill: 'rgba(0,0,0,0)',
		id: obj_type_floor + '_' + nextLandAreaId,
		object: obj_type_floor,
	});
	//canvas.viewportTransform = [0.7, 0, 0, 0.7, -50, 50];
	canvas.add(polygon);
	nextLandAreaId = nextLandAreaId + 1;
}

function editLandArea() {
	// clone what are you copying since you
	// may want copy and paste on different moment.
	// and you do not want the changes happened
	// later to reflect on the copy.
	//var poly = canvas.getObjects()[318];
	//alert('edit');
	//var poly = canvas.getItemById('floor_area');
	var poly = canvas.getActiveObject();
	if (poly.object === obj_type_floor) {
		canvas.setActiveObject(poly);
		poly.edit = !poly.edit;
		if (poly.edit) {
			var lastControl = poly.points.length - 1;
			poly.cornerStyle = 'circle';
			poly.cornerColor = 'rgba(0,0,255,0.5)';
			poly.controls = poly.points.reduce(function(acc, point, index) {
				acc['p' + index] = new fabric.Control({
					positionHandler: polygonPositionHandler,
					actionHandler: anchorWrapper(index > 0 ? index - 1 : lastControl, actionHandler),
					actionName: 'modifyPolygon',
					pointIndex: index
				});
				return acc;
			}, {});
		} else {
			poly.cornerColor = 'blue';
			poly.cornerStyle = 'rect';
			poly.controls = fabric.Object.prototype.controls;
		}
		poly.hasBorders = !poly.edit;
		canvas.requestRenderAll();
	}
}


fabric.Canvas.prototype.getItemById = function(id) {
	var object = null,
		objects = this.getObjects();
	for (var i = 0; i < objects.length; i++) {
		if (objects[i].id && objects[i].id === id) {
			object = objects[i];
			break;
		}
	}
	return object;
};

function addSolarPanel(top, left) {
	var solarPanel = new fabric.Rect({
		top: top,
		left: left,
		width: 20,
		height: 10,
		fill: COLOR_SOLAR_PANEL_INIT,
		stroke: 'gray',
		lockUniScaling: true,
		scaleX: lastSolarPanel_scale_x,
		scaleY: lastSolarPanel_scale_y,
		lockUniScaling: true,
		object: obj_type_panel,
		id: obj_type_panel + '_' + nextSolarPanelId,
		controls: false
	});
	canvas.add(solarPanel);
	if (top == 0 && left == 10) {
		canvas.viewportCenterObject(solarPanel);
	}
	lastSolarPanel_left = solarPanel.left;
	lastSolarPanel_top = solarPanel.top;
	lastSolarPanel_scale_x = solarPanel.scaleX;
	lastSolarPanel_scale_y = solarPanel.scaleY;
	nextSolarPanelId = nextSolarPanelId + 1;
	addItemTable(solarPanel);
	return solarPanel;
};

function addSolarPanelGrid(row, col) {
	var top = 0;
	var left = 10;
	var firstLeft = 10;
	var obj;
	for (var i = 1; i <= row; i++) {
		for (var j = 1; j <= col; j++) {
			obj = addSolarPanel(top, left);
			if (i == 1 && j == 1) {
				firstLeft = obj.left;
			}
			left = obj.left + 21;
			top = obj.top;
		}
		top = obj.top + 11;
		left = firstLeft;
	}
}


function addPcs() {
	var points = [
		{ x: 3, y: 3 },
		{ x: 4, y: 3 },
		{ x: 5, y: 5 },
		{ x: 5, y: 7 },
		{ x: 4, y: 9 },
		{ x: 3, y: 9 },
		{ x: 2, y: 9 },
		{ x: 1, y: 7 },
		{ x: 1, y: 5 },
		{ x: 2, y: 3 },
		{ x: 3, y: 3 }
	]

	var pcs = new fabric.Polygon(points, {
		left: 20,
		top: 30,
		scaleX: 4,
		scaleY: 4,
		fill: COLOR_PCS_INIT,
		strokeWidth: 0.5,
		stroke: 'blue',
		object: obj_type_pcs,
		id: obj_type_pcs + '_' + nextPcsId,
	});

	canvas.add(pcs);
	canvas.viewportCenterObject(pcs);
	nextPcsId = nextPcsId + 1;
	addItemTable(pcs);
};


function addCamera() {
	var imgURL = '../images/string-map/camera.png';
	var cameraImg = new Image();
	cameraImg.onload = function(img) {
		var camera = new fabric.Image(cameraImg, {
			width: 200,
			height: 200,
			left: 30,
			top: 10,
			scaleX: .10,
			scaleY: .10,
			object: obj_type_camera,
			id: obj_type_camera + '_' + nextCameraId,
		});
		canvas.add(camera);
		canvas.viewportCenterObject(camera);
		nextCameraId = nextCameraId + 1;
		addItemTable(camera);
	};
	cameraImg.src = imgURL;
}

function addGps(long, lat) {
	var imgURL = '../images/string-map/gps.png';
	var gpsImg = new Image();
	gpsImg.onload = function(img) {
		var gps = new fabric.Image(gpsImg, {
			width: 160,
			height: 250,
			left: 30,
			top: 10,
			scaleX: .10,
			scaleY: .10,
			object: obj_type_gps,
			id: obj_type_gps + '_' + nextGpsId,
			long: long,
			lat: lat,
		});
		canvas.add(gps);
		canvas.viewportCenterObject(gps);
		nextGpsId = nextGpsId + 1;
		addItemTable(gps);
	};
	gpsImg.src = imgURL;
}

function addJcb() {
	var jcb = new fabric.Rect({
		top: event.screenY,
		left: event.screenX,
		width: 20,
		height: 20,
		fill: COLOR_QBIC_INIT,
		stroke: 'gray',
		lockUniScaling: true,
		scaleX: 1,
		scaleY: 1,
		object: obj_type_jcb,
		id: obj_type_jcb + '_' + nextJcbId
	});
	canvas.add(jcb);
	canvas.viewportCenterObject(jcb);
	nextJcbId = nextJcbId + 1;
	addItemTable(jcb);
}

canvas.on({
	'mouse:down': function(e) {
		if (e.target && canvas.getActiveObject() && canvas.getActiveObject().type) {
			if (canvas.getActiveObject().type === obj_type_panel || canvas.getActiveObject().type === obj_type_pcs || canvas.getActiveObject().type === obj_type_camera) {
				var obj = canvas.getActiveObject();
				//var idBox = document.getElementById('object_id').value=obj.id ;      
			}
		}
	},
	'mouse:up': function(e) {
		if (e.target) {
			e.target.opacity = 1;
			canvas.renderAll();
		}
	},
	'object:moving': function(e) {
		var obj = canvas.getActiveObject();
		//lastSolarPanel_left = obj.left;
		//lastSolarPanel_top = obj.top;
		//console.log('['+e.e.movementX+','+e.e.movementY+']');
		//if(!obj.isOnScreen()){
		var delta = new fabric.Point(-e.e.movementX, -e.e.movementY);
		// canvas.absolutePan({
		//     x: e.e.movementX,
		//     y: e.e.movementY
		//  });

		canvas.relativePan(delta);

		//canvas.renderAll();
		// this.lastPosX = e.clientX;
		// this.lastPosY = evt.clientY;
		// var vpt = this.viewportTransform;
		// vpt[4] += e.clientX - this.lastPosX;
		// vpt[5] += e.clientY - this.lastPosY;
		// this.requestRenderAll();
		// this.lastPosX = e.clientX;
		// this.lastPosY = e.clientY;
		// this.setViewportTransform(this.viewportTransform);

		// }
	},
});


function deleteItem() {
	var selected = canvas.getActiveObjects(),
		selGroup = new fabric.ActiveSelection(selected, {
			canvas: canvas
		});
	if (selGroup && selected.length > 0) {
		if (confirm('Are you sure to delete the selected?')) {
			selGroup.forEachObject(function(obj) {
				canvas.remove(obj);
				removeFromTable(obj.id);
			});
		}
	} else {
		return false;
	}
	canvas.discardActiveObject().renderAll();
};

function demoAnomalies() {
	clearAnomalies();

	var objects = canvas.getObjects();
	var numSolarPanel = 0;
	var numPcs = 0;
	var anomalityObjects = [];
	var selectedObjects = [];
	for (var i = 0; i < objects.length; i++) {
		if (objects[i].object && objects[i].object === obj_type_panel) {
			numSolarPanel++;
			anomalityObjects.push(objects[i]);
		}
	}

	for (var i = 0; i < objects.length; i++) {
		if (objects[i].object && objects[i].object === obj_type_pcs) {
			numPcs++;
			anomalityObjects.push(objects[i]);
		}
	}

	for (var j = 0; j < Math.floor(Math.random() * 3) + 1; j++) {
		const rndInt = Math.floor(Math.random() * anomalityObjects.length) + 1;
		var object = canvas.getItemById(anomalityObjects[rndInt - 1].id);
		selectedObjects.push(anomalityObjects[rndInt - 1].objectId);
		showAnomalyOnTable(object.objectId);
		object.set('fill', COLOR_ANOMALY);
	}
	/*  for(var j=0; j<anomalityObjects.length; j++){
		 //const rndInt = Math.floor(Math.random() * anomalityObjects.length) + 1;
		 canvas.getItemById(anomalityObjects[j].id).set('fill',COLOR_ANOMALY);
	 }*/

	canvas.renderAll();
	//return selectedObjects;
};

function demoPanelStringAnomalies() {
	clearAnomalies();

	var anomalityObjects = [];

	const rndInt = Math.floor(Math.random() * strInMap.length);

	var i = 0;
	//strId=
	/*console.log(rndInt);
	//console.log(strInMap);
	console.log(strInMap[rndInt]);*/
	anomalityObjects.push(strMap[strInMap[rndInt]]);
	//console.log(anomalityObjects);

	for (var j = 0; j < anomalityObjects.length; j++) {
		for (var key in anomalityObjects[j]) {
			//console.log(anomalityObjects[j][key].id);
			var object = getElementByObjectId(anomalityObjects[j][key].id);
			//canvas.getItemById(anomalityObjects[j].objectId).set('fill',COLOR_ANOMALY);
			object.set('fill', COLOR_ANOMALY);
			showAnomalyOnTable(object.objectId);

		}
	}
	showAnomalyOnTable(strInMap[rndInt]);
	canvas.renderAll();
};

function demoJcbAnomalies() {
	clearAnomalies();

	var objects = canvas.getObjects();
	var numJcb = 0;
	var numPcs = 0;
	var anomalityObjects = [];
	for (var i = 0; i < objects.length; i++) {
		if (objects[i].object && objects[i].object === obj_type_jcb) {
			numJcb++;
			anomalityObjects.push(objects[i]);
		}
	}

	for (var j = 0; j < Math.floor(Math.random() * 3) + 1; j++) {
		const rndInt = Math.floor(Math.random() * anomalityObjects.length) + 1;
		var object = canvas.getItemById(anomalityObjects[rndInt - 1].id);
		object.set('fill', COLOR_ANOMALY);
		showAnomalyOnTable(object.objectId);
		var panelStrings = jcbMap[object.objectId];
		for (var key in panelStrings) {
			//console.log(panelStrings[key]);
			showAnomalyOnTable(panelStrings[key].id);
			var panels = panelStrings[key].panels;
			for (var panelKey in panels) {
				showAnomalyOnTable(panels[panelKey].id);
			}
		}
	}

	canvas.renderAll();
};

function getElementByObjectId(objectId) {
	var objects = canvas.getObjects();
	for (var i = 0; i < objects.length; i++) {
		//console.log(objects[i].objectId + "&&" + objectId);
		if (objects[i].objectId == objectId) {
			return objects[i];
		}
	}
}

function showAnomalyOnTable(objectId) {
	var obj = document.getElementById(objectId);
	obj.style.backgroundColor = 'red';
};

function clearAnomaliesOnTable() {
	$('#tbody-objects td').css('backgroundColor', '');
}

function clearAnomalies() {
	var objects = canvas.getObjects();
	for (var i = 0; i < objects.length; i++) {
		if (objects[i].object && objects[i].object === obj_type_panel) {
			canvas.getItemById(objects[i].id).set('fill', COLOR_SOLAR_PANEL_INIT);
		}
		else if (objects[i].object && objects[i].object === obj_type_pcs) {
			canvas.getItemById(objects[i].id).set('fill', COLOR_PCS_INIT);
		}
		else if (objects[i].object && objects[i].object === obj_type_jcb) {
			canvas.getItemById(objects[i].id).set('fill', COLOR_QBIC_INIT);
		}
	}
	canvas.renderAll();
	clearAnomaliesOnTable();
}

function clearAll() {
	canvas.clear();
	//clearDataTable();
	$('#chk-grid').prop("checked", false);
	//init();
}

function convertToJson() {
	return JSON.stringify(canvas.toJSON(['id', 'object', 'objectId', 'long', 'lat']));
}

function exportToJson() {
	var element = document.getElementById('downloadAnchor');
	var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(convertToJson());
	element.setAttribute("href", dataStr);
	element.setAttribute("download", "scene.json");
	element.click();
}
function loadCanvasFromJson(data) {
	var deferred = new $.Deferred();
	
	canvas.loadFromJSON(data, function() {
		canvas.renderAll();
	}, function(o, object) {
		canvas.add(object);
	});
	setTimeout(function(){
        deferred.resolve();
    }, 2000);
	return deferred.promise();
}

function loadCanvasFromJsonDisableEditing(data) {
	loadCanvasFromJson(data).done(function() {
    	var objects = canvas.getObjects();
		//console.log(objects);
		for (var i = 0; i < objects.length; i++) {
			objects[i].selectable = false;
		}
		canvas.renderAll();
	});
}

function updateStrInMap() {
	var dfd = new $.Deferred();

	var objects = canvas.getObjects();
	for (var i = 0; i < objects.length; i++) {
		if (objects[i].object === obj_type_panel) {
			var strId = $('#' + objects[i].objectId).parent().parent().find('td.str').attr('id');

			if (jQuery.inArray(strId, strInMap) === -1) {
				strInMap.push(strId);
			}
		}
	}

	dfd.resolve();
	return dfd.promise();
}

function addBackgroundImage(data) {
	fabric.Image.fromURL(data, function(img) {
		// add background image
		canvas.setBackgroundImage(img, canvas.renderAll.bind(canvas), {
			//scaleX: canvas.width / img.width,
			//scaleY: canvas.height / img.height
		});
	});
}

function clearCanvasBackgroundImage() {
	//add empty image
	fabric.Image.fromURL('', function(img) {
		canvas.setBackgroundImage(img, canvas.renderAll.bind(canvas), {
			//scaleX: canvas.width / img.width,
			//scaleY: canvas.height / img.height
		});
	});
}

function resetZoom() {
	canvas.setViewportTransform([1, 0, 0, 1, 0, 0]);
	//canvas.renderAll();
}

function addItemTable(item) {
	$('#tbl_item>tbody').append('<tr><td>' + item.id + '</td><td>' + item.object + '</td></tr>');
}

function reloadDataTable() {
	var objects = canvas.getObjects();
	for (var i = 0; i < objects.length; i++) {
		if (objects[i].object && objects[i].object != 'floor_area') {
			addItemTable(objects[i]);
		}
	}
}

function selectItem(id) {
	var activeObj = canvas.getItemById(id)
	activeObj.animate('opacity', '0', {
		duration: 1000,
		onChange: canvas.renderAll.bind(canvas),
		onComplete: function() {
			activeObj.set('opacity', '1');
			canvas.setActiveObject(activeObj);
			canvas.renderAll();
		}
	});
	enableDeviceInfo(activeObj.id);
	//   enableSelectedItemAnimation = true;
	//   initScaleX=element.scaleX;
	//   initScaleY=element.scaleY;
	//   animatedItem = element;
	//   var i=0;
	//   while(enableSelectedItemAnimation){
	//       if(i%2){
	//           element.scaleX=element.scaleX*2;
	//           element.scaleY=element.scaleY*2;
	//       }
	//       else{
	//         element.scaleX=element.scaleX/2;
	//         element.scaleY=element.scaleY/2;
	//       }
	//       canvas.renderAll();
	//       i=i+1;
	//   }
	//   animationTimer = setTimeout(function animate() {
	//     setTimeout(animate, 200);
	//   }, 200);
}
function enableDeviceInfo(id) {
	$('#div-device-info').removeClass('invisible');
	/*var content='<h5>Device Info</h5>';
	content+='<div class="row">';
	content+='<div class="col-md-3">Device ID:</div>';
	content+='<div class="col-md-9"><input type="text" id="object_id" value="'+id+'"/></div>';      
	content+='</div>';
	content+='<div class="row">';
	content+='<div class="col-md-12"><button type="button" onClick="saveDevice()" id="btn-save">Save</button></div>';
	content+='<input type="hidden" id="object_id_org" value="'+id+'"/>';*/

	var content = 'Device ID: <input type="text" id="object_id" value="' + id + '"/>';
	content += '<button type="button" onClick="saveDevice()" id="btn-save">Save</button></div>';
	content += '<input type="hidden" id="object_id_org" value="' + id + '"/>';
	$('#div-device-info').html(content);
}

function disableDeviceInfo() {
	$('#div-device-info').html('');
	$('#div-device-info').addClass('invisible');
}

function saveDevice() {
	var orgId = $('#object_id_org').val();
	var newId = $('#object_id').val();
	canvas.getItemById(orgId).set('id', newId);
	canvas.renderAll();

	$("#tbl_item tr td:first-child").filter(function() {
		return $(this).text().indexOf(orgId) > -1;
	}).text(function() {
		$(this).text(newId);
	});

}

function removeFromTable(id) {
	$("#tbl_item tr td:first-child").filter(function() {
		return $(this).text().indexOf(id) > -1;
	}).parent().remove();
}

function clearDataTable() {
	$("#tbl_item > tbody").empty();
	disableDeviceInfo();
}

function clearSelectedItem() {
	enableSelectedItemAnimation = false;
}

function convertToImage() {
	return canvas.toDataURL({
		width: canvas.width,
		height: canvas.height,
		left: 0,
		top: 0,
		format: 'png',
	});
}
function exportToImage() {
	const dataURL = convertToImage();
	const link = document.createElement('a');
	link.download = 'image.png';
	link.href = dataURL;
	document.body.appendChild(link);
	link.click();
	document.body.removeChild(link);
}

function saveChanges() {
	var site = {}
	site["id"] = $('#map-site-id').val();
	site["mapImage"] = convertToImage();

	$.ajax({
		type: "POST",
		contentType: "application/json",
		url: "/site/saveUpdateImage",
		data: JSON.stringify(site),
		dataType: 'json',
		cache: false,
		timeout: 600000,
		success: function(data) {
			console.log("SUCCESS : ", data);
		},
		error: function(e) {
			console.log("ERROR : ", e);
		}
	});

	var site = {}
	site["id"] = $('#map-site-id').val();
	site["mapJson"] = convertToJson();

	$.ajax({
		type: "POST",
		contentType: "application/json",
		url: "/site/saveUpdateJson",
		data: JSON.stringify(site),
		dataType: 'json',
		cache: false,
		timeout: 600000,
		success: function(data) {
			console.log("SUCCESS : ", data);
			alert("String map saved successfully");
		},
		error: function(e) {
			console.log("ERROR : ", e);
		}
	});
}

canvas.on('mouse:wheel', function(opt) {
	var delta = opt.e.deltaY;
	var zoom = canvas.getZoom();
	zoom *= 0.999 ** delta;
	if (zoom > 20) zoom = 20;
	if (zoom < 0.1) zoom = 0.1;
	canvas.zoomToPoint({ x: opt.e.offsetX, y: opt.e.offsetY }, zoom);
	opt.e.preventDefault();
	opt.e.stopPropagation();
});

canvas.on('mouse:down', function(opt) {
	var evt = opt.e;
	console.log(opt.e);
	if (evt.altKey === true) {
		this.isDragging = true;
		this.selection = false;
		this.lastPosX = evt.clientX;
		this.lastPosY = evt.clientY;
	}
});
canvas.on('mouse:move', function(opt) {
	if (this.isDragging) {
		var e = opt.e;
		var vpt = this.viewportTransform;
		vpt[4] += e.clientX - this.lastPosX;
		vpt[5] += e.clientY - this.lastPosY;
		this.requestRenderAll();
		this.lastPosX = e.clientX;
		this.lastPosY = e.clientY;
	}
});
canvas.on('mouse:up', function(opt) {
	// on mouse up we want to recalculate new interaction
	// for all objects, so we call setViewportTransform
	this.setViewportTransform(this.viewportTransform);
	this.isDragging = false;
	this.selection = true;
});

function copy() {
	canvas.getActiveObject().clone(function(cloned) {
		_clipboard = cloned;
	}, ['id', 'object']);
}

function paste() {
	_clipboard.clone(function(clonedObj) {
		canvas.discardActiveObject();
		clonedObj.set({
			left: clonedObj.left + 10,
			top: clonedObj.top + 10,
			evented: true,
		});
		if (clonedObj.type === 'activeSelection') {
			// active selection needs a reference to the canvas.
			clonedObj.canvas = canvas;
			clonedObj.forEachObject(function(obj) {
				obj.id = upgradeId(obj);
				canvas.add(obj);
				addItemTable(obj);
			});
			// this should solve the unselectability
			clonedObj.setCoords();
		} else {
			clonedObj.id = upgradeId(clonedObj);
			canvas.add(clonedObj);
			addItemTable(clonedObj);
		}
		canvas.setActiveObject(clonedObj);
		canvas.requestRenderAll();
	}, ['id', 'object']);
}

function upgradeId(obj) {
	if (obj.object === obj_type_panel) {
		return obj_type_panel + '_' + (nextSolarPanelId++);
	}
	else if (obj.object === obj_type_pcs) {
		return obj_type_pcs + '_' + (nextPcsId++);
	}
	else if (obj.object === obj_type_jcb) {
		return obj_type_jcb + '_' + (nextJcbId++);
	}
	else if (obj.object === obj_type_jcb) {
		return obj_type_jcb + '_' + (nextJcbId++);
	}
	else if (obj.object === obj_type_camera) {
		return obj_type_camera + '_' + (nextCameraId++);
	}
}

//undo-redo
var current;
var list = [];
var state = [];
var index = 0;
var index2 = 0;
var action = false;
var refresh = true;

canvas.on("object:added", function(e) {
	var object = e.target;
	console.log('object:added');

	if (action === true) {
		state = [state[index2]];
		list = [list[index2]];

		action = false;
		// console.log(state);
		index = 1;
	}
	//object.saveState();
	//console.log(object.originalState);
	//state[index] = JSON.stringify(object.originalState);
	state[index] = JSON.stringify(object.saveState());
	list[index] = object;
	index++;
	index2 = index - 1;
	refresh = true;
});

canvas.on("object:modified", function(e) {
	var object = e.target;
	console.log('object:modified');

	if (action === true) {
		state = [state[index2]];
		list = [list[index2]];

		action = false;
		//console.log(state);
		index = 1;
	}

	//object.saveState();

	//state[index] = JSON.stringify(object.originalState);
	state[index] = JSON.stringify(object.saveState());
	list[index] = object;
	index++;
	index2 = index - 1;

	//console.log(state);
	refresh = true;
});

function undo() {

	if (index <= 0) {
		index = 0;
		return;
	}

	if (refresh === true) {
		index--;
		refresh = false;
	}

	console.log('undo');

	index2 = index - 1;
	current = list[index2];
	console.log("current: " + current);
	current.setOptions(JSON.parse(state[index2]));

	index--;
	current.setCoords();
	canvas.renderAll();
	action = true;
}

function redo() {

	action = true;
	if (index >= state.length - 1) {
		return;
	}

	console.log('redo');

	index2 = index + 1;
	current = list[index2];
	current.setOptions(JSON.parse(state[index2]));

	index++;
	current.setCoords();
	canvas.renderAll();
}


var pos = { x: 0, y: 0 }
function moveViewPort(deltax, deltay) {
	pos.x += deltax;
	pos.y += deltay;
	canvas.absolutePan({
		x: pos.x,
		y: pos.y
	});
}


//move object by keyboard

const STEP = 1;

var Direction = {
	LEFT: 0,
	UP: 1,
	RIGHT: 2,
	DOWN: 3
};

fabric.util.addListener(document.body, 'keydown', function(e) {
	//preveing scroll moving by arrow keys
	if (["Space", "ArrowUp", "ArrowDown", "ArrowLeft", "ArrowRight"].indexOf(e.code) > -1) {
		e.preventDefault();
	}

	if (e.repeat) {
		return;
	}
	var key = e.which || e.keyCode; // key detection
	if (key === 37) { // handle Left key
		moveSelected(Direction.LEFT);
	} else if (key === 38) { // handle Up key
		moveSelected(Direction.UP);
	} else if (key === 39) { // handle Right key
		moveSelected(Direction.RIGHT);
	} else if (key === 40) { // handle Down key
		moveSelected(Direction.DOWN);
	}
});

function moveSelected(direction) {

	var activeObject = canvas.getActiveObject();
	if (activeObject) {
		switch (direction) {
			case Direction.LEFT:
				activeObject.left = (activeObject.left - STEP);
				break;
			case Direction.UP:
				activeObject.top = (activeObject.top - STEP);
				break;
			case Direction.RIGHT:
				activeObject.left = (activeObject.left + STEP);
				break;
			case Direction.DOWN:
				activeObject.top = (activeObject.top + STEP);
				break;
		}
		activeObject.setCoords();
		canvas.renderAll();
		//console.log('selected objects was moved');
	}

}

//drag and drop objects
function dropSolarPanel(top, left, object_id) {
	var solarPanel = new fabric.Rect({
		top: top,
		left: left,
		width: 20,
		height: 10,
		fill: COLOR_SOLAR_PANEL_INIT,
		stroke: 'gray',
		lockUniScaling: true,
		scaleX: lastSolarPanel_scale_x,
		scaleY: lastSolarPanel_scale_y,
		lockUniScaling: true,
		object: obj_type_panel,
		id: obj_type_panel + '_' + nextSolarPanelId,
		objectId: object_id,
		controls: false
	});
	canvas.add(solarPanel);
	if (top == 0 && left == 10) {
		canvas.viewportCenterObject(solarPanel);
	}
	lastSolarPanel_left = solarPanel.left;
	lastSolarPanel_top = solarPanel.top;
	lastSolarPanel_scale_x = solarPanel.scaleX;
	lastSolarPanel_scale_y = solarPanel.scaleY;
	nextSolarPanelId = nextSolarPanelId + 1;
	return solarPanel;
}

function dropSolarPanelString(top, left, row, col) {
	//console.log(top+" "+left+" "+row+" "+col);
	var top = top;
	var left = left;
    var firstLeft = 10;
    var obj;
    var panelIndex=0;
    for (var i = 1; i <= row; i++) {
		for (var j = 1; j <= col; j++) {
			//console.log(panels[panelIndex].id);
            obj = dropSolarPanel(top, left, panels[panelIndex].id);
			if(i==1 && j==1){
                firstLeft = obj.left;
            }
            left = obj.left + 21;
            top = obj.top;
            panelIndex++;
		}
        top = obj.top + 11;
        left = firstLeft;
	}
    strInMap.push(panels[0].panelStringId);
}



function dropJcb(top, left, object_id) {
	var jcb = new fabric.Rect({
		top: top,
		left: left,
		width: 20,
		height: 20,
		fill: COLOR_QBIC_INIT,
		stroke: 'gray',
		lockUniScaling: true,
		scaleX: 1,
		scaleY: 1,
		object: obj_type_jcb,
		id: obj_type_jcb + '_' + nextJcbId,
		objectId: object_id
	});
	canvas.add(jcb);
	//canvas.viewportCenterObject(jcb);
	nextJcbId = nextJcbId + 1;
}

function trackEmp() {
	var sid = $('#map-site-id').val();
	//var url = "/site/" + sid + "/user/gps/latest";
	//console.log(url);

	var deferred = new $.Deferred();

	$.ajax({
		type: "GET",
		contentType: "application/json",
		url: "/site/" + sid + "/user/gps/latest",
		cache: false,
		timeout: 600000,
		success: function(data) {
			//console.log("SUCCESS : ", data);
			userGPSMap = data.payload[0];
			//console.log(userGPSMap);
			deferred.resolve();
		},
		error: function(e) {
			console.log("ERROR : ", e);
		}
	});

	return deferred.promise();
}

function showNearByEngineers() {
	trackEmp().then(function() {
		var objects = canvas.getObjects();
		var gpsPointList = [];

		for (var i = 0; i < objects.length; i++) {
			//console.log(objects[i].object);
			if (objects[i].object && objects[i].object === obj_type_gps) {
				gpsPointList.push(canvas.getItemById(objects[i].id));
			}
		}

		var gpsPointLabelMap = new Object();

		$.each(userGPSMap, function(userIndex, user) {
			var distance = null;
			var gpsPoint = null;

			$.each(gpsPointList, function(pointIndex, point) {

				var d = distanceCalc(user.latitude, user.longitude, point.lat, point.long);

				if (distance === null || distance > d) {
					distance = d;
					gpsPoint = point;
				}
			});

			var val = [];

			if (gpsPointLabelMap.hasOwnProperty(gpsPoint.id)) {
				val = gpsPointLabelMap[gpsPoint.id];
			}

			var tmp = { name: user.fullName, logTime: user.logTime };
			val.push(tmp);

			gpsPointLabelMap[gpsPoint.id] = val;

		});

		for (var i = 0; i < objects.length; i++) {
			//console.log(objects[i].object);
			if (objects[i].object && objects[i].object === obj_type_gps) {

				canvas.getItemById(objects[i].id).set('fill', COLOR_SOLAR_PANEL_INIT);

				var lbl = "";

				var tmp = gpsPointLabelMap[objects[i].id];

				lbl = preparePointLabel(tmp, objects[i].lat, objects[i].long);

				if (lbl !== "") {
					//'1. ID Name (Just now), 2. ID Name (20mins ago)'
					var t1 = new fabric.Textbox(lbl, {
						width: 185,
						top: objects[i].top,
						left: objects[i].left + 10,
						fontSize: 16,
						textAlign: 'left',
						fixedWidth: 185,
						backgroundColor: 'yellow',
						selectable:'false'
					});
					canvas.add(t1);

				}

			}
		}

		console.log(gpsPointLabelMap);

	});
}

function preparePointLabel(arr, lat, long) {
	var lbl = "";

	if (typeof arr !== "undefined" && arr !== null && arr.length > 0) {
		for (var i = 0; i < arr.length; i++) {
			lbl += (i + 1 + ". ");
			lbl += (arr[i].name + " (" + jQuery.timeago(new Date(arr[i].logTime)) + ")");
			lbl += ("\n");
		}
	}

	if (lbl.length > 0) {
		lbl += ("Lat:" + lat + ", " + "Long:" + long);

	}

	return lbl;
}

function distanceCalc(lat1, lon1, lat2, lon2, unit) {
	var radlat1 = Math.PI * lat1 / 180
	var radlat2 = Math.PI * lat2 / 180
	var radlon1 = Math.PI * lon1 / 180
	var radlon2 = Math.PI * lon2 / 180
	var theta = lon1 - lon2
	var radtheta = Math.PI * theta / 180
	var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
	dist = Math.acos(dist)
	dist = dist * 180 / Math.PI
	dist = dist * 60 * 1.1515

	return dist
}

function hideNearByEngineers() {
	var objects = canvas.getObjects();
	for (var i = 0; i < objects.length; i++) {
		if (objects[i].get('type') == "textbox") {
			canvas.remove(objects[i]);
		}
	}
	userGPSMap = new Object();
}


function disableEditing() {
	var objects = canvas.getObjects();
	console.log(objects);
	for (var i = 0; i < objects.length; i++) {
		objects[i].selectable = false;
	}
	canvas.renderAll();
}

function viewMap() {
	var sid = $('#map-site-id').val();
	var url = "/site/" + sid + "/stringmap";

	window.open(url, "_blank");
}

function showErrors() {
	var sid = $('#map-site-id').val();

	$.ajax({
		type: "GET",
		url: "/site/" + sid + "/error",
		timeout: 600000,
		success: function(data) {
			displayStrErrorsOnMap(data);			
		},
		error: function(e) {
			console.log("ERROR : ", e);
		}
	});
}

setInterval(function(){ 
    //showErrors();
}, 15000);


function displayStrErrorsOnMap(data){
	clearAnomalies();
	$.each(data,function(i, panelId){
		var object = getElementByObjectId(panelId);
		showAnomalyOnTable(panelId);
		if(typeof object != 'undefined'){
			object.set('fill', COLOR_ANOMALY);
		}
	});
	canvas.renderAll();
}

canvas.on({
	'touch:gesture': function(e) {
		if (e.e.touches && e.e.touches.length == 2) {
			pausePanning = true;
			var point = new fabric.Point(e.self.x, e.self.y);
			if (e.self.state == "start") {
				zoomStartScale = self.canvas.getZoom();
			}
			var delta = zoomStartScale * e.self.scale;
			self.canvas.zoomToPoint(point, delta);
			pausePanning = false;
		}
	},
	'object:selected': function() {
		pausePanning = true;
	},
	'selection:cleared': function() {
		pausePanning = false;
	},
	'touch:drag': function(e) {
		if (pausePanning == false && undefined != e.e.layerX && undefined != e.e.layerY) {
			currentX = e.e.layerX;
			currentY = e.e.layerY;
			xChange = currentX - lastX;
			yChange = currentY - lastY;

			if( (Math.abs(currentX - lastX) <= 50) && (Math.abs(currentY - lastY) <= 50)) {
				var delta = new fabric.Point(xChange, yChange);
				canvas.relativePan(delta);
			}

			lastX = e.e.layerX;
			lastY = e.e.layerY;
		}
	}
});

canvas.on('mouse:wheel', function(opt) {
	//console.log(opt);
	var delta = opt.e.deltaY;
	//console.log(delta);
	var zoom = Math.abs(canvas.getZoom());
	//console.log(zoom);
	//zoom = 0.999 * delta;
	//if (zoom > 20) zoom = 20;
	//if (zoom < 0.1) zoom = 0.1;
	//canvas.zoomToPoint({ x: opt.e.offsetX, y: opt.e.offsetY }, zoom);
	opt.e.preventDefault();
	opt.e.stopPropagation();

	var objects = canvas.getObjects();

	for (var i = 0; i < objects.length; i++) {
		if (objects[i].type && objects[i].type === obj_type_textbox) {
			//console.log ("zoom:" + zoom);

			objects[i].fontSize = calculateTextBoxFontSize(zoom);
			objects[i].width = calculateTextBoxWidth(zoom);
			//console.log(objects[i].fontSize);
		}
	}

	canvas.renderAll();
});

function calculateTextBoxWidth(zoom) {
	var wd = 0;

	if (zoom < 1) {
		wd = 185;

	} else if (zoom < 2.5) {
		wd = 150;

	} else if (zoom < 50) {
		wd = 110;

	} else {
		wd = 75;
	}

	return wd;
}

function calculateTextBoxFontSize(zoom) {
	var fontSize = 0;
	
	if (zoom < 1) {
		fontSize = 14 / zoom;

	} else if (zoom < 2.5) {
		fontSize = 8;

	} else if (zoom < 50) {
		fontSize = 6;

	} else {
		fontSize = 2;
	}

	return fontSize;
}