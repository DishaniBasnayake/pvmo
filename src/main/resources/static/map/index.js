function initMap() {
	
    /*let locations  = [
        ['Facility-1','RISK','../../../../facility-detail', 35.652832, 139.839478],
        ['Facility-2', 'RISK','../../../../facility-detail', 35.514999, 138.756668],
        ['Facility-3', 'NORMAL','../../../../facility-detail', 36.6485, 138.1950]
      ];*/

	var id,name, status, url, lat, lng;
	var locations = new Array();
	let i;
	for(i=0; i<sites.length;i++){
		id = sites[i].id;
		name = sites[i].name;
		status = 'NORMAL';
		url = '/site/'+id;
		lat = sites[i].latitude;
		lng = sites[i].longitude;
		
		console.log(name);
		
		var location = [name,status,url,lat,lng];
		locations.push(location);
	}
		
    
    const map = new google.maps.Map(document.getElementById("map"), {
        zoom: 7,
        center: new google.maps.LatLng(locations[0][3], locations[0][4]), //Facility-1
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    
    const svgMarkerGreen = {
	    //path: "M10.453 14.016l6.563-6.609-1.406-1.406-5.156 5.203-2.063-2.109-1.406 1.406zM12 2.016q2.906 0 4.945 2.039t2.039 4.945q0 1.453-0.727 3.328t-1.758 3.516-2.039 3.070-1.711 2.273l-0.75 0.797q-0.281-0.328-0.75-0.867t-1.688-2.156-2.133-3.141-1.664-3.445-0.75-3.375q0-2.906 2.039-4.945t4.945-2.039z",
	    path: "M-20,0a20,20 0 1,0 40,0a20,20 0 1,0 -40,0",
	    fillColor: "green",
	    fillOpacity: 1,
	    strokeWeight: 0,
	    rotation: 0,
	    scale: 0.5,
	    anchor: new google.maps.Point(15, 30),
	};
	  
  	const svgMarkerRed = {
	    path: "M-20,0a20,20 0 1,0 40,0a20,20 0 1,0 -40,0",
	    fillColor: "red",
	    fillOpacity: 10,
	    strokeWeight: 0,
	    rotation: 0,
	    scale: 0.6,
	    //anchor: new google.maps.Point(15, 30),
	 };
	  
    let infowindow = new google.maps.InfoWindow();
    let marker;
    
    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][3], locations[i][4]),
          map: map,
          icon: (locations[i][1]=='RISK')?svgMarkerRed:svgMarkerGreen,
		  /*icon: {
			path: google.maps.SymbolPath.CIRCLE,
			strokeColor: (locations[i][1]=='RISK')?'red':'green',
			scale: (locations[i][1]=='RISK')?10:7
		  },*/
        });
		
        
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {
            infowindow.setContent('<div style="color: black !important;"><b>' + locations[i][0] +'</b><br />'+ 'Health: '+locations[i][1]+'<br/><a href="'+locations[i][2]+'">Details</a></div>');
            infowindow.open(map, marker);
          }
        })(marker, i));
    }

  }